<?php
/**

*/
class SxGeoExtended extends SxGeo {

	const EARTH_RADIUS = 6372795;

	public $array_ip = array(
											'Москва' => '195.200.213.0',
											'Челябинск' => '83.146.70.39',
											'Киев' => '91.224.3.255',
								);



	public $main_city		= 'Увильды';
	public $main_city_lat	= 55.531151;
	public $main_city_lon	= 60.569155;



	public function getCityFull( $ip='' ) {
		if ( !$ip ) $ip = $_SERVER[ 'REMOTE_ADDR' ];
		$info = parent :: getCityFull( $ip );
		$val = $this -> _validation( $info );
		if ( !$val ) return false;

		$info[ 'distance' ] = $this -> calculateTheDistance(
			$info[ 'city' ][ 'lat' ],
			$info[ 'city' ][ 'lon' ],
			$this -> main_city_lat,
			$this -> main_city_lon
		);

		return $info;
	}


	public function sess( ) {
		if ( isset( $_SESSION[ 'SxGeo' ] ) ) {
			return $_SESSION[ 'SxGeo' ];
		}
		else return false;
	}


	public function tpl( ) {
		$tpl = array( );

		if ( !isset( $_SESSION[ 'SxGeo' ][ 'city' ][ 'id' ] ) || !$_SESSION[ 'SxGeo' ][ 'city' ][ 'id' ] ) $tpl[ 'SXGEO.CITY.ID' ] = '';
		else $tpl[ 'SXGEO.CITY.ID' ] = $_SESSION[ 'SxGeo' ][ 'city' ][ 'id' ];
		if ( !isset( $_SESSION[ 'SxGeo' ][ 'city' ][ 'lat' ] ) || !$_SESSION[ 'SxGeo' ][ 'city' ][ 'lat' ] ) $tpl[ 'SXGEO.CITY.LAT' ] = '';
		else $tpl[ 'SXGEO.CITY.LAT' ] = $_SESSION[ 'SxGeo' ][ 'city' ][ 'lat' ];
		if ( !isset( $_SESSION[ 'SxGeo' ][ 'city' ][ 'lon' ] ) || !$_SESSION[ 'SxGeo' ][ 'city' ][ 'lon' ] ) $tpl[ 'SXGEO.CITY.LON' ] = '';
		else $tpl[ 'SXGEO.CITY.LON' ] = $_SESSION[ 'SxGeo' ][ 'city' ][ 'lon' ];
		if ( !isset( $_SESSION[ 'SxGeo' ][ 'city' ][ 'name_ru' ] ) || !$_SESSION[ 'SxGeo' ][ 'city' ][ 'name_ru' ] ) $tpl[ 'SXGEO.CITY.NAME_RU' ] = '';
		else $tpl[ 'SXGEO.CITY.NAME_RU' ] = $_SESSION[ 'SxGeo' ][ 'city' ][ 'name_ru' ];
		if ( !isset( $_SESSION[ 'SxGeo' ][ 'city' ][ 'name_en' ] ) || !$_SESSION[ 'SxGeo' ][ 'city' ][ 'name_en' ] ) $tpl[ 'SXGEO.CITY.NAME_EN' ] = '';
		else $tpl[ 'SXGEO.CITY.NAME_EN' ] = $_SESSION[ 'SxGeo' ][ 'city' ][ 'name_en' ];
		if ( !isset( $_SESSION[ 'SxGeo' ][ 'region' ][ 'id' ] ) || !$_SESSION[ 'SxGeo' ][ 'region' ][ 'id' ] ) $tpl[ 'SXGEO.REGION.ID' ] = '';
		else $tpl[ 'SXGEO.REGION.ID' ] = $_SESSION[ 'SxGeo' ][ 'region' ][ 'id' ];
		if ( !isset( $_SESSION[ 'SxGeo' ][ 'region' ][ 'name_ru' ] ) || !$_SESSION[ 'SxGeo' ][ 'region' ][ 'name_ru' ] ) $tpl[ 'SXGEO.REGION.NAME_RU' ] = '';
		else $tpl[ 'SXGEO.REGION.NAME_RU' ] = $_SESSION[ 'SxGeo' ][ 'region' ][ 'name_ru' ];
		if ( !isset( $_SESSION[ 'SxGeo' ][ 'region' ][ 'name_en' ] ) || !$_SESSION[ 'SxGeo' ][ 'region' ][ 'name_en' ] ) $tpl[ 'SXGEO.REGION.NAME_EN' ] = '';
		else $tpl[ 'SXGEO.REGION.NAME_EN' ] = $_SESSION[ 'SxGeo' ][ 'region' ][ 'name_en' ];
		if ( !isset( $_SESSION[ 'SxGeo' ][ 'region' ][ 'iso' ] ) || !$_SESSION[ 'SxGeo' ][ 'region' ][ 'iso' ] ) $tpl[ 'SXGEO.REGION.ISO' ] = '';
		else $tpl[ 'SXGEO.REGION.ISO' ] = $_SESSION[ 'SxGeo' ][ 'region' ][ 'iso' ];
		if ( !isset( $_SESSION[ 'SxGeo' ][ 'country' ][ 'id' ] ) || !$_SESSION[ 'SxGeo' ][ 'country' ][ 'id' ] ) $tpl[ 'SXGEO.COUNTRY.ID' ] = '';
		else $tpl[ 'SXGEO.COUNTRY.ID' ] = $_SESSION[ 'SxGeo' ][ 'country' ][ 'id' ];
		if ( !isset( $_SESSION[ 'SxGeo' ][ 'country' ][ 'iso' ] ) || !$_SESSION[ 'SxGeo' ][ 'country' ][ 'iso' ] ) $tpl[ 'SXGEO.COUNTRY.ISO' ] = '';
		else $tpl[ 'SXGEO.COUNTRY.ISO' ] = $_SESSION[ 'SxGeo' ][ 'country' ][ 'iso' ];
		if ( !isset( $_SESSION[ 'SxGeo' ][ 'country' ][ 'lat' ] ) || !$_SESSION[ 'SxGeo' ][ 'country' ][ 'lat' ] ) $tpl[ 'SXGEO.COUNTRY.LAT' ] = '';
		else $tpl[ 'SXGEO.COUNTRY.LAT' ] = $_SESSION[ 'SxGeo' ][ 'country' ][ 'lat' ];
		if ( !isset( $_SESSION[ 'SxGeo' ][ 'country' ][ 'lon' ] ) || !$_SESSION[ 'SxGeo' ][ 'country' ][ 'lon' ] ) $tpl[ 'SXGEO.COUNTRY.LON' ] = '';
		else $tpl[ 'SXGEO.COUNTRY.LON' ] = $_SESSION[ 'SxGeo' ][ 'country' ][ 'lon' ];
		if ( !isset( $_SESSION[ 'SxGeo' ][ 'country' ][ 'name_ru' ] ) || !$_SESSION[ 'SxGeo' ][ 'country' ][ 'name_ru' ] ) $tpl[ 'SXGEO.COUNTRY.NAME_RU' ] = '';
		else $tpl[ 'SXGEO.COUNTRY.NAME_RU' ] = $_SESSION[ 'SxGeo' ][ 'country' ][ 'name_ru' ];
		if ( !isset( $_SESSION[ 'SxGeo' ][ 'country' ][ 'name_en' ] ) || !$_SESSION[ 'SxGeo' ][ 'country' ][ 'name_en' ] ) $tpl[ 'SXGEO.COUNTRY.NAME_EN' ] = '';
		else $tpl[ 'SXGEO.COUNTRY.NAME_EN' ] = $_SESSION[ 'SxGeo' ][ 'country' ][ 'name_en' ];
		
		return $tpl;
	}


	
	public function save( $s ) {
		$val = $this -> _validation( $s );
		if ( !$val ) {
			return false;
		}
		$_SESSION[ 'SxGeo' ] = $s;
		$_SESSION[ 'SxGeo' ][ 'timestamp' ] = time( );
		return true;
	}

	public function clear( ) {
		unset( $_SESSION[ 'SxGeo' ] );
	}


	protected function _validation( $s ) {
		if ( !isset( $s[ 'city' ][ 'id' ] ) || !$s[ 'city' ][ 'id' ] ) return false;
		if ( !isset( $s[ 'city' ][ 'lat' ] ) || !$s[ 'city' ][ 'lat' ] ) return false;
		if ( !isset( $s[ 'city' ][ 'lon' ] ) || !$s[ 'city' ][ 'lon' ] ) return false;
		if ( !isset( $s[ 'city' ][ 'name_ru' ] ) || !$s[ 'city' ][ 'name_ru' ] ) return false;
		if ( !isset( $s[ 'city' ][ 'name_en' ] ) || !$s[ 'city' ][ 'name_en' ] ) return false;
		if ( !isset( $s[ 'region' ][ 'id' ] ) || !$s[ 'region' ][ 'id' ] ) return false;
		if ( !isset( $s[ 'region' ][ 'name_ru' ] ) || !$s[ 'region' ][ 'name_ru' ] ) return false;
		if ( !isset( $s[ 'region' ][ 'name_en' ] ) || !$s[ 'region' ][ 'name_en' ] ) return false;
		if ( !isset( $s[ 'region' ][ 'iso' ] ) || !$s[ 'region' ][ 'iso' ] ) return false;
		if ( !isset( $s[ 'country' ][ 'id' ] ) || !$s[ 'country' ][ 'id' ] ) return false;
		if ( !isset( $s[ 'country' ][ 'iso' ] ) || !$s[ 'country' ][ 'iso' ] ) return false;
		if ( !isset( $s[ 'country' ][ 'lat' ] ) || !$s[ 'country' ][ 'lat' ] ) return false;
		if ( !isset( $s[ 'country' ][ 'lon' ] ) || !$s[ 'country' ][ 'lon' ] ) return false;
		if ( !isset( $s[ 'country' ][ 'name_ru' ] ) || !$s[ 'country' ][ 'name_ru' ] ) return false;
		if ( !isset( $s[ 'country' ][ 'name_en' ] ) || !$s[ 'country' ][ 'name_en' ] ) return false;
		return true;
	}



	/*********************************
	 * Расстояние между двумя точками
	 * $lat1, $long1 - широта, долгота 1-й точки,
	 * $lat2, $long2 - широта, долгота 2-й точки
	 */
	public function calculateTheDistance( $lat1, $long1, $lat2, $long2 ) {

		// перевести координаты в радианы
		$lat1 = $lat1 * M_PI / 180;
		$lat2 = $lat2 * M_PI / 180;
		$long1 = $long1 * M_PI / 180;
		$long2 = $long2 * M_PI / 180;

		// косинусы и синусы широт и разницы долгот
		$cl1 = cos( $lat1 );
		$cl2 = cos( $lat2 );
		$sl1 = sin( $lat1 );
		$sl2 = sin( $lat2 );
		$delta = $long2 - $long1;
		$cdelta = cos( $delta );
		$sdelta = sin( $delta );

		// вычисления длины большого круга
		$y = sqrt( pow( $cl2 * $sdelta, 2 ) + pow( $cl1 * $sl2 - $sl1 * $cl2 * $cdelta, 2 ) );
		$x = $sl1 * $sl2 + $cl1 * $cl2 * $cdelta;

		$ad = atan2( $y, $x );
		$dist = $ad * self :: EARTH_RADIUS;

		$dist = ceil( $dist );
		$dist = $dist / 1000; 

		return $dist;

	}


}