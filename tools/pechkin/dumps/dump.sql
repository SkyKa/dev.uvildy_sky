USE `it-factory`;

SET CHARACTER SET utf8;
SET CHARACTER_SET_CLIENT = 'utf8';
SET CHARACTER_SET_RESULTS = 'utf8';
SET COLLATION_CONNECTION = 'utf8_general_ci';

CREATE TABLE `contragent` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `address` varchar(500) COLLATE utf8_unicode_ci DEFAULT NULL,
  `site` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `emails` text COLLATE utf8_unicode_ci,
  `phones` text COLLATE utf8_unicode_ci,
  `faxs` text COLLATE utf8_unicode_ci,
  `send` tinyint(2) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


INSERT into `pechkin`.`contragent` (`pechkin`.`contragent`.`name`,
									`pechkin`.`contragent`.`address`,
									`pechkin`.`contragent`.`emails`,
									`pechkin`.`contragent`.`phones`,
									`pechkin`.`contragent`.`faxs`)

SELECT	`bi`.`position_firms`.`name`,
		`bi`.`position_firms`.`address`,
		`bi`.`position_firms`.`phones`,
		`bi`.`position_firms`.`emails`,
		`bi`.`position_firms`.`faxs`
FROM `bi`.`position_firms`

WHERE	`bi`.`position_firms`.`site`='' &&
		`bi`.`position_firms`.`emails`!='' &&
		`bi`.`position_firms`.`phones` !=''

GROUP BY `bi`.`position_firms`.`emails` LIMIT 100000;