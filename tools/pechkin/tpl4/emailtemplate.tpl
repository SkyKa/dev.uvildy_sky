<html>
<head>
    <title>Скидка от курорта увильды</title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

</head>

<meta http-equiv="Content-Type" content="text/html; charset=utf-8" /><meta name="viewport" content="width=device-width, initial-scale=1.0"/>


<table bgcolor="#0374b9" width="100%">
	<tbody>
		<tr align="center">
			<td>
				<table width="600px">
					<tbody>
						<tr>
							<td width="65px"></td>
							<td>
								<a href="http://www.uvildy.ru/subscribe/?email={EMAIL}">
									<img src="cid:logo.jpg" alt="">
								</a>
							</td>
							<td style="color: white;
									    text-transform: uppercase;
									    font-family: 'Arial', sans-serif;
									    font-weight: 600;
									    font-size: 32px;
									    letter-spacing: 1px;"><a href="tel:+7(351)7111423" style="color:#FFF;text-decoration:none;">+7 (351) 711-14-23</a>
							</td>
						</tr>
					</tbody>
				</table>
			</td>
		</tr>
	</tbody>
</table>
<table width="100%" border="0" cellpadding="0" cellspacing="0" style="background-image: url(cid:background-letter.jpg); background-size: cover; font-family: 'Arial', sans-serif; ">
	<tbody>
		<tr align="center">
			<td>
				<table width="600px" bgcolor="white">
					<tbody>
						<tr align="center">
							<td width="65px"></td>
							<td style="border-bottom: 1px solid #bbbbbb; padding: 17px 0;color: #4f4f4f;">
								<strong style="font-weight: 600;">Добрый день!</strong><br /><br />
								Мы ждем Вас на Курорте Увильды!<br /><br />
								Ведь именно для Вас, мы подготовили специальную скидку:<br /><br />
							</td>
							<td width="65px"></td>
						</tr>
						<tr align="center">
							<td width="65px"></td>
							<td style="padding: 17px 0 37px 0; border-bottom: 1px solid #bbbbbb; padding: 17px 0;color: #4f4f4f;">
								<span  style="text-transform: uppercase; color: red; font-weight:bold;"><strong style="font-size:26px;">10%</strong> 
								на отдых с лечением  <strong style="font-size:24px;">в июне</strong> 2017 года<br /><br />
								Скдидка распространяется на <strong style="font-size:24px;">все</strong> программы!<br /><br />
								на <strong style="font-size:24px;">все</strong> категории номеров!
								</span>
							</td>
							<td width="65px"></td>
						</tr>
					</tbody>
				</table>
				<table width="600px" bgcolor="#FFFFFF" style="color: #4f4f4f;">
					<tr>
						<td style="text-transform: uppercase; padding: 12px;padding:30px 0 10px 40px;">
						<ul>
							<li style="padding-bottom:20px;">3-х разовое питание по системе Шведский стол.</li>
							<li style="padding-bottom:20px;">Уникальный природный климат</li>
							<li style="padding-bottom:20px;">отдых на берегу Кристально чистого озера.</li>
							<li >Широкий спектр медицинских услуг.</li>
						</ul>
						</td>
					</tr>
				</table>
				<table width="600px" bgcolor="#0374b9">
					<tr align="center">
						<td style="text-transform: uppercase; color: white; padding: 12px;">
							Бронируйте прямо сейчас!
						</td>
					</tr>
				</table>
				<table width="600px" bgcolor="white" style="padding-top:20px;">
					<tbody>
						<tr align="center">
							<td width="65px"></td>
							<td>
								<table style="width:100%;font-size:14px;">
									<tbody>
										<tr align="center">
											<td style="padding: 12px;font-weight:bold;text-transform:uppercase;"><a href="http://www.uvildy.ru/subscribe/programmy/klinika-mozga.html?email={EMAIL}" target="_blank" style="color:#0374b9">Клиника мозга<br /><br /><img src="cid:cl_brain.jpg"/></a></td>
											<td style="padding: 12px;font-weight:bold;text-transform:uppercase;"><a href="http://www.uvildy.ru/subscribe/programmy/detskoe-zdorove.html?email={EMAIL}" target="_blank" style="color:#0374b9">Детская программа<br /><br /><img src="cid:child.jpg"/></a></td>
										</tr>
										<tr align="center">
											<td style="padding: 12px;font-weight:bold;text-transform:uppercase;"><a href="http://www.uvildy.ru/subscribe/programmy/lechenie-besplodiya.html?email={EMAIL}" target="_blank" style="color:#0374b9">Лечение бесплодия<br /><br /><img src="cid:gestation.jpg"/></a></td>
											<td style="padding: 12px;font-weight:bold;text-transform:uppercase;"><a href="http://www.uvildy.ru/subscribe/programmy/obcshee-ozdorovlenie.html?email={EMAIL}" target="_blank" style="color:#0374b9">Общее оздоровление<br /><br /><img src="cid:all_hill.jpg"/></a></td>
										</tr>
										<tr align="center">
											<td style="padding: 12px;font-weight:bold;text-transform:uppercase;"><a href="http://www.uvildy.ru/subscribe/programmy/standartnaya-programma-ochicshenie-organizma.html?email={EMAIL}" target="_blank" style="color:#0374b9">Стандартная программа «очищение организма»<br /><br /><img src="cid:clean_based.jpg"/></a></td>
											<td style="padding: 12px;font-weight:bold;text-transform:uppercase;"><a href="http://www.uvildy.ru/subscribe/programmy/lyuks-programma-ochicshenie-organizma.html?email={EMAIL}" target="_blank" style="color:#0374b9">Люкс программа «очищение организма»<br /><br /><img src="cid:clean_lux.jpg"/></a></td>
										</tr>
										<tr align="center">
											<td style="padding: 12px;font-weight:bold;text-transform:uppercase;"><a href="http://www.uvildy.ru/subscribe/programmy/prezidentskaya-programma-ochicshenie-organizma.html?email={EMAIL}" target="_blank" style="color:#0374b9">Президентская программа «очищение организма»<br /><br /><img src="cid:clean_presedent.jpg"/></a></td>
											<td style="padding: 12px;font-weight:bold;text-transform:uppercase;"><a href="http://www.uvildy.ru/subscribe/programmy/putevka-vyhodnogo-dnya.html?email={EMAIL}" target="_blank" style="color:#0374b9"><br /><br />Путёвка выходного дня<br /><br /><img src="cid:weekend.jpg"/></a></td>
										</tr>
									</tbody>
								</table>
							</td>
							<td width="65px"></td>
						</tr>
					</tbody>
				</table>
				<table width="600px" bgcolor="white">
					<tbody>
						<tr>
							<td>
								<table>
									<tr>
										<td width="65px"></td>
										<td align="center" width="100%" style="padding: 15px 0 50px 0;">
											<a href="tel:+7(351)7111423" style="background: #ed4545; display: inline-block; padding: 15px 35px; color: #fff; text-decoration: none; text-transform: uppercase;">Позвонить</a>
										</td>
									</tr>
								</table>
							</td>
						</tr>
					</tbody>
				</table>
			</td>
		</tr>
	</tbody>
</table>
<table bgcolor="#0374b9" width="100%" style="font-family: 'Arial', sans-serif; ">
	<tbody>
		<tr align="center">
			<td>
				<table width="600px">
					<tbody>
						<tr>
							<td width="65px"></td>
							<td style="font-size: 11px; color: #fff; padding: 17px 0;">
								© Многопрофильный центр медицины <br>
								и реабилитации «Курорт Увильды»
							</td>
							<td style="color: white;
									    text-transform: uppercase;
									    font-family: 'Arial', sans-serif;
									    font-weight: 600;
									    font-size: 11px;
									    letter-spacing: 1px;">Присоединяйтесь!
							</td>
						</tr>
					</tbody>
				</table>
			</td>
		</tr>
	</tbody>
</table>



</html>