<?php 

	session_start();
	$path_cms = realpath( dirname( __FILE__ ) . '/..' ) . DIRECTORY_SEPARATOR . 'www' . DIRECTORY_SEPARATOR;
	require $path_cms . 'config.php';
	require $path_cms . 'cms.php';

	$dir = 'temp/';

	$table = new Table( 'catalog_section' );  
    $first_lvls = $table->select( 'SELECT * FROM `catalog_section` WHERE parent_id=:id ', array( 'id' => 317 ) );
    $table->select( 'DELETE FROM `catalog_section` WHERE parent_id=1147 ');
    $table->execute('TRUNCATE `section_number_class`');

    foreach ( $first_lvls as $first_lvl ) {

    	if ( $first_lvl['alias'] == 'nomera-s-foto') continue;
    	$second_lvls = $table->select( 'SELECT * FROM `catalog_section` WHERE parent_id=:id ', array( 'id' => $first_lvl["id"] ) );
    	if ($second_lvls) {
    		foreach ( $second_lvls as $second_lvl ) {
	    		$third_lvls = $table->select( 'SELECT * FROM `catalog_section` WHERE parent_id=:id ', array( 'id' => $second_lvl["id"] ) );
	    		if ( $third_lvls ) {
	    			foreach ( $third_lvls as $third_lvl ) {
	    				$table->select( 'DELETE FROM `catalog_section` WHERE id=:id ', array( 'id' => $third_lvl["id"] ) );
	    				$table->select( 'DELETE FROM `section_rooms` WHERE id=:id ', array( 'id' => $third_lvl["id"] ) );
	    				$table->select( 'DELETE FROM `section_offers` WHERE id=:id ', array( 'id' => $third_lvl["id"] ) );
	    			}		
	    		}
	    		$table->select( 'DELETE FROM `catalog_section` WHERE id=:id ', array( 'id' => $second_lvl["id"] ) );
	    		$table->select( 'DELETE FROM `section_rooms` WHERE id=:id ', array( 'id' => $second_lvl["id"] ) );
	    		$table->select( 'DELETE FROM `section_offers` WHERE id=:id ', array( 'id' => $second_lvl["id"] ) );
	    	}
    	}    	   	   	

    }


?>