<?php

	$_SERVER[ 'SERVER_NAME' ] = '';
	$_SERVER[ 'HTTP_HOST' ] = '';
	
	$document_root = 'www';
	$script_path = realpath( dirname( __FILE__ ) );
	$path_document_root = realpath( $script_path . '/..' ) . DIRECTORY_SEPARATOR . $document_root;

	include $path_document_root . DIRECTORY_SEPARATOR . "config.php";
	include $path_document_root . DIRECTORY_SEPARATOR . "cms.php";

	$table = new Table( 'catalog_section' );
	
	$alias = 'subscribe-new';
	$alias_end = 'subscribe-end';	
	$section_end = $table -> select( "SELECT * FROM `catalog_section` WHERE `alias`=:alias ORDER BY `id` LIMIT 1", array( 'alias' => $alias_end ) );
	if ( !count( $section_end ) ) return false;
	$section_end = end( $section_end );
	$section = $table -> select( "SELECT * FROM `catalog_section` WHERE `alias`=:alias ORDER BY `id` LIMIT 1", array( 'alias' => $alias ) );
	if ( !count( $section ) ) return false;
	$section = end( $section );
	$offers = $table -> select( "SELECT * FROM `catalog_section` WHERE `parent_id`=:pid ORDER BY `position`", array( 'pid' => $section[ 'id' ] ) );
	foreach( $offers as $offer ) {
		// настройки рассылки
		$section_offer = $table -> select( "SELECT * FROM `" . $offer[ 'section_table' ] . "` WHERE `id`=:id && `send`=1 LIMIT 1", array( 'id' => $offer[ 'id' ] ) );
		if ( !count( $section_offer ) ) continue;
		
		$section_offer = end( $section_offer );
		$tpl = $section_offer[ 'mail_body' ];
		$theme = $section_offer[ 'mail_theme' ];

		$images = array();
		preg_match_all( '~\bbackground(-image)?\s*:(.*?)\(\s*(\'|")?(?<image>.*?)\3?\s*\)~i', $tpl, $bg_images );
		foreach( $bg_images[ 'image' ] as $bg_image ) {
			$images[] = $bg_image;
		}
		preg_match_all( '/< *img[^>]*src *= *["\']?([^"\']*)/i', $tpl, $scr_images );
		foreach( $scr_images[ 1 ] as $scr_image ) {
			$images[] = $scr_image;
		}

		$new_images = array( );
		foreach ( $images as $image ) {

			$ex_f = explode( '/', $image );
			$fname = $ex_f[ count( $ex_f ) - 1 ];
			$ex_fname = explode( '.', $fname );
			$name = $ex_fname[ 0 ];
			$ext = end( $ex_fname );
			$new_images[] = sprintf( '%u', crc32( $image ) ) . '.' . $ext;
			get_file( $image, $script_path . DIRECTORY_SEPARATOR . sprintf( '%u', crc32( $image ) ) . '.' . $ext );
			$tpl = str_replace( $image, 'cid:' . sprintf( '%u', crc32( $image ) ) . '.' . $ext, $tpl );
		}
	
		
		// ищем базы клиентов
		$clients_parent_section = $table -> select( "SELECT * FROM `catalog_section` WHERE `parent_id`=:pid ORDER BY `position`", array( 'pid' => $section_offer[ 'id' ] ) );
		if ( !count( $clients_parent_section ) ) continue;
		$clients_parent_section = end( $clients_parent_section );
		$clients_sections = $table -> select( "SELECT * FROM `catalog_section` WHERE `parent_id`=:pid ORDER BY `position`", array( 'pid' => $clients_parent_section[ 'id' ] ) );
		if ( !count( $clients_sections ) ) continue;
		foreach( $clients_sections as $clients_section ) {
			$clients = $table -> select( "SELECT * FROM `" . $clients_section[ 'position_table' ] . "` WHERE `section_id`=:sid && `active`=1 && (isNull(`unsubscribe`) || `unsubscribe`=0)", array( 'sid' => $clients_section[ 'id' ] ) );
			if ( !count( $clients ) ) continue;
			foreach( $clients as $client ) {

				$exs = explode( ',', $client[ 'email' ] );
				foreach ( $exs as $email ) {

					$email = trim( $email );

					$mailer = new Mailer( );
					$mailer -> IsHTML( true );
					$mailer -> Subject = $theme;

					foreach ( $new_images as $new_image ) {
						$ex_fname = explode( '.', $new_image );
						$name = $ex_fname[ 0 ];
						$ext = end( $ex_fname );
						$embeded_image_src = $name . '.' . $ext;
						$mailer -> AddEmbeddedImage( $embeded_image_src );
					}

					// PHPMailer ONLY!!! обязательно сделать так, иначе картинки будут просто приатачены к письму
					$mailer -> MsgHTML( $tpl );

					$mailer -> AddAddress( $email );
					if ( $res = $mailer -> Send( ) ) {
						echo $email . " - OK =)\n";
					}
					else {
						echo $email . " - ERROR! =(\n";
						var_dump( $mailer -> errorInfo );
						var_dump( $res );
					}
				}
				
			}
		}

		
		$table -> execute( "UPDATE `catalog_section` SET `parent_id`=:pid WHERE `id`=:id LIMIT 1",
			array( 'pid' => $section_end[ 'id' ], 'id' => $offer[ 'id' ] ) );

		$table -> execute( "UPDATE `" . $offer[ 'section_table' ] . "` SET `send`=0, `datestamp_end`=UNIX_TIMESTAMP() WHERE `id`=:id LIMIT 1",
			array( 'id' => $offer[ 'id' ] ) );

	}

	
	




	/////////////////////////////////////////////////////////
	// $url					- url где лежит файл
	// $to_file				- путь куда ложить файл, если не задан, функция вернет содержимое файла
	// $other_options		- другие CURL заголовки
	// timeout				- таймаут запроса
	function get_file( $source, $to_file=false, $other_options=array(), $timeout=28800 ) {

		// информация об удаленном файле
		$stat_url = @get_headers( $source, 1 );

		// Что то не так
		if ( !$stat_url || empty( $stat_url[ 0 ] ) || $stat_url[ 0 ] != 'HTTP/1.1 200 OK' ) {
			$stat_url[ 0 ] = ( isset( $stat_url[ 0 ] ) ) ? $stat_url[ 0 ] : 'null';
			return false;
		}

		$options = array(
			CURLINFO_HEADER_OUT				=>	false,
			CURLOPT_TIMEOUT					=>	$timeout, // set this to 8 hours so we dont timeout on big files
			CURLOPT_SSL_VERIFYPEER	=> false,
			CURLOPT_SSL_VERIFYHOST => false,
		);

		if ( $to_file ) {
			$file_exists = file_exists( $to_file );
			if ( $file_exists ) {
				// информация о локальном файле
				$stat_file = stat( $to_file );
				// Файл уже скачан
				if ( $stat_url[ 'Content-Length' ] == $stat_file[ 'size' ] ) {
					return true;
				}
			}
			$fp = fopen( $to_file, "w" );
			$options[ CURLOPT_FILE ] = $fp;
		}
		else {
			$options[ CURLOPT_RETURNTRANSFER ] = true;
		}

		$ch = curl_init( $source );
		curl_setopt_array( $ch, $options );
		$result = curl_exec( $ch );
		$info = curl_getinfo( $ch );
		curl_close( $ch );

		if ( $info[ 'http_code' ] == '200' ) {
			$url_info = parse_url( $source );
			// копирует файл
			if ( $to_file ) {
				return $info;
			}
			// читаем
			else {
				return $result;
			}
		}
		else {
			return false;
		}
	}

	