'use strict';

function initLeafletMap() {

	var maxX = 237.5,
	maxY = 165.88;

	var map = L.map('leaflet-map', {
		zoomControl: false,
		minZoom: 1,
		maxZoom: 3,
		center: [0, 0],
		zoom: 1,
		crs: L.CRS.Simple
	});

	// dimensions of the image
	var w = 1900,
	    h = 1327,
	    url = '/mobile_static/img/map.jpg';

	// calculate the edges of the image, in coordinate space
	var southWest = map.unproject( [0, h], map.getMaxZoom() );
	var northEast = map.unproject( [w, 0], map.getMaxZoom() );
	var bounds = new L.LatLngBounds( southWest, northEast );

	// add the image overlay, 
	// so that it covers the entire map
	L.imageOverlay(url, bounds).addTo(map);

	// tell leaflet that the map is exactly as big as the image
	map.setMaxBounds(bounds);

	map.addControl(L.control.zoom({
		position: 'topright'
	}));

	var markers = new L.FeatureGroup();

	$('.all-infra-menu li a').each( function(index) {

		var iconImg = $(this).attr('data-small-icon'),
		lat = $(this).attr('data-y') - maxY,
		lng = $(this).attr('data-x'),
		className = $(this).attr('class'),
		title = $(this).text(),
		popupText = $(this).attr('data-popup-text');


		if( !iconImg || !lat || !lng || !title || !className ) return true;

		var icon = L.icon({
		    iconUrl: iconImg,
		    iconSize:     [34, 46],
		    iconAnchor:   [17, 46],
		    popupAnchor:  [0, -46],
		    className: className,
		});

		var marker = L.marker([lat, lng], {icon: icon, title: title});
		marker.setZIndexOffset(500);

		var popup = popupText ? popupText : title;
		popup = '<div class="content">' + popup + '</div>';
		marker.addTo(map).bindPopup(popup);

		$(this).on('click', function(e) {
			e.preventDefault();
			map.panTo(marker.getLatLng());
			marker.setZIndexOffset(501);
			marker.openPopup();
		});

		marker.on('popupclose', function() {
			marker.setZIndexOffset(500);
		});

		marker.on('mouseover', function (e) {
            this.openPopup();
        });

        marker.on('mouseout', function (e) {
            this.closePopup();
        });

	}); 

	gotoCenter();

	function gotoCenter() {
		map.panTo( [maxY/2, maxX/2] );
	}

}