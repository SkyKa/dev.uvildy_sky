'use strict';

;(function($, undefined) {

	$(document).on( 'ready', function() {
/*
	$('tr.new').prev().addClass('pre-new');

	$('.sort select').styler();

	$('.info-wrap').on('click', function() {
		$(this).find('.info').fadeIn();
	});

	$(document).mouseup(function (e) {
	    var container = $(".info-wrap .info");
	    if (container.has(e.target).length === 0){
	        container.hide();
	    }
	});

 	$('.title_block').on('click', function() {
      $('.accordion_item').find('.info').hide();
        var i = $(this).data('id');
       	$(this).parents('.accordion_item').find('#d'+ i + '').fadeIn();
    		$('.accordion_item').removeClass("active_block");
    		$(this).parents('.accordion_item').addClass("active_block");
    });
*/
  
		
	// Main menu handler
		$('.et-menu .et-not-link').click(function(e) {
			e.preventDefault();
		});

		$('.et-menu .et-dropdown .et-not-link').click(function() {
			if($(this).closest('.touchevents').length || ($(window).width() < 768)) {
				$(this).parent().siblings().removeClass('et-open');

				$(this).parent().toggleClass('et-open');
			}
		});


		$('.et-menu .et-dropdown > a').after('<div class="et-caret-wrapper"><div class="et-caret"></div></div>');

		$('.et-menu .et-dropdown .et-caret-wrapper').on('click', function(e) {
			e.preventDefault();

			$(this).parent().siblings().removeClass('et-open');

			$(this).parent().toggleClass('et-open');
		});

		if(Modernizr != null) {
			if(Modernizr.touchevents) {
				$('.et-caret-wrapper').show();
				$('.et-menu .et-dropdown > a').css('padding-right', '25px');
			}
		}

		$('.et-menu .et-menu-collapse-btn').click(function() {

			if( $(this).parent().next('ul').is(':animated') ) return;

			if($(this).parent().next('ul').hasClass('menu-collapsed')) { 

				$(this).removeClass('et-collapsed');

				$(this).parent().next('ul').animate({
					height: 'show',
					padding: 'show'
				}, 300, function() {
					$(this).removeClass('menu-collapsed');
				});

			} else {

				$(this).addClass('et-collapsed');

				$(this).parent().next('ul').animate({
					height: 'hide',
					padding: 'hide'
				}, 300, function() {
					$(this).addClass('menu-collapsed');
					$(this).removeAttr('style');
				});
			}
		});

    	$(document).click( function(e) {


	      	if($('.et-menu .et-dropdown').length) {
		        var container = $('.et-menu .et-dropdown');

		        if (!container.is(e.target) && container.has(e.target).length === 0) {
		        	$('.et-menu > ul > li').removeClass('et-open');
		        }
		    }


		    if($(".header-pages #search").length) {
		     	var container = $(".header-pages #search");

			    if (!container.is(e.target) && container.has(e.target).length === 0) {
			        container.addClass('collapsed');

			        $(".header-pages #search").find('.form-control').val('');

			        $('.header-pages #search.collapsed').on('click', function(e) {
						e.preventDefault();

						$(this).removeClass('collapsed');

						$(this).find('.form-control').focus();

						$(this).off('click');
					});
			    }
			}

			if($('.proc-info').length) {

		        var container = $('.proc-info, .select-number > *');

		        if (!container.is(e.target) && container.has(e.target).length === 0) {
		        	closeProcedureWindow()
		        }

		    }


	    });


	    $('#smooth-background').lightSlider({
    		item: 1,
    		controls: false,
    		pager: false,
    		enableDrag: false,
    		enableTouch: false,
    		loop: true,
    		auto: true,
    		mode: 'fade',
    		pause: 4000,
    		onSliderLoad: function() {
    			$('#smooth-background img').css('opacity', '1');
    		}
	    });


		$(document).imagesLoaded( function() {

			if($('.map-page').length) setMapSize();

			function setMapSize() {
				var height = $(window).outerHeight()*0.8;
				$('.map-page').height(height);

				initLeafletMap();
			}

			setTimeout( function() {
				$("#cancel-booking-modal").modal('show');
			}, 1000);

			setTimeout( function() {
				$("#window-offer-modal").modal('show');
			}, 1000);

		});


		$('.header-pages #search.collapsed').on('click', function(e) {
			e.preventDefault();

			$(this).removeClass('collapsed');

			$(this).find('.form-control').focus();

			$(this).off('click');
		});

		// Animated thumbnails
	    var $animThumb = $('#lightgallery-in-content');
	    if ($animThumb.length) {
	        $animThumb.justifiedGallery({
	            border: 6
	        }).on('jg.complete', function() {
	        	$('#lightgallery-in-content > a .gallery-poster').show();
	            $animThumb.lightGallery({
	                thumbnail: true,
	                loop: false,
	            });
	        });
	    }

	    if( $('.news-wrap.jscroll').length ) {
		    var win = $(window);
			// Each time the user scrolls
			win.scroll(function() {
				// End of the document reached?
				if ($(document).height() - win.height() == win.scrollTop()) {
			
					//$('#loading').show();

					$.ajax({
						url: '/ajax/?mod=catalog.action.lazy_news',
						dataType: 'json',
						data: {
							number_news: $('.news-wrap .item-wrap').length,
							page_alias: $( '.breadcrumbs span' ).data( 'alias' ),
						}

					}).done( function(data) {
						
						if( !data.s ) {
							return false;
						}

						$('.news-wrap.jscroll').append(data.html);

					});
				}
			});
		}



	    var $animThumbPhoto = $('.lightgalleries');
	    if ($animThumbPhoto.length) {

	    	$animThumbPhoto.each(function() {
	    		$(this).justifiedGallery({
		            border: 6
		        }).on('jg.complete', function() {
		        	$('.lightgalleries > a .gallery-poster').show();
		            $(this).lightGallery({
		                thumbnail: true,
		                loop: false,
		            });
		        });

		        $('.lightgalleries').show();
	    	});
	 
	    }

	    var $animThumbVideo = $('.video-gallery');
	    if ($animThumbVideo.length) {
	    	$animThumbVideo.each(function() {
		        $(this).justifiedGallery({
		            border: 6,
		            rowHeight: 240,
		            margins: 10
		        }).on('jg.complete', function() {
		        	$('.video-gallery > a .gallery-poster').show();
		            $(this).lightGallery({
		                loadYoutubeThumbnail: true,
	    				youtubeThumbSize: 'mqdefault',
		            });
		        });

		        $('.video-gallery').show();
		    });
	    }
	 
    	$(window).on('scroll', function() {

    		if( $(window).scrollTop() > $('header').outerHeight() + 200) {

    			if( $('header .fixed-menu').length ) return;

    			$('.header-top').css('margin-bottom', $('.et-menu').outerHeight());
    			$('.et-menu').wrapAll('<div class="fixed-menu"><div class="container"></div></div>');
    			$('header .fixed-menu').slideDown(200);

    		}
    		else {

    			if( !$('header .fixed-menu').length ) return;

    			$('header .fixed-menu').slideUp(200, function() {

	    			$('.header-top').css('margin-bottom', '');
	    			$('.et-menu').unwrap().unwrap();

    			});

    		}

    	});

    	var lightSlider = '';
    	var isInitLightSlider = false;
    	function initLightslider( selector ) {
    		lightSlider = $( selector ).lightSlider({
	    		item: 1,
	    		gallery: true,
	    		thumbItem: 2,
	    		controls: false,
	    		pager: true,
	    		enableDrag: false,
    			enableTouch: false,
	    		galleryMargin: 18,
	    		thumbMargin: 18,
	    		onAfterSlide: function() {

	    			var index = $('.lSPager.lSGallery > li.active').index() / 2;
	    			index = Math.floor(index);

	    			$('.custom-pager li').removeClass('active');
	    			$('.custom-pager li').eq(index).addClass('active');

	    		},
	    		onSliderLoad: function() {

	    			$('#item-slider').css('visibility', 'visible');
	    			
	    			var numbers = $('.lSPager.lSGallery > li').length;

	    			numbers = Math.ceil(numbers / 2);
	    			if( numbers < 2 ) return;

	    			$('.lSSlideOuter').append('<ul class="custom-pager"></ul');


	    			for( var i = 0; i < numbers; i++ ) {

	    				if( i == 0 ) {
	    					$('.lSSlideOuter .custom-pager').append('<li class="active"></li>');
	    				}
	    				else {
	    					$('.lSSlideOuter .custom-pager').append('<li></li>');
	    				}

	    			}	

	    			$('.custom-pager li').click( function() {

	    				if( $(this).hasClass('active') ) return;

	    				var number = $(this).index();

	    				$(this).siblings().removeClass('active');
	    				$(this).addClass('active');

	    				number = (number+1) * 2 - 2;

	    				var offset = 0;

	    				if( (number + 1)  ==  $('.lSPager.lSGallery > li').length ) {
	    					number--;
	    				} 
	    				else if( !number ) {
	    					offset = 0;
	    				}
	    				else {
	    					offset = ($('.lSSlideOuter').outerWidth() - $('.lSPager.lSGallery > li').outerWidth())/2;
	    				}

	    				var position = $('.lSPager.lSGallery > li').eq(number).position().left - offset;

	    				$('.lSPager.lSGallery').css('transform', 'translate3d(-'+ position +'px , 0, 0)');


	     			});
	    		}

	    	});

	    	isInitLightSlider = true;
    	}

    	if( $("#item-slider").length ) {
	    	initLightslider("#item-slider");
	    }

	// Custom radio
		if( $(".select-number").length ) {
			$(".select-number").buttonset();
		}

		if( $(".custom-radio").length ) {
			$(".custom-radio").buttonset();
		}

		$('.fancybox').fancybox();


	// Inteview custom option
		$(".cancel-booking-modal form .custom-radio input").change( function() {
			if($('.cancel-booking-modal .custom-radio #radiobox-42').is(':checked'))  {
				$('.cancel-booking-modal .custom-option-textarea').show(0);
				$('.cancel-booking-modal .custom-option-textarea textarea').focus();
			}
			else {
				$('.cancel-booking-modal .custom-option-textarea').hide(0);
			}
		});


	// Phone input mask
		if($('.phone input').length) {
			$(".phone input").mask("?+7 (999) 999-99-99");
		}

		if($('input[name="phone"]').length) {
			$('input[name="phone"]').mask("?+7 (999) 999-99-99");
		}

	// Close procedures window
		$('.proc-info .close-btn').on('click', function(e) {
			e.preventDefault();
			closeProcedureWindow();
		});
		function closeProcedureWindow() {
			$('.proc-info').fadeOut(200);
		}

	// Open procedures window
		$('#select-number .ui-button-text').on('click', function() {

			var nDays = $(this).text();

			$('.proc-info:not(#proc-info-'+ nDays +')').fadeOut(0, function() {
				$('#proc-info-' + nDays).fadeIn(300);
			});

		});

	// Get rooms info Ajax
	// get first room by default
		var class_room_id;
		var room_id;
		$('body').on('click', '[data-target="#room-modal"]', function(e) {

			e.preventDefault();

			if( !($('.select-room input:checked').length && ( $('.select-room input[data-room-id]').length 
				&& $('.select-room input[data-main-place]').length && $('.select-room input[data-additional-place]').length )) ) {
				$('.select-room input:checked').prop('checked', false);
				$('.select-room').buttonset('refresh');
			}
			else {
				$('.select-room').buttonset('refresh');
			}

			getRoomsAjax.call(this, true);

		});

	// get selected room in a modal window
		$('body').on('change', 'input[name="radio-rooms"]', function() {
			
			getRoomsAjax.call(this, false);

		});

		function getRoomsAjax(isClassRoom) {

			if( isClassRoom ) {
				class_room_id = $(this).closest('.check-title').prev().attr('id');
				class_room_id = class_room_id.slice( class_room_id.indexOf('-') + 1 );
				room_id = 'default';
			} 
			else {
				room_id = $('.room-content-wrap input[name="radio-rooms"]:checked').attr('id');
				room_id = room_id.slice( room_id.indexOf('-') + 1 );

				$('#room-modal .room-content-wrap').fadeOut(300, function() {

			    	$('#room-modal #preloader-ajax').css('display', '');
					$('#room-modal .modal-body').css('height', '');

				});
			}

			setTimeout( function() {
				$.ajax({

					url: '/ajax/?mod=catalog.action.rooms_ajax',
					dataType: 'json',
					method: 'post',
					data: {
						class_room_id: class_room_id,
						room_id: room_id,
					}

				}).done( function(data) {
					
					if( !data.s ) {

						console.log(data);
						$('.modal-body').css('height', 'auto');
						$('#room-modal #preloader-ajax').css('display', 'none');
						$('#room-modal .room-content-wrap')
							.fadeIn(300)
							.html('<h3 style="text-align: center;">Не удалось получить данные от сервера. Попробуйте позднее.</h3>');
						return false;

					}


					if( ($("#room-modal").data('bs.modal') || {isShown: false}).isShown ) {
						showRoomsModal( data.html );
					}
					else {
						$('#room-modal').on('shown.bs.modal', function () {
							showRoomsModal( data.html );

							$('#room-modal').off('shown.bs.modal');
						});
					}

				});
			}, 500);

				

		}

		$('body').on('hidden.bs.modal', '#room-modal', function () {
		    $('#room-modal .room-content-wrap').css('display', '');
		    $('#room-modal #preloader-ajax').css('display', '');
		    $('.modal-body').css('height', '');
		});

	// Show rooms modal
		function showRoomsModal( data ) {

			if( isInitLightSlider ) {

				lightSlider.destroy();
				isInitLightSlider = false;

			}

			$('#room-modal .room-content-wrap').empty().html( data );

			if( $('#room-slider').length ) initLightslider("#room-slider");

			$('.room-modal .modal-body').css('height', 'auto');

			if( $(".select-number-rooms .custom-radio").length ) {
				$(".select-number-rooms .custom-radio").buttonset();
			}

			$('#room-modal #preloader-ajax').hide();
			$('#room-modal .room-content-wrap').fadeIn(300);

		}


		//Карта
		if( $('#map').length ) {
			var routeBuilder = new function() {

				var self = this;

				var directionsService = new google.maps.DirectionsService();
				var directionsDisplay = new google.maps.DirectionsRenderer({suppressMarkers: true});
				var destination = new google.maps.LatLng(55.533046, 60.571168);

				var mapOptions = {
					zoom: 10,
					center: destination,
					scrollwheel: false
				}

				var map = new google.maps.Map(document.getElementById("map"), mapOptions);

				directionsDisplay.setMap(map);

				var end_icon = new google.maps.MarkerImage(
						// URL
						'/static/img/mapicon.png',
						new google.maps.Size( 113, 73 ),
						// The origin point (x,y)
						new google.maps.Point( 0, 0 ),
						// The anchor point (x,y)
						new google.maps.Point( 66, 73 )
					);

				makeMarker( destination, end_icon, 'Курорт Увильды' );

				var start_icon = new google.maps.MarkerImage(
						'/static/img/start_location_icon.png',
						new google.maps.Size( 50, 73 ),
						new google.maps.Point( 0, 0 ),
						new google.maps.Point( 25, 73 )
					);

					
				function makeMarker( position, icon, title ) {
					var infowindow = new google.maps.InfoWindow({
				      content: title
				    });
					var marker = new google.maps.Marker({
						position: position,
						map: map,
						icon: icon,
						title: title
					});
					google.maps.event.addListener(marker, 'click', function() {
				      infowindow.open(map,marker);
				    });
				}

				var startPosition;

				if ("geolocation" in navigator) {

					navigator.geolocation.getCurrentPosition( function(position) {
						self.startPosition = new google.maps.LatLng(position.coords.latitude,  position.coords.longitude);
						self.calcRoute('car');
					});

				} else {
					alert('Не удалось определить Ваше местоположение, геолокация недоступна для этого браузера.');
				}

				this.directionsService = directionsService;
				this.directionsDisplay = directionsDisplay;
				this.destination = destination;
				this.map = map;

				this.calcRoute = function(mode) {

					if( typeof this.startPosition == 'undefined' ) {
						return false;
					}

					var travelMode;
					var transitOptions;

					switch(mode) {
						case 'car':
							travelMode = google.maps.TravelMode.DRIVING;
							break;

						case 'walking':
							travelMode = google.maps.TravelMode.WALKING;
							break;
					}

					var request = {
						origin: this.startPosition,
						destination: this.destination,
						travelMode: travelMode,
						transitOptions: transitOptions,
						provideRouteAlternatives: true,
					};

					directionsService.route(request, function(result, status) {
						if (status == google.maps.DirectionsStatus.OK) {
							directionsDisplay.setDirections(result);
							var leg = result.routes[ 0 ].legs[ 0 ];
							makeMarker( leg.end_location, end_icon, 'Увильды' );
							makeMarker( leg.start_location, start_icon, 'Ваше текущее расположение' );
						}
						else if( status == google.maps.DirectionsStatus.ZERO_RESULTS ) {
							alert('Этот вид проезда не поддерживается');
						}
					});

				};

			}
		}


		$('.way-to-get .type').on('click', function(e) {
			e.preventDefault();

			$('.way-to-get > p.selected').removeClass('selected');
			$(this).closest('p').addClass('selected');

			var travelMode = $(this).closest('p').attr('data-travel-type');
			routeBuilder.calcRoute(travelMode);

		});


		$('.programm-page .details a').on('click', function(e) {
			e.preventDefault();
			if($('.programm-page .content-rolled').is(':visible')) {
				$('.programm-page .content-rolled').slideUp();
				$(this).text('Подробнее');
			} else {
				$('.programm-page .content-rolled').slideDown();
				$(this).text('Свернуть');
			}
		});

		$('.content table').wrap('<div class="table-responsive"></div>');


		$('.programms .item').hover(
			function() {
			  	$(this).find('.details a').css('background-color', '#ff6c00');
			},
			function() {
			  	$(this).find('.details a').css('background-color', '');
		});

		$('.programms .item .button-wrapper .basic-button:not(.details) a').hover(
			function() {
				$(this).closest('.basic-button').siblings('.details').find('a').css('background-color', '');
			},
			function() {
			  	$(this).closest('.basic-button').siblings('.details').find('a').css('background-color', '#ff6c00');
		});


		$('.turn-menu').on('click', function(e) {

			e.preventDefault();

			if($(this).next().is(':animated')) return false;

			var top = $('.turn-menu').outerHeight();
			var height = $('.map-page').outerHeight() - top;
			$(this).next().outerHeight(height);

			if( !$(this).next().hasClass('open') ) {

				$(this).next().animate({
					bottom: 0,
				}, 400).addClass('open');

				$(this).find('img').css('transform', 'rotate(180deg)');

			}
			else {

				$(this).next().animate({
					bottom: '100%',
				}, 400).removeClass('open').css('top', 'auto');

				$(this).find('img').css('transform', 'rotate(0)');
			}

		});


	// Check selected number
		$('body').on('click', '#confirm-room', function() {

			$('.humans-wrapper').empty();

			$('.booking-error-message').remove();

			$('#room-' + class_room_id).prop('checked', true)
				.attr('data-room-id', $('.select-number-rooms input:checked').attr('data-room-id'))
				.attr('data-main-place', $('.select-number-rooms input:checked').attr('data-main-place'))
				.attr('data-additional-place', $('.select-number-rooms input:checked').attr('data-additional-place'));

			$(".select-room").buttonset('refresh');

			$('.humans-wrapper').show();

			$('.booking-overlay').show();

			var number = $('.booking-step2-page .select-room input:checked[data-room-id]').attr('data-room-id');

			$.ajax({
				url: '/ajax/?mod=catalog.action.booking_ajax',
				dataType: 'json',
				method: 'post',
				data: {
					method: 'get_humans_form',
					number: number				
				}
			}).done( function( data ) {

				$('.booking-overlay').hide();

				if( !data.s ) {
					var errors = '';
					for( var i = 0; i < data.error.length; i++ ) {
						errors += data.error[i] + '</br>';
					}

					$('#room-modal').after('<p class="booking-error-message">'+ errors +'</p>');

					$('html, body').animate({
				        scrollTop: $(".booking-error-message").offset().top - 60
				    }, 500);

					return false;
				}

				$('.humans-wrapper').html( data.humans_form );	

			});

		});


		$('#detect-city').on('submit', function(e) {
			
			var ip = $('#ip').val();

			e.preventDefault();
			$.ajax({
				url: '/ajax/?mod=catalog.action.get_city_by_ip_ajax',
				dataType: 'json',
				method: 'post',
				data: {
					ip: ip				
				}
			}).done( function( data ) {

				
				if( !data.city ) {
					var nameCountry = 'Страна не найдена'
					$('#your-city').val(nameCountry);
					return;
				}

				var nameCountry = data.city.country.name_ru;
				$('#your-city').val(nameCountry);
			});

		});


	});

}(jQuery))