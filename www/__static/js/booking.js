'use strict';

;(function($, undefined) {

	$(document).on('ready', function() {

	


		var booking = new function() {

			this.getStep = function( step ) {

				$('.step-wrap').empty();

				$('.step-wrap').html( step );

				if( $(".select-room").length ) {
					$(".select-room").buttonset();
				}

				if( $(".select-programm-wrap .custom-radio").length ) {
					$(".select-programm-wrap .custom-radio").buttonset();
				}

			// Flip items on booking map
				if($('html').hasClass('no-touchevents')) {
					$('.booking-map .back').show();
					$(".booking-map .flip-card").flip({
						axis: 'y',
						trigger: 'hover',
						speed: 300,
					});
				}

			// Validate form
				$('.booking-step2-page .add-person-form .old input').on('input', function() {
					this.value = this.value.replace(/[^0-9\.]/g,'');
					if( this.value > 120 ) this.value = 120;
					this.value = this.value.replace(/^0+/, '');
					if( (this.value < 0) && this.value.length ) this.value = 0;
				});

				$('body').on('input', '.booking-step2-page .select-during .number-days', function() {
					this.value = this.value.replace(/[^0-9]/g,'');
					if( this.value > 120 ) this.value = 120;
					this.value = this.value.replace(/^0+/, '');
					if( (this.value < 1) && this.value.length ) this.value = 1;
				});

				
				
				$('.booking-overlay').hide();

				// Select date
				$(function() {

					if( !$('.from-to').length ) return; 

					$( "#from" ).datepicker({
						defaultDate: "+1w",
						changeMonth: true,
						changeYear: true,
						numberOfMonths: 1,
						onClose: function( selectedDate ) {
							if( isWeekend() ) {
								$('#prog-putevka-vyhodnogo-dnya').closest('.form-group').show();
							}
							else {
								if( $('#prog-putevka-vyhodnogo-dnya').is(':checked') ) {
									$('#prog-putevka-vyhodnogo-dnya').closest('.form-group').hide();
									$('#prog-putevka-vyhodnogo-dnya').prop('checked', false);

									var radio = $('.select-during .custom-radio[data-programm-id="prog-putevka-vyhodnogo-dnya"]');
									radio.hide();

									$('.select-programm-inner .custom-radio .form-group:first input').prop('checked', true);
									$('.select-during .field-number-days:first').show();

									$('#prog-putevka-vyhodnogo-dnya').closest('.custom-radio').buttonset("refresh");
								}
							}

							daysWeekendProgramm();
			
						},
						// onClose: function( selectedDate ) {
						// 	$( "#to" ).datepicker( "option", "minDate", selectedDate );
						// },
					});
					// }).datepicker("setDate", new Date());

					// $( "#to" ).datepicker({
					// 	defaultDate: "+1w",
					// 	changeMonth: true,
					// 	changeYear: true,
					// 	numberOfMonths: 1,
					// 	onClose: function( selectedDate ) {
					// 		$( "#from" ).datepicker( "option", "maxDate", selectedDate );
					// 	}
					// });

					$( "#from" ).datepicker( "option", "minDate", new Date());

					$.datepicker.setDefaults( $.datepicker.regional['ru'] );

					if( isWeekend() ) {
						$('#prog-putevka-vyhodnogo-dnya').closest('.form-group').show();
					}

					daysWeekendProgramm();

				});

			}

		}


	
	// Get current step
		if( $('.step-wrap').length ) {
			$.ajax({

				url: '/ajax/?mod=catalog.action.booking_ajax',
				dataType: 'json',
				method: 'post',
				data: {
					method: 'get_step',
				}

			}).done( function( data ) {

				if( !data.s ) {
					$('.booking-overlay').hide();
					console.log(data.mess);
					alert('Произошла ошибка, попробуйте еще раз позднее.');
					return false;
				}

				booking.getStep(data.html);

				// Phone input mask
				if($('.step-wrap input[name="phone"]').length) {
					$('.step-wrap input[name="phone"]').mask("?+7 (999) 999-99-99");
				}

			});
		}

	
	if ( $( '.number-days-programm' ).length ) {
		$('body').on('input', '.form-control.number-days-programm.field-number-days', function() {
			this.value = this.value.replace(/[^0-9]/g,'');
			if( this.value > 120 ) this.value = 120;
			this.value = this.value.replace(/^0+/, '');
			if( (this.value < 1) && this.value.length ) this.value = 1;
		});
	}
		
		
	// Get previous step
		$('body').on('click', '.booking-page .booking-back', function(e) {

			e.preventDefault();

			$('.booking-overlay').show();

			$.ajax({

				url: '/ajax/?mod=catalog.action.booking_ajax',
				dataType: 'json',
				method: 'post',
				data: {
					method: 'get_previous_step',
				}

			}).done( function( data ) {

				if( !data.s ) {
					$('.booking-overlay').hide();
					console.log(data.mess);
					alert('Произошла ошибка, попробуйте еще раз позднее.');
					return false;
				}

				booking.getStep(data.html);

			});

		});

	// Select building.
		$('body').on('click', '.booking-map-page .booking-icon', function(e) {

			e.preventDefault();

			$('.booking-overlay').show();

			var building = $(this).attr('id');

			$.ajax({

				url: '/ajax/?mod=catalog.action.booking_ajax',
				dataType: 'json',
				method: 'post',
				data: {
					method: 'select_building',
					building: building,
				}

			}).done( function( data ) {


				if( !data.s ) {
					$('.booking-overlay').hide();
					console.log(data.mess);
					alert('Не удалось выбрать здание, попробуйте еще раз позднее.');
					return false;
				}

				booking.getStep(data.step);

			});

		});

	// Apply to programm with programm page.
		$('.programm-page .select-programm').on('submit', function(e) {

			e.preventDefault();

			$('.booking-overlay').show();

			var programm = $('h1[data-programm-id]').attr('data-programm-id');
			var duration;
			var duration_count = $(this).find('.select-number').data( 'duration-count' );

			if ( duration_count ) {
				duration = $(this).find('.select-number input:checked').val();
			}
			else {
				duration = $(this).find('.select-number input').val();
			}

			$.ajax({
				url: '/ajax/?mod=catalog.action.booking_ajax',
				dataType: 'json',
				method: 'post',
				data: {
					method: 'write_to_programm',
					duration: duration,
					programm: programm
				}
			}).done( function( data ) {

				$('.booking-overlay').hide();

				if( !data.s ) {
					console.log(data.mess);
					alert('Не удалось записаться на программу, попробуйте записаться через раздел бронирование.');
					return false;
				}

				window.location.href = '/bronirovanie.html';

			});

		});

	// Apply to programm with all programms page.
		$('.programms-page .item .enroll, .programms .item .enroll').on('click', function(e) {

			e.preventDefault();
			e.stopPropagation();

			var programm = $(this).closest('.item').attr('data-programm-id');

			$.ajax({
				url: '/ajax/?mod=catalog.action.booking_ajax',
				dataType: 'json',
				method: 'post',
				data: {
					method: 'write_to_programm',
					duration: 'default',
					programm: programm
				}
			}).done( function( data ) {

				if( !data.s ) {
					console.log(data.mess);
					alert('Не удалось записаться на программу, попробуйте записаться через раздел бронирование.');
					return false;
				}

				window.location.href = '/bronirovanie.html';

			});

		});


	// Open person form.
		$('body').on('click', '.booking-page .add-man button', function(e) {

			e.preventDefault();

			$('.booking-step2-page .booking-error-message').remove();

			if( !$('.select-room input:checked').length ) {
				$('.booking-step2-page .add-man').after('<p class="booking-error-message">Перед добавлением людей выберите номер</p>');
				return false;
			}

			var maxPeople = +$('.select-room input:checked').attr('data-main-place') + +$('.select-room input:checked').attr('data-additional-place');

			if( maxPeople == $('.added-humans .item').length ) {
				$('.booking-step2-page .add-man').after('<p class="booking-error-message">Больше нельзя добавить людей для выбранного номера</p>')
				return false;
			}

			if( $(".custom-radio").length ) {
				$(".custom-radio").buttonset();
			}

			$('.add-person-form-wrap').slideDown();

		});

	// Close person form.
		$('body').on('click', '.booking-page .close-form button', function(e) {

			e.preventDefault();

			$('.add-person-form-wrap').slideUp();

		});


	// Select programm
		$('body').on( 'change', '.select-programm-wrap .select-programm-inner .custom-radio input', function() {

			var programmId = $(this).attr('id');
			
			$('.select-during .custom-radio').hide();
			$('.select-during .number-days').hide();



			if( programmId == 'prog-without-programm' ) {
				$('.select-during .number-days').show();
				$('.select-during h2').text('Введите продолжительность курса (дней)');
				return;
			}	
			else {
				var duration_count = $('.select-programm-wrap .select-during .custom-radio[data-programm-id="'+ programmId +'"]').data( 'duration-count' );
				if ( duration_count ) {
					$('.select-during h2').text('Выберите продолжительность курса (дней)');
					$('.select-during .custom-radio[data-programm-id="'+ programmId +'"]').show();
					return;
				}
				else {
					$('.select-during .custom-radio[data-programm-id="'+ programmId +'"]').show();
					$('.select-during h2').text('Введите продолжительность курса (дней)');
				}
			}
			
			


		});


	// Add person.
		$('body').on('click', '.add-person-form-wrap .save-person button', function(e) {

			e.preventDefault();

			$('.booking-error-message').remove();
			$('.booking-overlay').show();

			var age = $('.add-person-form-wrap .old .custom-radio input[type="radio"]:checked').attr('data-age-title');
			var food = $('.add-person-form-wrap .old .custom-radio input[type="radio"]:checked').attr('data-food-title');

			if( $('.add-person-form-wrap .old .custom-radio input[type="radio"]:checked').closest('.form-group').find('.check-food input[type="checkbox"]').length ) {	

				if( !$('.add-person-form-wrap .old .custom-radio input[type="checkbox"]:checked').closest('.form-group')
					.find('.check-food input[type="checkbox"]:checked').length ) {
					var food = 'без питания';
				}

			}

			$.ajax({
				url: '/ajax/?mod=catalog.action.booking_ajax',
				dataType: 'json',
				method: 'post',
				data: {
					method: 'add_person',
					age: age,
					food: food
				}
			}).done( function( data ) {

				$('.booking-overlay').hide();

				if( !data.s ) {
					var errors = '';
					for( var i = 0; i < data.error.length; i++ ) {
						errors += data.error[i] + '</br>';
					}

					$('#room-modal').after('<p class="booking-error-message">'+ errors +'</p>');

					$('html, body').animate({
				        scrollTop: $(".booking-error-message").offset().top - 60
				    }, 500);

					return false;
				}

				$('.add-person-form-wrap').hide();

				$('.added-humans').html(data.added_persons);

			});

		});


	// Delete person
		$('body').on('click', '.added-humans .delete', function(e) {

			e.preventDefault();

			$('.booking-error-message').remove();

			$('.booking-overlay').show();

			var person_id = $(this).closest('.item').attr('data-person-id');

			$.ajax({
				url: '/ajax/?mod=catalog.action.booking_ajax',
				dataType: 'json',
				method: 'post',
				data: {
					method: 'remove_person',
					person_id: person_id,
				}
			}).done( function( data ) {

				$('.booking-overlay').hide();

				if( !data.s ) {
					var errors = '';
					for( var i = 0; i < data.error.length; i++ ) {
						errors += data.error[i] + '</br>';
					}

					$('#room-modal').after('<p class="booking-error-message">'+ errors +'</p>');

					$('html, body').animate({
				        scrollTop: $(".booking-error-message").offset().top - 60
				    }, 500);

					return false;
				}

				$('.add-person-form-wrap').hide();

				$('.added-humans').html(data.added_persons);

			});

		});

		$('body').on('click', '#get-step-2', function(e) {
			e.preventDefault();
			
			var startDate = $('.booking-step2-page .dates #from').val();
			var programm = $('.select-programm-wrap .select-programm-inner input:checked').data( 'programm-id' );
			var duration;

			if ( programm == 'without-programm' ) {
				duration = $('.select-programm-wrap .select-during .number-days').val();
			}
			else {
				var programm_id = $('.select-programm-wrap .select-programm-inner input:checked').attr( 'id' );
				var duration_count = $('.select-programm-wrap .select-during .custom-radio[data-programm-id="'+ programm_id +'"]').data( 'duration-count' );
				if ( duration_count ) {
					duration = $('.select-programm-wrap .select-during .custom-radio[data-programm-id="'+ programm_id +'"] input:checked').val();
				}
				else {
					duration = $('.select-programm-wrap .select-during .custom-radio[data-programm-id="'+ programm_id +'"] input').val();
				}
				//console.log( programm_id );
				//console.log( duration_count );
				//console.log( duration );
			}

			$('.booking-error-message').remove();
			$('.booking-overlay').show();

			$.ajax({
				url: '/ajax/?mod=catalog.action.booking_ajax',
				dataType: 'json',
				method: 'post',
				data: {
					method: 'get_step_2',
					start_date: startDate,
					programm: programm,
					duration: duration					
				}
			}).done( function( data ) {

				$('.booking-overlay').hide();

				if( !data.s ) {
					if( 'error' in data ) {
						var errors = '';
						for( var i = 0; i < data.error.length; i++ ) {
							errors += data.error[i] + '</br>';
						}

						$('.booking-step2-page .dates').before('<p class="booking-error-message">'+ errors +'</p>');

						$('html, body').animate({
					        scrollTop: $(".booking-error-message").offset().top - 60
					    }, 500);
					}

					return false;
				}
				
				$('.step-wrap').empty();

				$('.step-wrap').html( data.html );	

				if( $(".select-room").length ) {
					$(".select-room").buttonset();
				}

			});
		});


		$('body').on('click', '#to-last-step', function(e) {
			e.preventDefault();

			$('.booking-error-message').remove();

			$('.booking-overlay').show();

			$.ajax({
				url: '/ajax/?mod=catalog.action.booking_ajax',
				dataType: 'json',
				method: 'post',
				data: {
					method: 'get_last_step',			
				}
			}).done( function( data ) {

				$('.booking-overlay').hide();

				if( !data.s ) {
					var errors = '';
					for( var i = 0; i < data.error.length; i++ ) {
						errors += data.error[i] + '</br>';
					}

					$('#room-modal').after('<p class="booking-error-message">'+ errors +'</p>');

					$('html, body').animate({
				        scrollTop: $(".booking-error-message").offset().top - 60
				    }, 500);

					return false;
				}
				
				$('.step-wrap').empty();

				$('.step-wrap').html( data.html );

				// Phone input mask
				if($('.step-wrap input[name="phone"]').length) {
					$('.step-wrap input[name="phone"]').mask("?+7 (999) 999-99-99");
				}

			});
		});

		var weekday = new Array(7);
		weekday[0] = "Sunday";
		weekday[1] = "Monday";
		weekday[2] = "Tuesday";
		weekday[3] = "Wednesday";
		weekday[4] = "Thursday";
		weekday[5] = "Friday";
		weekday[6] = "Saturday";

		function isWeekend() {
			var date = $( "#from" ).datepicker('getDate');
			if( !date ) return false;

			var dayOfWeek = weekday[date.getUTCDay()];

			if( (dayOfWeek == weekday[4]) || (dayOfWeek == weekday[5]) || (dayOfWeek == weekday[6]) ) {
				return true;
			}
		}

		function daysWeekendProgramm() {
			if($('#prog-putevka-vyhodnogo-dnya').length) {
				var radio = $('.select-during .custom-radio[data-programm-id="prog-putevka-vyhodnogo-dnya"]');
				radio.find('label').show();

				radio.find('input:first').prop('checked', true);

				var date = $( "#from" ).datepicker('getDate');
				if( !date ) return false;
				
				var dayOfWeek = weekday[date.getUTCDay()];

				if( dayOfWeek == weekday[5] || dayOfWeek == weekday[6] ) {
					radio.find('label:last').hide();
				}

				if( dayOfWeek == weekday[6] ) {
					radio.find('label:eq(1)').hide();
				}

				radio.buttonset("refresh");
			}
		}

		
	});

}(jQuery))