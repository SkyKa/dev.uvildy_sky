'use strict'; 

;(function($, undefined) { 

	$(document).on( 'ready', function() {

		if(window.matchMedia('(max-width: 767px)').matches)
			{
				$('.landing_slider_radon').slick();

				$('.landing_second_slider_radon').slick({
					prevArrow: $('.science_slider_left'),
					nextArrow: $('.science_slider_right'),
					slidesToShow: 1
				});

				$('.landing_third_slider_radon').slick({
					prevArrow: $('.news_slider_left'),
					nextArrow: $('.news_slider_right'),
					slidesToShow: 1
				});

				$('.landing_fourth_slider_radon').slick({
					prevArrow: $('.med_slider_left'),
					nextArrow: $('.med_slider_right'),
					slidesToShow: 1
				});

				$('.landing_fifth_slider_radon').slick({
					prevArrow: $('.metod_slider_left'),
					nextArrow: $('.metod_slider_right'),
					slidesToShow: 1
				});

				$('.landing_sixth_slider_radon').slick({
					prevArrow: $('.ill_slider_left'),
					nextArrow: $('.ill_slider_right'),
					slidesToShow: 1
				});

				$('.landing_seventh_slider_radon').slick({
					prevArrow: $('.worlds_slider_left'),
					nextArrow: $('.worlds_slider_right')
				});
			} else {

						$('.landing_slider_radon').slick();

						$('.landing_second_slider_radon').slick({
							prevArrow: $('.science_slider_left'),
							nextArrow: $('.science_slider_right'),
							slidesToShow: 2
						});

						$('.landing_third_slider_radon').slick({
							prevArrow: $('.news_slider_left'),
							nextArrow: $('.news_slider_right'),
							slidesToShow: 2
						});

						$('.landing_fourth_slider_radon').slick({
							prevArrow: $('.med_slider_left'),
							nextArrow: $('.med_slider_right'),
							slidesToShow: 2
						});

						$('.landing_fifth_slider_radon').slick({
							prevArrow: $('.metod_slider_left'),
							nextArrow: $('.metod_slider_right'),
							slidesToShow: 3
						});

						$('.landing_sixth_slider_radon').slick({
							prevArrow: $('.ill_slider_left'),
							nextArrow: $('.ill_slider_right'),
							slidesToShow: 3
						});

						$('.landing_seventh_slider_radon').slick({
							prevArrow: $('.worlds_slider_left'),
							nextArrow: $('.worlds_slider_right'),
							slidesToShow: 4
						});

						$('.restorante_weddings_slider').slick({							
							slidesToShow: 4
						});

						$('.restorante_guest_reviews_slider').slick({							
							slidesToShow: 2
						});

						$('.restorante_stocks_slider').slick({							
							slidesToShow: 2
						});

				}

	});




})( jQuery );