$(document).ready(function(){

	/* SolutionsHeadersMobile: Start */
	var solHeaders = $('.solutions-header'),
		 shProblem = solHeaders.find('.slt-prbl').clone(),
		 shSolution = solHeaders.find('.slt-slts').clone(),
		 shResult = solHeaders.find('.slt-res').clone();

	$('.prob-gitem-wrap').prepend( shProblem );
	$('.slts-gitem-wrap').prepend( shSolution );
	$('.res-gitem-wrap').prepend( shResult );
	/* SolutionsHeadersMobile: End */

	// LazyLoad
	$('.lazy').Lazy({
		effect: 'fadeIn', 
		effectTime: 1250,
		afterLoad: function (e) {
			e.css('display', '')
		}
	});

	/* JCF: Start */
	jcf.replaceAll();
	
	// set options for Checkbox module
	jcf.setOptions('Checkbox', {
		checkedClass: 'test',
		wrapNative: false
	});
	/* JCF: End */

	// MagnificPopupTranslations
	$.extend(true, $.magnificPopup.defaults, {
		tClose: 'Закрыть (Esc)',
		tLoading: 'Загрузка...',
		gallery: {
			tPrev: 'Назад',
		tNext: 'Далее',
	    tCounter: '%curr% из %total%'
	  },
	  image: { tError: '<a href="%url%">Изображение</a> не удалось загрузить.' },
	  ajax: { tError: '<a href="%url%">Содержимое</a> не удалось загрузить.' }
	});


	/* CertificateSlider: Start */
	var certificatesSlider = $('.certificates-slider');
	
	function callback(event) {
		certificatesSlider.find('a').magnificPopup({
			type: 'image',
			midClick: true,
			removalDelay: 600,
			mainClass: 'mfp-fade',
			closeMarkup: '<div title="Закрыть (Esc)" class="mfp-close">&#215;</div>',
		});
	}
	
	certificatesSlider.owlCarousel({
		margin: 12,
		loop: true,
		autoWidth: true,
		items: 4,
		dots: true,
		smartSpeed: 500,
		onInitialized: callback,
		responsive : {
			// 767 : {
			// 	items: 1
			// }
		}
	});
	/* CertificateSlider: End */

	/* SpecislistSlider: Start */
	$('.specislist-slider').owlCarousel({
			loop: true,
			items: 1,
			dots: true,
			smartSpeed: 500
		});
	/* SpecislistSlider: End */

	// MainMenuButton
	$('#sandwich').click(function(){
		$(this).toggleClass('open');
		$('.main-menu-container').slideToggle(); // menu container
		// if ( $('#sandwich').hasClass('open') ) {}
	});

	/* SVG For Everybody */
	svg4everybody();

	/* Back to top button: Start */
	var navButton = $('#top-button'),
	    screenHeight = $(window).height(),
		 topShow = screenHeight, // Не показывать до (screenHeight или Number), px 
		 navSpeed = 1000; // Скорость прокрутки, мс 

	function scrollCalc() {
		var scrollOut = $(window).scrollTop();

		if ( scrollOut > topShow && ( navButton.attr('class') == '' || navButton.attr('class') == undefined ) )
			navButton.fadeIn().removeClass('down').addClass('up').attr('title', 'Наверх');
		if ( scrollOut < topShow && navButton.attr('class') == 'up' )
			navButton.fadeOut().removeClass('up down');
		if ( scrollOut > topShow && navButton.attr('class') == 'down' )
			navButton.fadeIn().removeClass('down').addClass('up');
	}

	$(window).bind('scroll', scrollCalc);
	var lastPos = 0;

	navButton.bind('click', function () {
		scrollOut = $(window).scrollTop();

		if ( navButton.attr('class') == 'up' ) {
			lastPos = scrollOut;
			$(window).unbind('scroll', scrollCalc);
			
			$('body, html').animate({
				scrollTop: 0
			}, navSpeed, function () {
				navButton.removeClass('up').addClass('down').attr('title', 'Вернуться');
				$(window).bind('scroll', scrollCalc);
			});
		}
		if ( navButton.attr('class') == 'down' ) {
			$(window).unbind('scroll', scrollCalc);
			
			$('body, html').animate({
				scrollTop: lastPos
			}, navSpeed, function () {
				navButton.removeClass('down').addClass('up').attr('title', 'Наверх');
				$(window).bind('scroll', scrollCalc);
			});
		}
	});
	/* Back to top button: End */

	/* Don't drag elements: Start */
	$('*').each(function (){
		$(this).bind('dragstart', function(e) {
			if (window.event) event.preventDefault();
				e.cancelBubble = true; return false;
			});
	});
	/* Don't drag elements: End */

	/* Hide input's placeholders onfocus: Start */
	document.body.onclick = function (e) {
		if (e.target.hasAttribute('placeholder')) {
			var ph = e.target.placeholder;
			if ( e.target.placeholder != '' ) {
				e.target.removeAttribute('placeholder');
				e.target.addEventListener('focusout', function () { this.placeholder = ph; } );
			}
		}
	};
	/* Hide input's placeholders onfocus: End */

});

/* GoogleMap: Start */
var map;

function initMap() {
	map = new google.maps.Map(document.getElementById('map'), {
		center: {lat: 55.1293551, lng: 61.4354874},
		zoom: 13,
		scrollwheel: false,
	});

	// Create a marker and set its position.
  var marker = new google.maps.Marker({
  	map: map,
  	position: {lat: 55.1293551, lng: 61.4354874},
  	title: 'Челябинская область Аргаяшский район, посёлок Увильды (Корпус №2, этаж 4, кабинет 431)',
 	icon: {
		url: "/static/img/map-marker.png",
		scaledSize: new google.maps.Size(125, 100	)
	}
  });



}



/* GoogleMap: End */