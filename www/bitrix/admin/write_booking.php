<?php 

require '../../config.php';
require '../../cms.php';

$string = file_get_contents("1cbitrix-82f9ea63-46bb-4274-8977-20dad074bdfe.json");
$json = json_decode($string, true);

$table = new Table('catalog_section');

foreach( $json['Номера'] as $number ) {

	$start_date = date('Y-m-d', strtotime($number['ДатаНачалаБронирования']));
	$end_date = date('Y-m-d', strtotime($number['ДатаОкончанияБронирования']));

	$table -> execute('INSERT INTO `section_booking` (`room_title`, `start_date`, `end_date`) VALUES (:room_title, :start_date, :end_date)', 
		array( 'room_title' => $number['Номер'], 'start_date' => $start_date, 'end_date' => $end_date ));

}

