<?php 


require '../../config.php';
require '../../cms.php';

$dir = 'temp/';

$exchange_id = md5( time( ) );


if(!empty($_SERVER['HTTP_USER_AGENT'])){
    session_name(md5($_SERVER['HTTP_USER_AGENT']));
}

session_start();

if($_GET['type'] == 'catalog' && $_GET['mode'] == 'checkauth') {

    // $fo = fopen( '11111.txt', 'a+' );
    // $a = 1;
    // fwrite( $fo, $a );
    // fclose( $fo );

    print "success\n";
    print session_name()."\n";
    print session_id();
}

if( $_GET['type'] == 'catalog' && $_GET['mode'] == 'init') {

// Удаляем все загруженные ранее файлы перед выполнением обмена
//     $files = glob(__DIR__ . '/' . $dir . '*');
//     foreach($files as $file) { 
//         if(is_file($file)) {
//			unlink($file);
//		 }
//    }

    print "zip=no\n";
    print "file_limit=2000000\n";
}

if( $_GET['type'] == 'catalog' && $_GET['mode'] == 'import') {
    print "success\n";
}

if( $_GET['type'] == 'catalog' && $_GET['mode'] == 'file') {
    $filename = $_GET['filename'];

    if( strpos( $filename, 'Reports' ) !== false ) {

        upload_data();

        $files = glob(__DIR__ . '/' . $dir . '*');
        foreach($files as $file) { 
            if(is_file($file))
            {
				unlink($file);
			}
        }
    }

    if( strpos( $filename, '.xml' ) !== false ) {
        $f = fopen($dir.$filename, 'ab');
        fwrite($f, file_get_contents('php://input'));
        fclose($f);
    }


    print "success\n";
}


// Б Р О Н И Р О В А Н И Е   Н О М Е Р А

// Начало сеанса
if ( $_GET[ 'type' ] == 'sale' && $_GET[ 'mode' ] == 'checkauth' ) {
	print "success\n";
    print session_name()."\n";
    print session_id();
}

// Уточнение параметров сеанса
if ( $_GET[ 'type' ] == 'sale' && $_GET[ 'mode' ] == 'init' ) {
	print "zip=no\n";
	print "file_limit=10485760\n";
}

// Получение файла обмена с сайта
if ( $_GET[ 'type' ] == 'sale' && $_GET[ 'mode' ] == 'query' ) {

	$exchange_id = md5( time( ) );
	
	$table = new Table( 'position_orders' );
	$table -> execute( 'UPDATE `position_orders` SET `exchange_id`=:exchange_id', array( 'exchange_id' => $exchange_id ) );

	$orders = $table -> select( 'SELECT * FROM `position_orders` WHERE `in1c`=0 ORDER BY `date_create`' );
	if ( count( $orders ) ) {
	
		$array = array( );
		$i = 0;
		foreach ( $orders as $order ) {
			$array[ $i ][ 'ИдВыгрузки' ] = $exchange_id;
			$array[ $i ][ 'Ид' ] = $order[ 'id' ];
			$array[ $i ][ 'Период' ] = $order[ 'period' ];
			$array[ $i ][ 'Программа' ] = $order[ 'programm' ];
			$array[ $i ][ 'ВидПитания' ] = $order[ 'client_food' ];
			$array[ $i ][ 'Номер' ][ 'Ид' ] = $order[ 'num_1cid' ];
			$array[ $i ][ 'Номер' ][ 'Тип' ] = $order[ 'num_type' ];
			//$array[ $i ][ 'Номер' ][ 'КоличествоКомнат' ] = $order[ 'num_rooms' ];
			//$array[ $i ][ 'Номер' ][ 'КоличествоОсновныхМест' ] = $order[ 'num_place' ];
			//$array[ $i ][ 'Номер' ][ 'КоличествоДополнительныхМест' ] = $order[ 'num_dopplace' ];
			$array[ $i ][ 'Номер' ][ 'Наименование' ] = $order[ 'num_title' ];
			$array[ $i ][ 'Номер' ][ 'СтоимостьОдногоДня' ] = $order[ 'price' ];
			$array[ $i ][ 'Номер' ][ 'Сумма' ] = $order[ 'summ' ];
			$array[ $i ][ 'Номер' ][ 'Подселение' ] = $order[ 'sharing' ];

/*
			$clients = unserialize( $order[ 'clients' ] );
			foreach ( $clients as $key => $client ) {
				$array[ 'Клиенты' ][ 'Клиент' ][] = $client;
			}
*/

			$array[ $i ][ 'Подробности' ][ 'Наименование' ] = $order[ 'client_name' ];
			$array[ $i ][ 'Подробности' ][ 'Почта' ] = $order[ 'client_email' ];
			$array[ $i ][ 'Подробности' ][ 'Телефон' ] = $order[ 'client_phone' ];
			$array[ $i ][ 'Подробности' ][ 'Комментарий' ] = $order[ 'client_comm' ];
			++$i;

		}


		//creating object of SimpleXMLElement
		$xml_user_info = new SimpleXMLElement("<?xml version=\"1.0\"?><Заказы></Заказы>");

		//function call to convert array to xml
		$xml_file = array_to_xml( $array, $xml_user_info );

		$xml_file = $xml_user_info -> asXML();

		header( 'Content-type: text/xml' );
		echo  $xml_file;
	}
}

// Б Р О Н И Р О В А Н И Е   Н О М Е Р А   ( данные закружены в 1С )
if ( $_GET[ 'type' ] == 'sale' && $_GET[ 'mode' ] == 'success'  ) {

	$table = new Table( 'position_orders' );

	$table -> execute( 'UPDATE `position_orders` SET `in1c`=1 WHERE `exchange_id`=:exchange_id',
		array( 'exchange_id' => $_GET[ 'exchange_id' ] ) );

	print "success\n";
}

// Б Р О Н И Р О В А Н И Е   Н О М Е Р А   ( json-файлик )
if ( $_GET[ 'type' ] == 'sale' && $_GET[ 'mode' ] == 'file'  ) {
    $filename = $_GET['filename'];

    if( (strpos( $filename, '1cbitrix' ) !== false) && (strpos( $filename, '.json' ) !== false) ) {

        $booking_dir = 'booking/';
        $new_file_name = date("Y-m-d-h-i-s");

        if (!file_exists($booking_dir.$new_file_name)) {

            $f = fopen($booking_dir.$new_file_name.'.json', 'ab');

            $string = file_get_contents('php://input');

        // Создаем файл
            fwrite($f, $string);
            fclose($f);

        // Записываем его в архив
            $zip = new ZipArchive();
            $zipfilename = $booking_dir.$new_file_name.'.zip';

            if ( $zip->open($zipfilename, ZipArchive::CREATE) !== TRUE ) {
                exit( "Невозможно открыть <$zipfilename>\n" );
            }

            $zip->addFile( $booking_dir.$new_file_name.'.json', $new_file_name.'.json' );
            $zip->close();

        // Удаляем файл
            if(is_file($booking_dir.$new_file_name.'.json')) {
                //unlink($booking_dir.$new_file_name.'.json');
			}
            $json = json_decode($string, true);

            $table = new Table('catalog_section');

            $table->execute('TRUNCATE `position_booking_rooms`');

            foreach( $json['Номера'] as $number ) {

                $start_date = strtotime(date('Y-m-d', strtotime($number['ДатаНачалаБронирования'])));
                $end_date = strtotime(date('Y-m-d', strtotime($number['ДатаОкончанияБронирования'])));

                // $title = str_replace(' ', '', $number['Номер']);

                $table -> execute('INSERT INTO `position_booking_rooms` (`section_id`, `start_date`, `end_date`) 
                    VALUES ((SELECT `section_rooms`.`id` FROM `section_rooms` WHERE `section_rooms`.`id_1c`=:id LIMIT 1), :start_date, :end_date)', 
                    array( 'id' => $number['ИД'], 'start_date' => $start_date, 'end_date' => $end_date ));

            }
        }

    }

    print "success\n";
}


//function defination to convert array to xml
function array_to_xml($array, &$xml_user_info) {
    foreach($array as $key => $value) {
        if(is_array($value)) {
            if(!is_numeric($key)){
                $subnode = $xml_user_info->addChild("$key");
                array_to_xml($value, $subnode);
            }else{
                $subnode = $xml_user_info->addChild("Заказ");
                array_to_xml($value, $subnode);
            }
        }else {
            $xml_user_info->addChild("$key",htmlspecialchars("$value"));
        }
    }
}


function upload_data() {

    $dir = 'temp/';

    $properties = array();
    $items = array();
    $groups = array();
    $prices = array();

    $procedures_price = array();
    $procedures = array();

    if ($handle = opendir( __DIR__ . '/' . $dir)) {
        while (false !== ($entry = readdir($handle))) {
            if ($entry != "." && $entry != "..") {

                $file_name = $entry;

                if ( strpos( $file_name, '.xml' ) === false ) {
                    continue;
                }

                $xml = simplexml_load_file( __DIR__ . '/' . $dir . $file_name );

				//throw new Exception( var_export( $xml, true ) );
				
				
                if( property_exists($xml->Классификатор, 'Свойства') && ( (string) $xml->Классификатор->Наименование == 'Основной каталог товаров' ) ) {
                    $properties = $xml->Классификатор->Свойства->Свойство;
                }

                if( property_exists($xml, 'Каталог') ) {
                    if( property_exists($xml->Каталог, 'Товары') && ( (string) $xml->Каталог->Наименование == 'Основной каталог товаров' )) {
                        $items = $xml->Каталог->Товары->Товар;
                    }
                }

                if( property_exists($xml, 'Классификатор') && property_exists($xml->Классификатор, 'Группы') && ( (string) $xml->Каталог->Наименование == 'Основной каталог товаров' ) ) {
                    $groups = $xml->Классификатор->Группы->Группа;
                }

                if( (strpos( $file_name, 'prices' ) !== false) && property_exists($xml, 'ПакетПредложений') 
                    && property_exists($xml->ПакетПредложений, 'Предложения') && ( (string) $xml->ПакетПредложений->Наименование == 'Пакет предложений (Основной каталог товаров)' ) ) {
                    $prices = $xml->ПакетПредложений->Предложения->Предложение;
                }

                if( property_exists($xml, 'Каталог') ) {
                    if( property_exists($xml->Каталог, 'Товары') && ( (string) $xml->Каталог->Наименование == 'Для амбулаторных больных' )) {
                        $procedures = $xml->Каталог->Товары->Товар;
                    }
                }

                if( (strpos( $file_name, 'prices1' ) !== false) && property_exists($xml, 'ПакетПредложений') 
                    && property_exists($xml->ПакетПредложений, 'Предложения') && ( (string) $xml->ПакетПредложений->Наименование == 'Пакет предложений (Для амбулаторных больных)' ) ) {
                    $procedures_price = $xml->ПакетПредложений->Предложения->Предложение;
                }

                if( property_exists($xml, 'Классификатор') && property_exists($xml->Классификатор, 'Группы') && ( (string) $xml->Каталог->Наименование == 'Для амбулаторных больных' ) ) {
                    $procedures_group = $xml->Классификатор->Группы->Группа;
                }

            }
        }
        closedir($handle);
    }

    //$xml = simplexml_load_file( $file );

    if( !empty( $groups ) ) {
        upload_groups($groups, 806);
        upload_groups($groups, 807);
        upload_groups($groups, 1147, false);
    }

    if( !empty( $items ) && !empty( $properties ) ) {
        set_numbers($items, $properties);
    }

    if( !empty( $items ) && !empty( $properties ) && !empty( $prices ) ) {
        set_offers($items, $properties, $prices);
    }

    if( !empty( $procedures_group ) ) {
        set_procedures_group($procedures_group);
    }

    if( !empty( $procedures ) && !empty( $procedures_price ) ) {
        set_procedures_price($procedures, $procedures_price);
    }

}

function set_numbers( $items, $properties ) {

    $dir = 'temp/';

    $table = new Table('catalog_section');

    foreach( $items as $item ) {

        $number_type = $table->select('SELECT `section_number_class`.`id` FROM `section_number_class` INNER JOIN `catalog_section` 
                    ON (`section_number_class`.`id`=`catalog_section`.`id`) WHERE `section_number_class`.`id_1c`=:id_1c AND `catalog_section`.`parent_id`=806 LIMIT 1', array( 'id_1c' => (string) $item->Группы->Ид ));

        if( empty($number_type) ) continue;

        $number_type = end( $number_type );

        $is_deleted = false;

        switch( strtolower($item->ПометкаУдаления) ) {
            case 'true': 
                $is_deleted = true;
                break;
            case 'false': 
                $is_deleted = false;
                break;
        }

        $prop_array = array();

        // Количество комнат
        foreach( $item->ЗначенияСвойств->ЗначенияСвойства as $value ) {
            if( $value->Ид == 'b2cf492e-8b90-11e4-9206-a938ec62ee95' ) {
                $prop_array[(string) $value->Ид] = (string) $value->Значение;
                break;
            }
        }           

        // Количество основных мест
        foreach( $item->ЗначенияСвойств->ЗначенияСвойства as $value ) {
            if( $value->Ид == '579d22d4-8378-11e4-9206-a938ec62ee95' ) {
                $prop_array[(string) $value->Ид] = (string) $value->Значение;
                break;
            }
        }

        // Количество доп. мест
        foreach( $item->ЗначенияСвойств->ЗначенияСвойства as $value ) {
            if( $value->Ид == '579d22d9-8378-11e4-9206-a938ec62ee95' ) {
                $prop_array[(string) $value->Ид] = (string) $value->Значение;
                break;
            }
        }

        $prop_array = get_room_properties( $prop_array, $properties );

        // $title = str_replace(' ', '', (string) $item->Наименование);

        $is_exist_number = $table->select('SELECT `id` FROM `section_rooms` WHERE `id_1c`=:id_1c LIMIT 1', array( 'id_1c' => $item->Ид )); 
        if( empty($is_exist_number) ) {

            if( $is_deleted ) continue;

            $table = new Table('catalog_section');
            $obj = new stdClass();
            $obj -> title = $item->Наименование;
            $obj -> parent_id = $number_type['id'];
            $obj -> section_table = 'section_rooms';
            $obj -> position_table = 'position_booking_rooms';
            $obj -> alias = Utils::getUniqueAlias( Utils::translit( (string) $item->Наименование ), 'catalog_section' );
            $id = $table -> save($obj);

            $table->execute('INSERT INTO `section_rooms` (`id`, `id_1c`, `n_rooms`, `n_main_place`, `n_additional_place`) VALUES (:id, :id_1c, 
                :n_rooms, :n_main_place, :n_additional_place)', 
                array( 
                    'id' => $id, 'id_1c' => $item->Ид, 
                    'n_rooms' => isset($prop_array['b2cf492e-8b90-11e4-9206-a938ec62ee95']) ? $prop_array['b2cf492e-8b90-11e4-9206-a938ec62ee95'] : null,
                    'n_main_place' => isset($prop_array['579d22d4-8378-11e4-9206-a938ec62ee95']) ? $prop_array['579d22d4-8378-11e4-9206-a938ec62ee95'] : null, 
                    'n_additional_place' => isset($prop_array['579d22d9-8378-11e4-9206-a938ec62ee95']) ? $prop_array['579d22d9-8378-11e4-9206-a938ec62ee95'] : null
                )
            );

        }
        else {

            if( $is_deleted ) {
                $table->execute('DELETE FROM `catalog_section` WHERE `id`=:id', array( 'id' => $is_exist_number[0]['id'] ));
                $table->execute('DELETE FROM `section_rooms` WHERE `id`=:id', array( 'id' => $is_exist_number[0]['id'] ));
            }
            else {

                $table->execute('UPDATE `catalog_section` SET `title`=:title WHERE `id`=:id', array( 'title' => $item->Наименование, 'id' => $is_exist_number[0]['id'] ));

                $table->execute('UPDATE `section_rooms` SET `n_rooms`=:n_rooms, `n_main_place`=:n_main_place, `n_additional_place`=:n_additional_place WHERE `id`=:id', 
                    array( 
                        'id' => $is_exist_number[0]['id'], 
                        'n_rooms' => isset($prop_array['b2cf492e-8b90-11e4-9206-a938ec62ee95']) ? $prop_array['b2cf492e-8b90-11e4-9206-a938ec62ee95'] : null,
                        'n_main_place' => isset($prop_array['579d22d4-8378-11e4-9206-a938ec62ee95']) ? $prop_array['579d22d4-8378-11e4-9206-a938ec62ee95'] : null, 
                        'n_additional_place' => isset($prop_array['579d22d9-8378-11e4-9206-a938ec62ee95']) ? $prop_array['579d22d9-8378-11e4-9206-a938ec62ee95'] : null
                    )
                );
            }

        }
    }
}


function set_offers( $items, $properties, $prices ) {

    $dir = 'temp/';

    $table = new Table('catalog_section');

    $table->execute('TRUNCATE `section_number_prices`');

    foreach( $prices as $price ) {

        foreach( $items as $item ) {
            
            if( (string) $price->Ид == (string) $item->Ид ) {

                $number_type = $table->select('SELECT `section_number_class`.`id` FROM `section_number_class` INNER JOIN `catalog_section` 
                	ON (`section_number_class`.`id`=`catalog_section`.`id`) WHERE `section_number_class`.`id_1c`=:id_1c AND `catalog_section`.`parent_id`=807 LIMIT 1', array( 'id_1c' => (string) $item->Группы->Ид ));

                if( empty($number_type) ) break;

                $number_type = end( $number_type );

                $is_deleted = false;

                switch( strtolower($item->ПометкаУдаления) ) {
                    case 'true': 
                        $is_deleted = true;
                        break;
                    case 'false': 
                        $is_deleted = false;
                        break;
                }

                $prop_array = array();

                // Количество комнат
                foreach( $item->ЗначенияСвойств->ЗначенияСвойства as $value ) {
                    if( $value->Ид == 'b2cf492e-8b90-11e4-9206-a938ec62ee95' ) {
                        $prop_array[(string) $value->Ид] = (string) $value->Значение;
                        break;
                    }
                }           

                // Количество основных мест
                foreach( $item->ЗначенияСвойств->ЗначенияСвойства as $value ) {
                    if( $value->Ид == '579d22d4-8378-11e4-9206-a938ec62ee95' ) {
                        $prop_array[(string) $value->Ид] = (string) $value->Значение;
                        break;
                    }
                }

                // Количество доп. мест
                foreach( $item->ЗначенияСвойств->ЗначенияСвойства as $value ) {
                    if( $value->Ид == '579d22d9-8378-11e4-9206-a938ec62ee95' ) {
                        $prop_array[(string) $value->Ид] = (string) $value->Значение;
                        break;
                    }
                }

                $prop_array = get_room_properties( $prop_array, $properties );

                if( !isset( $prop_array['b2cf492e-8b90-11e4-9206-a938ec62ee95'] ) ) {
                    $prop_array['b2cf492e-8b90-11e4-9206-a938ec62ee95'] = 0;
                }

                $title = $prop_array['b2cf492e-8b90-11e4-9206-a938ec62ee95'] . '-комнатный, осн. мест: ' . $prop_array['579d22d4-8378-11e4-9206-a938ec62ee95'] . ', доп. мест: ' . $prop_array['579d22d9-8378-11e4-9206-a938ec62ee95'];

                $is_exist_number = $table->select('SELECT `id` FROM `section_offers` WHERE `id_1c`=:id_1c LIMIT 1', array( 'id_1c' => $item->Ид )); 
                if( empty($is_exist_number) ) {

                    if( $is_deleted ) continue;

                    $table = new Table('catalog_section');
                    $obj = new stdClass();
                    $obj -> title = $title;
                    $obj -> parent_id = $number_type['id'];
                    $obj -> section_table = 'section_offers';
                    $obj -> position_table = 'position_gallery';
                    $obj -> alias = Utils::getUniqueAlias( Utils::translit( (string) $title ), 'catalog_section' );
                    $id = $table -> save($obj);

                    $table->execute('INSERT INTO `section_offers` (`id`, `id_1c`, `n_rooms`, `n_main_place`, `n_additional_place`) VALUES (:id, :id_1c, 
                        :n_rooms, :n_main_place, :n_additional_place)', 
                        array( 
                            'id' => $id, 'id_1c' => $item->Ид, 
                            'n_rooms' => isset($prop_array['b2cf492e-8b90-11e4-9206-a938ec62ee95']) ? $prop_array['b2cf492e-8b90-11e4-9206-a938ec62ee95'] : null,
                            'n_main_place' => isset($prop_array['579d22d4-8378-11e4-9206-a938ec62ee95']) ? $prop_array['579d22d4-8378-11e4-9206-a938ec62ee95'] : null, 
                            'n_additional_place' => isset($prop_array['579d22d9-8378-11e4-9206-a938ec62ee95']) ? $prop_array['579d22d9-8378-11e4-9206-a938ec62ee95'] : null
                        )
                    );
                }
                else {

                    if( $is_deleted ) {
                        $table->execute('DELETE FROM `catalog_section` WHERE `id`=:id', array( 'id' => $is_exist_number[0]['id'] ));
                        $table->execute('DELETE FROM `section_offers` WHERE `id`=:id', array( 'id' => $is_exist_number[0]['id'] ));
                    }
                    else {

                        $table->execute('UPDATE `catalog_section` SET `title`=:title WHERE `id`=:id', array( 'title' => $title, 'id' => $is_exist_number[0]['id'] ));

                        $table->execute('UPDATE `section_offers` SET `n_rooms`=:n_rooms, `n_main_place`=:n_main_place, `n_additional_place`=:n_additional_place WHERE `id`=:id', 
                            array( 
                                'id' => $is_exist_number[0]['id'], 
                                'n_rooms' => isset($prop_array['b2cf492e-8b90-11e4-9206-a938ec62ee95']) ? $prop_array['b2cf492e-8b90-11e4-9206-a938ec62ee95'] : null,
                                'n_main_place' => isset($prop_array['579d22d4-8378-11e4-9206-a938ec62ee95']) ? $prop_array['579d22d4-8378-11e4-9206-a938ec62ee95'] : null, 
                                'n_additional_place' => isset($prop_array['579d22d9-8378-11e4-9206-a938ec62ee95']) ? $prop_array['579d22d9-8378-11e4-9206-a938ec62ee95'] : null
                            )
                        );
                    }
                }

                $ages = array();

                foreach( $properties as $property ) {
                    if( (string) $property->Ид == '81c13e6c-8394-11e4-9206-a938ec62ee95' ) {

                        $table->execute('TRUNCATE `section_ages`');

                        foreach( $property->ВариантыЗначений->Справочник as $value ) {

                            $ages[(string) $value->Значение] = (string) $value->ИдЗначения;

                            $table->execute('INSERT INTO `section_ages` (`title`, `id_1c`) VALUES (:title, :id_1c)', 
                            array( 
                                'title' => (string) $value->Значение,
                                'id_1c' => (string) $value->ИдЗначения
                            ));

                        }


                        break;
                    }
                }

                foreach( $price->Цены->Цена as $p ) {

					$dates = explode( '-', (string) $p->Период );

					$dates[0] = date('Y-m-d', strtotime($dates[0]));
					$dates[1] = date('Y-m-d', strtotime($dates[1]));

                    $programm = '';

                    $vl = (string) $p->ВЛ;
                    if( !empty($vl) ) {
                        $programm = mb_strtolower($vl, 'UTF-8');
                    }
                    else {
                        $programm = 'без лечения';
                    }

                    if( $programm == 'эрл люкс' ) {
                        $programm = 'люкс программа очищение организма';
                    }

                    if( $programm == 'эрл президентский' ) {
                        $programm = 'президентская программа очищение организма';
                    }

                    if( $programm == 'эрл стандарт' ) {
                        $programm = 'стандартная программа очищение организма';
                    }

					if ( $programm == 'detox-intensiv') {
                        $programm = 'детокс интенсив';
					}
					if ( $programm == 'detox-light' ) {
                        $programm = 'детокс лайт';
					}
					
					
                    if( (strpos($dates[0], '12-31') !== false ) && (strpos($dates[1], '01-01') !== false ) ) {
                        $programm = 'новогодняя программа';
                    }

                    $age_id = $ages[(string) $p->КЛ ];

					$table->execute('INSERT INTO `section_number_prices` (`offers_id_1c`, `start_date`, `end_date`, `age`, `age_id_1c`, `food_type`, `place_type`, `price`, `programm`) VALUES (:offers_id_1c, :start_date, :end_date, :age, :age_id_1c, :food_type, :place_type, :price, :programm)', 
						array( 
							'offers_id_1c' => $price->Ид,
							'start_date' => $dates[0],
							'end_date' => $dates[1],
							'age' => $p->КЛ,
                            'age_id_1c' => $age_id,
                            'food_type' => $p->ВП,
							'place_type' => $p->ВМ,
							'price' => $p->Цена,
                            'programm' => $programm
						));

				}

                break;
            }
        }
    }

}

function get_room_properties($key_value, $properties) {

    $property_arr = array();

    foreach( $key_value as $key => $value ) {
        foreach( $properties as $property ) {
            if( (string) $property->Ид == $key ) {
                foreach( $property->ВариантыЗначений->Справочник as $property_val ) {
                    if( (string) $property_val->ИдЗначения == $value ) {
                        $property_arr[$key] = (string) $property_val->Значение;
                        break;
                    }
                }
                break;
            }
        }
    }

    return $property_arr;
}

function upload_groups($number_types, $parent_id, $remove_exist=true) {
    $dir = 'temp/';

    $table = new Table('catalog_section');

    // Выгрузка и обновление типов номеров
    foreach( $number_types as $number_type ) {

        $is_exist = $table->select('SELECT `section_number_class`.`id` FROM `section_number_class` INNER JOIN `catalog_section` 
        	ON (`section_number_class`.`id`=`catalog_section`.`id`) WHERE `section_number_class`.`id_1c`=:id_1c 
        	AND `catalog_section`.`parent_id`='. $parent_id .' LIMIT 1', array( 'id_1c' => $number_type->Ид )); 

        $title = str_replace('_', ' ', (string) $number_type->Наименование);

        $is_deleted = false;
        switch( strtolower($number_type->ПометкаУдаления) ) {
            case 'true': 
                $is_deleted = true;
                break;
            case 'false': 
                $is_deleted = false;
                break;
        }

        if( empty( $is_exist ) ) {

            if( $is_deleted ) continue;

            $obj = new stdClass();
            $obj -> title = $title;
            $obj -> parent_id = $parent_id;
            $obj -> section_table = 'section_number_class';
            $obj -> alias = Utils::getUniqueAlias( Utils::translit( (string) $title ), 'catalog_section' );
            $id = $table -> save($obj);

            $table->execute('INSERT INTO `section_number_class` (`id`, `id_1c`) VALUES (:id, :id_1c)', array( 'id' => $id, 'id_1c' => $number_type->Ид ));
        }
        else {
            if($is_deleted) {
            	if($remove_exist) {
	                $table->execute('DELETE FROM `catalog_section` WHERE `id`=:id', array( 'id' => $is_exist[0]['id'] ));
	                $table->execute('DELETE FROM `section_number_class` WHERE `id`=:id', array( 'id' => $is_exist[0]['id'] ));
	            }
            }
            else {
                $table->execute('UPDATE `catalog_section` SET `title`=:title WHERE `id`=:id', array( 'title' => $title, 'id' => $is_exist[0]['id'] ));
            }
        }
    }
}

function set_procedures_group($groups) {

    $table = new Table('catalog_section');

    foreach( $groups as $group ) {

        $is_exist = $table->select('SELECT `section_cabinets`.`id` FROM `section_cabinets` INNER JOIN `catalog_section` 
            ON (`section_cabinets`.`id`=`catalog_section`.`id`) WHERE `section_cabinets`.`id_1c`=:id_1c LIMIT 1', array( 'id_1c' => $group->Ид )); 

        $is_deleted = false;
        switch( strtolower($group->ПометкаУдаления) ) {
            case 'true': 
                $is_deleted = true;
                break;
            case 'false': 
                $is_deleted = false;
                break;
        }

        if( empty( $is_exist ) ) {

            if( $is_deleted ) continue;

            $obj = new stdClass();
            $obj -> title = (string) $group->Наименование;
            $obj -> parent_id = 432;
            $obj -> section_table = 'section_cabinets';
            $obj -> position_table = 'position_cabinets';
            $obj -> alias = Utils::getUniqueAlias( Utils::translit( (string) $group->Наименование ), 'catalog_section' );
            $id = $table -> save($obj);

            $table->execute('INSERT INTO `section_cabinets` (`id`, `id_1c`, `public`) VALUES (:id, :id_1c, :public)', array( 'id' => $id, 'id_1c' => $group->Ид, 'public' => 1 ));
        }
        else {
            if($is_deleted) {
                $table->execute('DELETE FROM `catalog_section` WHERE `id`=:id', array( 'id' => $is_exist[0]['id'] ));
                $table->execute('DELETE FROM `section_cabinets` WHERE `id`=:id', array( 'id' => $is_exist[0]['id'] ));
            }
            else {
                $table->execute('UPDATE `catalog_section` SET `title`=:title, `position_table`="position_cabinets" WHERE `id`=:id', array( 'title' => (string) $group->Наименование, 'id' => $is_exist[0]['id'] ));
            }
        }
    }
}

function set_procedures_price($items, $prices) {

    $table = new Table('catalog_section');

    foreach( $prices as $price ) {

        foreach( $items as $item ) {
            if( (string) $price->Ид == (string) $item->Ид ) {

                $is_exist = $table->select('SELECT `position_cabinets`.`id` FROM `position_cabinets` WHERE `position_cabinets`.`id_1c`=:id_1c LIMIT 1', array( 'id_1c' => (string) $item->Ид )); 

                $is_deleted = false;
                switch( strtolower($item->ПометкаУдаления) ) {
                    case 'true': 
                        $is_deleted = true;
                        break;
                    case 'false': 
                        $is_deleted = false;
                        break;
                }

                $section_id = $table->select('SELECT `section_cabinets`.`id` FROM `section_cabinets` WHERE `section_cabinets`.`id_1c`=:id_1c LIMIT 1', array( 'id_1c' => (string) $item->Группы->Ид ));

                if( empty( $section_id ) ) continue;

                $section_id = end($section_id);

                if( empty( $is_exist ) ) {

                    if( $is_deleted ) continue;

                    $table->execute('INSERT INTO `position_cabinets` (`section_id`, `id_1c`, `title`, `view`, `price`, `public`) VALUES (:section_id, :id_1c, :title, :view, :price, :public)', 
                    array( 
                        'section_id' => $section_id['id'], 
                        'id_1c' => $item->Ид, 
                        'title' => $item->Наименование,
                        'view' => $price->Цены->Цена->Представление,
                        'price' => $price->Цены->Цена->ЦенаЗаЕдиницу,
                        'public' => 1 ));
                }
                else {
                    if($is_deleted) {
                        $table->execute('DELETE FROM `position_cabinets` WHERE `id`=:id', array( 'id' => $is_exist[0]['id'] ));
                    }
                    else {
                        $table->execute('UPDATE `position_cabinets` SET `section_id`=:section_id, `id_1c`=:id_1c, `title`=:title, `view`=:view, `price`=:price,  WHERE `id`=:id', array( 'title' => $item->Наименование, 'section_id' => $section_id['id'], 'id_1c' => $item->Ид, 'view' => $price->Цены->Цена->Представление, 'price' => $price->Цены->Цена->ЦенаЗаЕдиницу, 'id' => $is_exist[0]['id'] ));
                    }
                }

                break;
            }
        }

    }

}