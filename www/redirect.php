<?php

	$redirect_301 = array(

		'/publication/sapropelevaya-gryaz-kurorta.html' => '/publications/sapropelevaya-gryaz-kurorta.html',
		'/publication/uvildinskaya-radonovaya-voda.html' => '/publications/uvildinskaya-radonovaya-voda.html',
		'/publication/novosti-o-lechenii-i-reabilitacii-vzroslyh-i-detej-s-nevrozami.html' => '/publications/novosti-o-lechenii-i-reabilitacii-vzroslyh-i-detej-s-nevrozami.html',
		'/publication/valeriano-bromnye-vanny.html' => '/publications/valeriano-bromnye-vanny.html',
		'/publication/peptidi-v-medicine.html' => '/publications/peptidi-v-medicine.html',
		'/akcii-i-novosti/akciya-goryacshaya-putevka.html' => '/discounts/akciya-goryacshaya-putevka.html',
		'/akcii-i-novosti/8marta--eto-simvol-zhenskoj-krasoty-lyubvi-i-nezhnosti.html' => '/news/8marta--eto-simvol-zhenskoj-krasoty-lyubvi-i-nezhnosti.html',
		'/akcii-i-novosti/akciya-uznaj-kurort-uvildy-za-2500-rublej-prodolzhaetsya.html' => '/discounts/akciya-uznaj-kurort-uvildy-za-2500-rublej-prodolzhaetsya.html',
		'/akcii-i-novosti/maslenica.html' => '/news/maslenica.html',
		'/akcii-i-novosti/23-fevralya-uzhe-ne-za-gorami.html' => '/news/23-fevralya-uzhe-ne-za-gorami.html',
		'/akcii-i-novosti/uspejte-priobresti-putevki-po-samoj-vygodnoj-cene.html' => '/publications/uspejte-priobresti-putevki-po-samoj-vygodnoj-cene.html',
		'/akcii-i-novosti/st-valentines-day.html' => '/news/st-valentines-day.html',
		'/akcii-i-novosti/peptidi-lechenie-profilactika.html' => '/news/peptidi-lechenie-profilactika.html',
		'/akcii-i-novosti/nash-kurort-postoyanno-razvivaetsya-vnimatelno-sledit-i-ispolzuet-novejshie-i-effektivnye-dostizheniya-v-medicine.html' => '/news/nash-kurort-postoyanno-razvivaetsya-vnimatelno-sledit-i-ispolzuet-novejshie-i-effektivnye-dostizheniya-v-medicine.html',
		'/akcii-i-novosti/romanticheskoe-svidanie.html' => '/discounts/romanticheskoe-svidanie.html',
		'/akcii-i-novosti/akciya-uznaj-kurort-uvildy-prodolzhaetsya2.html' => '/discounts/akciya-uznaj-kurort-uvildy-prodolzhaetsya2.html',
		'/akcii-i-novosti/otdyh-po-putevkam-vyhodnogo-dnya.html' => '/discounts/otdyh-po-putevkam-vyhodnogo-dnya.html',
		'/akcii-i-novosti/programma-kurortnyj-detoks.html' => '/news/programma-kurortnyj-detoks.html',
		'/akcii-i-novosti/biznes-predlozheniya-turisticheskim-firmam.html' => '/news/biznes-predlozheniya-turisticheskim-firmam.html',
		'/akcii-i-novosti/novyj-god-otprazdnovali-skoro-leto.html' => '/news/novyj-god-otprazdnovali-skoro-leto.html',

		'/akcii-i-novosti/' => '/discounts.html',
		'/akcii-i-novosti.html' => '/discounts.html',

		'/order/' => '/bronirovanie.html',
		'/rooms/' => '/bronirovanie.html',
		'/feeding/' => '/vsya-infrastruktura.html',
		'/infrastructure/' => '/vsya-infrastruktura.html',
		'/corporate/' => '/sposoby-oplaty.html',
		'/register/' => '/sposoby-oplaty.html',
		'/info/feedback/' => '/otzyvy.html',
		'/resort/transfer/' => '/kontakty.html',
		'/resort/contacts/' => '/kontakty.html',
		'/resort/gallery/' => '/fotogalereya/',
		'/resort/job/' => '/o-nas.html',
		'/resort/' => '/o-nas.html',
		'/brain_clinic/' => '/programmy/klinika-mozga.html',
		'/natural_healing_center/' => '/programmy/standartnaya-programma-ochicshenie-organizma.html',
		'/cardiology/' => '/programmy/obcshee-ozdorovlenie.html',
		'/gynecology/' => '/programmy/ginekologiya.html',
		'/therapy/' => '/programmy/obcshee-ozdorovlenie.html',


		// ��� �����, �� ����� ������
		'/pulmonology/' => '/programmy.html',
		'/gastroenterology/' => '/programmy.html',
		'/revmatology/' => '/programmy.html',
	
	);


	$urlUser = ( isset( $_SERVER[ 'REQUEST_URI' ] ) ) ? $_SERVER[ 'REQUEST_URI' ] : '';

	foreach( $redirect_301 as $req => $target ) {

		if ( strpos( $urlUser, $req ) === 0 ) {
			header( 'HTTP/1.1 404 Not Found' ); 
			header( 'HTTP/1.1 301 Moved Permanently' );
			header( 'Location: '.  $target );
			die( '������������� �� <a href="'. $target .'">'. $target .'</a>' );
		}

	}


	