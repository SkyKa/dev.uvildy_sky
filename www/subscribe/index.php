<?php

		session_start( );

		$dirpath = dirname( __FILE__ );
		$dirname = explode( DIRECTORY_SEPARATOR, $dirpath );
		$dirname = end( $dirname );

		$_SESSION[ $dirname ] = true;
		$path = realpath( $dirpath . '/../' ) . DIRECTORY_SEPARATOR . 'logs_' . $dirname . DIRECTORY_SEPARATOR;

		$ya_fo = fopen( $path . date( 'Ym' ) . '.log', 'a+' );
		fwrite( $ya_fo, '--------------------' . "\n", 3000 );
		fwrite( $ya_fo, date( 'm-d-Y H:i:s.' ) . "\n", 3000 );
		fwrite( $ya_fo, var_export( $_SERVER, true ) . "\n", 3000 );
		fwrite( $ya_fo, var_export( $_SESSION[ $dirname ], true ) . "\n", 3000 );
		fwrite( $ya_fo, "\n\n\n", 8500 );
		fclose( $ya_fo );

		// http://www.dev.uvildi/subscribe/programmy/lechenie-besplodiya.html?email=masha@radist.ru
		$new_request_uri = str_replace( '/' . $dirname . '/', '/', $_SERVER[ 'REQUEST_URI' ] );
		/** �� ����� ����� �����, ������� ��������� GET ������� ****/
		$new_request_uri = explode( '?', $new_request_uri );
		$new_request_uri = current( $new_request_uri );
		/** �� ����� ����� ����� ****/
		$target = $_SERVER[ 'REQUEST_SCHEME' ] . '://' . $_SERVER[ 'HTTP_HOST' ] . $new_request_uri;

		header( $_SERVER[ 'SERVER_PROTOCOL' ] . ' 404 Not Found' ); 
		header( $_SERVER[ 'SERVER_PROTOCOL' ] . ' 301 Moved Permanently' );
		header( 'Location: ' . $target );
		die( '������������� �� <a href="'. $target .'">'. $target .'</a>' );
