<?php include "mhtml/templates/header.tpl.php" ?>

<div class="et-container booking booking-form">
	<div class="content-container">
		<h2>Заполните простую форму</h2>
		<form action="">
			<div class="form-group">
				<input type="text" name="name" class="form-control" placeholder="Ваше имя">
			</div>
			<div class="form-group">
				<input type="text" name="email" class="form-control" placeholder="Email">
			</div>
			<div class="form-group phone">
				<input type="text" name="phone" class="form-control" placeholder="Контактный телефон">
			</div>
			<div class="form-group">
				<textarea class="form-control" name="message" placeholder="Ваши пожелания"></textarea>
			</div>
			<div class="basic-button">
				<button type="submit">Забронировать</button>
			</div>
		</form>
	</div>
</div>

<?php include "mhtml/templates/footer.tpl.php" ?>