<?php include "mhtml/templates/header.tpl.php" ?>

<header>
	<div class="et-menu-collapse-btn et-collapsed">
		<div class="et-collapse-line"></div>
		<div class="et-collapse-line"></div>
		<div class="et-collapse-line"></div>
	</div>
	
	<div class="h1-wrap">
		<h1><?php mod('pages.show.title') ?></h1>
	</div>

	<a href="/mobile_static/programms.html" class="back-button">
		<img src="/mobile_static/img/back_button.png" alt="">
	</a>

</header>

<div class="content-wrapper">
	<div class="wrapper">
		<div class="content-inner">

			<div class="et-container page-sheet">
				<div class="content-container">
					<div class="content">
						<?php mod('catalog.action.parse_short_codes') ?>
					</div>
				</div>
			</div>


<?php include "mhtml/templates/footer.tpl.php" ?>
