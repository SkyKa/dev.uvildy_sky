<?php include "mhtml/templates/header.tpl.php" ?>

<header>
	<div class="et-menu-collapse-btn et-collapsed">
		<div class="et-collapse-line"></div>
		<div class="et-collapse-line"></div>
		<div class="et-collapse-line"></div>
	</div>
	
	<div class="h1-wrap">
		<h1><?php mod('pages.show.title') ?></h1>
	</div>

	<a href="" class="back-button">
		<img src="/mobile_static/img/back_button.png" alt="">
	</a>

</header>

<div class="content-wrapper">
	<div class="wrapper">
		<div class="content-inner">

			<div class="et-container contacts-page">
				<div class="content-container">
					<div class="places">
						<div class="item">
							<h2>курорт</h2>
							<p class="address">456890, Челябинская обл., Аргаяшский р-н, пос. Увильды.</p>
							<p class="phones"><a href="tel:+7 (351) 225-14-14">+7 (351) 225-14-14</a>, <a href="tel:+7 (351) 225-14-14">+7 (351) 225-14-14</a>, <a href="tel:+7 (351) 225-14-14">+7 (351) 225-14-14</a>, <a href="tel:+7 (351) 225-14-14">+7 (351) 225-14-14</a></p>
							<p class="email"><a href="mailto:sales_uvildy@uvildy.ru">sales_uvildy@uvildy.ru</a></p>
						</div>
						<div class="item">
							<h2>офис в челябинске</h2>
							<p class="address">454000, г. Челябинск, ул. Сони Кривой, 28.</p>
							<p class="phones"><a href="tel:+7 (351) 225-14-14">+7 (351) 225-14-14</a>, <a href="tel:+7 (351) 225-14-14">+7 (351) 225-14-14</a></p>
						</div>
						<div class="basic-button big">
							<a href="tel:73512251616">позвонить</a>
						</div>
					</div>
				</div>
				<div class="way-to-get-wrap">
					<div class="head">
						<h2>Способы проезда до курорта</h2>
						<p>
							МЦМиР «Курорт Увильды» находится на северо-восточном 
							берегу озера Увильды в Аргаяшском районе Челябинской 
							области, в 90 км к северо-западу от Челябинска.
						</p>
					</div>
					<div class="way-to-get">
						<h2>Как к нам приехать?</h2>
						<!-- <p class="plane" data-travel-type="plane">
							<span class="type">Самолётом</span> — в Челябинск (аэропорт Баландино — 
							<a href="">www.aeroport-74.ru</a>) или Екатеринбург (аэропорт Кольцово — 
							<a href="">www.koltsovo.ru</a>).
						</p>
						<p class="train" data-travel-type="train">
							<span class="type">Поездом</span> — до станции Челябинск-Пассажирский 
							или Ектеринбург-Пассажирский.
						</p>
						<p class="bus" data-travel-type="bus">
							<span class="type">Автобусом</span> — рейс №589 из Екатеринбурга (Южный 
							автовокзал) до Челябинска (автовокзал «Юность»), рейс №670 
							из Челябинска до пос. Увильды.
						</p> -->
						<p class="car selected" data-travel-type="car">
							<a href="" class="type">Автомобилем</a>
						</p>
						<p class="walking" data-travel-type="walking">
							<a href="" class="type">Пешком</a>
						</p>
						<!-- <p class="taxi">
							<span class="type">Заказ такси</span> — по тел.: <a href="tel:+7 (351-31) 2-34-12">+7 (351-31) 2-34-12</a>.
						</p> -->
					</div>
				</div>
			</div>		
	<div id="map"></div>

<?php include "mhtml/templates/footer.tpl.php" ?>