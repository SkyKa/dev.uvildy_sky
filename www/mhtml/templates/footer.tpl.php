			</div>

			<footer>
				<div class="logo-wrap">
					<a href=""><img src="/mobile_static/img/logo_footer.png" alt=""></a>
				</div>
				<div class="phone-wrap">
					<a href="tel:73512251616">+7 (351) 225-16-16</a>
					<div class="basic-button small"><a href="tel:73512251616">позвонить</a></div>
				</div>

				<?php mod('catalog.action.mobile_cancel_booking') ?>

			</footer>

		</div>
	</div>


	<link rel="stylesheet" href="<?php Utils :: isChange( '/mobile_static/css/waves.min.css' )?>">
	<link rel="stylesheet" href="<?php Utils :: isChange( '/mobile_static/css/mobiscroll.custom-2.6.2.min.css' )?>">
	<link rel="stylesheet" href="<?php Utils :: isChange( '/mobile_static/css/jquery-ui.min.css' )?>">
	<link rel="stylesheet" href="<?php Utils :: isChange( '/mobile_static/css/owl.carousel.min.css' )?>">
	<link rel="stylesheet" href="<?php Utils :: isChange( '/mobile_static/css/animate.css' )?>">
	<link rel="stylesheet" href="<?php Utils :: isChange( '/mobile_static/css/leaflet.css' )?>">
	<link rel="stylesheet" href="<?php Utils :: isChange( '/mobile_static/css/justifiedgallery.min.css' )?>">
    <link rel="stylesheet" href="<?php Utils :: isChange( '/mobile_static/css/lightgallery.min.css' )?>">
	<link rel="stylesheet" href="<?php Utils :: isChange( '/mobile_static/css/style.css' )?>">
	<link rel="stylesheet" href="<?php Utils :: isChange( '/mobile_static/css/forms_js.css' )?>">
	<script src="<?php Utils :: isChange( '/mobile_static/js/jquery-2.1.4.min.js' )?>"></script>
	<script src="<?php Utils :: isChange( '/mobile_static/js/imagesloaded.pkgd.min.js' )?>"></script>
	<script src="<?php Utils :: isChange( '/mobile_static/js/jquery-ui.min.js' )?>"></script>
	<script src="<?php Utils :: isChange( '/mobile_static/js/mobiscroll.custom-2.6.2.min.js' )?>"></script>
	<script src="<?php Utils :: isChange( '/mobile_static/js/waves.min.js' )?>"></script>
	<script src="<?php Utils :: isChange( '/mobile_static/js/owl.carousel.min.js' )?>"></script>
	<script src="/ajax/?mod=catalog.action.forms_js&debug"></script>
	<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBvkbR7jb_BJnTCaCwN5VU2UwKfF17ez8A"></script>
	<script src="<?php Utils :: isChange( '/mobile_static/js/jquery.maskedinput.min.js' )?>"></script>
	<script src="<?php Utils :: isChange( '/mobile_static/js/animatedmodal.min.js' )?>"></script>
	<script src="<?php Utils :: isChange( '/mobile_static/js/leaflet.js' )?>"></script>
	<script src="<?php Utils :: isChange( '/mobile_static/js/jquery.justifiedgallery.min.js' )?>"></script>
	<script src="<?php Utils :: isChange( '/mobile_static/js/lightgallery-all.min.js' )?>"></script>
	<script src="<?php Utils :: isChange( '/mobile_static/js/action.js' )?>"></script>
	<script src="<?php Utils :: isChange( '/mobile_static/js/booking.js' )?>"></script>
	<script src="<?php Utils :: isChange( '/static/js/leaflet.smoothmarkerbouncing.js' )?>"></script>
	<script src="<?php Utils :: isChange( '/mobile_static/js/leaflet-map.js' )?>"></script>
</body>
</html>