<?php include "mhtml/templates/header.tpl.php" ?>

<header>
	<div class="et-menu-collapse-btn et-collapsed">
		<div class="et-collapse-line"></div>
		<div class="et-collapse-line"></div>
		<div class="et-collapse-line"></div>
	</div>
	
	<div class="h1-wrap">
		<h1><?php mod('pages.show.title') ?></h1>
	</div>

	<a href="/mobile_static/programms.html" class="back-button">
		<img src="/mobile_static/img/back_button.png" alt="">
	</a>

</header>

<div class="all-infra-menu" style="visibility: visible;">
	<?php mod('catalog.action.mobile_map_menu') ?>
</div>

<div class="content-wrapper">
	<div class="wrapper">
		<div class="content-inner"> 

			<div class="et-container support-page">
				<div class="content-container">
					<p class="info">
						Сотрудники нашей службы<br>
						поддержки готовы ответить<br>
						на любой Ваш вопрос.
					</p>
					<div class="basic-button">
						<a href="tel:73512251616">позвонить</a>
					</div>
					<div class="phone-wrap">
						<a href="tel:73512251616" class="phone">+7 (351) 225-16-16</a>
					</div>
					<p class="info">
						Операторы работают<br>
						с 11:00 до 23:00 (по Мск).  
					</p>
				</div>
			</div>

<?php include "mhtml/templates/footer.tpl.php" ?>