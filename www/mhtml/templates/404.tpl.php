<?php ?>
<?php header('HTTP/1.1 404 Not Found'); ?>
<?php include "mhtml/templates/header.tpl.php" ?>

<header>
	<div class="et-menu-collapse-btn et-collapsed">
		<div class="et-collapse-line"></div>
		<div class="et-collapse-line"></div>
		<div class="et-collapse-line"></div>
	</div>
	
	<div class="h1-wrap">
		<h1><?php mod('pages.show.title') ?></h1>
	</div>

	<a href="/mobile_static/programms.html" class="back-button">
		<img src="/mobile_static/img/back_button.png" alt="">
	</a>

</header>

<div class="all-infra-menu" style="visibility: visible;">
	<?php mod('catalog.action.mobile_map_menu') ?>
</div>

<div class="content-wrapper">
	<div class="wrapper">
		<div class="content-inner">

			<div class="et-container page-404 content-container">
				запрашиваемая страница не существует
			</div>
			
<?php include "mhtml/templates/footer.tpl.php" ?>
