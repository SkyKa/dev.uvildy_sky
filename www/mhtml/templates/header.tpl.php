<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="utf-8">
    <title><?php $title = $registry->title; if($title) echo $title; ?></title>
    <meta name="description" content="<?php out('description')?>" />
    <meta name="keywords" content="<?php out('keywords')?>" />
    <base href="<?php echo $base ?>" />
    <?php mod('catalog.editor.header')?>
    <link rel="shortcut icon" href="<?php Utils :: isChange( '/img/favicon.ico' )?>" />
    <link rel="stylesheet" href="<?php Utils :: isChange( '/mobile_static/css/preloader.css' )?>">
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
</head>

<body>

	<div class="et-preloader">
		<div id="preloader-center">
			<div id="preloader-center-absolute">
				<div class="preloader-object"></div>
				<div class="preloader-object"></div>
				<div class="preloader-object"></div>
				<div class="preloader-object"></div>
				<div class="preloader-object"></div>
				<div class="preloader-object"></div>
				<div class="preloader-object"></div>
				<div class="preloader-object"></div>
				<div class="preloader-object"></div>
			</div>
		</div>
	</div>
	
	<nav class="et-menu et-menu-left" style="visibility: visible;">

        <?php mod('menus.show.mobile_main') ?>
		
		<div class="logo-wrap">
        	<a href=""><img src="/mobile_static/img/logo.png" alt=""></a>
        </div>

    </nav>
		
	<div class="site-overlay"></div>

	<!-- <header>
		<div class="et-menu-collapse-btn et-collapsed">
			<div class="et-collapse-line"></div>
			<div class="et-collapse-line"></div>
			<div class="et-collapse-line"></div>
		</div>
		
		<div class="h1-wrap">
			<h1><?php mod('pages.show.title') ?></h1>
		</div>

		<a href="/mobile_static/programms.html" class="back-button">
			<img src="/mobile_static/img/back_button.png" alt="">
		</a>

	</header>

	<div class="all-infra-menu" style="visibility: visible;">
		<?php mod('catalog.action.mobile_map_menu') ?>
    </div>
	
	<div class="content-wrapper">
		<div class="wrapper">
			<div class="content-inner"> -->