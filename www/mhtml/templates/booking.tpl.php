<?php include "mhtml/templates/header.tpl.php" ?>

<header>
	<div class="et-menu-collapse-btn et-collapsed">
		<div class="et-collapse-line"></div>
		<div class="et-collapse-line"></div>
		<div class="et-collapse-line"></div>
	</div>
	
	<div class="h1-wrap">
		<h1><?php mod('pages.show.title') ?></h1>
	</div>

	<a href="/mobile_static/programms.html" class="back-button">
		<img src="/mobile_static/img/back_button.png" alt="">
	</a>

</header>

<div class="all-infra-menu" style="visibility: visible;">
	<?php mod('catalog.action.mobile_map_menu') ?>
</div>

<div class="content-wrapper">
	<div class="wrapper">
		<div class="content-inner">

			<div class="et-container booking">

				<div class="select-building booking-item">
					<img class="territory" src="/mobile_static/img/territory.png" alt="">
					<div class="select-building-inner">
						<div class="title">Выберите строение</div>
						<a href="/shag-2.html" class="mark mark1" data-alias="1korpus">
							<img src="/mobile_static/img/mark.png" alt="">
							<div class="info">
								<p class="name">1 Корпус</p>
								<p class="text">
									Расстояние<br>до ресторана:<br><b>200 м</b>
								</p>
							</div>
						</a>
						<a href="/shag-2.html" class="mark mark2" data-alias="2korpus">
							<img src="/mobile_static/img/mark.png" alt="">
							<div class="info">
								<p class="name">2 Корпус</p>
								<p class="text">
									Расстояние<br>до ресторана:<br><b>200 м</b>
								</p>
							</div>
						</a>
						<a href="/shag-2.html" class="mark mark3" data-alias="cott">
							<img src="/mobile_static/img/mark.png" alt="">
							<div class="info">
								<p class="name">Коттеджи</p>
								<p class="text">
									Расстояние<br>до ресторана:<br><b>200 м</b>
								</p>
							</div>
						</a>
						<a href="/shag-2.html" class="mark mark4 left" data-alias="4korpus">
							<img src="/mobile_static/img/mark.png" alt="">
							<div class="info">
								<p class="name">4 Корпус</p>
								<p class="text">
									Расстояние<br>до ресторана:<br><b>200 м</b>
								</p>
							</div>
						</a>
						<a href="/vsya-infrastruktura.html" class="all-infra">Вся инфраструктура</a>
					</div>
				</div>


				<div class="select-number-date booking-item">
					<div class="content-container">
						<div class="select-number">
							<h2>Выберите номер</h2>
							<select name="select-number" id="select-number" class="custom-select-room">
								<option value="president">Президентский</option>
								<option value="luxe">Люкс</option>
								<option value="self-luxe">Полулюкс</option>
								<option value="cott-luxe">Коттедж-люкс</option>
								<option value="comfort">Комфорт</option>
								<option value="cott-base">Коттедж-стандарт</option>
								<option value="base">Стандарт</option>
							</select>
						</div>
						<div class="select-date">
							<h2>Выберите дату проживания</h2>
							<div class="start-date">
								<p class="subtitle">Дата заезда</p>
								<input name="start-date" id="start-date" hidden>
							</div>
							<div class="end-date">
								<p class="subtitle">Дата отъезда</p>
								<input name="end-date" id="end-date" hidden>
							</div>
						</div>
						<div class="numbers-guests">
							<h2>Количество гостей</h2>
						</div>
						<div class="basic-button">
							<a href="" class="add-human">Добавить человека</a>
						</div>
					</div>
				</div>


				<div class="select-number-guests booking-item">
					<div class="content-container">
						<form id="add-human-form" class="add-human-form">
							<h2>
								Возраст проживающего
								<span>(полных лет)</span>
							</h2>
							<div class="age-wrap">
								<select name="select-age" id="select-age" class="number">
								 	<option value="1">1</option>
								 	<option value="2">2</option>
								 	<option value="3">3</option>
								 	<option value="4">4</option>
								 	<option value="5">5</option>
								 	<option value="6">6</option>
								 	<option value="7">7</option>
								 	<option value="8">8</option>
								 	<option value="9">9</option>
								 	<option value="10">10</option>
								 	<option value="11">11</option>
								 	<option value="12">12</option>
								 	<option value="13">13</option>
								 	<option value="14">14</option>
								 	<option value="15">15</option>
								 	<option value="16">16</option>
								 	<option value="17">17</option>
								 	<option value="18">18</option>
								 	<option value="19">19</option>
								 	<option value="20">20</option>
								 	<option value="21">21</option>
								 	<option value="22">22</option>
								 	<option value="23">23</option>
								 	<option value="24">24</option>
								 	<option value="25">25</option>
								 	<option value="26">26</option>
								 	<option value="27">27</option>
								 	<option value="28">28</option>
								 	<option value="29">29</option>
								 	<option selected value="30">30</option>
								 	<option value="31">31</option>
								 	<option value="32">32</option>
								 	<option value="33">33</option>
								 	<option value="34">34</option>
								 	<option value="35">35</option>
								 	<option value="36">36</option>
								 	<option value="37">37</option>
								 	<option value="38">38</option>
								 	<option value="39">39</option>
								 	<option value="40">40</option>
								 	<option value="41">41</option>
								 	<option value="42">42</option>
								 	<option value="43">43</option>
								 	<option value="44">44</option>
								 	<option value="45">45</option>
								 	<option value="46">46</option>
								 	<option value="47">47</option>
								 	<option value="48">48</option>
								 	<option value="49">49</option>
								 	<option value="50">50</option>
								 	<option value="51">51</option>
								 	<option value="52">52</option>
								 	<option value="53">53</option>
								 	<option value="54">54</option>
								 	<option value="55">55</option>
								 	<option value="56">56</option>
								 	<option value="57">57</option>
								 	<option value="58">58</option>
								 	<option value="59">59</option>
								 	<option value="60">60</option>
								 	<option value="61">61</option>
								 	<option value="62">62</option>
								 	<option value="63">63</option>
								 	<option value="64">64</option>
								 	<option value="65">65</option>
								 	<option value="66">66</option>
								 	<option value="67">67</option>
								 	<option value="68">68</option>
								 	<option value="69">69</option>
								 	<option value="70">70</option>
								 	<option value="71">71</option>
								 	<option value="72">72</option>
								 	<option value="73">73</option>
								 	<option value="74">74</option>
								 	<option value="75">75</option>
								 	<option value="76">76</option>
								 	<option value="77">77</option>
								 	<option value="78">78</option>
								 	<option value="79">79</option>
								 	<option value="80">80</option>
								 	<option value="81">81</option>
								 	<option value="82">82</option>
								 	<option value="83">83</option>
								 	<option value="84">84</option>
								 	<option value="85">85</option>
								 	<option value="86">86</option>
								 	<option value="87">87</option>
								 	<option value="88">88</option>
								 	<option value="89">89</option>
								 	<option value="90">90</option>
								 	<option value="91">91</option>
								 	<option value="92">92</option>
								 	<option value="93">93</option>
								 	<option value="94">94</option>
								 	<option value="95">95</option>
								 	<option value="96">96</option>
								 	<option value="97">97</option>
								 	<option value="98">98</option>
								 	<option value="99">99</option>
								 	<option value="100">100</option>
								 	<option value="101">101</option>
								 	<option value="102">102</option>
								 	<option value="103">103</option>
								 	<option value="104">104</option>
								 	<option value="105">105</option>
								 	<option value="106">106</option>
								 	<option value="107">107</option>
								 	<option value="108">108</option>
								 	<option value="109">109</option>
								 	<option value="110">110</option>
								 	<option value="111">111</option>
								 	<option value="112">112</option>
								 	<option value="113">113</option>
								 	<option value="114">114</option>
								 	<option value="115">115</option>
								 	<option value="116">116</option>
								 	<option value="117">117</option>
								 	<option value="118">118</option>
								 	<option value="119">119</option>
								 	<option value="120">120</option>
								</select>
							</div>
					
							<div class="select-number-wrap">
								<h2>Выберите программу</h2>
								<select name="select-number" id="select-number" class="custom-select">
									<option value="">Общая программа</option>
									<option value="">Клиника мозга</option>
									<option value="">Эндоэкология, лимфология</option>
									<option value="">Детокс за 3 дня</option>
									<option value="">Кардиология</option>
									<option value="">Пульмонология</option>
									<option value="">Урология</option>
								</select>
							</div>

							<div class="select-during-wrap">
								<h2>выберите продолжительность курса (количество дней)</h2>
								<div id="select-number-days" class="select-number-days">
								    <input type="radio" id="number1" name="number" checked><label for="number1">10</label>
									<input type="radio" id="number2" name="number"><label for="number2">14</label>
									<input type="radio" id="number3" name="number"><label for="number3">21</label>
								</div>
							</div>
							
							<div class="basic-button save"><button type="button">Сохранить</button></div>

						</form>
					</div>
				</div>

				<div class="show-number-guests booking-item" style="display: block;">
					<div class="content-container">
						<div class="item adult">
							<p class="title">Взрослый</p>
							<p class="programm">Программа: <span>Клиника мозга</span></p>
							<p class="during">Длительность курса: <span>21 день</span></p>
							<a class="delete-button" href="">Удалить</a>
						</div>
						<div class="item child">
							<p class="title">Детский (6-12 лет)</p>
							<p class="programm">Программа: <span>Клиника мозга</span></p>
							<p class="during">Длительность курса: <span>21 день</span></p>
							<a class="delete-button" href="">Удалить</a>
						</div>
						<div class="item small-child">
							<p class="title">Детский (3-5 лет)</p>
							<a class="delete-button" href="">Удалить</a>
						</div>
						<div class="basic-button">
							<a href="" class="add-human">Добавить человека</a>
						</div>
						<div class="price-continue">
							
						</div>
					</div>
				</div>

			</div>

<?php include "mhtml/templates/footer.tpl.php" ?>