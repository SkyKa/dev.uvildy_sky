<?php include "mhtml/templates/header.tpl.php" ?>

<div class="et-container about-page">
	<a href="" class="item">
		<div class="img-wrap">
			<div class="img" style="background-image: url(/mobile_static/img/about1.png);"></div>
		</div>
		<div class="text-wrap">
			<div class="text">
				<p>ИСТОРИЯ КУРОРТА</p>
			</div>
		</div>
	</a>
	<a href="" class="item">
		<div class="img-wrap">
			<div class="img" style="background-image: url(/mobile_static/img/about2.png);"></div>
		</div>
		<div class="text-wrap">
			<div class="text">
				<p>КАРТА КУРОРТА</p>
			</div>
		</div>
	</a>
	<a href="" class="item">
		<div class="img-wrap">
			<div class="img" style="background-image: url(/mobile_static/img/about3.png);"></div>
		</div>
		<div class="text-wrap">
			<div class="text">
				<p>КАК ДОБРАТЬСЯ</p>
			</div>
		</div>
	</a>
	<a href="" class="item">
		<div class="img-wrap">
			<div class="img" style="background-image: url(/mobile_static/img/about4.png);"></div>
		</div>
		<div class="text-wrap">
			<div class="text">
				<p>ОТЗЫВЫ</p>
			</div>
		</div>
	</a>
</div>

<?php include "mhtml/templates/footer.tpl.php" ?>