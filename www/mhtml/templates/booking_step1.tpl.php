<?php include "mhtml/templates/header.tpl.php" ?>

<div class="et-container booking select-building">
	<img class="territory" src="/mobile_static/img/territory.png" alt="">
	<div class="select-building-inner">
		<div class="title">Выберите строение</div>
		<a href="/shag-2.html" class="mark mark1" data-alias="1korpus">
			<img src="/mobile_static/img/mark.png" alt="">
			<div class="info">
				<p class="name">1 Корпус</p>
				<p class="text">
					Расстояние<br>до ресторана:<br><b>200 м</b>
				</p>
			</div>
		</a>
		<a href="/shag-2.html" class="mark mark2" data-alias="2korpus">
			<img src="/mobile_static/img/mark.png" alt="">
			<div class="info">
				<p class="name">2 Корпус</p>
				<p class="text">
					Расстояние<br>до ресторана:<br><b>200 м</b>
				</p>
			</div>
		</a>
		<a href="/shag-2.html" class="mark mark3" data-alias="cott">
			<img src="/mobile_static/img/mark.png" alt="">
			<div class="info">
				<p class="name">Коттеджи</p>
				<p class="text">
					Расстояние<br>до ресторана:<br><b>200 м</b>
				</p>
			</div>
		</a>
		<a href="/shag-2.html" class="mark mark4 left" data-alias="4korpus">
			<img src="/mobile_static/img/mark.png" alt="">
			<div class="info">
				<p class="name">4 Корпус</p>
				<p class="text">
					Расстояние<br>до ресторана:<br><b>200 м</b>
				</p>
			</div>
		</a>
		<a href="/vsya-infrastruktura.html" class="all-infra">Вся инфраструктура</a>
	</div>
</div>

<?php include "mhtml/templates/footer.tpl.php" ?>