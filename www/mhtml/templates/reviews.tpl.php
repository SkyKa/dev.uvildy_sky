<?php include "mhtml/templates/header.tpl.php" ?>

<header>
	<div class="et-menu-collapse-btn et-collapsed">
		<div class="et-collapse-line"></div>
		<div class="et-collapse-line"></div>
		<div class="et-collapse-line"></div>
	</div>
	
	<div class="h1-wrap">
		<h1><?php mod('pages.show.title') ?></h1>
	</div>

	<a href="" class="back-button">
		<img src="/mobile_static/img/back_button.png" alt="">
	</a>

</header>

<div class="content-wrapper">
	<div class="wrapper">
		<div class="content-inner">

			<div class="et-container reviews-page">
				<div class="content-container">
					<?php mod('catalog.action.mobile_reviews') ?>
				</div>
			</div>

<?php include "mhtml/templates/footer.tpl.php" ?>