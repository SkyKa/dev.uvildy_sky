<?php include "mhtml/templates/header.tpl.php" ?>

<header>
	<div class="et-menu-collapse-btn et-collapsed">
		<div class="et-collapse-line"></div>
		<div class="et-collapse-line"></div>
		<div class="et-collapse-line"></div>
	</div>
	
	<div class="h1-wrap">
		<h1><?php mod('pages.show.title') ?></h1>
	</div>

	<a href="/mobile_static/programms.html" class="back-button">
		<img src="/mobile_static/img/back_button.png" alt="">
	</a>

</header>

<div class="all-infra-menu" style="visibility: visible;">
	<?php mod('catalog.action.mobile_map_menu') ?>
</div>

<div class="content-wrapper">
	<div class="wrapper">
		<div class="content-inner">

			<div class="et-container personal-card-page">
				<img class="logo" src="/mobile_static/img/personal_card_logo.jpg" alt="" class="brain">
				
				<div class="content-container">
					<form action="">
						<div class="form-group phone">
							<input type="text" class="form-control" name="phone" placeholder="+7 (      )     -    - ">
						</div>
						<div class="form-group">
							<input type="text" class="form-control" name="number" placeholder="Номер персональной карты">
						</div>
						<div class="basic-button">
							<button type="submit">активировать карту</button>
						</div>
					</form>
				</div>
			</div>

<?php include "mhtml/templates/footer.tpl.php" ?>