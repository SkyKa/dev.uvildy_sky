<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="utf-8">
    <title><?php $title = $registry->title; if($title) echo $title; ?></title>
    <meta name="description" content="<?php out('description')?>" />
    <meta name="keywords" content="<?php out('keywords')?>" />
    <meta property="og:type" content="article" />
	<meta property="og:title" content="<?php  $title = $registry->title; if($title) echo $title; ?>" />
	<meta property="og:description" name="description" content="<?php out('description')?>" />
	<meta property="og:url" content="http://www.uvildy.ru/" />
	<meta property="og:image" content="<?php 

	$table = new Table ('pages');

	$img = $table -> select ('SELECT `img_src`, `alias` FROM `pages` WHERE `alias`="index" LIMIT 1');

	echo 'http://www.uvildy.ru/' . $img[0]['img_src'];

	?>" />
    <base href="<?php echo $base ?>" />
    <?php mod('catalog.editor.header')?>
    <link rel="shortcut icon" href="<?php Utils :: isChange( '/img/favicon.ico' )?>">
    <link rel="stylesheet" href="<?php Utils :: isChange( '/static/css/bootstrap.min.css' )?>">
    <link rel="stylesheet" href="<?php Utils :: isChange( '/static/css/lightslider.min.css' )?>">
    <link rel="stylesheet" href="<?php Utils :: isChange( '/static/css/jquery-ui.min.css' )?>">
    <link rel="stylesheet" href="<?php Utils :: isChange( '/static/css/jquery.fancybox.css' )?>">
    <link rel="stylesheet" href="<?php Utils :: isChange( '/static/css/animate.css' )?>">
    <link rel="stylesheet" href="<?php Utils :: isChange( '/static/css/forms_js.css' )?>">
    <link rel="stylesheet" href="<?php Utils :: isChange( '/static/css/justifiedgallery.min.css' )?>">
    <link rel="stylesheet" href="<?php Utils :: isChange( '/static/css/lightgallery.min.css' )?>">
    <link rel="stylesheet" href="<?php Utils :: isChange( '/static/css/style.css' )?>">
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
</head>

<body>
	<div class="wrapper">
		<div class="content-inner">
			<header class="header-main">
				<div class="container">
					<div class="header-top">
						<div class="phones">
							<?php mod( 'catalog.action.yandex_direct_phone', array( 'num' => 1 ) )?>
							<?php mod( 'catalog.action.yandex_direct_phone', array( 'num' => 2 ) )?>
							<?php mod( 'catalog.action.yandex_direct_phone', array( 'num' => 3 ) )?>
							<?php mod( 'catalog.action.yandex_direct_phone', array( 'num' => 4 ) )?>
							<br>
							<p class="work-time">Прием звонков осуществляется с 7:00 до 16:00 МСК</p>
						</div>
						<div class="buttons-wrap">
							<div class="basic-button booking-button">
								<a href="/bronirovanie.html">забронировать номер</a>
							</div>
							<div class="basic-button booking-button">
								<a href="" data-toggle="modal" data-target="#callback-modal">заказать звонок</a>
							</div>
						</div>
						<form action="/search.html" id="search">
							<input type="search" name="q" maxlength="50" class="form-control">
							<button type="submit">
								<img src="/static/img/loupe.png" alt="">
							</button>
						</form>
					</div>
					<div class="et-menu">
						<div class="et-menu-header">
							<div class="et-menu-collapse-btn et-collapsed">
								<div class="et-collapse-line"></div>
								<div class="et-collapse-line"></div>
								<div class="et-collapse-line"></div>
							</div>
						</div>
						<?php mod( 'menus.show.main' ) ?>
						<div class="clearfix"></div>
					</div>
				</div>
			</header>