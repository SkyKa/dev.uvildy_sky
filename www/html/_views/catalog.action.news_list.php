<?php

if( !empty($params[0]) ) {
	mod( 'catalog.action.news_item' );
	return;
}

$shares = get_shares_by_programm();

if( !empty( $shares ) ) {
	mod('catalog.action.shares_by_programm', array( 'shares_id' => $shares['shares'], 'programm_title' => $shares['title'] ) );
	return;
}

$table = new Table('catalog_section');

$section = $table -> select('SELECT `id` FROM `catalog_section` WHERE `alias`=:alias LIMIT 1', array( 'alias' => $alias ) );
if( !count( $section ) ) return false;


$news_list = $table -> select('SELECT * FROM `position_news` WHERE `section_id`=:id AND `public` ORDER BY `datestamp` DESC LIMIT 15', array( 'id' => $section[0]['id'] ) );
if( !count( $news_list ) ) return false;

$str = '<div class="breadcrumbs">
			<div class="container">
				<nav class="breadcrumbs">
					'. val('catalog.action.breadcrumbs') .'
				</nav>
			</div>
		</div>
		<div class="container news-page">

			<h1>'. val('pages.show.title') .'</h1>

			' . val( 'catalog.action.submenu', array( 'parent_alias' => 'akcii-i-novosti' ) ) . '

			<div class="row news-wrap jscroll">';

// ПОСТРАНИЧНЫЙ ВЫВОД
// $number_pages = $table -> select( 'SELECT * FROM `section_news` WHERE `id`=:id', array( 'id' => $section[0]['id'] ) );
// $page_size = 0;

// if( !empty( $number_pages ) && !empty( $number_pages[0]['page_size'] ) ) {
// 	$page_size = $number_pages[0]['page_size'];
// }
// else {
// 	$page_size = count($news_list);
// }

// var_dump( $page_size );


// $page = Utils :: getGET( 'page' ) ? Utils::getGET( 'page' ) : 1;
// $page = $page < 1 ? 1 : $page;
// $rows = $table -> select( 'SELECT COUNT(*) AS `count` FROM `position_news` WHERE `section_id`=:id', array( 'id' => $section[0]['id'] ) );
// $total = $rows[ 0 ][ 'count' ];
// $count = ceil( ( int )$total / ( int )$page_size );

// if ( Utils :: getGET( 'page' ) == 'all' ) {
//   $limit = '';
// }
// else {
//   $limit = ' limit ' . $page_size * ( $page - 1 ) . ',' . $page_size;
// }

// $rows = $table -> select( 'SELECT * FROM `position_news` WHERE `section_id`=:id ORDER BY `datestamp` DESC' . $limit, array( 'id' => $section[0]['id'] ) );

// $news_list = $rows;

foreach ($news_list as $news_item) {
	$str .= '<div class="item-wrap">';

	if( empty( $news_item['content'] ) ) {
		$str .= '<div class="item">';
	}
	else {
		$str .= '<a href="/'. $alias .'/' . $news_item['alias'] .'.html" class="item">';
	}
			
	$style = '';
	if( !empty($news_item['img']) ) {
		$str .= '<img src="'. get_cache_pic($news_item['img'], 430, 286, true) .'" class="image">';
		
	}
	else {
		$style = 'style="margin-top: 35px;"';
	}

	if( !empty($news_item['is_share']) ) {
			if ( !$news_item[ 'enddatestamp' ] || $news_item[ 'enddatestamp' ] > time() ) {
				$str .= '<img src="/static/img/share.png" class="share-img">';
			}
			else {
				$str .= '<img src="/static/img/share_gray.png" class="share-img">';
			}
	}

	$str .=			'<h2 '. $style .'>'. $news_item['title'] .'</h2>';

	$month = date( 'm', $news_item['datestamp']);
	$month = Langvars::replaceMonth($month);

	if( empty($news_item['is_share']) ) {
		$str .=     '<p class="date">'. date( 'd ', $news_item['datestamp']) . $month . date( ' Y', $news_item['datestamp']) .'</p>';
	}
	else {
		$str .=     '<p class="date"></p>';
	}

	$str .=			'<p class="desc">'. $news_item['description'] .'</p>';

	if( !empty( $news_item['content'] ) ) {
		$str .=    '<p class="details">Подробнее</p>
					<div class="clearfix"></div>';
	}

	if( empty( $news_item['content'] ) ) {
		$str .= '</div>';
	}
	else {
		$str .= '</a>';
	}
				
	$str .=		'</div>';
}

// $str .= val( 'catalog.action.pager', array( 'count' => $count, 'page' => $page ) );

$str .=    '</div>
			<div class="clearfix"></div>
		</div>';

echo $str;


function get_shares_by_programm() {

	$table = new Table('catalog_section');

	$programm = Utils :: getVar( 'programm' );
	if( empty($programm) ) return false;

	$programm_section = $table -> select('SELECT `id`, `title` FROM `catalog_section` WHERE `alias`=:alias LIMIT 1', array( 'alias' => $programm ) );
	if( empty( $programm_section ) ) return false;

	$programm_section = end( $programm_section );
	$programm_title = $programm_section['title'];

	$shares_section = $table -> select('SELECT `id` FROM `catalog_section` WHERE `parent_id`=:id and `position_table`="position_select_shares" LIMIT 1', 
		array( 'id' => $programm_section['id'] ) );
	if( empty( $shares_section ) ) return false;

	$shares_section = end( $shares_section );

	$shares = $table -> select('SELECT * FROM `position_select_shares` WHERE `section_id`=:id', array( 'id' => $shares_section['id'] ) );
	if( empty( $shares ) ) return false;

	$array = array( 'shares' => $shares, 'title' => $programm_title );

	return $array;

}

/*
<div class="breadcrumbs">
		<div class="container">
			<nav class="breadcrumbs">
				<?php mod('catalog.action.breadcrumbs') ?>
			</nav>
		</div>
	</div>

	<div class="container news-page">
		<h1><?php mod('pages.show.title') ?></h1>
		<div class="row news-wrap">
			<div class="item-wrap">
				<a href="" class="item">
					<img src="/static/img/slide1.jpg" class="image">
					<img src="/static/img/share.png" class="share-img">
					<h2>Здравствуй, лето!</h2>
					<p class="date">25 июля 2016</p>
					<p class="desc">
						Всесоюзный центральный исполнительный комитет распорядился о строительстве нового курорта. Местом послужил берег озера Увильды. Оно является одним из самых чистых и красивых во всей Челябинской области. Также его берега богаты полезными веществами, которые впоследствии и сделали "Увильды" уникальным курортом.
					</p>
					<p class="details">Подробнее</p>
					<div class="clearfix"></div>
				</a>
			</div>
			<div class="item-wrap">
				<a href="" class="item">
					<img src="/static/img/slide1.jpg" class="image">
					<h2>Здравствуй, лето!</h2>
					<p class="date">25 июля 2016</p>
					<p class="desc">
						Всесоюзный центральный исполнительный комитет распорядился о строительстве нового курорта. Местом послужил берег озера Увильды. Оно является одним из самых чистых и красивых во всей Челябинской области. Также его берега богаты полезными веществами, которые впоследствии и сделали "Увильды" уникальным курортом.
					</p>
					<p class="details">Подробнее</p>
					<div class="clearfix"></div>
				</a>
			</div>
			<div class="item-wrap">
				<a href="" class="item">
					<img src="/static/img/slide1.jpg" class="image">
					<h2>Здравствуй, лето!</h2>
					<p class="date">25 июля 2016</p>
					<p class="desc">
						Всесоюзный центральный исполнительный комитет распорядился о строительстве нового курорта. Местом послужил берег озера Увильды. Оно является одним из самых чистых и красивых во всей Челябинской области. Также его берега богаты полезными веществами, которые впоследствии и сделали "Увильды" уникальным курортом.
					</p>
					<p class="details">Подробнее</p>
					<div class="clearfix"></div>
				</a>
			</div>
			<div class="item-wrap">
				<a href="" class="item">
					<img src="/static/img/slide1.jpg" class="image">
					<h2>Здравствуй, лето!</h2>
					<p class="date">25 июля 2016</p>
					<p class="desc">
						Всесоюзный центральный исполнительный комитет распорядился о строительстве нового курорта. Местом послужил берег озера Увильды. Оно является одним из самых чистых и красивых во всей Челябинской области. Также его берега богаты полезными веществами, которые впоследствии и сделали "Увильды" уникальным курортом.
					</p>
					<p class="details">Подробнее</p>
					<div class="clearfix"></div>
				</a>
			</div>
			<div class="item-wrap">
				<a href="" class="item">
					<img src="/static/img/slide1.jpg" class="image">
					<h2>Здравствуй, лето!</h2>
					<p class="date">25 июля 2016</p>
					<p class="desc">
						Всесоюзный центральный исполнительный комитет распорядился о строительстве нового курорта. Местом послужил берег озера Увильды. Оно является одним из самых чистых и красивых во всей Челябинской области. Также его берега богаты полезными веществами, которые впоследствии и сделали "Увильды" уникальным курортом.
					</p>
					<p class="details">Подробнее</p>
					<div class="clearfix"></div>
				</a>
			</div>
			<div class="item-wrap">
				<a href="" class="item">
					<img src="/static/img/slide1.jpg" class="image">
					<h2>Здравствуй, лето!</h2>
					<p class="date">25 июля 2016</p>
					<p class="desc">
						Всесоюзный центральный исполнительный комитет распорядился о строительстве нового курорта. Местом послужил берег озера Увильды. Оно является одним из самых чистых и красивых во всей Челябинской области. Также его берега богаты полезными веществами, которые впоследствии и сделали "Увильды" уникальным курортом.
					</p>
					<p class="details">Подробнее</p>
					<div class="clearfix"></div>
				</a>
			</div>
		</div>
	</div>
*/