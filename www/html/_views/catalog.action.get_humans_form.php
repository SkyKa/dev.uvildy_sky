<?php 

$age_categories = $args -> age_categories;

$str = '<div class="row added-humans">
        </div>
        <form action="">
            <div class="add-man basic-button big"><button type="button">добавить человека</button></div>
            <div class="add-person-form-wrap">
                <div class="add-person-form clearfix">
                    <div class="old">
                        <h2>Выберите возрастную категорию</h2>
                        <div class="custom-radio">';

$ages = array();


foreach( $age_categories as $category ) {
    if( array_key_exists($category['age'], $ages) ) {
        if( ($ages[$category['age']]['food'] != $category['food_type']) && ( ($category['food_type'] != 'Ресторан "Акватория"') && ($category['programm'] != 'новогодняя программа') ) ) {

            if( $ages[$category['age']]['food'] == 'Ресторан "Акватория"' ) {
                $ages[$category['age']]['food'] = $category['food_type'];
            }
            else {
                $ages[$category['age']]['is_checkbox'] = true; 
                if( empty( $ages[$category['age']]['food'] ) ) {
                    $ages[$category['age']]['food'] = $category['food_type'];
                }
            }

        }
    }
    else {
        $ages[$category['age']]['food'] = $category['food_type']; 
    }
}

ksort( $ages );

$i = 0;
foreach( $ages as $key => $age ) {
    $checked = '';
    if( $i == 0 ) $checked = 'checked'; 

    $name = Utils::translit( $key );

    $str .= '<div class="form-group">
                <input type="radio" id="'. $name .'" name="age" '. $checked .' data-age-title="'. $key .'" data-food-title="'. htmlentities($age['food']) .'">
                <label for="'. $name .'">
                    <div class="square"></div>
                    <span class="title">'. $key .'</span>
                </label>';

    if ( isset( $age['is_checkbox'] ) && $age['is_checkbox'] ) {
        $str .= '<div class="check-food">
                    <input type="checkbox" id="'. $name .'-checkbox" name="'. $name .'-checkbox" checked>
                    <label for="' . $name . '-checkbox">
                        <div class="square"></div>
                        <span class="title">'. $age['food'] .'</span>
                    </label>
                </div>';
    }

    $str .=     '</div>';


    $i++;
}

$str .= '</div>
            </div>
            </div>
            <div class="basic-button big save-person"><button type="button">сохранить</button></div>
            <div class="basic-button big gray close-form"><button>закрыть форму</button></div>
        </div>
    </form>
    <div class="basic-button big pull-right"><a href="" id="to-last-step">далее</a></div>';


echo $str;