<?php 

$table = new Table('catalog_section');

$import_1c = new Import1c();


$properties_xml = $import_1c->getNumberTypes();
$properties = $properties_xml->Классификатор->Свойства->Свойство;


// Выгрузка и обновление типов номеров
foreach( $properties as $property ) {

	if( $property->Ид == '579d22de-8378-11e4-9206-a938ec62ee95' ) {

		foreach( $property->ВариантыЗначений->Справочник as $room ) {

			if( empty( $room->ИдЗначения ) || empty( $room->Значение ) ) continue;

			$is_exist = $table->select('SELECT `id` FROM `section_number_class` WHERE `id_1c`=:id_1c LIMIT 1', array( 'id_1c' => $room->ИдЗначения )); 
			if( empty($is_exist) ) {

				$table = new Table('catalog_section');
				$obj = new stdClass();
				$obj -> title = $room->Значение;
				$obj -> parent_id = 317;
				$obj -> section_table = 'section_number_class';
				$obj -> alias = Utils::getUniqueAlias( Utils::translit( (string) $room->Значение ), 'catalog_section' );
				$id = $table -> save($obj);

				$table->execute('INSERT INTO `section_number_class` (`id`, `id_1c`) VALUES (:id, :id_1c)', array( 'id' => $id, 'id_1c' => $room->ИдЗначения ));

			}
			else {
				$table->execute('UPDATE `catalog_section` SET `title`=:title WHERE `id`=:id', array( 'title' => $room->Значение, 'id' => $is_exist[0]['id'] ));
			}
		}

		break;

	}

}



$items_xml = $import_1c->getGoods();
$items = $items_xml->Каталог->Товары->Товар;

$rests_xml = $import_1c->getRests();
$rests = $rests_xml->ПакетПредложений->Предложения->Предложение;


// Выгрузка номеров
foreach( $items as $item ) {

	foreach( $item->ЗначенияСвойств->ЗначенияСвойства as $property ) {

		if( $property->Ид == '579d22de-8378-11e4-9206-a938ec62ee95' ) {

			$number_type = $table->select('SELECT `id` FROM `section_number_class` WHERE `id_1c`=:id_1c LIMIT 1', array( 'id_1c' => $property->Значение ));

			if( empty($number_type) ) break;

			$number_type = end( $number_type );

			$is_deleted = false;

			switch( strtolower($item->ПометкаУдаления) ) {
		        case 'true': 
		        	$is_deleted = true;
		        	break;
		        case 'false': 
		        	$is_deleted = false;
		        	break;
		    }

		    $rest = get_rests( $item->Ид, $rests );
		    $prop_array = array();

			// Количество комнат
			foreach( $item->ЗначенияСвойств->ЗначенияСвойства as $value ) {
				if( $value->Ид == 'b2cf492e-8b90-11e4-9206-a938ec62ee95' ) {
					$prop_array[(string) $value->Ид] = (string) $value->Значение;
					break;
				}
			}			

		    // Количество основных мест
			foreach( $item->ЗначенияСвойств->ЗначенияСвойства as $value ) {
				if( $value->Ид == '579d22d4-8378-11e4-9206-a938ec62ee95' ) {
					$prop_array[(string) $value->Ид] = (string) $value->Значение;
					break;
				}
			}

			// Количество доп. мест
			foreach( $item->ЗначенияСвойств->ЗначенияСвойства as $value ) {
				if( $value->Ид == '579d22d9-8378-11e4-9206-a938ec62ee95' ) {
					$prop_array[(string) $value->Ид] = (string) $value->Значение;
					break;
				}
			}


			$prop_array = get_room_properties( $prop_array, $properties );

			$is_exist_number = $table->select('SELECT `id` FROM `section_rooms` WHERE `id_1c`=:id_1c LIMIT 1', array( 'id_1c' => $item->Ид )); 
			if( empty($is_exist_number) ) {

				if( $is_deleted ) break;

				$table = new Table('catalog_section');
				$obj = new stdClass();
				$obj -> title = $item->Наименование;
				$obj -> parent_id = $number_type['id'];
				$obj -> section_table = 'section_rooms';
				$obj -> position_table = 'position_gallery';
				$obj -> alias = Utils::getUniqueAlias( Utils::translit( (string) $item->Наименование ), 'catalog_section' );
				$id = $table -> save($obj);

				$table->execute('INSERT INTO `section_rooms` (`id`, `id_1c`, `rest`, `n_rooms`, `n_main_place`, `n_additional_place`) VALUES (:id, :id_1c, :rest, 
					:n_rooms, :n_main_place, :n_additional_place)', 
					array( 
						'id' => $id, 'id_1c' => $item->Ид, 
						'rest' => $rest, 
						'n_rooms' => $prop_array['b2cf492e-8b90-11e4-9206-a938ec62ee95'],
						'n_main_place' => $prop_array['579d22d4-8378-11e4-9206-a938ec62ee95'], 
						'n_additional_place' => $prop_array['579d22d9-8378-11e4-9206-a938ec62ee95'] 
					)
				);

			}
			else {

				if( $is_deleted ) {
					$table->execute('DELETE FROM `catalog_section` WHERE `id`=:id', array( 'id' => $is_exist_number[0]['id'] ));
					$table->execute('DELETE FROM `section_rooms` WHERE `id`=:id', array( 'id' => $is_exist_number[0]['id'] ));
				}
				else {

					$table->execute('UPDATE `catalog_section` SET `title`=:title WHERE `id`=:id', array( 'title' => $item->Наименование, 'id' => $is_exist_number[0]['id'] ));

					$table->execute('UPDATE `section_rooms` SET `rest`=:rest, `n_rooms`=:n_rooms, `n_main_place`=:n_main_place, `n_additional_place`=:n_additional_place WHERE `id`=:id', 
						array( 
							'id' => $is_exist_number[0]['id'], 
							'rest' => $rest, 
							'n_rooms' => $prop_array['b2cf492e-8b90-11e4-9206-a938ec62ee95'],
							'n_main_place' => $prop_array['579d22d4-8378-11e4-9206-a938ec62ee95'], 
							'n_additional_place' => $prop_array['579d22d9-8378-11e4-9206-a938ec62ee95']
						)
					);
				}

			}

			break;
		}

	}

}


// Выгрузка цен
$offers_xml = $import_1c->getPrices();
$offers = $offers_xml->ПакетПредложений->Предложения->Предложение;


foreach( $offers as $offer ) {

	$is_exist_number = $table->select('SELECT `id` FROM `section_rooms` WHERE `id_1c`=:id_1c LIMIT 1', array( 'id_1c' => $offer->Ид ));

	if( empty( $is_exist_number ) ) continue;

	$table->execute('TRUNCATE `section_number_prices`');
	foreach( $offer->Цены->Цена as $price ) {
		$table->execute('INSERT INTO `section_number_prices` (`offers_id_1c`, `period`, `age`, `place_type`, `price`) VALUES (:offers_id_1c, :period, :age, :place_type, :price)', 
			array( 
				'offers_id_1c' => $offer->Ид,
				'period' => $price->Период,
				'age' => $price->КЛ,
				'place_type' => $price->ВМ,
				'price' => $price->Цена
			));
	}


}




function get_rests($id, $rests) {

	$places_rest = 0; 

	foreach( $rests as $rest ) {
		if( (string) $rest->Ид == (string) $id ) {
			$places_rest = (int) $rest->Остатки->Остаток[0]->Склад->Количество;
			break;
		}
	}

	return $places_rest;
}


function get_room_properties($key_value, $properties) {

	$property_arr = array();

	foreach( $key_value as $key => $value ) {
		foreach( $properties as $property ) {
			if( (string) $property->Ид == $key ) {
				foreach( $property->ВариантыЗначений->Справочник as $property_val ) {
					if( (string) $property_val->ИдЗначения == $value ) {
						$property_arr[$key] = (string) $property_val->Значение;
						break;
					}
				}
				break;
			}
		}
	}

	return $property_arr;
}
