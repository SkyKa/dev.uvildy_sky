<?php

$my_alias = 'torgovye-predlozheniya';

$table = new Table('catalog_section');
$section = $table -> select('SELECT `id` FROM `catalog_section` WHERE `alias`=:alias LIMIT 1', array( 'alias' => $my_alias ) );
if( empty($section) ) return false;

$number_classes_section = $table -> select('SELECT 
	`catalog_section`.`id`,
	`catalog_section`.`parent_id`,
	`catalog_section`.`title`,
	`section_number_class`.`number_stars`,
	`section_number_class`.`id_1c`
	FROM `catalog_section` INNER JOIN `section_number_class` ON `catalog_section`.`id`=`section_number_class`.`id` WHERE `catalog_section`.`parent_id`=:id 
	ORDER BY cast(`section_number_class`.`number_stars` as char(3)) * 1 DESC', array( 'id' => $section[0]['id'] ) );

if( empty($number_classes_section) ) return false;

$select_date = $_SESSION['booking']['start_date'];
$end_date = date('Y-m-d', strtotime( "+" . $_SESSION['booking']['duration'] . " days" , strtotime($_SESSION['booking']['start_date'])));

$programm_id = $_SESSION['booking']['programm'];

$programm = $table->select('SELECT `id`, `title` FROM `catalog_section` WHERE `id`=:id LIMIT 1', 
	array( 'id' => $programm_id ) );

if(empty($programm) && ($programm_id != 'without-programm')) {
	$res['s'] = false;
	$res['mess'] = 'Ошибка #' . __LINE__;
	echo json_encode( $res );
	exit();
}

$programm_title = '';

if( $programm_id == 'without-programm' ) {
	$programm_title = 'без лечения';
}
else {
	$programm_title = mb_strtolower($programm[0]['title'], 'UTF-8');
}

$str = '';

$i = 0;

$end_date = date('Y-m-d', strtotime( "+" . $_SESSION['booking']['duration'] . " days" , strtotime($_SESSION['booking']['start_date']))); 

foreach( $number_classes_section as $class_section ) {

	$section_rooms = $table -> select(
	'SELECT 
		`section_offers`.`id`, 
		`section_offers`.`id_1c`,
		`section_offers`.`n_rooms`,
		`section_offers`.`n_main_place`,
		`section_offers`.`n_additional_place`,
		`section_number_prices`.`start_date`,
		`section_number_prices`.`end_date`,
		`section_number_prices`.`programm`,
		`section_number_prices`.`place_type`,
		`section_number_prices`.`food_type`,
		`section_number_prices`.`age`
	FROM 
		`catalog_section` 
	INNER JOIN 
		`section_offers`
	ON 
		`catalog_section`.`id`=`section_offers`.`id`
	INNER JOIN 
		`section_number_prices`
	ON 
		`section_offers`.`id_1c`=`section_number_prices`.`offers_id_1c`
	WHERE 
		(`catalog_section`.`parent_id`=:id AND 
		`catalog_section`.`section_table`="section_offers" AND
		(DATE(:select_date) <= `section_number_prices`.`end_date`) AND
        (DATE(:end_date) > `section_number_prices`.`start_date`) AND
		`section_number_prices`.`programm`=:programm_title) OR
        (`catalog_section`.`parent_id`=:id AND
        (DATE(:select_date) <= `section_number_prices`.`end_date`) AND
        (DATE(:end_date) > `section_number_prices`.`start_date`) AND
        `section_number_prices`.`programm`="новогодняя программа")
	ORDER BY `section_number_prices`.`start_date`',
	array( 'id' => $class_section['id'], 'select_date' => $select_date, 'end_date' => $end_date, 'programm_title' => $programm_title ) );

	search_period_room( $section_rooms, $select_date, $end_date );

    $categories = $section_rooms;

    foreach( $categories as $key => $category ) {
        if( !isset( $category['in_out'] ) || !$category['in_out'] ) {
            unset($section_rooms[$key]);
        }
    }

    Utils::dumpre( $section_rooms );

	if( empty( $section_rooms ) ) continue;

	// Получаем id класса номера для всех номеров
	$section_number_class = $table -> select(
		'SELECT `catalog_section`.`id`, `section_number_class`.`id_1c` 
		FROM `section_number_class` 
		INNER JOIN `catalog_section` 
		ON (`section_number_class`.`id`=`catalog_section`.`id`)
		WHERE
			`section_number_class`.`id_1c`=:class_id_1c
		AND
			`catalog_section`.`parent_id`=806
		LIMIT 1',
		array( 'class_id_1c' => $class_section['id_1c'] ));

	if( empty( $section_number_class ) ) continue;

	$section_number_class = end( $section_number_class );

	$is_free_number = false;
	$search_params = array();

	$start = microtime(true);

    // Проверяем есть ли хотя бы один свободный номер в выбранном классе 
	foreach( $section_rooms as $room ) {

		$n_rooms = '';

		if( ($room['n_rooms'] === null) || ($room['n_rooms'] == 0) ) {
			$n_rooms = '`section_rooms`.`n_rooms` IS NULL';
		}
		else {
			$n_rooms = '`section_rooms`.`n_rooms`=' . $room['n_rooms'];
		}

		if( empty( $search_params ) || !in_array( $room['id_1c'], $search_params ) ) {

			$search_params[] = $room['id_1c'];

			// Ищем хотя бы один номер, где есть нужное количество свободных мест
			$founded_rooms = $table -> select(
				'SELECT `section_rooms`.`id`
				FROM `section_rooms`
				INNER JOIN `catalog_section` 
				ON (`section_rooms`.`id`=`catalog_section`.`id`)
				WHERE `catalog_section`.`parent_id`=:id AND
					`section_rooms`.`n_main_place`=:n_main_place AND
					`section_rooms`.`n_additional_place`=:n_additional_place AND
					'. $n_rooms .' AND
					(
						SELECT COUNT(*) 
						FROM `position_booking_rooms` 
						WHERE 
						`position_booking_rooms`.`section_id`=`section_rooms`.`id` AND
						(
							( DATE(:select_date) >= DATE(FROM_UNIXTIME(`start_date`)) AND DATE(:select_date) < DATE(FROM_UNIXTIME(`end_date`)) ) OR
							( DATE(:end_date) > DATE(FROM_UNIXTIME(`start_date`)) AND DATE(:end_date) <= DATE(FROM_UNIXTIME(`end_date`)) ) OR
							( DATE(:select_date) < DATE(FROM_UNIXTIME(`start_date`)) AND DATE(:end_date) > DATE(FROM_UNIXTIME(`end_date`)) )
						)
					)
					+
					IFNULL ((
						SELECT `num_clients` 
						FROM `position_orders` 
						WHERE 
						`position_orders`.`num_1cid`=`section_rooms`.`id_1c` AND
						(
							( DATE(:select_date) >= DATE(FROM_UNIXTIME(`start_date`)) AND DATE(:select_date) < DATE(FROM_UNIXTIME(`end_date`)) ) OR
							( DATE(:end_date) > DATE(FROM_UNIXTIME(`start_date`)) AND DATE(:end_date) <= DATE(FROM_UNIXTIME(`end_date`)) ) OR
							( DATE(:select_date) < DATE(FROM_UNIXTIME(`start_date`)) AND DATE(:end_date) > DATE(FROM_UNIXTIME(`end_date`)) )
						)
					), 0) < :n_main_place
					LIMIT 1',
				array( 'id' => $section_number_class['id'], 'n_main_place' => $room['n_main_place'], 'n_additional_place' => $room['n_additional_place'],
					'select_date' => $select_date, 'end_date' => $end_date ));	

			// Utils::dumpre($section_number_class['id']);
			// Utils::dumpre($room['n_main_place']);
			// Utils::dumpre($room['n_additional_place']);
			// Utils::dumpre($select_date);
			// Utils::dumpre($end_date);
			// Utils::dumpre($n_rooms);

// 			SELECT `section_rooms`.`id`, `position_booking_rooms`.`section_id`
// FROM `section_rooms`
// INNER JOIN `catalog_section` 
// ON (`section_rooms`.`id`=`catalog_section`.`id`)
// LEFT JOIN `position_booking_rooms`
// ON (`section_rooms`.`id`=`position_booking_rooms`.`section_id`)
// WHERE `catalog_section`.`parent_id`=1254 AND
// `section_rooms`.`n_main_place`=1 AND
// `section_rooms`.`n_additional_place`=2 AND
// (
// 	(
//         ( DATE("2016-12-15") >= DATE(FROM_UNIXTIME(`start_date`)) AND DATE("2016-12-15") < DATE(FROM_UNIXTIME(`end_date`)) ) OR
// 	( DATE("2016-12-22") > DATE(FROM_UNIXTIME(`start_date`)) AND DATE("2016-12-22") <= DATE(FROM_UNIXTIME(`end_date`)) ) OR
// 	( DATE("2016-12-15") < DATE(FROM_UNIXTIME(`start_date`)) AND DATE("2016-12-22") > DATE(FROM_UNIXTIME(`end_date`)) )
//         )
        
// )
// GROUP BY `section_rooms`.`id`
	

			if( !empty( $founded_rooms ) ) {
				$is_free_number = true;
				break;
			}

		}

	}

	// Utils::dumpre(microtime(true) - $start);

	if( !$is_free_number ) continue;

	$rooms = $table -> select('SELECT `id` FROM `catalog_section` WHERE `parent_id`=:id LIMIT 1', array( 'id' => $class_section['id'] ) );

	if( empty($rooms) ) continue;

	if( !$i ) {
		$str = '<h2>ВЫБЕРИТЕ ТИП НОМЕРА</h2>
			<div class="select-room">';
		$i++;
	}


	$str .= '<div class="form-group">
				<div class="stars">';

	switch( $class_section['number_stars'] ) {

		case '5':
			$str .= '<img src="/static/img/full_star.png" alt="">
					<img src="/static/img/full_star.png" alt="">
					<img src="/static/img/full_star.png" alt="">
					<img src="/static/img/full_star.png" alt="">
					<img src="/static/img/full_star.png" alt="">';
			break;

		case '4.5':
			$str .= '<img src="/static/img/full_star.png" alt="">
					<img src="/static/img/full_star.png" alt="">
					<img src="/static/img/full_star.png" alt="">
					<img src="/static/img/full_star.png" alt="">
					<img src="/static/img/self_star.png" alt="">';
			break;

		case '4':
			$str .= '<img src="/static/img/full_star.png" alt="">
					<img src="/static/img/full_star.png" alt="">
					<img src="/static/img/full_star.png" alt="">
					<img src="/static/img/full_star.png" alt="">
					<img src="/static/img/empty_star.png" alt="">';
			break;

		case '3.5':
			$str .= '<img src="/static/img/full_star.png" alt="">
					<img src="/static/img/full_star.png" alt="">
					<img src="/static/img/full_star.png" alt="">
					<img src="/static/img/self_star.png" alt="">
					<img src="/static/img/empty_star.png" alt="">';
			break;

		case '3':
			$str .= '<img src="/static/img/full_star.png" alt="">
					<img src="/static/img/full_star.png" alt="">
					<img src="/static/img/full_star.png" alt="">
					<img src="/static/img/empty_star.png" alt="">
					<img src="/static/img/empty_star.png" alt="">';
			break;

		case '2.5':
			$str .= '<img src="/static/img/full_star.png" alt="">
					<img src="/static/img/full_star.png" alt="">
					<img src="/static/img/self_star.png" alt="">
					<img src="/static/img/empty_star.png" alt="">
					<img src="/static/img/empty_star.png" alt="">';
			break;

		case '2':
			$str .= '<img src="/static/img/full_star.png" alt="">
					<img src="/static/img/full_star.png" alt="">
					<img src="/static/img/empty_star.png" alt="">
					<img src="/static/img/empty_star.png" alt="">
					<img src="/static/img/empty_star.png" alt="">';
			break;

		case '1.5':
			$str .= '<img src="/static/img/full_star.png" alt="">
					<img src="/static/img/self_star.png" alt="">
					<img src="/static/img/empty_star.png" alt="">
					<img src="/static/img/empty_star.png" alt="">
					<img src="/static/img/empty_star.png" alt="">';
			break;

		case '1':
			$str .= '<img src="/static/img/full_star.png" alt="">
					<img src="/static/img/empty_star.png" alt="">
					<img src="/static/img/empty_star.png" alt="">
					<img src="/static/img/empty_star.png" alt="">
					<img src="/static/img/empty_star.png" alt="">';
			break;

	}

	$checked = '';
	//if( !$i ) $checked = 'checked';

	//Utils::dumpre($class_section);

	$str .= '</div>
			<input type="radio" id="room-'. $class_section['id'] .'" name="room" '. $checked .'>
			<div class="check-title">
				<label for="room-'. $class_section['id'] .'" class="square" data-toggle="modal" data-target="#room-modal"></label>
				<a href="" target="_blank" class="title" data-toggle="modal" data-target="#room-modal">'. $class_section['title'] .'</a>
			</div>
		</div>';

	//$i++;
}

if( empty( $str ) ) {
	$str .= '<p class="booking-error-message">К сожалению, для выбранных параметров не найдено ни одного номера. Попробуйте выбрать другие параметры.</p>';
	$_SESSION['booking']['failed'] = true;
} 

$str .= '</div>';

echo $str;


function search_period_room( &$age_categories, $select_date, $end_date ) {


    foreach( $age_categories as $key => $category ) {

        if( isset($age_categories[$key]['in_out']) && $age_categories[$key]['in_out'] ) continue; 

        if( (strtotime("+1 day", strtotime( $category['end_date'])) >= strtotime( $end_date )) && (strtotime( $category['start_date'] ) <= strtotime( $select_date )) ) { 
            $age_categories[$key]['in_out'] = true;
            continue;            
        }
        else {
            if( strtotime( $category['start_date'] ) <= strtotime( $select_date ) ) {

                $indexes = find_req_period_room( $age_categories, $select_date, $end_date, $category, $key );

                foreach( $indexes as $index ) {
                    $age_categories[$index]['in_out'] = true; 
                }
            }
        }

    }

}

function find_req_period_room( &$age_categories, $select_date, $end_date, $category, $key, $index=array() ) {

   	$first_day = strtotime("+1 day", strtotime($category['end_date']));
    $search_param = array();

    $search_param['age'] = $category['age'];
    $search_param['food_type'] = $category['food_type'];
    $search_param['place_type'] = $category['place_type'];
    $search_param['id_1c'] = $category['id_1c'];

    $index[] = $key;

    foreach( $age_categories as $k => $cat ) {
        if( ($first_day == strtotime($cat['start_date']) ) 
            && ($search_param['age'] == $cat['age']) 
            && ($search_param['food_type'] == $cat['food_type']) 
            && ($search_param['place_type'] == $cat['place_type'])
            && ($search_param['id_1c'] == $cat['id_1c']) ) {
            
            if( strtotime($cat['end_date']) >= strtotime("-1 day", strtotime($end_date)) ) {
                $index[] = $k;
                return $index;
            }
            else {
                return find_req_period_room($age_categories, $select_date, $end_date, $cat, $k, $index);
            }
        }
    }

    return array();

}