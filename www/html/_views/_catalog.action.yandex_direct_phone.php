<?php

	$str = '';

	$num_phone = $args -> num;
	$postfix = $args -> postfix;
	
	$yandex_direct = ( isset( $_SESSION[ 'yandex_direct' ] ) ) ? $_SESSION[ 'yandex_direct' ] : false;
	
	if ( $yandex_direct ) {
	
		$phone = val( 'banner.show.yandex_direct_phone' . $num_phone );

		if ( $phone ) {
			$str .=	'<a class="phone" href="tel:' . 
					Utils :: phone_number( $phone ) .
					'">' . $phone . '</a>';
		}
	}
	else {
	
		$phone = val( 'banner.show.phone' . $num_phone );

		if ( $phone ) {
			$str .=	'<a class="phone" href="tel:' . 
					Utils :: phone_number( $phone ) .
					'">' . $phone . '</a>' . $postfix;
		}

	}
	
	echo $str;