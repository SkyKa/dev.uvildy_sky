<?php

    $alias = Utils :: getVar( 'alias' );
	$items = $data -> getItems( $menu -> id );
    $uri_orig = $_SERVER[ 'REQUEST_URI' ];
    $uris = explode( '?', $uri_orig );
    $uri = $uris[ 0 ];

    if( count($items) ) {
        echo '<li>
                <div class="socials">
                    <span>Мы в соцсетях: </span>
					<br />';
    }

    foreach( $items as $row )
    {
        if ( $row[ 'visible' ] != 1 ) continue;
        echo '<a href="'. $row[ 'type_link' ] .'" ' . $row[ 'attr' ] . '><img src="/'. $row[ 'img_src' ] .'" alt=""></a>';
    }

    echo '</div>
        </li>';