<div class="booking-step2-page booking-page">

	<div class="container">
		<h1>Онлайн бронирование номера</h1>
		<p class="step">Шаг <span>2</span> из <span>3</span></p>

		<div class="modal fade room-modal" id="room-modal" tabindex="-1" role="dialog" aria-labelledby="room-modalLabel" aria-hidden="true">
			<div class="modal-dialog">
				<div class="modal-content">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
					<div class="modal-body">
						<div id="preloader-ajax">
							<div id="preloader-ajax-center">
								<div id="preloader-ajax-center-absolute">
									<div id="preloader-ajax-center-absolute-one">
										<div class="preloader-ajax-object-one"></div>
										<div class="preloader-ajax-object-one"></div>
										<div class="preloader-ajax-object-one"></div>
										<div class="preloader-ajax-object-one"></div>
										<div class="preloader-ajax-object-one"></div>
										<div class="preloader-ajax-object-one"></div>
									</div>
									<div id="preloader-ajax-center-absolute-two">
										<div class="preloader-ajax-object-two"></div>
										<div class="preloader-ajax-object-two"></div>
										<div class="preloader-ajax-object-two"></div>
										<div class="preloader-ajax-object-two"></div>
										<div class="preloader-ajax-object-two"></div>
										<div class="preloader-ajax-object-two"></div>
									</div>
								</div>
							</div>
						</div>
						<div class="room-content-wrap">
							
						</div>
					</div>
				</div>
			</div>
		</div>
		
		<form action="">
		
			<?php mod('catalog.action.rooms') ?>
			
			<h2>ВВЕДИТЕ ДАТУ ПРОЖИВАНИЯ</h2>
			<div class="dates">
				<div class="from-to">
					<label for="from">Дата заезда</label>
					<input type="text" id="from" name="from" class="form-control">
				</div>
				<div class="from-to">
					<label for="to">Дата отъезда</label>
					<input type="text" id="to" name="to" class="form-control">
				</div>
			</div>

			<div class="add-person-form-wrap">
				<div class="add-person-form clearfix">
					<div class="old">
						<h2>
							Возраст проживающего
							<p>(полных лет)</p>
						</h2>
						<input type="text" class="form-control">
					</div>
					<div class="select-programm-wrap">
						<h2>Выберите программу</h2>
						<div class="custom-radio">
							<div class="form-group">
								<input type="radio" id="prog1" name="radio" checked>
							    <label for="prog1">
							    	<div class="square"></div>
							    </label>
							    <a href="" target="_blank" class="title">Общая программа</a>
							</div>

							<div class="form-group">
							    <input type="radio" id="prog2" name="radio">
							    <label for="prog2">
							    	<div class="square"></div>
							    </label>
							    <a href="" target="_blank" class="title">Гинекология</a>
							</div>
							
							<div class="form-group">
								<input type="radio" id="prog3" name="radio">
								<label for="prog3">
							    	<div class="square"></div>
							    </label>
							    <a href="" target="_blank" class="title">Клиника мозга</a>
							</div>
						</div>
					</div>
					<div class="select-during clearfix">
						<h2>Выберите продолжительность курса</h2>
						<div class="custom-radio">
						    <input type="radio" id="during1" name="radio-during" checked><!-- 
						     --><label for="during1">7</label><span class="days">дней</span><br>

						    <input type="radio" id="during2" name="radio-during"><label for="during2">10</label><!-- 
						     --><span class="days">дней</span><br>

							<input type="radio" id="during3" name="radio-during"><label for="during3">14</label><!-- 
							 --><span class="days">дней</span><br>

							<input type="radio" id="during4" name="radio-during"><label for="during4">21</label><!-- 
							 --><span class="days">дней</span>
						</div>
					</div>
				</div>
				<div class="basic-button big"><button>сохранить</button></div>
				<div class="basic-button big gray"><button>очистить</button></div>
			</div>
		</form>
	</div>

	<div class="container">

		<div class="row added-humans">
			<div class="col-md-4 col-sm-6 item">
				<img class="icon" src="/static/img/adult_icon.png" alt="">
				<h2>1 взрослый</h2>
				<p class="character">Программа: <span>Клиника мозга</span></p>
				<p class="character">Продолжительность курса: <span>21 день</span></p>
				<button type="button" class="delete"><span>Удалить</span></button>
			</div>
			<div class="col-md-4 col-sm-6 item">
				<img class="icon" src="/static/img/child_icon.png" alt="">
				<h2>1 детский <span>(6-12 лет)</span></h2>
				<p class="character">Программа: <span>Клиника мозга</span></p>
				<p class="character">Продолжительность курса: <span>21 день</span></p>
				<button type="button" class="delete"><span>Удалить</span></button>
			</div>
			<div class="col-md-4 col-sm-6 item">
				<img class="icon" src="/static/img/small_child_icon.png" alt="">
				<h2>1 детский <span>(3-5 лет)</span></h2>
				<button type="button" class="delete"><span>Удалить</span></button>
			</div>
		</div>

		<div class="add-man basic-button big"><button type="button">добавить человека</button></div>
	</div>
	
	<div class="container indicative-cost-wrap">
		<div class="indicative-cost">
			<div class="basic-button big"><a href="">далее</a></div>
			<p class="title">Ориентировочная стоимость проживания: <span>15 000</span> <span class="rouble">a</span></p>
			<div class="clearfix"></div>
			<p class="description">
				Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quasi numquam eum distinctio aliquid. Accusantium consectetur, possimus odio! Aut nam adipisci incidunt itaque, iusto, magni. Ratione ipsam, voluptatum in delectus nam velit atque praesentium ducimus vel veniam sapiente cumque est itaque reiciendis deleniti non repudiandae soluta quae commodi minus cum officiis. Atque in accusantium soluta porro, pariatur quaerat quo corporis? Delectus, quia, aspernatur.
			</p>
		</div>
	</div>
</div>


<div class="booking-step3-page booking-page">
	<div class="container">
		<h1>Онлайн бронирование номера</h1>
		<p class="step">Шаг <span>3</span> из <span>3</span></p>
	</div>

	<div class="selected-booking">
		<div class="h2-wrap">
			<h2>Вы выбрали</h2>
		</div>
		<div class="container">
			<p class="selected-item cost"><b>Стоимость: </b>от 2000 <span class="rouble">c</span> в сутки</p>
			<p class="selected-item room"><b>Номер: </b>Стандарт 1-местный, 1-комнатный</p>
			<p class="selected-item date"><b>Дата проживания: </b>24.07.2016 — 24.08.2016</p>

			<div class="row number-person">
				<div class="col-md-4 col-sm-6 item">
					<h2>1 взрослый</h2>
					<p class="character">Программа: <span>Клиника мозга</span></p>
					<p class="character">Продолжительность курса: <span>21 день</span></p>
				</div>
				<div class="col-md-4 col-sm-6 item">
					<h2>1 детский <span>(6-12 лет)</span></h2>
					<p class="character">Программа: <span>Клиника мозга</span></p>
					<p class="character">Продолжительность курса: <span>21 день</span></p>
				</div>
				<div class="col-md-4 col-sm-6 item">
					<h2>1 детский <span>(3-5 лет)</span></h2>
				</div>
			</div>
			<div class="indicative-cost-block">
				<p>Ориентировочная стоимость проживания:&nbsp; <span>15 000 </span><span class="rouble">c</span></p>
			</div>
		</div>

		<div class="formalize-apply">
			<div class="container">
				<form action="">
					<div class="row">
						<div class="col-md-6">
							<div class="form-group">
								<input type="text" class="form-control">
							</div>
							<div class="form-group">
								<input type="text" class="form-control">
							</div>
							<div class="form-group">
								<input type="text" class="form-control">
							</div>
						</div>
						<div class="col-md-6">
							<div class="form-group">
								<textarea class="form-control"></textarea>
							</div>
						</div>
					</div>
					<div class="basic-button big">
						<button>Отправить</button>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>