<?php

    $alias = Utils :: getVar( 'alias' );
	$items = $data -> getItems( $menu -> id );
    $uri_orig = $_SERVER[ 'REQUEST_URI' ];
    $uris = explode( '?', $uri_orig );
    $uri = $uris[ 0 ];

	echo "<ul>";

    foreach( $items as $row )
    {

        if ( $row[ 'visible' ] != 1 ) continue;
        $href = SF . $row[ 'type_link' ];
        $class = "";
        if ( $uri == $href || main_mobile_active_razdel( $row[ 'type_id' ], $alias ) ) {
            $class = 'selected';
        }

        $subitems = $data -> getItems( $menu -> id, $row[ 'id' ] );
		
		echo "<li class='et-link ". $class ."'>";

		echo "<a href='" . $row[ 'type_link' ] . "'>" . $row[ 'title' ] . "</a>";

        echo '</li>';
    }

	echo "</ul>";

	function main_mobile_active_razdel( $tid, $alias ) {
		$table = new Table( 'pages' );
		$rows = $table -> select( "SELECT `id` FROM `pages` WHERE `alias`=:alias AND (`id`=:tid OR `parent_id`=:tid) LIMIT 1",
			array( 'tid' => $tid, 'alias' => $alias ) );

		if ( count( $rows ) > 0 ) return true;
		else return false;
	}
