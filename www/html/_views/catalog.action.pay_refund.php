<?php

	$refund_url = Registry :: __instance() -> site_name;
	$refund_url = 'http://' . $refund_url;
	$refund_url .= $_SERVER[ 'REQUEST_URI' ];
	
	$pay = Registry :: __instance() -> pay;
	$refund_order = $pay -> get_order( array( 'pay_refund_url' => $refund_url ) );
	
	if ( !count( $refund_order ) ) return false;
	$refund_order = end( $refund_order );
	
	$pay_order_id = $refund_order[ 'pay_order_id' ];
	$amount = $refund_order[ 'summ' ] * 100;

	if ( !$refund_order[ 'pay_refund_message' ] ) {
		$pay_refund = $pay -> refund( $pay_order_id, $amount );
		if ( $pay_refund[ 'errorMessage' ] ) {
			echo $pay_refund[ 'errorMessage' ];
		}
	}
	else {
		echo $refund_order[ 'pay_refund_message' ];
	}

	echo '<p>&nbsp;</p>';

