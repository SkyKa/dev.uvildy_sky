<div class="paging-wrapper">
	<ul class="paging">
    <?php


    $url = Utils :: getGET( 'uri' ) . '.html';

    for ( $i = 1; $i <= $args -> count; $i++ ) {
        /** если всего 1на страница, выходим из цикла */
        if ( $args->count == 1 ) break;

        $href = $i === 1 ? $url : $url . '?page=' . $i;

        if ( $args->page == $i ) {
            echo '<li class="active"><a href="' . $href . '">' . $i . '</a></li>';
        }
        else {
            echo '<li><a href="' . $href . '">' . $i . '</a></li>';
        }

    }

    ?>
		<div class="clearfix"></div>
	</ul>
</div>
