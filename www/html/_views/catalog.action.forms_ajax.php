<?php

	sleep( 1 );

	$json = array(	's' => false,
							'err' => array( ),
							'mess' => false,
							'action' => false );

	header( 'content-type: application/json; charset=utf-8' );

	$form_act = Utils :: getVar( 'form_act' );
	$form_alias = Utils :: getVar( 'form_alias' );

	// не указаны параметры
	if ( !$form_act || !$form_alias ) {
		$json[ 'mess' ] = 'Ошибка #' . __LINE__;
		echo json_encode( $json );
		exit( );
	}

	
	
	$form = Form :: getInstance();
	$form -> setRows( $form_alias );
	if ( $form -> errorInfo ) {
		$json[ 'err' ] = $form -> errorInfo;
		$json[ 'mess' ] = 'Ошибка #' . __LINE__;
		echo json_encode( $json );
		exit( );
	}

	
	$table = new Table( 'catalog_section' );
	// нет формы
	$section_form = $table -> select( "SELECT `t2`.`title`,`t2`.`position`,`t2`.`alias`,`t1`.* 
	FROM `section_forms` as `t1` LEFT JOIN `catalog_section` AS `t2` ON `t1`.`id`=`t2`.`id` 
	WHERE `t2`.`alias`=:alias && `t2`.`section_table`='section_forms' && `t2`.`position_table`='position_forms' &&
	(`t1`.`method`='ajax-post' || `t1`.`method`='ajax-get') ORDER BY `t2`.`position`",
	array( 'alias' => $form_alias ) );

	if ( !count( $section_form ) ) {
		$json[ 'mess' ] = 'Ошибка #' . __LINE__;
		echo json_encode( $json );
		exit( );
	}

	$fields = $table -> select( "SELECT * FROM `position_forms` WHERE `section_id`=:sid ORDER BY `position`", array( 'sid' => $section_form[ 0 ][ 'id' ] ) );


	// нет полей формы
	if ( !count( $fields ) ) {
		$json[ 'mess' ] = 'Ошибка #' . __LINE__;
		echo json_encode( $json );
		exit( );
	}

	$arr_err = array( );
	$arr_req = array( );
	$pwd = null;

	foreach ( $fields as $field ) {

		$arr_req[ $field[ 'nameid' ] ] = Utils :: getVar( $field[ 'nameid' ] );
		$arr_req[ $field[ 'nameid' ] ] = trim( $arr_req[ $field[ 'nameid' ] ] );
		
		if ( ( !$arr_req[ $field[ 'nameid' ] ] || $arr_req[ $field[ 'nameid' ] ] == 'false' ) && $field[ 'valid_empty' ] ) {
			$arr_err[ $field[ 'nameid' ] ] = 'Не заполнено обязательное поле';
		}
		if ( $field[ 'valid_email' ] && !preg_match( "/[0-9a-zA-Z_\-]+@[0-9a-zA-Z_\.\-]+\.[a-zA-Z_]{2,3}/i", $arr_req[ $field[ 'nameid' ] ] ) ) {
			$arr_err[ $field[ 'nameid' ] ] = 'Неверный формат';
		}

		// валидация паролей при регистрации
		if ( $form_act == 'register' ) {
			if ( $field[ 'type_id' ] == 'pwd' ) {
				// пароль
				if ( !$pwd ) {
					if ( mb_strlen( $arr_req[ $field[ 'nameid' ] ] ) < 5 ) {
						$arr_err[ $field[ 'nameid' ] ] = 'Пароль не должен быть менее 5ти символов';
					}
					$pwd = $arr_req[ $field[ 'nameid' ] ];
				}
				// подтверждение пароля
				else {
					if ( $pwd != $arr_req[ $field[ 'nameid' ] ] ) {
						$arr_err[ $field[ 'nameid' ] ] = 'Пароль и подтверждение пароля не совпадают';
					}
				}
			}
		}
		// валидация паролей при авторизации
		// ...

		// валидация паролей при смене пароля
		// ...


	}

	// есть ошибки
	if ( count( $arr_err ) ) {
		$json[ 'err' ] = $arr_err;
		$json[ 'mess' ] = 'Ошибка #' . __LINE__;
		echo json_encode( $json );
		exit( );
	}

	$section_form = $form -> getSection( );

	Registry :: __instance( ) -> section_form = $section_form;

	$smsru = new Smsru( );
	
	// нет ошибок
	switch ( $form_act ) {

		// регистрация
		case 'register':

			$auth = Auth_gup :: getInstance( );

			//////////////////////////
			// ONLY GUP
			//////////////////////////

			// login_lizing
			// login_buy
			// login_sell

			if ( strpos( $form_alias, 'reg_partners_lizing' ) !== FALSE ) {

				if ( $form_alias == 'reg_partners_lizing_your' ) $arr_req[ 'company_type' ] = 'your';
				if ( $form_alias == 'reg_partners_lizing_ip' ) $arr_req[ 'company_type' ] = 'ip';
				if ( $form_alias == 'reg_partners_lizing_fiz' ) $arr_req[ 'company_type' ] = 'fiz';

				$arr_req[ 'status' ] = 'login_lizing';
			}
			else if ( $form_alias == 'reg_partners' ) {
				if ( $arr_req[ 'type' ] == 'Покупатель' ) {
					$arr_req[ 'status' ] = 'login_buy';
				}
				else {
					$arr_req[ 'status' ] = 'login_sell';
				}
			}
			else {
				$arr_req[ 'status' ] = 'login_sell';
			}

			//////////////////////////
			//////////////////////////

			$register = $auth -> register( $arr_req );
			// ошибка регистрации
			if ( !$register ) {
				$json[ 'err' ] = $auth -> err;
				echo json_encode( $json );
				exit( );
			}
			// все ок
			else {
				$json[ 's' ] = true;
				$json[ 'mess' ] = $auth -> mess;
			}

		break;

		// авторизация
		case 'login':
			$auth = Auth_gup :: getInstance( );
			$login = $auth -> login( $arr_req );
			if ( !$login ) {
				$json[ 'err' ] = $auth -> err;
				echo json_encode( $json );
				exit( );
			}
			else {
				$json[ 's' ] = true;
				$json[ 'mess' ] = $auth -> mess;
				$json[ 'action' ][ 'location' ] = 'self';
			}

		break;

		// выход
		case 'logout':

			$auth = Auth_gup :: getInstance( );
			$auth -> logout( );
			$json[ 's' ] = true;
			$json[ 'action' ][ 'location' ] = 'self';

		break;

		// воостановить пароль
		case 'restore_pass':

			//$auth = Auth_gup :: login( );
		break;


		case 'booking':

			$name = Utils :: getVar( 'name' );
			$email = Utils :: getVar( 'email' );
			$phone = Utils :: getVar( 'phone' );
			$comment = Utils :: getVar( 'comment' );

			$table = new Table('catalog_section');

			$start_date = date('d.m.Y', strtotime($_SESSION['booking']['start_date']));
			$end_date = date('d.m.Y', strtotime( "+" . $_SESSION['booking']['duration'] . " days" , strtotime($_SESSION['booking']['start_date'])));

			$period = $start_date . '-' . $end_date;

			$programm_title = 'без лечения';

			if($_SESSION['booking']['programm'] != 'without-programm') {

			    $programm = $table->select('SELECT `id`, `title` FROM `catalog_section` WHERE `id`=:id LIMIT 1', array( 'id' => $_SESSION['booking']['programm'] ) );

			    if( !empty( $programm ) ) {
			    	$programm_title = $programm[0]['title'];
			    }
			}

			$room = $table -> select('SELECT `catalog_section`.`id`, `catalog_section`.`parent_id`, `catalog_section`.`title`,
				`section_offers`.`n_rooms`, `section_offers`.`n_main_place`, `section_offers`.`n_additional_place` 
				FROM `catalog_section` 
				INNER JOIN 
					`section_offers` 
				ON
					`section_offers`.`id`=`catalog_section`.`id`
				WHERE `catalog_section`.`id`=:id LIMIT 1', array( 'id' => $_SESSION['booking']['number'] ));

			$class_room_title = '';
			$class_room_id = '';

			if( !empty( $room ) ) {
				$class_room = $table -> select('SELECT `catalog_section`.`id`, `catalog_section`.`title`, `section_number_class`.`id_1c` FROM `catalog_section` 
					INNER JOIN `section_number_class` 
					ON (`section_number_class`.`id`=`catalog_section`.`id`)
					WHERE `catalog_section`.`id`=:id LIMIT 1', array( 'id' => $room[0]['parent_id'] ));

				$class_room_title = $class_room[0]['title'];
				$class_room_id = $class_room[0]['id_1c'];
			}

			$n_rooms = $room[0]['n_rooms'];
			$n_main_place = $room[0]['n_main_place'];
			$n_additional_place = $room[0]['n_additional_place'];

			$class_number_all_number = $table -> select('SELECT `section_number_class`.`id` FROM `section_number_class` INNER JOIN `catalog_section` WHERE `catalog_section`.`parent_id`=806 AND `section_number_class`.`id_1c`=:id_1c LIMIT 1', 
				array( 'id_1c' => $class_room_id ));

			$number = array();

			$num_clients = count($_SESSION[ 'booking' ][ 'persons' ]);

			$n_rooms_sql = '';

			if( ($n_rooms === null) || ($n_rooms == 0) ) {
				$n_rooms_sql = '`section_rooms`.`n_rooms` IS NULL';
			}
			else {
				$n_rooms_sql = '`section_rooms`.`n_rooms`=' . $n_rooms;
			}

			$start_date_param = $_SESSION['booking']['start_date'];
			$end_date_param = date('Y-m-d', strtotime( "+" . $_SESSION['booking']['duration'] . " days" , strtotime($_SESSION['booking']['start_date'])));

		// Ищем только номера с возможностью подселения
			$number = $table -> select('SELECT `section_rooms`.`id`, `section_rooms`.`id_1c`, `catalog_section`.`title`
				FROM `section_rooms`
				INNER JOIN `catalog_section` 
				ON (`section_rooms`.`id`=`catalog_section`.`id`)
				WHERE `catalog_section`.`parent_id`=:id AND
					`section_rooms`.`n_main_place`=:n_main_place AND
					`section_rooms`.`n_additional_place`=:n_additional_place AND
					`section_rooms`.`busy` IS NULL AND
					'. $n_rooms_sql .' AND
					((
						SELECT COUNT(*)
						FROM `position_booking_rooms` 
						WHERE 
						`position_booking_rooms`.`section_id`=`section_rooms`.`id` AND
						(
							( DATE(:select_date) >= DATE(FROM_UNIXTIME(`start_date`)) AND DATE(:select_date) < DATE(FROM_UNIXTIME(`end_date`)) ) OR
							( DATE(:end_date) > DATE(FROM_UNIXTIME(`start_date`)) AND DATE(:end_date) <= DATE(FROM_UNIXTIME(`end_date`)) ) OR
							( DATE(:select_date) < DATE(FROM_UNIXTIME(`start_date`)) AND DATE(:end_date) > DATE(FROM_UNIXTIME(`end_date`)) )
						)
					) > 0
					OR 
					IFNULL ((
						SELECT SUM(`num_clients`) 
						FROM `position_orders` 
						WHERE 
						`position_orders`.`num_1cid`=`section_rooms`.`id_1c` AND
						`position_orders`.`in1c`=0 AND
						(
							( DATE(:select_date) >= DATE(FROM_UNIXTIME(`start_date`)) AND DATE(:select_date) < DATE(FROM_UNIXTIME(`end_date`)) ) OR
							( DATE(:end_date) > DATE(FROM_UNIXTIME(`start_date`)) AND DATE(:end_date) <= DATE(FROM_UNIXTIME(`end_date`)) ) OR
							( DATE(:select_date) < DATE(FROM_UNIXTIME(`start_date`)) AND DATE(:end_date) > DATE(FROM_UNIXTIME(`end_date`)) )
						) AND
						((`position_orders`.`pay_status`=2 AND `position_orders`.`pay_refund_amount` IS NOT NULL) OR (TIMESTAMPDIFF(SECOND, `position_orders`.`date_create`, now()) <= 7200) OR (`position_orders`.`busy`))
						GROUP BY `position_orders`.`num_1cid`
					), 0) > 0) AND
					((
						SELECT COUNT(*)
						FROM `position_booking_rooms` 
						WHERE 
						`position_booking_rooms`.`section_id`=`section_rooms`.`id` AND
						(
							( DATE(:select_date) >= DATE(FROM_UNIXTIME(`start_date`)) AND DATE(:select_date) < DATE(FROM_UNIXTIME(`end_date`)) ) OR
							( DATE(:end_date) > DATE(FROM_UNIXTIME(`start_date`)) AND DATE(:end_date) <= DATE(FROM_UNIXTIME(`end_date`)) ) OR
							( DATE(:select_date) < DATE(FROM_UNIXTIME(`start_date`)) AND DATE(:end_date) > DATE(FROM_UNIXTIME(`end_date`)) )
						)
					)
					+
					IFNULL ((
						SELECT SUM(`num_clients`) 
						FROM `position_orders` 
						WHERE 
						`position_orders`.`num_1cid`=`section_rooms`.`id_1c` AND
						`position_orders`.`in1c`=0 AND
						(
							( DATE(:select_date) >= DATE(FROM_UNIXTIME(`start_date`)) AND DATE(:select_date) < DATE(FROM_UNIXTIME(`end_date`)) ) OR
							( DATE(:end_date) > DATE(FROM_UNIXTIME(`start_date`)) AND DATE(:end_date) <= DATE(FROM_UNIXTIME(`end_date`)) ) OR
							( DATE(:select_date) < DATE(FROM_UNIXTIME(`start_date`)) AND DATE(:end_date) > DATE(FROM_UNIXTIME(`end_date`)) )
						) AND
						((`position_orders`.`pay_status`=2 AND `position_orders`.`pay_refund_amount` IS NOT NULL) OR (TIMESTAMPDIFF(SECOND, `position_orders`.`date_create`, now()) <= 7200) OR (`position_orders`.`busy`))
						GROUP BY `position_orders`.`num_1cid`
					), 0) + '. $num_clients .' <= '. $n_main_place .')
					LIMIT 1',
				array( 'id' => $class_number_all_number[0]['id'], 'n_main_place' => $n_main_place, 'n_additional_place' => $n_additional_place,
					'select_date' => $start_date_param, 'end_date' => $end_date_param ));

		// Если не нашли с подселением, то ищем другие свободные номера
			if( empty( $number ) ) {
				$number = $table -> select('SELECT `section_rooms`.`id`, `section_rooms`.`id_1c`, `catalog_section`.`title`
					FROM `section_rooms`
					INNER JOIN `catalog_section` 
					ON (`section_rooms`.`id`=`catalog_section`.`id`)
					WHERE `catalog_section`.`parent_id`=:id AND
						`section_rooms`.`n_main_place`=:n_main_place AND
						`section_rooms`.`n_additional_place`=:n_additional_place AND
						`section_rooms`.`busy` IS NULL AND
						'. $n_rooms_sql .' AND
						((
							SELECT COUNT(*)
							FROM `position_booking_rooms` 
							WHERE 
							`position_booking_rooms`.`section_id`=`section_rooms`.`id` AND
							(
								( DATE(:select_date) >= DATE(FROM_UNIXTIME(`start_date`)) AND DATE(:select_date) < DATE(FROM_UNIXTIME(`end_date`)) ) OR
								( DATE(:end_date) > DATE(FROM_UNIXTIME(`start_date`)) AND DATE(:end_date) <= DATE(FROM_UNIXTIME(`end_date`)) ) OR
								( DATE(:select_date) < DATE(FROM_UNIXTIME(`start_date`)) AND DATE(:end_date) > DATE(FROM_UNIXTIME(`end_date`)) )
							)
						) = 0
						OR 
						IFNULL ((
							SELECT SUM(`num_clients`) 
							FROM `position_orders` 
							WHERE 
							`position_orders`.`num_1cid`=`section_rooms`.`id_1c` AND
							`position_orders`.`in1c`=0 AND
							(
								( DATE(:select_date) >= DATE(FROM_UNIXTIME(`start_date`)) AND DATE(:select_date) < DATE(FROM_UNIXTIME(`end_date`)) ) OR
								( DATE(:end_date) > DATE(FROM_UNIXTIME(`start_date`)) AND DATE(:end_date) <= DATE(FROM_UNIXTIME(`end_date`)) ) OR
								( DATE(:select_date) < DATE(FROM_UNIXTIME(`start_date`)) AND DATE(:end_date) > DATE(FROM_UNIXTIME(`end_date`)) )
							) AND
							((`position_orders`.`pay_status`=2 AND `position_orders`.`pay_refund_amount` IS NOT NULL) OR (TIMESTAMPDIFF(SECOND, `position_orders`.`date_create`, now()) <= 7200) OR (`position_orders`.`busy`))
							GROUP BY `position_orders`.`num_1cid`
						), 0) = 0)
						LIMIT 1',
					array( 'id' => $class_number_all_number[0]['id'], 'n_main_place' => $n_main_place, 'n_additional_place' => $n_additional_place,
						'select_date' => $start_date_param, 'end_date' => $end_date_param ));
			}

		// Не нашли ни одного номера
			if( empty( $number ) ) {
				$json[ 'general_err' ] = 'Извините, не найдено свободного номера. Вы можете выбрать номер с другими параметрами или позвонить нам!';
				echo json_encode( $json );
				exit();
			}
				
			$number = end( $number );

			$number_title = $number[ 'title' ];
			$number_id = $number[ 'id_1c' ];
			
			$sharing = 0;
			if( ($n_main_place + $n_additional_place) < $_SESSION['booking']['persons'] ) {
				$sharing = 1;
			}

			$sum = $_SESSION['booking']['sum'];

			foreach( $_SESSION['booking']['persons'] as $key => $person ) {
				$age = $table -> select('SELECT `id`, `age_id_1c` FROM `section_number_prices` WHERE `age`=:age LIMIT 1', array( 'age' => $person['age'] ) );

				$_SESSION['booking']['persons'][$key]['age_id'] = $age[0]['age_id_1c'];
			}
			
			$price = $sum / $_SESSION['booking']['duration'];

			$clients = json_encode( $_SESSION[ 'booking' ][ 'persons' ] );

			$stamp1 = md5( microtime() . var_export( $_SESSION, true ) . '1' );
			$stamp2 = md5( microtime() . var_export( $_SESSION, true ) . '2' );

			$section_id = 1236;


			$insert_row = array(	'stamp' => $stamp1,
									'duration' => $_SESSION[ 'booking' ][ 'duration' ],
									'start_date' => strtotime($start_date_param),
									'end_date' => strtotime($end_date_param),
									'section_id' => $section_id,
									'period' => $period,
									'programm' => $programm_title,
									'num_1cid' => $number_id,
									'num_type' => $class_room_title,
									'num_rooms' => $n_rooms,
									'num_place' => $n_main_place,
									'num_dopplace' => $n_additional_place,
									'num_title' => $number_title,
									'num_clients' => $num_clients,
									'price' => $price,
									'summ' => $sum,
									'sharing' => $sharing,
									'client_name' => $name,
									'client_email' => $email,
									'client_phone' => $phone,
									'client_comm' => $comment,
									'clients' => $clients );

			$table -> execute( 'INSERT INTO `position_orders` (`stamp`, `section_id`, `duration`, `start_date`, `end_date`, `date_create`, `period`, `programm`, `num_1cid`, `num_type`, `num_rooms`, `num_place`, `num_dopplace`, `num_title`, `num_clients`, `price`, `summ`, `sharing`, `client_name`, `client_email`, `client_phone`, `client_comm`, `clients`)
			VALUES (:stamp, :section_id, :duration, :start_date, :end_date, now(), :period, :programm, :num_1cid, :num_type, :num_rooms, :num_place, :num_dopplace, :num_title, :num_clients, :price, :summ, :sharing, :client_name, :client_email, :client_phone, :client_comm, :clients)',
			$insert_row );

			$error_info = $table -> errorInfo;

			$order1 = array();

			if ( !$error_info ) {

				$order1 = $table -> select( 'SELECT `id` FROM `position_orders` WHERE `stamp`=:stamp LIMIT 1', array( 'stamp' => $stamp1 ) );
				$order1 = end( $order1 );
				$order1_id = $order1[ 'id' ];

				$description =	'Бронирование номера: ' . 
										$class_room_title . ', ' . $n_rooms . 'к, ' .
										$n_main_place . 'м + ' . $n_additional_place . 'доп ' .
										( count( $_SESSION[ 'booking' ][ 'persons' ] ) ) .
										'ч., период ' . $period;
				$json[ 'order1_id' ] = $order1_id;
				$pay = Registry :: __instance() -> pay;
				$pay_register = $pay -> register( $order1_id, ( $sum * 100 ), $description, $clients );
				//$pay_register = true;
				$json[ 'pay_register' ] = $pay_register;

				// ЗАПРОС В ЭКВАЙРИНГ
				if ( $pay_register ) {

					if ( isset( $pay_register[ 'errorCode' ] ) && $pay_register[ 'errorCode' ] ) {
						$json[ 'general_err' ] = 'Ошибка #' . __LINE__ . ', ' . $pay_register[ 'errorMessage' ] . ', попробуйте повторить запрос через 30 мин.';
						echo json_encode( $json );
						exit( );
					}
					else {

						// записываем в сессию данные созданного заказа
						$order1 = $pay -> get_order( array( 'id' => $order1_id ) );
						$order1 = end( $order1 );
						$_SESSION[ 'orders' ][ $stamp1 ] = $order1;

						$json[ 'errorInfo' ] = $table -> errorInfo;
						$json[ 'orders' ] = $_SESSION[ 'orders' ];

						$arr_req[ 'order.id' ] = $order1[ 'id' ];
						$arr_req[ 'order.pay.id' ] = $order1[ 'pay_order_id' ];
						$arr_req[ 'order.pay.url' ] = $order1[ 'pay_form_url' ];
						// $arr_req[ 'order.pay.url_link' ] = $order1[ 'pay_form_url' ];
						$arr_req[ 'order.period' ] = $order1[ 'period' ];
						$arr_req[ 'order.programm' ] = $order1[ 'programm' ];
						$arr_req[ 'order.num.type' ] = $order1[ 'num_type' ];
						$arr_req[ 'order.num.rooms' ] = $order1[ 'num_rooms' ];
						$arr_req[ 'order.num.place' ] = $order1[ 'num_place' ];
						$arr_req[ 'order.num.dopplace' ] = $order1[ 'num_dopplace' ];
						$arr_req[ 'order.num.title' ] = $order1[ 'num_title' ];
						$arr_req[ 'order.price' ] = $order1[ 'price' ];
						$arr_req[ 'order.summ' ] = $order1[ 'summ' ];
						$arr_req[ 'order.persons.count' ] = count( $_SESSION[ 'booking' ][ 'persons' ] );

						$arr_req[ 'order.pay.link_text' ] = 'Ваша ссылка для оплаты бронирования';

						$arr_req[ 'order.persons' ] = '
							<table border="1" style="border-collapse: collapse; border-color: #e9e9e9;" border-color="#e9e9e9" width="100%">
								<tr bgcolor="#e9e9e9">
									<td style="padding: 5px; font-weight: 600;color: #4f4f4f;">
										Возраст
									</td>
									<td style="padding: 5px; font-weight: 600;color: #4f4f4f;">
										Программа
									</td>
									<td style="padding: 5px; font-weight: 600;color: #4f4f4f;">
										Питание
									</td>
									<td style="padding: 5px; font-weight: 600;color: #4f4f4f;">
										Дни
									</td>
									<td style="padding: 5px; font-weight: 600;color: #4f4f4f;">
										Стоимость
									</td>
									<td style="padding: 5px; font-weight: 600;color: #4f4f4f;">
										Сумма
									</td>
								</tr>';

						unset( $person );

						foreach ( $_SESSION[ 'booking' ][ 'persons' ] as $person ) {

							$style = 'style="padding: 5px; border-bottom: 0;color: #4f4f4f;"';

							$cost_by_day_text = '';
							$cost_text = '';

							if( $person[ 'price' ] == 0 ) {
								$cost_by_day_text = 'рассчитывается индивидуально';
								$cost_text = 'рассчитывается индивидуально';
							}
							else {
								$cost_by_day_text = Utils :: price( $person[ 'price' ] / $_SESSION[ 'booking' ][ 'duration' ] ) . ' руб.';
								$cost_text = Utils :: price( $person[ 'price' ] ) . ' руб.';
							}

							$programm_text = $programm_title;
							if( ($programm_text == 'Детская программа') && ( $person['age'] == 'взрослый' ) ) {
								$programm_text = 'Общее оздоровление';
							}

							$arr_req[ 'order.persons' ] .= '
									<tr>
										<td ' . $style . '>' . $person[ 'age' ] . '</td>
										<td ' . $style . '>' . $programm_text . '</td>
										<td ' . $style . '>' . $person[ 'food' ] . '</td>
										<td ' . $style . ' align="center">' . $_SESSION[ 'booking' ][ 'duration' ] . '</td>
										<td ' . $style . '>' . $cost_by_day_text . '</td>
										<td ' . $style . '>' . $cost_text . '</td>
									</tr>';
						}

						$arr_req[ 'order.pay.button' ] = '<table bgcolor="white" width="600px">
															<tbody>
																<tr>
																	<td>
																	<table>
																		<tbody>
																			<tr>
																				<td width="65px"></td>
																				<td align="center" style="padding: 15px 0 45px 0;" width="100%"><a href="'. $arr_req[ 'order.pay.url' ] .'" style="background: #0374B9; display: inline-block; padding: 15px 35px; color: #fff; text-decoration: none; text-transform: uppercase;">Оплатить</a></td>
																			</tr>
																		</tbody>
																	</table>
																	</td>
																</tr>
															</tbody>
														</table>';

						$arr_req[ 'order.persons' ] .= '</table>';

					// Отправляем письмо клиенту
						$json = send_emails( $json, $email, $section_form, $arr_req, $arr_err );

					// Отправляем письмо администраторам 
						$arr_req[ 'order.pay.link_text' ] = 'Ссылка для оплаты бронирования';
						$arr_req[ 'order.pay.button' ] = '';
						$json = send_emails( $json, '', $section_form, $arr_req, $arr_err, true );
						$_SESSION[ 'order_booking' ][ $stamp1 ] = $_SESSION[ 'booking' ];
						unset( $_SESSION[ 'booking' ] );

					}
				}
				// ЭКВАЙРИНГ СЕРВИС НЕДОСТУПЕН !!!
				else {
					$json[ 'general_err' ] = 'Онлайн платежи временно недоступны, попробуйте повторить запрос через 30 мин.';
					echo json_encode( $json );
					exit( );
				}

			}
			// Ошибка создания заказа
			else {
				$json[ 'general_err' ] = 'Ошибка #' . __LINE__ . ', попробуйте повторить запрос через 30 мин.';
				echo json_encode( $json );
				exit( );
			}
/*

Есть в товарных предложениях, но нет в нумерном фонде:
Стандарт -> Без программы -> 1-комнатный, осн. мест: 2, доп. мест: 0

---

SELECT `catalog_section`.`id`, `catalog_section`.`title`, `section_rooms`.`id_1c` 
FROM `catalog_section` 
INNER JOIN `section_rooms`
ON (`catalog_section`.`id`=`section_rooms`.`id`)
WHERE 
`catalog_section`.`parent_id`="808" AND
`section_rooms`.`n_rooms`="1" AND
`section_rooms`.`n_main_place`="2" AND
`section_rooms`.`n_additional_place`="0"
LIMIT 1;

*/			if( !empty($order1[ 'pay_form_url' ]) ) {
				$json['action']['pay'] = $order1[ 'pay_form_url' ];
			}

			////////////////////////////////// SMS
			$smsmess = 'Бронь: ' . $order1[ 'summ' ] . " р.\n" . $name . "\n" . $phone;
			//$smsru -> send( 89123238494, $smsmess );
			//$smsru -> send( 89193333327, $smsmess );
			//////////////////////////////////
			
			echo json_encode( $json );

			break;

		// Сохранить в базе данных и отправить письмо администратору
		case 'send_email_save_bd':

			// отзыв
			if ( $form_alias == 'ostavit_otzyv' ) {
				$table = new Table( 'position_reviews' );
				$insert = $table -> getEntity();
				$insert -> section_id = 340;
				$insert -> name = $arr_req[ 'name' ];
				$insert -> email = $arr_req[ 'email' ];
				$insert -> phone = $arr_req[ 'phone' ];
				$insert -> text = $arr_req[ 'message' ];
				$insert -> public = 0;
				$insert -> datestamp = time( );
				$table -> save( $insert );
				$json = send_emails( $json, '', $section_form, $arr_req, $arr_err, true );

				////////////////////////////////// SMS
				$smsmess = 'Отзыв: ' . mb_substr( $arr_req[ 'message' ], 0, 32 ) . "...\n" . $arr_req[ 'name' ] . "\n" . $arr_req[ 'phone' ];
				//$smsru -> send( 89123238494, $smsmess );
				//$smsru -> send( 89193333327, $smsmess );
				//////////////////////////////////

				echo json_encode( $json );

			}

			// подписка на рассылку
			if ( $form_alias == 'subscribe_form' ) {
				$table = new Table( 'position_subscribe' );
				$insert = $table -> getEntity();
				$insert -> section_id = 1881;
				$insert -> email = $arr_req[ 'email' ];
				$insert -> datestamp = time( );
				$insert -> active = 1;
				$table -> save( $insert );
				$json = send_emails( $json, '', $section_form, $arr_req, $arr_err, true );

				////////////////////////////////// SMS
				$smsmess = 'Подписка: ' . $arr_req[ 'email' ];
				//$smsru -> send( 89123238494, $smsmess );
				//$smsru -> send( 89193333327, $smsmess );
				//////////////////////////////////

				echo json_encode( $json );
			}
			
		break;
			
		// отправить письмо администратору
		case 'send_email':

			$json = send_emails( $json, '', $section_form, $arr_req, $arr_err, true );
			
			if ( $form_alias == 'zakazat_zvonok' ) {

				////////////////////////////////// SMS
				$smsmess = 'Позвонить!' . "\n" . $arr_req[ 'name' ] . "\n" . $arr_req[ 'phone' ];
				//$smsru -> send( 89123238494, $smsmess );
				//$smsru -> send( 89193333327, $smsmess );
				//////////////////////////////////

			}
			else if ( $form_alias == 'otmena_bronirovaniya_mobilnye' ) {

				////////////////////////////////// SMS
				$smsmess = 'Причина!' . "\n" . mb_substr( $arr_req[ 'cancel_radio' ], 0, 32 ) . "...\n" . $arr_req[ 'name' ] . "\n" . $arr_req[ 'phone' ];
				//$smsru -> send( 89123238494, $smsmess );
				//$smsru -> send( 89193333327, $smsmess );
				//////////////////////////////////

			}
			
			
			echo json_encode( $json );

		break;
	
}
	
	

	function send_emails( $json, $emails='', $section_form, $arr_req, $arr_err, $debug=false ) {
	
			if ( $emails == '' ) $emails = $section_form[ 'email' ];
			$ex_emails = explode( ',', $emails );

			foreach ( $ex_emails as $e ) {

				$e = trim( $e );
				if ( !$e ) continue;

				$mailer = new Mailer( );
				$mailer	-> IsHTML( true );
				$mailer -> From		= 'itfactory@mail.ru';

				if ( isset( $_SESSION[ 'sms' ] ) ) {
					$section_form[ 'esubject' ] = str_replace( '"Увильды"', '«Увильды»', $section_form[ 'esubject' ] );
				}
				
				if ( isset( $_SESSION[ 'yandex_direct' ] ) ) {
					$section_form[ 'esubject' ] = str_replace( '"Увильды"', '" Увильды "', $section_form[ 'esubject' ] );
				}

				if ( isset( $_SESSION[ 'subscribe' ] ) ) {
					$section_form[ 'esubject' ] = str_replace( '"Увильды"', '|Увильды|', $section_form[ 'esubject' ] );
				}

				$mailer -> Subject	= Form :: parse_mail_tpl( $section_form[ 'esubject' ], $arr_req, $debug );
				$mailer -> Body		= Form :: parse_mail_tpl( $section_form[ 'html' ], $arr_req, $debug );

				$mailer -> AddAddress( $e );

				$send = $mailer -> Send( );

				if ( !$send ) {
					$arr_err[ 'system_message' ] = 'Ошибка #' . __LINE__;
				}

			}

			$json[ 'trace' ] = $mailer -> errorInfo;
			
			if ( count( $arr_err ) ) {
				$json[ 'err' ] = $arr_err;
				$json[ 'mess' ] = 'Ошибка #' . __LINE__;
				echo json_encode( $json );
				exit( );
			}
			else {
				$json[ 's' ] = true;
				$json[ 'mess' ] = Form :: parse_mail_tpl( $section_form[ 'success_message' ] );
			}
			
			return $json;
	}



	




	

