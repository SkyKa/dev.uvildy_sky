<?php

	$result = array(	
					's' => false, 
					'mess' => '',
					'booking' => '',
					);

	if( !isset( $_SESSION[ 'booking' ] ) ) {
		$result[ 'mess' ] = 'Ошибка #' . __LINE__;
	    echo json_encode( $result );
		exit();

	}

	$result[ 'booking' ] = $_SESSION[ 'booking' ];
	$result[ 's' ] = true;

	header( 'content-type: application/json; charset=utf-8' );
	echo json_encode( $result );

	exit();