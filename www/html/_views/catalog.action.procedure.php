<?php 

$table = new Table('catalog_section');

$section = $table -> select('SELECT `id`, `title`, `parent_id` FROM `catalog_section` WHERE `alias`=:alias LIMIT 1', array( 'alias' => $params[1] ) );
if( empty( $section ) ) return false;
$section = end( $section );


$procedure = $table -> select('SELECT * FROM `section_separate_procedures` WHERE `id`=:id', array( 'id' => $section['id'] ) );
if( empty( $procedure ) ) return false;

$parent_section = $table -> select('SELECT `id`, `title`, `alias` FROM `catalog_section` WHERE `id`=:parent_id', 
	array( 'parent_id' => $section['parent_id'] ) );
if( empty( $parent_section ) ) return false;

$procedure = end( $procedure );

$str = '<div class="procedure-page">
			<div class="breadcrumbs">
				<div class="container">
					<nav class="breadcrumbs">
						'. val('catalog.action.breadcrumbs', array( 'last_link' => true ) ) .'
						<a href="' . $alias . '/'. $parent_section[0]['alias'] .'.html">'. $parent_section[0]['title'] .'</a>&nbsp;&nbsp;|&nbsp;&nbsp;
						<span>'. $section['title'] .'</span>
					</nav>
				</div>
			</div>

			<div class="container">
				<div class="row">
				<div class="left-col">
					<aside>
						<ul class="side-menu">';

$menu_items = $table -> select('SELECT * FROM `catalog_section` WHERE `parent_id`=:id ORDER BY `position`', array( 'id' => 364 ));
if( !empty( $menu_items ) ) {

	$class = '';

	foreach( $menu_items as $menu_item ) {

		// if( $menu_item[ 'alias' ] == $section[ 'alias' ] ) {
		// 	$class = 'selected';
		// }

		$str .= '<li class="'. $class .'"><a href="/'. $alias . '/' . $menu_item[ 'alias' ] .'.html">'. $menu_item[ 'title' ] .'</a></li>';

		$class = '';

	}

} 

$str .=					'</ul>
					</aside>
				</div>
				<div class="right-col">
					<h1>'. $section['title'] .'</h1>';


if( count( $procedure['img'] ) ) {

	$str .= '<a href="/'. get_cache_pic( $procedure['img'], 600, 665, true ) .'" class="fancybox">
				<img class="image" src="/'. get_cache_pic( $procedure['img'], 487, 439, true ) .'" alt="">
			</a>';

}

if( !empty( $procedure[ 'description' ] ) ) {
	$str .= '<h2>Описание</h2>
			<p class="desc">'. $procedure[ 'description' ] .'</p>';
}

$str .= '<div class="clearfix"></div>';

if( !empty( $procedure[ 'result' ] ) ) {
	$str .= '<div class="result">
				<div class="content">
					<h2><span>Результат</span></h2>
					'. $procedure[ 'result' ] .'
				</div>
			</div>';
}

if( !empty( $procedure[ 'content' ] ) ) {
	$str .= '<div class="content procedure-content">'. $procedure[ 'content' ] .'</div>';
}

$str .=      '</div>
	     </div>
	   </div>
	</div>';

echo $str;