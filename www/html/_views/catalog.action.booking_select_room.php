<div class="booking-step2-page booking-page">
	<div class="container">
		<h1>Онлайн бронирование номера</h1>
		<p class="step">Шаг <span>1</span> из <span>3</span></p>

		<h2>ВВЕДИТЕ ДАТУ ЗАЕЗДА</h2>
		<div class="dates">
			<div class="from-to">
				<label for="from">Дата заезда</label>
				<?php 

					$value = '';

					if( !empty( $_SESSION['booking']['start_date'] ) ) {
						$value = date("d.m.Y", strtotime($_SESSION['booking']['start_date']));
					}
				?>
				<input type="text" id="from" name="from" class="form-control" value="<?php echo $value; ?>">
			</div>
			<p style="margin-top: 8px;">Для бронирования мест по путевке выходного дня нужно выбрать пятницу, субботу или воскресенье. Поэтому срок заезда в этом случае составит от 1 до 3 суток.</p>
			<!-- <div class="from-to">
				<label for="to">Дата отъезда</label>
				<input type="text" id="to" name="to" class="form-control">
			</div> -->
		</div>

		<div class="select-programm-wrap">
			<div class="row">
				<div class="col-md-8 select-programm-inner">
					<h2>Выберите программу</h2>
					<div class="custom-radio">
					<?php
						$table = new Table('catalog_section');

						$programm_alias = 'programmy';

						$section = $table -> select('SELECT * FROM `catalog_section` WHERE `alias`=:alias LIMIT 1', 
							array( 'alias' => $programm_alias ));

						if( empty( $section ) ) return false;
						$section = end( $section );

						$items = $table -> select('SELECT `catalog_section`.* FROM `catalog_section` INNER JOIN `section_programms` 
							ON (`catalog_section`.`id`=`section_programms`.`id`) WHERE `parent_id`=:id AND `section_programms`.`public` AND `section_programms`.`is_programm`
							ORDER BY `catalog_section`.`position`', array( 'id' => $section[ 'id' ] ));

						$checked = 'checked';

						if( !empty($_SESSION['booking']['programm']) ) {
							if( $_SESSION['booking']['programm'] != 'without-programm' ) {
								$checked = '';
							}
						}

						echo '<div class="form-group">
									<input type="radio" id="prog-without-programm" data-programm-id="without-programm" name="radio" '. $checked .'>
								    <label for="prog-without-programm">
								    	<div class="square"></div>
								    	<span class="title">Без программы</span>
								    </label>
								</div>';

						foreach( $items as $item ) {

							$checked = '';

							if( !empty($_SESSION['booking']['programm']) ) {
								if( $_SESSION['booking']['programm'] == $item[ 'id' ] ) {
									$checked = 'checked';
								}
							}

							$section_curse_duration = $table -> select('SELECT `id` FROM `catalog_section` WHERE `parent_id`=:id and 
								`position_table`="position_curse_duration" LIMIT 1', array( 'id' => $item['id'] ) );

							if( empty( $section_curse_duration ) ) continue;

							$display = 'inline-block';
							$style = '';

							// Не показываем, если это путевка выходного дня. Покажем после выбора соответствующей даты.
							if( ( $item[ 'alias' ] == 'putevka-vyhodnogo-dnya' ) ) {

								if( empty($_SESSION['booking']['programm']) || ( !empty($_SESSION['booking']['programm'] ) 
									&& $_SESSION['booking']['programm'] != $item[ 'id' ] ) ) {
										$style = 'style="color: #ff6c00;"';
										$display = 'none';
								}

							}

							echo '<div class="form-group" style="display: '. $display .';" >
									<input type="radio" id="prog-'. $item[ 'alias' ] .'" data-programm-id="'. $item[ 'id' ] .'" name="radio" '. $checked .'>
								    <label for="prog-'. $item[ 'alias' ] .'">
								    	<div class="square"></div>
								    	<span class="title" '. $style .'>'. $item[ 'title' ] .'<a href="/'. $programm_alias . '/' . $item[ 'alias' ] . '.html" target="_blank" class="details">?</a></span>
								    </label>
								</div>';
						}

					?>
					</div>
				</div>
				
				<div class="col-md-4">
					<div class="select-during clearfix">
						<?php 
							$title = 'Введите продолжительность курса (дней)';
							$value = '';
							$display = 'block';
							if( !empty($_SESSION['booking']['programm']) ) {
								if( $_SESSION['booking']['programm'] == 'without-programm' ) {
									if( !empty($_SESSION['booking']['duration']) ) {
										$value = $_SESSION['booking']['duration'];
									}
								}
								else {
									$display = 'none';
									$title = 'ВЫБЕРИТЕ ПРОДОЛЖИТЕЛЬНОСТЬ КУРСА (ДНЕЙ)';
								}
							}
						?>
						<h2><?php echo $title; ?></h2>
						<?php

							
							echo '<input type="text" style="display: '. $display .'" class="form-control number-days field-number-days" value="'. $value .'" />';

							$i = 0;
							foreach( $items as $item ) {

								$programm = $table -> select('SELECT * FROM `section_programms` WHERE `id`=:id', 
									array( 'id' => $item[ 'id' ] ));

								if( empty( $programm ) ) continue;

								$programm = end( $programm );
								//echo '<pre>';
								$section_curse_duration = $table -> select('SELECT `id` FROM `catalog_section` WHERE `parent_id`=:id and 
									`position_table`="position_curse_duration" LIMIT 1', array( 'id' => $item['id'] ) );
								if( empty( $section_curse_duration ) ) continue;

								$section_curse_duration = end( $section_curse_duration );

								$curse_durations = $table -> select('SELECT * FROM `position_curse_duration` WHERE `section_id`=:id ORDER BY `curse_duration`', array( 'id' => $section_curse_duration['id'] ) );
								$duration_count = count( $curse_durations );

								/*
								var_dump( $item[ 'title' ] );
								var_dump( $curse_durations );
								var_dump(  count( $curse_durations ) );
								echo '</pre>';
								*/
								$value = '';
								if( !empty($_SESSION['booking']['programm']) ) {
									if( $_SESSION['booking']['programm'] == $item[ 'id' ] ) {
										if( !empty($_SESSION['booking']['duration']) ) {
											$value = $_SESSION['booking']['duration'];
										}
									}
								}

								$display = 'none';
								if( !empty( $value ) ) {
									$display = 'block';
								}

								
								echo '<div class="custom-radio field-number-days" style="display:'. $display .';" data-programm-id="prog-'. $item[ 'alias' ] .'" data-duration-count="' . $duration_count . '">';
								

								if ( !$duration_count ) {
									echo '<input type="text" class="form-control number-days-programm field-number-days" value="'. $value .'" id="number-0-'. $i .'" name="radio-duration-'. $i .'" />';
								}
								
								$j = 0;
								foreach( $curse_durations as $curse_duration ) {

									$checked = '';

									if( !empty( $value ) ) {
										if( $value == $curse_duration['curse_duration'] ) {
											$checked = 'checked';
										}
									}
									else {
										if( !$j ) {
											$checked = 'checked';
										}
									}


									echo '<input type="radio" value="'. $curse_duration['curse_duration'] .'" id="number-'. $curse_duration['curse_duration'] .'-'. $i .'" name="radio-duration-'. $i .'" 
											'. $checked .'><label for="number-'. $curse_duration['curse_duration'] .'-'. $i .'" class="days">'. $curse_duration['curse_duration'] .'</label><br>';
									$j++;

								}

								echo '</div>';

								$i++;
							}

						?>
					</div>
				</div>
			</div>
		</div>
		<div class="basic-button big"><a href="" class="pull-right" id="get-step-2">далее</a></div>
</div>