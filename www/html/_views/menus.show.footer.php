<?php

    $alias = Utils :: getVar( 'alias' );
	$items = $data -> getItems( $menu -> id );
    $uri_orig = $_SERVER[ 'REQUEST_URI' ];
    $uris = explode( '?', $uri_orig );
    $uri = $uris[ 0 ];

    foreach( $items as $row )
    {

        if ( $row[ 'visible' ] != 1 ) continue;
        $href = SF . $row[ 'type_link' ];
        $class = "";
        if ( $uri == $href || footer_active_razdel( $row[ 'type_id' ], $alias ) ) {
            $class = 'selected';
        }
		
		echo "<li class='". $class ."'><a href='" . $row[ 'type_link' ] . "'>" . $row[ 'title' ] . "</a></li>";

    }

	function footer_active_razdel( $tid, $alias ) {
		$table = new Table( 'pages' );
		$rows = $table -> select( "SELECT `id` FROM `pages` WHERE `alias`=:alias AND (`id`=:tid OR `parent_id`=:tid) LIMIT 1",
			array( 'tid' => $tid, 'alias' => $alias ) );

		if ( count( $rows ) > 0 ) return true;
		else return false;
	}
