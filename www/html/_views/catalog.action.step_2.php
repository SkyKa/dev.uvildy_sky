<?php 

$str = '<div class="booking-step2-page booking-page">
		<div class="container">
			<h1>Онлайн бронирование номера</h1>
			<p class="step">Шаг <span>2</span> из <span>3</span></p>
			<a href="" class="booking-back"><img src="/static/img/arr_back.png" alt=""></a>

			<div class="modal fade room-modal" id="room-modal" tabindex="-1" role="dialog" aria-labelledby="room-modalLabel" aria-hidden="true">
				<div class="modal-dialog">
					<div class="modal-content">
						<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
						<div class="modal-body">
							<div id="preloader-ajax">
								<div id="preloader-ajax-center">
									<div id="preloader-ajax-center-absolute">
										<div id="preloader-ajax-center-absolute-one">
											<div class="preloader-ajax-object-one"></div>
											<div class="preloader-ajax-object-one"></div>
											<div class="preloader-ajax-object-one"></div>
											<div class="preloader-ajax-object-one"></div>
											<div class="preloader-ajax-object-one"></div>
											<div class="preloader-ajax-object-one"></div>
										</div>
										<div id="preloader-ajax-center-absolute-two">
											<div class="preloader-ajax-object-two"></div>
											<div class="preloader-ajax-object-two"></div>
											<div class="preloader-ajax-object-two"></div>
											<div class="preloader-ajax-object-two"></div>
											<div class="preloader-ajax-object-two"></div>
											<div class="preloader-ajax-object-two"></div>
										</div>
									</div>
								</div>
							</div>
							<div class="room-content-wrap">
								
							</div>
						</div>
					</div>
				</div>
			</div>';

			$str .= val('catalog.action.rooms');
				
$str .=			'<div class="humans-wrapper">
				
				</div>
			</div>

			<!-- <div class="container indicative-cost-wrap" style="display: none;">
				<div class="indicative-cost">
					<div class="basic-button big"><a href="">далее</a></div>
					<p class="title">Ориентировочная стоимость проживания: <span>0</span> <span class="rouble">a</span></p>
					<div class="clearfix"></div>
					<p class="description">
						
					</p>
				</div>
			</div> -->
		</div>
	</div>';

echo $str;