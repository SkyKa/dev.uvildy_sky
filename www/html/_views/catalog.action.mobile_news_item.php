<?php

$table = new Table('catalog_section');

$news = $table -> select('SELECT * FROM `position_news` WHERE `alias`=:alias AND `public`', array( 'alias' => $params[0] ) );

$title = '';
if( count( $news ) ) $title = $news[0]['title'];

echo 	'<header>
			<div class="et-menu-collapse-btn et-collapsed">
				<div class="et-collapse-line"></div>
				<div class="et-collapse-line"></div>
				<div class="et-collapse-line"></div>
			</div>
			
			<div class="h1-wrap">
				<h1>'. $title .'</h1>
			</div>

			<a href="/mobile_static/programms.html" class="back-button">
				<img src="/mobile_static/img/back_button.png" alt="">
			</a>

		</header>
		
		<div class="content-wrapper">
			<div class="wrapper">
				<div class="content-inner">';


if( !count( $news ) ) return false;
$news = end( $news );


$new_date = explode( ' ', date( "d m Y", $news[ 'datestamp' ] ) );
$date = $new_date[ 0 ] . ' ' . Langvars :: replaceMonth( $new_date[ 1 ] ) . ' ' . $new_date[ 2 ];

$str = '<div class="et-container page-sheet page-news-item">
			<div class="content-container">';

if( empty($news['is_share']) ) {
	$str .= '<p class="date">'. $date .'</p>';
}

$str .= '<div class="content"><img src="/'. get_cache_pic($news['img'], 540, 351, true) .'" alt="">' . $news['content'] .'</div>';

$str .=   '</div>
		</div>';

echo $str;