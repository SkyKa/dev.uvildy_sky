<?php
	
	$table = new Table('catalog_section');
	$str = '';
	
	foreach( $_SESSION['booking']['persons'] as $key => $person ) {

		// $programm_title[0]['title'] = '';

		// if( $person['programm_id'] == 'without-programm' ) {
		// 	$programm_title[0]['title'] = 'Без программы';
		// }
		// else {
		// 	$programm_title = $table -> select('SELECT `id`, `title` FROM `catalog_section` WHERE `id`=:id LIMIT 1',
		// 		array( 'id' => $person['programm_id'] ));

		// 	if( empty( $programm_title ) ) continue;
		// }

		if( $person['age'] == 'Взрослый' ) {

			$str .= '<div class="col-md-4 col-sm-6 item" data-person-id="'. $key .'">
					<img class="icon" src="/static/img/adult_icon.png" alt="">
					<h2>'. $person['age'] .'</h2>';

			if( !empty($person['food']) ) {
				$str .= '<p>Питание: '. $person['food'] .'</p>';
			}

			$str .=	'<button type="button" class="delete"><span>Удалить</span></button>
				</div>';

		}
		else {

			$str .= '<div class="col-md-4 col-sm-6 item" data-person-id="'. $key .'">
						<img class="icon" src="/static/img/child_icon.png" alt="">';

			$str .=	'<h2>'. $person['age'] .'</h2>';
			
			if( !empty($person['food']) ) {
				$str .= '<p>Питание: '. $person['food'] .'</p>';
			}

			$str .=	 '<button type="button" class="delete"><span>Удалить</span></button>
					</div>';

		}
		// else if( $person['age'] == '3' ) {

		// 	$str .= '<div class="col-md-4 col-sm-6 item" data-person-id="'. $key .'">
		// 			<img class="icon" src="/static/img/small_child_icon.png" alt="">
		// 			<h2>Детский <span>(3-5 лет)</span></h2>
		// 			<button type="button" class="delete"><span>Удалить</span></button>
		// 		</div>';
		// }
	}

	echo $str;