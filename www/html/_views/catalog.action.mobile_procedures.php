<?php 

if( !empty( $params[1] ) ) {
	mod('catalog.action.mobile_procedure');
	return;
}

$my_alias = 'procedury6';

if( !empty( $params[0] ) ) {
	$my_alias = $params[0];
}

echo 	'<header>
			<div class="et-menu-collapse-btn et-collapsed">
				<div class="et-collapse-line"></div>
				<div class="et-collapse-line"></div>
				<div class="et-collapse-line"></div>
			</div>
			
			<div class="h1-wrap">
				<h1>'. val('pages.show.title') .'</h1>
			</div>

			<a href="" class="back-button">
				<img src="/mobile_static/img/back_button.png" alt="">
			</a>

		</header>

		<div class="content-wrapper">
			<div class="wrapper">
				<div class="content-inner">';

$table = new Table('catalog_section');
$section = $table -> select('SELECT `id` FROM `catalog_section` WHERE `alias`=:alias LIMIT 1', array( 'alias' => $my_alias ));

if( !count( $section ) ) return false;

$section = end( $section );

$rows = $table -> select('SELECT * FROM `catalog_section` WHERE `parent_id`=:id ORDER BY `position`' , array( 'id' => $section['id'] )); 

if( !count($rows) ) return false;

$str = '<div class="et-container">';

foreach( $rows as $row ) {

	$item = $table -> select('SELECT * FROM `section_separate_procedures` WHERE `id`=:id AND `public` LIMIT 1', array( 'id' => $row[ 'id' ] ));

	if( !count( $item ) ) continue;

	$item = end( $item );

	$href = '';
	if( !empty( $params[0] ) ) {
		$href .= $alias . '/' . $params[0] . '/' . $row['alias'] .'.html';
	}
	else {
		$href .= $alias . '/' . $row['alias'] .'.html';
	}

	$str .= '<a href="/'. $href .'" class="menu-vertical menu-item" style="background-image: url(/'. get_cache_pic( $item['img'], 207, 264, false ) .');">
				<div class="title-wrap">
					<div class="title">
						<p>'. $row[ 'title' ] .'</p>
					</div>
				</div>
			</a>';

}

$str .= '</div>';

echo $str;