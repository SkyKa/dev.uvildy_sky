<?php

$res = array(
		's' => false,
		'mess' => '',
		'html' => '',
	);

$class_room_id = Utils::getVar( 'class_room_id' );
$room_id = Utils::getVar( 'room_id' );

if( empty( $class_room_id ) || empty( $room_id ) ) {
	$res['s'] = false;
	$res['mess'] = 'Ошибка #' . __LINE__;
	echo json_encode( $res );

	exit();
}

$table = new Table('catalog_section');

$class_room_section = $table -> select('SELECT `catalog_section`.`id`, `catalog_section`.`title`, `section_number_class`.`id_1c` FROM `catalog_section` INNER JOIN `section_number_class` ON (`catalog_section`.`id`=`section_number_class`.`id`) WHERE `catalog_section`.`id`=:id AND `catalog_section`.`section_table`="section_number_class" AND `catalog_section`.`parent_id`=807 LIMIT 1', array( 'id' => $class_room_id ) );

if( empty( $class_room_section ) ) {
	$res['s'] = false;
	$res['mess'] = 'Ошибка #' . __LINE__;
	echo json_encode( $res );

	exit();
}

$class_room_section = end( $class_room_section ); 

$select_date = $_SESSION['booking']['start_date'];

$programm_id = $_SESSION['booking']['programm'];

$programm = $table->select('SELECT `id`, `title` FROM `catalog_section` WHERE `id`=:id LIMIT 1', 
	array( 'id' => $programm_id ) );

if(empty($programm) && ($programm_id != 'without-programm')) {
	$res['s'] = false;
	$res['mess'] = 'Ошибка #' . __LINE__;
	echo json_encode( $res );
	exit();
}

$programm_title = '';

if( $programm_id == 'without-programm' ) {
	$programm_title = 'без лечения';
}
else {
	$programm_title = mb_strtolower($programm[0]['title'], 'UTF-8');
}

$end_date = date('Y-m-d', strtotime( "+" . $_SESSION['booking']['duration'] . " days" , strtotime($_SESSION['booking']['start_date'])));

$section_rooms = $table -> select(
	'SELECT 
		`section_offers`.`id`, 
		`section_offers`.`id_1c`,
		`section_offers`.`n_rooms`,
		`section_offers`.`n_main_place`,
		`section_offers`.`n_additional_place`,
		`section_number_prices`.`start_date`,
		`section_number_prices`.`end_date`,
		`section_number_prices`.`programm`,
		`section_number_prices`.`place_type`,
		`section_number_prices`.`food_type`,
		`section_number_prices`.`age`,
		`section_number_prices`.`price`
	FROM 
		`catalog_section` 
	INNER JOIN 
		`section_offers`
	ON 
		`catalog_section`.`id`=`section_offers`.`id`
	INNER JOIN 
		`section_number_prices`
	ON 
		`section_offers`.`id_1c`=`section_number_prices`.`offers_id_1c`
	WHERE 
		(`catalog_section`.`parent_id`=:id AND 
		`catalog_section`.`section_table`="section_offers" AND
		(DATE(:select_date) <= `section_number_prices`.`end_date`) AND
        (DATE(:end_date) > `section_number_prices`.`start_date`) AND
		`section_number_prices`.`programm`=:programm_title) OR
        (`catalog_section`.`parent_id`=:id AND
        (DATE(:select_date) <= `section_number_prices`.`end_date`) AND
        (DATE(:end_date) > `section_number_prices`.`start_date`) AND
        `section_number_prices`.`programm`="новогодняя программа")
    -- GROUP BY `section_offers`.`id_1c`
	ORDER BY `section_number_prices`.`start_date`',
	array( 'id' => $class_room_section['id'], 'select_date' => $select_date, 'end_date' => $end_date, 'programm_title' => $programm_title ) );

search_period_room_ajax( $section_rooms, $select_date, $end_date );
$categories = $section_rooms;

foreach( $categories as $key => $category ) {
    if( !isset( $category['in_out'] ) || !$category['in_out'] ) {
        unset($section_rooms[$key]);
    }
}

$res['rooms'] = $section_rooms;

if( empty( $section_rooms ) ) {
	$res['s'] = false;
	$res['mess'] = 'Ошибка #' . __LINE__;
	echo json_encode( $res );

	exit();
}

$str = '<div class="row">';

if( ($room_id == 'default') && !empty($_SESSION['booking']['number']) ) {
	$room_id = $_SESSION['booking']['number'];
}		
else if( $room_id == 'default' ) {
	$room_id = reset($section_rooms)['id'];
}

$class_offer_section = $table -> select('SELECT `catalog_section`.`id` FROM `catalog_section` INNER JOIN `section_number_class` ON (`catalog_section`.`id`=`section_number_class`.`id`) WHERE `section_number_class`.`id_1c`=:id_1c AND `catalog_section`.`parent_id`=1147 LIMIT 1', array( 'id_1c' => $class_room_section['id_1c'] ) );

if( empty( $class_offer_section ) ) {
	$res['s'] = false;
	$res['mess'] = 'Ошибка #' . __LINE__;
	echo json_encode( $res );

	exit();
}

$offer = $table -> select(
	'SELECT 
		`id`,
		`n_rooms`,
		`n_main_place`,
		`n_additional_place`
	FROM 
		`section_offers`
	WHERE 
		`id`=:id LIMIT 1',
	array( 'id' => $room_id ) ); 

if( empty( $offer ) ) {
	$res['s'] = false;
	$res['mess'] = 'Ошибка #' . __LINE__;
	echo json_encode( $res );

	exit();
}

$photo_section = $table -> select(
	'SELECT 
		`section_appearance`.`id`
	FROM 
		`section_appearance`
	INNER JOIN
		`catalog_section`
	ON
		`section_appearance`.`id`=`catalog_section`.`id`
	WHERE 
		`catalog_section`.`parent_id`=:parent_id AND `section_appearance`.`n_rooms`=:n_rooms AND `section_appearance`.`n_main_place`=:n_main_place AND `section_appearance`.`n_additional_place`=:n_additional_place LIMIT 1',
	array( 'parent_id' => $class_offer_section[0]['id'], 'n_rooms' => $offer[0]['n_rooms'], 'n_main_place' => $offer[0]['n_main_place'], 'n_additional_place' => $offer[0]['n_additional_place'] ) );

if( !empty( $photo_section ) ) {

	$images = $table -> select('SELECT * FROM `position_photo_gallery` WHERE `section_id`=:id ORDER BY `position` DESC', 
		array( 'id' => $photo_section[0]['id'] ) );

	if( !empty( $images ) ) {
		$str .= '<div class="col-sm-6 slider-wrap">
					<ul id="room-slider" class="room-slider">';

		foreach( $images as $image ) {

			$str .= '<li data-thumb="/'. get_cache_pic( $image['img'], 184, 123, false ) .'">
						<a href="/'. get_cache_pic( $image['img'], 965, 645, false ) .'" class="fancybox" data-fancybox-group="rooms-group">
							<img src="/'. get_cache_pic( $image['img'], 386, 258, false ) .'" alt="">
						</a>
					</li>';

		}

		$str .=    '</ul>
				</div>';

	}
}

$str .= '<div class="col-sm-6 room-desc">
			<h2>'. $class_room_section['title'] .'</h2>
			<div class="select-number-rooms">
				<h2>ВЫБЕРИТЕ НОМЕР</h2>
				<div class="custom-radio">';


$costs = '';
$content = '';

$age_cat = get_age_categories($section_rooms);
$age_cat = implode('","', $age_cat);

$ids = array();

$i = 0;

foreach( $section_rooms as $room ) {

	if( empty( $room ) ) continue;

	if( !in_array( $room['id'], $ids ) ) {
		$ids[] = $room['id'];
	}
	else {
		continue;
	}
	
	if( $room_id == $room['id'] ) {

		if( !empty( $room['description'] ) ) {
			$content = $room['description'];
		}

		$costs = $table->select('SELECT 
			`section_number_prices`.`age`,
			GROUP_CONCAT(
                CONCAT_WS(
                    "|-|",
                    `section_number_prices`.`place_type`,
                    `section_number_prices`.`food_type`,
                    `section_number_prices`.`price`,
                    `section_number_prices`.`start_date`,
                    `section_number_prices`.`end_date`
                )
                ORDER BY `section_number_prices`.`place_type` DESC, `section_number_prices`.`start_date`
                SEPARATOR "-_-"
            ) as params
			FROM `section_number_prices` WHERE 
			`offers_id_1c`=:offers_id_1c AND
			(DATE(:select_date) <= `section_number_prices`.`end_date`) AND
	        (DATE(:end_date) > `section_number_prices`.`start_date`) AND
			(`section_number_prices`.`programm`=:programm_title OR
			`section_number_prices`.`programm`="новогодняя программа") AND 
			`section_number_prices`.`food_type`="'. addslashes($room['food_type']) .'" AND
			`section_number_prices`.`age` IN ("'. $age_cat .'")
			GROUP BY `section_number_prices`.`age`
			ORDER BY `section_number_prices`.`place_type` ASC', 
			array( 'offers_id_1c' => $room['id_1c'], 'select_date' => $select_date, 'end_date' => $end_date, 'programm_title' => $programm_title ) );

		$res['costs'][] = $costs;

		if( empty($costs) ) continue;

	}

	if( $i ) $str .= '<br>';

	$checked = '';
	if( $room['id'] == $room_id ) $checked = 'checked';

	$num_rooms = '';

	if( !empty( $room['n_rooms'] ) ) {
		$num_rooms = $room['n_rooms'] . '-комнатный, ';
	} 


	$str .= '<input type="radio" id="number-'. $room['id'] .'" data-room-id="'. $room['id'] .'" name="radio-rooms" '. $checked .' data-main-place="'. $room['n_main_place'] .'" data-additional-place="'. $room['n_additional_place'] .'">
		    <label for="number-'. $room['id'] .'">
		    	<div class="square"></div>
		    	<span>' . $num_rooms . 'основных мест: '. $room['n_main_place'] . ', доп. мест: ' . $room['n_additional_place'] .'</span>
		    </label>';

	$i++;
}



$str .=  '</div>
		</div>';

if( !empty( $costs ) ) {

	$str .= '<div class="cost-wrap">
				<h2>Стоимость номера</h2>';

	foreach ($costs as $cost) {

		$str .= '<h3>'. $cost['age'] .'</h3>';

		$arr_params = explode( '-_-', $cost['params'] );

		foreach( $arr_params as $arr_param ) {

			$data = explode( '|-|', $arr_param );

			$str .= '<p>'. $data[0];

			if( !empty($data[1]) ) {
				$str .= ' (питание: '. $data[1] .', период: '. date("d.m.Y", strtotime($data[3])) .'-'. date("d.m.Y", strtotime($data[4])) .')';
			}

			$str .= ': <span>'. $data[2] . '</span> <span class="rouble">c</span> <span>/ сутки</span></p>';
		}

	}

	$str .= '</div>';

}


$str .= '</div>
	</div>
	<div class="clearfix"></div>
	<div class="content">'. $content .'</div>
	<div class="buttons">
		<div class="basic-button big"><a href="" data-dismiss="modal" aria-hidden="true">другие номера</a></div>
		<div class="basic-button big"><a href="" id="confirm-room" data-dismiss="modal" aria-hidden="true">подтвердить выбранный номер</a></div>
	</div>
	<div class="clearfix"></div>';


$res['mess'] = 'Все ОК!';
$res['html'] = $str;

$res['s'] = true;

echo json_encode( $res );

function search_period_room_ajax( &$age_categories, $select_date, $end_date ) {

	$include_numbers = array();

    foreach( $age_categories as $key => $category ) {

        if( isset($age_categories[$key]['in_out']) && $age_categories[$key]['in_out'] ) continue; 

        // if( !empty($include_numbers) ) {

        // 	$is_exist = false;

        // 	foreach( $include_numbers as $number ) {
        // 		if( ($category['n_rooms'] == $number['n_rooms']) && ($category['n_main_place'] == $number['n_main_place']) 
        // 			&& ($category['n_additional_place'] == $number['n_additional_place']) ) {
        // 			$is_exist = true;
        // 		}
        // 	}

        // 	if( $is_exist ) continue;

        // }

        if( (strtotime("+1 day", strtotime( $category['end_date'])) >= strtotime( $end_date )) && (strtotime( $category['start_date'] ) <= strtotime( $select_date )) ) { 
            $age_categories[$key]['in_out'] = true;
            $include_numbers[] = $category;
            continue;            
        }
        else {
            if( strtotime( $category['start_date'] ) <= strtotime( $select_date ) ) {
                $indexes = find_req_period_room_ajax( $age_categories, $select_date, $end_date, $category, $key );

                foreach( $indexes as $index ) {
                    $age_categories[$index]['in_out'] = true; 
                }

                if( !empty( $indexes ) ) {
                	$include_numbers[] = $category;
                }
            }
        }

    }

}

function find_req_period_room_ajax( &$age_categories, $select_date, $end_date, $category, $key, $index=array() ) {

    $first_day = strtotime("+1 day", strtotime($category['end_date']));
    $search_param = array();

    $search_param['age'] = $category['age'];
    $search_param['food_type'] = $category['food_type'];
    $search_param['place_type'] = $category['place_type'];
    $search_param['id_1c'] = $category['id_1c'];

    $index[] = $key;

    foreach( $age_categories as $k => $cat ) {
        if( ($first_day == strtotime($cat['start_date']) ) 
            && ($search_param['age'] == $cat['age']) 
            && ($search_param['food_type'] == $cat['food_type']) 
            && ($search_param['place_type'] == $cat['place_type'])
            && ($search_param['id_1c'] == $cat['id_1c']) ) {
            
            if( strtotime($cat['end_date']) >= strtotime("-1 day", strtotime($end_date)) ) {
                $index[] = $k;
                return $index;
            }
            else {
                return find_req_period_room_ajax($age_categories, $select_date, $end_date, $cat, $k, $index);
            }
        }
    }

    return array();

}


function get_age_categories($rooms) {
	$ages = array();
	foreach( $rooms as $room ) {
		if(!in_array( $room['age'], $ages )) {
			$ages[] = $room['age'];
		}
	}
	return $ages;
}