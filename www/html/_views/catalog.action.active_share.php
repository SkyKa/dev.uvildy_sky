<?php

$table = new Table('catalog_section');

$share = $table -> select('SELECT * FROM `position_news` WHERE `is_share` AND `public` ORDER BY `datestamp` DESC LIMIT 1');
if( !count( $share ) ) return false;

$share = end( $share );

$str = '<a href="/akcii-i-novosti/'. $share['alias'] .'.html" class="share" >
			<div class="info">
				<p class="title">Акция!</p>
				<p class="text">'. $share['title'] .'</p>
				<div class="cost">
					<p>'. mb_substr( $share['description'], 0, 80, mb_detect_encoding($share['description']) ) . '...' .'</p>
					<div class="basic-button blue"><span href="">Подробнее</span></div>
				</div>
			</div>
			<div class="img-wrap" style="background-image: url('. $share['img'] .');"></div>
		</a>';

echo $str;