<?php

    $uri_orig = $_SERVER[ 'REQUEST_URI' ];
    $uris = explode( '?', $uri_orig );
    $uri = $uris[ 0 ];


	$table = new Table( 'menus_item' );
	$parent = $table -> select( 'SELECT * FROM  `menus_item` WHERE `menus_id`=1 && `type_link`="/' . $args -> parent_alias . '.html" LIMIT 1' );
	if ( !count( $parent ) ) return false;
	$parent = end( $parent );

	$items = $table -> select( 'SELECT * FROM  `menus_item` WHERE `parent_id`=:pid && `visible`=1 ORDER BY `position`', array( 'pid' => $parent[ 'id' ] ) );
	if ( !count( $items ) ) return false;

	echo "<div class='btn-section-wrap'>";

    foreach( $items as $row )
    {

        if ( $row[ 'visible' ] != 1 ) continue;
        $href = SF . $row[ 'type_link' ];
        $class = '';
		$ico = ( !$row[ 'img_src' ] ) ? '' : '<img src="/' . $row[ 'img_src' ] . '" />';
        if ( $uri == $href || submenu_active_razdel( $row[ 'type_id' ], $alias ) ) {
            $class = ' class="active"';
        }

		echo "<a href='" . $row[ 'type_link' ] . "'" . $class . ">";
		echo $ico;
		echo $row[ 'title' ];
		echo "</a>";

    }

	echo "</div>";
	
	function submenu_active_razdel( $tid, $alias ) {
		$table = new Table( 'pages' );
		$rows = $table -> select( "SELECT `id` FROM `pages` WHERE `alias`=:alias AND (`id`=:tid OR `parent_id`=:tid) LIMIT 1",
			array( 'tid' => $tid, 'alias' => $alias ) );

		if ( count( $rows ) > 0 ) return true;
		else return false;
	}

/*
				<div class="btn-section-wrap">
					<a href="" class="btn-sale active">�����</a>
					<a href="" class="btn-news">�������</a>
					<a href="" class="btn-article">������</a>
				</div>
*/
