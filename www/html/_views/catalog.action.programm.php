<?php

$table = new Table('catalog_section');
$section = $table -> select('SELECT * FROM `catalog_section` WHERE `alias`=:alias LIMIT 1', array( 'alias' => $params[0] ));

if( empty( $section ) ) return false;
$section = end( $section );

$item = $table -> select('SELECT * FROM `section_programms` WHERE `id`=:id AND `public` LIMIT 1', array( 'id' => $section[ 'id' ] ));

if( empty( $item ) ) return false;
$item = end( $item );

$str = '<div class="programm-page item-page">
			<div class="breadcrumbs">
				<div class="container">
					<nav class="breadcrumbs">
						'. val('catalog.action.breadcrumbs', array( 'last_link' => true ) ) .'
						<span>'. $section['title'] .'</span>
					</nav>
				</div>
			</div>

			<div class="container">
			<div class="row">
				<div class="left-col">
					<aside>
						<ul class="side-menu">';

$menu_items = $table -> select('SELECT * FROM `catalog_section` WHERE `parent_id`=:parent_id ORDER BY `position`', array( 'parent_id' => $section[ 'parent_id' ] ));
if( !empty( $menu_items ) ) {

	$class = '';

	foreach( $menu_items as $menu_item ) {

		if( $menu_item[ 'alias' ] == $section[ 'alias' ] ) {
			$class = 'selected';
		}

		$str .= '<li class="'. $class .'"><a href="/'. $alias . '/' . $menu_item[ 'alias' ] .'.html">'. $menu_item[ 'title' ] .'</a></li>';

		$class = '';

	}

} 

$str .=					'</ul>
					</aside>
				</div>
				<div class="right-col">
					<h1 data-programm-id='. $item['id'] .'>'. $section['title'] .'</h1>';

$images = $table -> select('SELECT * FROM `position_gallery` WHERE `section_id`=:id AND `public` ORDER BY `position`', 
	array( 'id' => $section[ 'id' ] ));

if( count( $images ) ) {

	$str .= '<ul id="item-slider">';

	foreach( $images as $image ) {
		$str .= '<li data-thumb="/'. get_cache_pic( $image['img'], 235, 177, false ) .'">
					<a href="/'. get_cache_pic( $image['img'], 600, 665, true ) .'" class="fancybox" data-fancybox-group="group"><img src="/'. $image['img'] .'" alt=""></a>
				</li>';
	}

	$str .= '</ul>';

}

if( !empty( $item[ 'subtitle' ] ) ) {
	$str .= '<h2>'. $item[ 'subtitle' ] .'</h2>';
}

if( !empty( $item[ 'description' ] ) ) {
	$str .= '<p class="desc">'. $item[ 'description' ] .'</p>';
}

if( !empty($item['is_programm']) ) {

	$str .= '<form class="select-programm">';

	$curse_durations = array();

	$section_curse_duration = $table -> select('SELECT `id` FROM `catalog_section` WHERE `parent_id`=:id and 
		`position_table`="position_curse_duration" LIMIT 1', array( 'id' => $item['id'] ) );

	if( !empty( $section_curse_duration ) ) {

		$section_curse_duration = end( $section_curse_duration );

		$curse_durations = $table -> select('SELECT * FROM `position_curse_duration` WHERE `section_id`=:id ORDER BY `curse_duration`', 
			array( 'id' => $section_curse_duration['id'] ) );
		$curse_durations_count = count( $curse_durations );

		if( $curse_durations_count ) {

			$str .= '<h2>выберите количество дней</h2>
					<div id="select-number" class="select-number" data-duration-count="' . $curse_durations_count . '">';


			foreach( $curse_durations as $curse_duration ) {

				$checked = '';

				if( $curse_duration == reset( $curse_durations ) ) {
					$checked = 'checked';
				}

				$str .= '<input type="radio" value="'. $curse_duration['curse_duration'] .'" id="number-'. $curse_duration['curse_duration'] .'" class="number-'. $curse_duration['curse_duration'] .'" name="radio-duration" 
						'. $checked .'><label for="number-'. $curse_duration['curse_duration'] .'">'. $curse_duration['curse_duration'] .'</label>';

			}

			$str .= '</div>';

			if( !empty($curse_durations) ) {
				$str .= '<div class="basic-button big enroll"><button>записаться на программу</button></div>';
			}

		}
		else {
			$str .= '<h2>введите количество дней</h2>
					<div id="select-number" class="select-number" data-duration-count="0">';

			if ( !$curse_durations_count ) {
				$str .= '<input type="text" class="form-control number-days-programm field-number-days" value="1" id="number-0" name="radio-duration" />';
			}

			$str .= '</div>';
			$str .= '<div class="basic-button big enroll"><button>записаться на программу</button></div>';
		}


	}

	if( !empty( $item[ 'price' ] ) ) {
		$str .= '<h2>Стоимость программы</h2>
				 <p class="cost">от <span class="number">'. $item[ 'price' ] .'</span> <span class="rouble">c</span> / сутки</p>';
	}

	$str .=  '</form>';
}

$str .= '<div class="clearfix"></div>';


	if( !empty( $item[ 'result' ] ) ) {
		$str .= '<div class="result">
					<div class="content">
						<h2><span>Результат</span></h2>
						'. $item[ 'result' ] .'
					</div>
				</div>';
	}


	if( !empty( $item[ 'content' ] ) ) {
		$str .= '<div class="details big basic-button clearfix"><a href="">Подробнее</a></div>';
		$str .= '<div class="content-rolled">';
		$str .= '<div class="content programm-content">'. $item[ 'content' ] .'</div>';
		
if( !empty($item['is_programm']) ) {


		$str .= '<form action="" class="select-programm">';

		if( $curse_durations_count ) {

			$str .= '<h2>выберите количество дней</h2>
							<div id="select-number2" class="select-number" data-duration-count="' . $curse_durations_count . '">';

			foreach( $curse_durations as $curse_duration ) {

				$checked = '';

				if( $curse_duration == reset( $curse_durations ) ) {
					$checked = 'checked';
				}

				$str .= '<input type="radio" value="'. $curse_duration['curse_duration'] .'" id="number2-'. $curse_duration['curse_duration'] .'" class="number-'. $curse_duration['curse_duration'] .'" name="radio-duration" 
						'. $checked .'><label for="number2-'. $curse_duration['curse_duration'] .'">'. $curse_duration['curse_duration'] .'</label>';

			}

			$str .=      '</div>';

			if( !empty($curse_durations) ) {
				$str .= '<div class="basic-button big enroll"><button>записаться на программу</button></div>';
			}

		}
		else {
			$str .= '<h2>введите количество дней</h2>
					<div id="select-number" class="select-number" data-duration-count="0">';

			if ( !$curse_durations_count ) {
				$str .= '<input type="text" class="form-control number-days-programm field-number-days" value="1" id="number-0" name="radio-duration" />';
			}

			$str .= '</div>';
			$str .= '<div class="basic-button big enroll"><button>записаться на программу</button></div>';
		}

		$str .= '
				</form>';

		if( !empty( $item[ 'price' ] ) ) {
		$str .= '<h2>Стоимость программы</h2>
				 <p class="cost">от <span class="number">'. $item[ 'price' ] .'</span> <span class="rouble">c</span> / сутки</p>';
		}

		$str .= '<div class="clearfix"></div>
				<div class="basic-button booking-button big">
					<a href="" data-toggle="modal" data-target="#callback-modal">заказать звонок</a>
				</div>
			</div>';
	}

}
	$str .=        '</div>
				</div>	
			</div>';

$str .= '</div>';

echo $str;