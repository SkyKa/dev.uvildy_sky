<?php
	
	$js = '';
	$app = '';


	$referer = str_replace( 'http://', '', ( isset( $_SERVER[ 'HTTP_REFERER' ] ) ) ? $_SERVER[ 'HTTP_REFERER' ] : '' );
	$e_referer = explode( '/', $referer );

	$url = ( isset( $e_referer[ 0 ] ) && trim( $e_referer[ 0 ] ) != '' ) ? 'http://' . $e_referer[ 0 ] : '';
	$uri = ( isset( $e_referer[ 1 ] ) && trim( $e_referer[ 0 ] ) != '' ) ? '/' . $e_referer[ 1 ] : '';

	$table = new Table( 'section_forms' );
	
	$forms = $table -> select( "SELECT `t2`.`title`,`t2`.`position`,`t2`.`alias`,`t1`.* FROM `section_forms` as `t1` LEFT JOIN `catalog_section` AS `t2` ON `t1`.`id`=`t2`.`id` WHERE (`t1`.`method`='ajax-post' || `t1`.`method`='ajax-get') ORDER BY `t2`.`position`" );

	if ( !count( $forms ) ) return false;
	$js .= "
;( function( $, undefined ) {

	$( document ).on( 'ready', function() {

		window.formAjax = {

			conf: {
				url: '" . $url . "',
				uri: '" . $uri . "',
				bg: '#ebebeb',
				errBg: '#e4ae99',
				preloader: function() {
					var arr = [];
					arr.push( '<div id=\"preloader-ajax\">' );
					arr.push( 	'<div id=\"preloader-ajax-center\">' );
					arr.push(		'<div id=\"preloader-ajax-center-absolute\">' );
					arr.push(		'<div id=\"preloader-ajax-center-absolute-one\">' );
					arr.push(		'<div class=\"preloader-ajax-object-one\"></div>' );
					arr.push(		'<div class=\"preloader-ajax-object-one\"></div>' );
					arr.push(		'<div class=\"preloader-ajax-object-one\"></div>' );
					arr.push(		'<div class=\"preloader-ajax-object-one\"></div>' );
					arr.push(		'<div class=\"preloader-ajax-object-one\"></div>' );
					arr.push(		'<div class=\"preloader-ajax-object-one\"></div>' );
					arr.push(		'</div>' );
					arr.push(		'<div id=\"preloader-ajax-center-absolute-two\">' );
					arr.push(		'<div class=\"preloader-ajax-object-two\"></div>' );
					arr.push(		'<div class=\"preloader-ajax-object-two\"></div>' );
					arr.push(		'<div class=\"preloader-ajax-object-two\"></div>' );
					arr.push(		'<div class=\"preloader-ajax-object-two\"></div>' );
					arr.push(		'<div class=\"preloader-ajax-object-two\"></div>' );
					arr.push(		'<div class=\"preloader-ajax-object-two\"></div>' );
					arr.push(		'</div>' );
					arr.push(		'</div>' );
					arr.push(	'</div>' );
					arr.push( '</div>' );
					return arr.join( '' );
				},

			},";

	
	foreach ( $forms as $form ) {

		$method = str_replace( 'ajax-', '', $form[ 'method' ] );
		$method = mb_strtoupper( $method );

		// для выхода другое событие
		if ( $form[ 'action' ] == 'logout' ) {
			;
		}
		else {
			// application concat
			$app .= "
				$( 'body' ).on( 'click', '#" . $form[ 'html_id' ] . " button[type=\"submit\"]', function() {

					console.log($('#" . $form[ 'html_id' ] . " button[type=\"submit\"]').text());
				  $( '#" . $form[ 'html_id' ] . "' ).find( '.err-tooltip' ).remove();
				  $( '#" . $form[ 'html_id' ] . "' ).hide();
				  $( '#" . $form[ 'html_id' ] . "' ).after( window.formAjax.conf.preloader );
				  $( '#" . $form[ 'html_id' ] . "' ).nextAll( '.success-message' ).remove();
				  window.formAjax." . $form[ 'alias' ] . "();
				  return false;
				});
				";
		}
		
		// ------------------
		
		$js .= "

			// " . $form[ 'title' ] . "
			" . $form[ 'alias' ] . ": function() {

				var selectors = {
					form_act: '#" . $form[ 'html_id' ] . " input[name=\"form_act\"]',
					form_alias: '#" . $form[ 'html_id' ] . " input[name=\"form_alias\"]',";

		$fields = $table -> select( "SELECT * FROM `position_forms` WHERE `section_id`=:sid ORDER BY `position`", array( 'sid' => $form[ 'id' ] ) );

		//if ( !count( $fields ) ) continue;

		$array_uniq = array( );

		foreach ( $fields as $field ) {

			if ( $field[ 'type_id' ] == 'submit' || $field[ 'type_id' ] == 'label' ) continue;

			if ( isset( $array_uniq[ $field[ 'nameid' ] ] ) ) continue;

			$array_uniq[ $field[ 'nameid' ] ] = true;

			if (	$field[ 'type_id' ] == 'text' ||
					$field[ 'type_id' ] == 'pwd' || 
					$field[ 'type_id' ] == 'file' ) {

				$js .= "
					" . $field[ 'nameid' ] . ": '#" . $form[ 'html_id' ] . " input[name=\"" . $field[ 'nameid' ] . "\"]',";

			}
			else if ( $field[ 'type_id' ] == 'select' ) {

				$js .= "
					" . $field[ 'nameid' ] . ": '#" . $form[ 'html_id' ] . " select[name=\"" . $field[ 'nameid' ] . "\"]',";

			}
			else if ( $field[ 'type_id' ] == 'memo' ) {

				$js .= "
					" . $field[ 'nameid' ] . ": '#" . $form[ 'html_id' ] . " textarea[name=\"" . $field[ 'nameid' ] . "\"]',";

			}
			else if ( $field[ 'type_id' ] == 'check' ) {

				$js .= "
					" . $field[ 'nameid' ] . ": '#" . $form[ 'html_id' ] . " input[name=\"" . $field[ 'nameid' ] . "\"]',";

			}
			else if ( $field[ 'type_id' ] == 'radiobox' ) {

				$js .= "
					" . $field[ 'nameid' ] . ": '#" . $form[ 'html_id' ] . " input[name=\"" . $field[ 'nameid' ] . "\"]',";

			}

		}

		$js = substr( $js, 0, -1 );
		$js .= "
				}";

		$js .= "
				var selectors_value = {}
				for ( var key in selectors ) {

					if ( ($( selectors[ key ] ).attr( 'type' ) == 'checkbox') || ($( selectors[ key ] ).attr( 'type' ) == 'radio') ) {
						selectors_value[ key ] = $( selectors[ key ] + ':checked' ).val( );
					}
					else {
						selectors_value[ key ] = $( selectors[ key ] ).val( );
					}

				}";


				$js .= "
				$.ajax({
					url: '/ajax/?mod=catalog.action.forms_ajax',
					dataType: 'JSON',
					method: '" . $method . "',
					data: selectors_value

				}).done( function( data ) {

					console.log( data );

					$( '#" . $form[ 'html_id' ] . "' ).nextAll( '#preloader-ajax' ).remove();

					if ( data.s ) {";

				// форма выхода
				if ( $form[ 'action' ] == 'logout' ) {
					;
				}
				// остальные формы
				else {

					$js .= "
						for( var key in selectors ) {

							$( selectors[ key ] ).css( 'background-color', window.formAjax.conf.bg );
						}

						$( '#" . $form[ 'html_id' ] . "' ).after( '<p class=\"success-message\">' + data.mess + '</p>' );";
				}

				$js .= "
						if ( data.action != undefined && Object.keys( data.action ).length ) {
							window.formAjax.action_js( data.action );
						}

						console.log( 'Все ок' );

					}
					else {

						if( 'general_err' in data ) {
							$( '#" . $form[ 'html_id' ] . "' ).after( '<p class=\"error-message\">' + data.general_err + '</p>' );
							return false;
						}

						$( '#" . $form[ 'html_id' ] . "' ).show();

						for( var key in data.err ) {

							console.log( selectors[ key ] );

							if ( key == 'system_message' ) continue;

							if ( $( selectors[ key ] ).hasClass( 'hidden-field' ) ) {
								$( selectors[ key ] ).next().append( '<div class=\"err-tooltip\">' + data.err[ key ] + '</div>' );
								$( selectors[ key ] ).next().css( 'background', window.formAjax.conf.errBg );
								$( 'body' ).on( 'click',selectors[ key ], function() {
									$( this ).next().removeAttr('style');
									$( this ).next().find( '.err-tooltip' ).remove();
								});

								continue;
							}

							if( $( selectors[ key ] ).attr('type') == 'radio' && !$( selectors[ key ] ).closest('.custom-radio').find('.err-tooltip').length ) {
								$( selectors[ key ] ).closest('.custom-radio').append( '<div class=\"err-tooltip\">' + data.err[ key ] + '</div>');

								$( 'body' ).on( 'click', selectors[ key ], function() {
									$( this ).closest('.custom-radio').find('.err-tooltip').remove();
								});
		
								continue;
							}

							$( selectors[ key ] ).css( 'background-color', window.formAjax.conf.errBg );
							$( selectors[ key ] ).after( '<div class=\"err-tooltip\">' + data.err[ key ] + '</div>' );

							$( 'body' ).on( 'click', selectors[ key ], function() {
								$( this ).css( 'background-color', '' );
								$( this ).siblings( '.err-tooltip' ).remove();
							});

						}

					}

				});

			},";

	}


	$js .= "
			action_js: function( arr ) {
				for( var key in arr ) {
					// перенаправления
					if ( key == 'location' ) {
						// текущая страничка
						if ( arr[ key ] == 'self' ) {
							location.href = window.formAjax.conf.url + window.formAjax.conf.uri;
						}
						else {
							location.href = window.formAjax.conf.url + '/' + arr[ key ];
						}
					}
					else {
						location.href = arr[ key ];
					}
					// ...
				}
			},
			app: function() {
				" . $app . "
			}
		}
		window.formAjax.app();
	});

})( jQuery )";
	

	header( 'Content-Type: application/javascript; charset=utf-8' );

	// минимизация js
	if ( !isset( $_GET[ 'debug' ] ) ) {
		$min_js = new Min_js();
		$js = $min_js -> squeeze( $js, true, false );
	}
	//
	
	echo $js;