<?php

$table = new Table('catalog_section');
$section = $table -> select('SELECT `id` FROM `catalog_section` WHERE `alias`=:alias LIMIT 1', array( 'alias' => $alias ) );

if( !count( $section ) ) return false;


$history = $table -> select('SELECT * FROM `position_history` WHERE `section_id`=:id ORDER BY `year` ASC', array( 'id' => $section[0]['id'] ) );

if( !count( $history ) ) return false;

$str = '';

foreach( $history as $item ) {

	$str .= '<div class="item">
				<div class="year"><p>'. $item['year'] .'</p></div>
				<div class="info">
					<h2>'. $item['title'] .'</h2>
					<div class="content">'. $item['description'] .'</div>
				</div>
			</div>';

}

echo $str;