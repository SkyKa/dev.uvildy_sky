<?php

$table = new Table('catalog_section');

$section = $table -> select('SELECT `id`, `title` FROM `catalog_section` WHERE `alias`=:alias LIMIT 1', array( 'alias' => $alias ) );
if( empty( $section ) ) return false;

$rows = $table -> select('SELECT * FROM `catalog_section` WHERE `parent_id`=:id ORDER BY `position`', array( 'id' => $section[0]['id'] ) );
if( empty( $rows ) ) return false;

$str = '<div class="cabinets-page">
			<div class="breadcrumbs">
				<div class="container">';
				if( !empty( $params[0] ) ) {
					$str .= val('catalog.action.breadcrumbs', array( 'last_link' => true ) );
					$str .= '<span>'. $section[0]['title'] .'</span>';
				} 
				else {
					$str .= val('catalog.action.breadcrumbs');
				}
				$str .= '</div>
			</div>
			<div class="container cabinets">
				<h1>'. val('pages.show.title') .'</h1>
				<div class="row">';

$i = 1;
$modals = '';
foreach( $rows as $row ) {

	$item = $table -> select('SELECT * FROM `section_cabinets` WHERE `id`=:id AND `public`', array( 'id' => $row['id'] ) );

	if( !count( $item ) ) continue;

	$item = end( $item );

	$str .= '<div class="col-sm-6 item-wrap">';
			$str .=	'<div class="item">
					<div class="basic-button big">
						<a href="" data-toggle="modal" data-target="#cab-info-'. $i .'">'. $row['title'] .'</a>
					</div>
				</div>
			</div>';

	$modals .= '<div class="modal fade cabinet-modal" id="cab-info-'. $i .'" tabindex="-1" role="dialog" aria-labelledby="callback-modalLabel" aria-hidden="true">
				<div class="modal-dialog">
					<div class="modal-content">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
							<h2>'. $row['title'] .'</h2>
						</div>
						<div class="modal-body content">';

	if( !empty( $item['content'] ) ) {
		$modals .= $item['content'];
	}
	else {

		$procedures = $table->select('SELECT * FROM `position_cabinets` WHERE `section_id`=:id ORDER BY `position`', array( 'id' => $row['id'] ));

		$exist_unit = $table->select('SELECT `id` FROM `position_cabinets` WHERE `section_id`=:id AND `position_cabinets`.`view` IS NOT NULL 
			AND `position_cabinets`.`view` <> "" LIMIT 1', array( 'id' => $row['id'] ));

		$modals .=				'<div class="table-responsive">
									<table>
										<tbody>';

		if( empty($exist_unit) ) {
			$modals .= '<tr>
							<td>Наименование услуги</td>
							<td>Цена, руб.</td>
						</tr>';
		}
		else {
			$modals .= '<tr>
							<td>Наименование услуги</td>
							<td>Ед. изм.</td>
							<td>Цена, руб.</td>
						</tr>';
		}

		foreach( $procedures as $procedure ) {

			if( empty($exist_unit) ) {
				$modals .= '<tr>
								<td>'. $procedure['title'] .'</td>
								<td style="text-align: center;">'. $procedure['price'] .'</td>
							</tr>';
			}
			else {
				$modals .= '<tr>
								<td>'. $procedure['title'] .'</td>
								<td style="text-align: center;">'. $procedure['view'] .'</td>
								<td style="text-align: center;">'. $procedure['price'] .'</td>
							</tr>';
			}

		}

		$modals .=						'</tbody>
									</table>
								</div>';
	}


	$modals .=			'</div>
					</div>
				</div>
			</div>';

	$i++;

}

$str .=        '</div>
			</div>
		</div>';

$str .= $modals;

echo $str;