<?php

	$str = '';
	$id = Utils :: getVar( 'building' );

	$result = array(	
						's' => false, 
						'name' => '',
						'img' => '', 
						'info' => ''
					);

	$table = new Table('catalog_section');
	$man = $table -> select("SELECT * FROM `position_staff` WHERE `id`=:id AND `public` LIMIT 1", array( 'id' => $id ) );

	if ( !count( $man ) ) return false;
	$man = end( $man );

	$result[ 'name' ] = $man[ 'name' ];

	$img = get_cache_pic( $man[ 'img' ], 389,  389, true );
	$result[ 'img' ] = $img;
	$result[ 'info' ] = $man[ 'info' ];

	$result[ 'id' ] = $id;

	$result[ 's' ] = true;

	header( 'content-type: application/json; charset=utf-8' );

	echo json_encode( $result );
	exit( );