<?php

$my_alias = 'karta-infrastruktury';

$table = new Table('catalog_section');
$section = $table -> select('SELECT `id` FROM `catalog_section` WHERE `alias`=:alias LIMIT 1', array( 'alias' => $my_alias ) );

if( !count( $section ) ) return false;

$rows = $table -> select('SELECT * FROM `position_map_objects` WHERE `section_id`=:id AND `public` ORDER BY `position` DESC', array( 'id' => $section[0]['id'] ) );

if( !count( $rows ) ) return false;

$str = '<ul>';

foreach( $rows as $row ) {

	$str .= '<li>
				<a class="'. $row[ 'alias' ] .'" href="" style="background-image: url(/'. $row[ 'icon_menu' ] .');" 
					data-small-icon="/'. get_cache_pic( $row[ 'icon_map' ], 34, 46, false ) .'" data-x="'. $row[ 'map_x' ] .'" data-y="'. $row[ 'map_y' ] .'"  data-popup-text="'. htmlspecialchars($row[ 'popup_text' ]) .'"">
						'. $row[ 'title' ] .'
				</a>
			</li>';

}

$str .= '</ul>';

echo $str;


/*

<ul>
	<li><a class="admin" href="" style="background-image: url(/mobile_static/img/menu15.png);">Администрация</a></li>
	<li><a class="housing1" href="" style="background-image: url(/mobile_static/img/menu10.png);">1 корпус</a></li>
	<li><a class="housing2" href="" style="background-image: url(/mobile_static/img/menu10.png);">2 корпус</a></li>
	<li><a class="housing4" href="" style="background-image: url(/mobile_static/img/menu10.png);">4 корпус</a></li>
	<li><a class="cott" href="" style="background-image: url(/mobile_static/img/menu16.png);">Коттеджи</a></li>
	<li><a class="phys" href="" style="background-image: url(/mobile_static/img/menu13.png);">Лечебный корпус</a></li>
	<li><a href="" style="background-image: url(/mobile_static/img/menu17.png);">Основной ресторан</a></li>
	<li><a href="" style="background-image: url(/mobile_static/img/menu17.png);">Ресторан "Акватория"</a></li>
	<li><a href="" style="background-image: url(/mobile_static/img/menu17.png);">Летний ресторан "Шамбала"</a></li>
	<li><a class="music-rest" href="" style="background-image: url(/mobile_static/img/menu17.png);">Музыкальный ресторан</a></li>
	<li><a class="banya" href="" style="background-image: url(/mobile_static/img/banya_icon.png);">Баня</a></li>
	<li><a class="sauna" href="" style="background-image: url(/mobile_static/img/sauna.png);">Сауна</a></li>
	<li><a class="pool" href="" style="background-image: url(/mobile_static/img/pool.png);">Бассейн</a></li>
	<li><a class="hammam" href="" style="background-image: url(/mobile_static/img/hammam.png);">Хаммам</a></li>
	<li><a class="bank" href="" style="background-image: url(/mobile_static/img/menu14.png);">Рецепция/банкомат</a></li>
	<li><a href="" style="background-image: url(/mobile_static/img/menu1.png);">Пункт проката зима/лето</a></li>
	<li><a class="beach" href="" style="background-image: url(/mobile_static/img/menu2.png);">Пункт проката "Пляж"</a></li>
	<li><a class="recliner" href="" style="background-image: url(/mobile_static/img/menu3.png);" data-small-icon="/mobile_static/img/recliners_icon.png" data-x="72.5" data-y="75">Пункт проката шезлонгов</a></li>
	<li><a class="beach" href="" style="background-image: url(/mobile_static/img/beach.png);">Пляж</a></li>
	<li><a class="dance" href="" style="background-image: url(/mobile_static/img/dance.png);">Танцевальная площадка</a></li>
</ul>

*/