<?php

    $alias = Utils :: getVar( 'alias' );
	$items = $data -> getItems( $menu -> id );
    $uri_orig = $_SERVER[ 'REQUEST_URI' ];
    $uris = explode( '?', $uri_orig );
    $uri = $uris[ 0 ];

    foreach( $items as $item ) {

        echo    '<a href="'. $item[ 'type_link' ] .'" class="menu-item" style="background-image: url(/'. $item[ 'img_src' ] .');">
					<div class="title-wrap">
						<div class="title">
							<p>'. $item[ 'title' ] .'</p>
						</div>
					</div>
				</a>';

    }
