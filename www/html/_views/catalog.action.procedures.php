<?php 

if( !empty( $params[1] ) ) {
	mod('catalog.action.procedure');
	return;
}

$my_alias = 'procedury6';

if( !empty( $params[0] ) ) {
	$my_alias = $params[0];
}

$table = new Table('catalog_section');

$section = $table -> select('SELECT `id`, `title` FROM `catalog_section` WHERE `alias`=:alias LIMIT 1', array( 'alias' => $my_alias ) );
if( empty( $section ) ) return false;

$rows = $table -> select('SELECT * FROM `catalog_section` WHERE `parent_id`=:id', array( 'id' => $section[0]['id'] ) );
if( empty( $rows ) ) return false;

$str = '<div class="procedures-page">
			<div class="breadcrumbs">
				<div class="container">';
				if( !empty( $params[0] ) ) {
					$str .= val('catalog.action.breadcrumbs', array( 'last_link' => true ) );
					$str .= '<span>'. $section[0]['title'] .'</span>';
				} 
				else {
					$str .= val('catalog.action.breadcrumbs');
				}
				$str .= '</div>
			</div>
			<div class="container programms">
				<h1>'. val('pages.show.title') .'</h1>
				<div class="row">';

foreach( $rows as $row ) {

	$item = $table -> select('SELECT * FROM `section_separate_procedures` WHERE `id`=:id AND `public`', array( 'id' => $row['id'] ) );

	if( !count( $item ) ) continue;

	$item = end( $item );

	$str .= '<div class="col-md-3 col-sm-6 item-wrap">';
				$str .=	'<div class="item" onclick="window.location.href=\'/'. $alias . '/' . $params[0] . '/' . $row['alias'] .'.html\'">
				<div class="img-wrap" style="background-image: url(/'. get_cache_pic( $item['img'], 345, 439, true ) .');"></div>
					<p class="title">'. $row['title'] .'</p>
					<div class="overlay-wrap">
						<div class="overlay">
							<div class="button-wrapper">
								<div class="basic-button details">';
								if( !empty( $params[0] ) ) {
									$str .= '<a href="/'. $alias . '/' . $params[0] . '/' . $row['alias'] .'.html">Подробнее</a>';
								}
								else {
									$str .= '<a href="/'. $alias . '/' . $row['alias'] .'.html">Подробнее</a>';
								}
							$str.= '</div>
							</div>
						</div>
					</div>
				</div>
			</div>';

}

$str .=        '</div>
			</div>
		</div>';

echo $str;