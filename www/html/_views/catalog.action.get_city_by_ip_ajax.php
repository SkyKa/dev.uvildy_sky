<?php

header( 'content-type: application/json; charset=utf-8' );
usleep(100000);

$json = array(
		's' => false,
		'mess' => '',
		'city' => '',
	);

$ip = Utils :: getVar('ip');

// Подключаем SxGeo.php класс
include("/home/cefar1/projects/uvildi/sypex/SxGeo.php");
// Создаем объект
// Первый параметр - имя файла с базой (используется оригинальная бинарная база SxGeo.dat)
// Второй параметр - режим работы: 
//     SXGEO_FILE   (работа с файлом базы, режим по умолчанию); 
//     SXGEO_BATCH (пакетная обработка, увеличивает скорость при обработке множества IP за раз)
//     SXGEO_MEMORY (кэширование БД в памяти, еще увеличивает скорость пакетной обработки, но требует больше памяти)
$SxGeo = new SxGeo('/home/cefar1/projects/uvildi/sypex/SxGeoCity.dat');
//$SxGeo = new SxGeo('SxGeoCity.dat', SXGEO_BATCH | SXGEO_MEMORY); // Самый производительный режим, если нужно обработать много IP за раз

$json[ 'city' ] = $SxGeo->getCityFull($ip);
$json[ 'mess' ] = 'Все ОК!';
$json[ 's' ] = true;

echo json_encode( $json );
