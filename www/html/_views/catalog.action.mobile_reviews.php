<?php

$table = new Table( 'catalog_section' );
$section = $table -> select('SELECT `id` FROM `catalog_section` WHERE `alias`=:alias LIMIT 1', array( 'alias' => $alias ) );


echo 	'<div class="container reviews-wrap">';

if( !empty( $section ) ) {
	show_reviews( $section[0]['id'] );
}

echo    '</div>';

echo 	'<div class="review-form-wrap">
			<h2>Оставить отзыв</h2>
			'. val( 'catalog.action.forms', array( 'alias' => 'ostavit_otzyv' ) ) .'
		</div>';


function show_reviews( $id ) {

	$table = new Table( 'catalog_section' );
	$reviews = $table -> select('SELECT * FROM `position_reviews` WHERE `section_id`=:id AND `public`', array( 'id' => $id ) );

	if( empty( $reviews ) ) return false;

	$str = '';

	foreach( $reviews as $review ) {

		$new_date = explode( ' ', date( "d m Y", $review[ 'datestamp' ] ) );
		$date = $new_date[ 0 ] . ' ' . Langvars :: replaceMonth( $new_date[ 1 ] ) . ' ' . $new_date[ 2 ];

		$str .= '<div class="item">
					<p class="name">'. $review[ 'name' ] .'</p>
					<p class="date">'. $date .'</p>
					<p class="text">'. $review[ 'text' ] .'</p>
				</div>';

	}

	echo $str;

}