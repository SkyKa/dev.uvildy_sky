<?php 

if( !empty( $params[1] ) ) {
	mod('catalog.action.mobile_programm_details');
	return;
}

$table = new Table('catalog_section');
$section = $table -> select('SELECT * FROM `catalog_section` WHERE `alias`=:alias LIMIT 1', array( 'alias' => $params[0] ));

if( empty( $section ) ) return false;
$section = end( $section );

$str = '<header>
			<div class="et-menu-collapse-btn et-collapsed">
				<div class="et-collapse-line"></div>
				<div class="et-collapse-line"></div>
				<div class="et-collapse-line"></div>
			</div>
			
			<div class="h1-wrap">
				<h1>'. $section[ 'title' ] .'</h1>
			</div>

			<a href="/mobile_static/programms.html" class="back-button">
				<img src="/mobile_static/img/back_button.png" alt="">
			</a>

		</header>

		<div class="all-infra-menu" style="visibility: visible;">
			'. val('catalog.action.mobile_map_menu') .'
	    </div>
		
		<div class="content-wrapper">
			<div class="wrapper">
				<div class="content-inner">';

$item = $table -> select('SELECT * FROM `section_programms` WHERE `id`=:id AND `public` LIMIT 1', array( 'id' => $section[ 'id' ] ));

if( empty( $item ) ) return false;
$item = end( $item );

$str .= '<div class="et-container brain-clinic">';

$images = $table -> select('SELECT * FROM `position_gallery` WHERE `section_id`=:id AND `public` ORDER BY `position`', 
	array( 'id' => $section[ 'id' ] ));

if( ( !empty($images) ) ) {
	$str .= '<img src="/'. get_cache_pic( $images[0]['img'], 504, 642, true ) .'" alt="" class="brain">';
}
			
$str .=		'<div class="content-container">
				<div class="content">';

if( !empty( $item[ 'subtitle' ] ) ) {
	$str .= '<h2>'. $item[ 'subtitle' ] .'</h2>';
}

if( !empty( $item[ 'description' ] ) ) {
			$str .= '<p>'. $item[ 'description' ] .'</p>';
}
			$str .=	'<div class="line"></div>';

if( !empty( $item[ 'price' ] ) ) {
			$str .= '<p class="info-wrap">Стоимость программы: <span class="info">от '. $item[ 'price' ] .' <span class="rouble">a</span> / сутки</span></p>';
}

if( !empty( $item[ 'mobile_number_days' ] ) ) {
			$str .= '<p class="info-wrap">Количество дней: <span class="info">от '. $item[ 'mobile_number_days' ] .' дней</span></p>';
}

$str .=				'<a href="/'. $alias . '/' . $params[0] . '/' . 'details' .'.html" class="details">Подробное описание</a>
				</div>
			</div>
		</div>';

echo $str; 