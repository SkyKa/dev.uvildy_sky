<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="utf-8">
    <?php 	$title = $registry -> title;
					mod('catalog.action.open_graph', array('title' => $title))
    ?>
    <meta name="description" content="<?php out('description')?>" />
    <meta name="keywords" content="<?php out('keywords')?>" />
	<meta name="yandex-verification" content="ee5c6d79ea87d9f4" />
    <base href="<?php echo $base ?>" />
    <?php mod('catalog.editor.header')?>
    <link rel="shortcut icon" href="<?php Utils :: isChange( '/favicon.ico' )?>">
    <link rel="stylesheet" href="<?php Utils :: isChange( '/static/css/bootstrap.min.css' )?>">
    <link rel="stylesheet" href="<?php Utils :: isChange( '/static/css/kosm-main.css' )?>">
    <link rel="stylesheet" href="<?php Utils :: isChange( '/static/css/lightslider.min.css' )?>">
    <link rel="stylesheet" href="<?php Utils :: isChange( '/static/css/jquery-ui.min.css' )?>">
    <link rel="stylesheet" href="<?php Utils :: isChange( '/static/css/jquery.fancybox.css' )?>">
    <link rel="stylesheet" href="<?php Utils :: isChange( '/static/css/leaflet.css' )?>">
    <link rel="stylesheet" href="<?php Utils :: isChange( '/static/css/animate.css' )?>">
    <link rel="stylesheet" href="<?php Utils :: isChange( '/static/css/forms_js.css' )?>">
    <link rel="stylesheet" href="<?php Utils :: isChange( '/static/css/justifiedgallery.min.css' )?>">
    <link rel="stylesheet" href="<?php Utils :: isChange( '/static/css/lightgallery.min.css' )?>">
    <link rel="stylesheet" href="<?php Utils :: isChange( '/static/css/slick-theme.css' )?>">
    <link rel="stylesheet" href="<?php Utils :: isChange( '/static/css/slick.css' )?>">
    <link rel="stylesheet" href="<?php Utils :: isChange( '/static/css/style.css' )?>">
    <link rel="stylesheet" href="<?php Utils :: isChange( '/static/css/restorante_styles.css' )?>">
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
</head>

<body class="restorante_body_class">
	<header class="restorante_header">
		<div class="container">
            <div class="restorante_header_flex">
                <div class="restorante_header_logo">
                    <img src="/static/img/logo_restaraunts.png">
                </div>
                <div class="restorante_header_text_after_logo">
                    <a href="/">Организация банкетов и торжеств</a>
                </div>
                <div class="restorante_header_phone">
                    <a class="restorante_phone" href="tel:+73512251616">+7 (351) 225 16-16</a>
                    <a class="restorante_email" href="mailto:info@mail.ru">info@mail.ru</a>
                </div>
                <div>
                    <button class="restorante_white_button">Заказать звонок</button>
                </div>
            </div>
        </div>
	</header>

    <section class="restorante_slider">
        <img src="/static/img/restorante_slider_back.jpg">
        <div class="container">
            <h1><span>Ваша свадьба</span><br><span>в ресторане «Караван»</span></h1>
        </div>        
    </section>

    <section class="restorante_first_infoblock">
        <div class="container">
            <h2>
                <img src="/static/img/restorante_vetka_infoblock.png">
                Проведение свадьбы<br>в ресторане «Караван»
            </h2>
            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
            tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
            quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
            consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
            cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
            proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
            <div class="col-xs-12 col-sm-6 col-md-3 restorante_first_infoblock_item">
                <div class="first_infoblock_item_header">
                    <img src="/static/svg/3_people.svg">
                </div>
                <div class="first_infoblock_item_flex">
                    <p>вместительность<br>до 120 человек</p>
                    <img src="/static/img/restorante_item_infoblock.png">
                </div>              
            </div>
            <div class="col-xs-12 col-sm-6 col-md-3 restorante_first_infoblock_item">
                <div class="first_infoblock_item_header">
                    <img src="/static/svg/salut.svg">
                </div>
                <div class="first_infoblock_item_flex">
                    <p>азвлекательная<br>программа</p>
                    <img src="/static/img/restorante_item_infoblock.png">
                </div>              
            </div>
            <div class="col-xs-12 col-sm-6 col-md-3 restorante_first_infoblock_item">
                <div class="first_infoblock_item_header">
                    <img src="/static/svg/microphone.svg">
                </div>
                <div class="first_infoblock_item_flex">
                    <p>караоке, диджей<br>и живой звук</p>
                    <img src="/static/img/restorante_item_infoblock.png">
                </div>              
            </div>
            <div class="col-xs-12 col-sm-6 col-md-3 restorante_first_infoblock_item">
                <div class="first_infoblock_item_header">
                    <img src="/static/svg/golubki.svg">
                </div>
                <div class="first_infoblock_item_flex">
                    <p>уникальный дизайн<br>интерьера ресторана</p>
                    <img src="/static/img/restorante_item_infoblock.png">
                </div>              
            </div>
        </div>
    </section>

    <section class="restorante_second_infoblock">
        <img src="/static/img/restorante_kissing.png">
        <div class="container restorante_second_infoblock_cont">
            <div class="restorante_second_infoblock_flex">
                <div class="col-xs-12 col-sm-6 col-md-3 restorante_second_infoblock_item">
                    <h3>43</h3>
                    <p>свадьбы<br>проведено</p>
                </div>
                <div class="col-xs-12 col-sm-6 col-md-3 restorante_second_infoblock_item">
                    <h3>1 234</h3>
                    <p>счастливых пары<br>соеденены</p>
                </div>
                <div class="col-xs-12 col-sm-6 col-md-3 restorante_second_infoblock_item">
                    <h3>3 689</h3>
                    <p>довольных гостей<br>ресторана</p>
                </div>
                <div class="col-xs-12 col-sm-6 col-md-3 restorante_second_infoblock_item">
                    <h3>36 483</h3>
                    <p>приготовлено<br>блюд шеф-поваром</p>
                </div>
            </div>            
        </div>
    </section>

    <section class="restorante_third_infoblock">
        <img src="/static/img/third_infoblock_back.jpg">
        <div class="container">
            <h2>Выездная регистрация<br>на берегу озера увильды</h2>
            <p>До недавнего времени считалось, что плант бренда реально развивается контент. План размещения изменяет пак-шот, расширяя долю рынка размещения.</p>
            <button class="restorante_red_button">заказать регистрацию</button>
        </div>
    </section>

    <section class="restorante_weddings">
        <h2>проведённые свадьбы</h2>
        <div class="restorante_weddings_slider">
            <div class="restorante_weddings_item">
                <a href="#">
                    <div class="restorante_weddings_item_img">
                        <img src="/static/img/restorante_weddings_img_1.jpg">
                    </div>
                    <p class="restorante_weedings_date">12 / 06 / 2017</p>
                    <p class="restorante_weedings_short_desc">Свадьба Лукиных Татьяны и Ивана, 120 гостей, шоу-программа, диджей и официанты</p>
                </a>
            </div>
        </div>      
    </section>

    <section class="restorante_guest_reviews">
        <h2>Отзывы гостей</h2>
        <div class="restorante_guest_reviews_slider">
            <div class="restorante_guest_reviews_slider_item">
                <div class="restorante_guest_reviews_slider_item_border">
                    <img class="restorante_guest_reviews_slider_item_border_img" src="/static/img/restorante_guest_reviews_slider_item_border.png">
                    <div class="restorante_guest_reviews_slider_item_photo">
                        <img src="/static/img/restorante_guest_reviews_slider_item_photo.png">
                    </div>     
                    <p class="restorante_guest_reviews_slider_item_review">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                    tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                    quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
                    consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
                    cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
                    proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
                    <p class="restorante_guest_reviews_slider_item_name">Агапова Татьяна Ильинична</p>
                    <p class="restorante_guest_reviews_slider_item_status">38 лет, сестра невесты</p>              
                </div>              
            </div>
            <div class="restorante_guest_reviews_slider_item">
                <div class="restorante_guest_reviews_slider_item_border">
                    <img class="restorante_guest_reviews_slider_item_border_img" src="/static/img/restorante_guest_reviews_slider_item_border.png">
                    <div class="restorante_guest_reviews_slider_item_photo">
                        <img src="/static/img/restorante_guest_reviews_slider_item_photo.png">
                    </div>     
                    <p class="restorante_guest_reviews_slider_item_review">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                    tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                    quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
                    consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
                    cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
                    proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
                    <p class="restorante_guest_reviews_slider_item_name">Агапова Татьяна Ильинична</p>
                    <p class="restorante_guest_reviews_slider_item_status">38 лет, сестра невесты</p>              
                </div>              
            </div>
        </div>
        <div class="restorante_guest_reviews_buttons">            
            <div class="restorante_guest_reviews_slider_left">
                <i class="fas fa-chevron-left"></i>
            </div>
            <div class="restorante_guest_reviews_slider_right">
                <i class="fas fa-chevron-right"></i>
            </div>     
        </div>
    </section>

    <section class="restorante_stocks">
        <h2>Акции</h2>
        <div class="container">
            <div class="restorante_stocks_slider">
                <div class="restorante_stocks_slider_item">
                    <a href="#">
                        <!-- <img src="/static/img/restorante_stocks_slider_item_photo.jpg"> -->
                        <img src="/static/img/restorante_stocks_slider_item_photo.jpg">
                        <div class="restorante_stocks_slider_item_desc">
                            <p class="restorante_stocks_slider_item_desc_date">12 / 06 / 2017</p>
                            <p class="restorante_stocks_slider_item_desc_title">скидка 50% при раннем бронировании</p>
                            <p class="restorante_stocks_slider_item_desc_short">До недавнего времени считалось, что плант бренда реально развивается контент. План размещения изменяет пак-шот, расширяя долю рынка размещения.</p>
                            <button class="restorante_white_button restorante_stocks">подробнее</button>
                        </div>                                                
                    </a>
                </div>
            </div>            
        </div>
    </section>

    <section class="restorante_wedding_celebrate">
        <h2>Где ещё я могу отметить свадьбу?</h2>
            <div class="restorante_wedding_celebrate_item">
                <img src="/static/img/restorante_wedding_celebrate_item_img.jpg">
                <div class="restorante_wedding_celebrate_item_inner">
                    <div class="restorante_wedding_celebrate_item_inner_title">
                        <p>Зал до <span>200</span> человек</p>
                        <h3>Ресторан шинок</h3>
                    </div>
                    <p class="restorante_wedding_celebrate_item_inner_desc">До недавнего времени считалось, что плант бренда реально развивается контент. План размещения изменяет пак-шот, расширяя</p>
                    <a class="restorante_white_button" href="#">подробнее</a>
                </div>
            </div>
            <div class="restorante_wedding_celebrate_item">
                <img src="/static/img/restorante_wedding_celebrate_item_img.jpg">
                <div class="restorante_wedding_celebrate_item_inner">
                    <div class="restorante_wedding_celebrate_item_inner_title">
                        <p>Зал до <span>200</span> человек</p>
                        <h3>Ресторан шинок</h3>
                    </div>
                    <p class="restorante_wedding_celebrate_item_inner_desc">До недавнего времени считалось, что плант бренда реально развивается контент. План размещения изменяет пак-шот, расширяя</p>
                    <a class="restorante_white_button" href="#">подробнее</a>
                </div>
            </div> 
            <div class="restorante_wedding_celebrate_item">
                <img src="/static/img/restorante_wedding_celebrate_item_img.jpg">
                <div class="restorante_wedding_celebrate_item_inner">
                    <div class="restorante_wedding_celebrate_item_inner_title">
                        <p>Зал до <span>200</span> человек</p>
                        <h3>Ресторан шинок</h3>
                    </div>
                    <p class="restorante_wedding_celebrate_item_inner_desc">До недавнего времени считалось, что плант бренда реально развивается контент. План размещения изменяет пак-шот, расширяя</p>
                    <a class="restorante_white_button" href="#">подробнее</a>
                </div>
            </div>   
    </section>


    <section class="restorante_honeymoon">
        <img src="/static/img/restorante_honeymoon_back.png">
        <div class="container">
            <div class="col-xs-12 col-md-offset-5 col-md-7 restorante_honeymoon_item">
                <h2>Провести незабываемый медовый месяц на курорте <span>«Увильды»</span></h2>
                <button>Забронировать дату</button>
            </div>
        </div>
    </section>

    <footer>
        <div class="container">
            <div class="restorante_footer_flex">
                <div class="restorante_footer_logo">
                    <img src="/static/img/logo_restaraunts_footer.png">
                </div>
                <div class="restorante_footer_text_after_logo">
                    <a href="/">Организация банкетов и торжеств</a>
                </div>
                <div class="restorante_header_phone">
                    <a class="restorante_phone_footer" href="tel:+73512251616">+7 (351) 225 16-16</a>
                    <a class="restorante_email_footer" href="mailto:info@mail.ru">info@mail.ru</a>
                </div>
                <div>
                    <button class="restorante_red_button">Заказать звонок</button>
                </div>
            </div>
            <div class="restorante_footer_flex">
                <div class="restorante_footer_privacy">
                    <p>© УВИЛЬДЫ<br>Все права защищенны 2018г.</p>
                    <a href="/">Политика конфиденциальности</a>
                </div>
                <div class="restorante_footer_socials">
                    <a href="/"><i class="fab fa-facebook-f"></i></a>
                    <a href="/"><i class="fab fa-vk"></i></a>
                    <a href="/"><i class="fab fa-instagram"></i></a>
                    <a href="/"><i class="fab fa-odnoklassniki"></i></a>
                    <a href="/"><i class="fab fa-telegram-plane"></i></a>
                    <a href="/"><i class="fab fa-youtube"></i></a>
                </div>                
            </div>
        </div>
    </footer>
