<?php include "html/templates/header_main.tpl.php" ?>

<div class="main-page">

	<section>
		<div class="container-fluid">
			<div class="row">
			<div class="container">
		<div class="message_button">
			<button type="button" class="btn btn-primary" data-toggle="modal" data-target=".bs-example-modal-lg">Large modal</button>
			<div class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel">
			  <div class="modal-dialog modal-lg" role="document">
			    <div class="modal-content">
			      <div class="modal-header">
			        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			        <div class="large_modal_form_1">
			        <h4 class="modal-title" id="gridSystemModalLabel">Программа детокс</h4>
			   		</div>
			      </div>
			      <div class="modal-body">
			      	<div class="large_modal_form_2">
			      	<div class="row">
			      		<div class="large_modal_item">
				      		<div class="col-xs-12 col-md-2 large_modal_img">
				      			<h2 class="first_h2">Детокс лайт</h2>
				      			<img src="/static/img/stand_man.jpg">
				      		</div>
				      		<div class="col-xs-12 col-md-10 large_modal_content">
				      			<div class="col-xs-12">
				      			<h2>Детокс лайт</h2>
				      			<p>Программа «Детокс Лайт» — настоящая революция в очищении и обновлении организма.Это способ повернуть время вспять, заново запустить стрелку часов жизни.</p>
				      			<div class="just_c">
				      			<span class="large_modal_old_price">6 000 руб.</span>
				      			<span class="large_modal_new_price">3 000 руб.</span>
				      			</div>
				      			</div>
				      			<div class="col-xs-12 col-sm-6 large_modal_l_button"><button>Подробнее</button></div>
				      			<div class="col-xs-12 col-sm-6 large_modal_r_button"><button>Записаться</button></div>
				      		</div>
			      		</div>
			      		<div class="large_modal_item">
				      		<div class="col-xs-12 col-md-2 large_modal_img">
				      			<h2>Детокс лайт</h2>
				      			<img src="/static/img/stand_man.jpg">
				      		</div>
				      		<div class="col-xs-12 col-md-10 large_modal_content">
				      			<div class="col-xs-12">
				      			<h2>Детокс лайт</h2>
				      			<p>Программа «Детокс Лайт» — настоящая революция в очищении и обновлении организма.Это способ повернуть время вспять, заново запустить стрелку часов жизни.</p>
				      			<div class="just_c">
				      			<span class="large_modal_old_price">6 000 руб.</span>
				      			<span class="large_modal_new_price">3 000 руб.</span>
				      			</div>
				      			</div>
				      			<div class="col-xs-12 col-sm-6 large_modal_l_button"><button>Подробнее</button></div>
				      			<div class="col-xs-12 col-sm-6 large_modal_r_button"><button>Записаться</button></div>
				      		</div>
			      		</div>
			      		<div class="large_modal_item">
				      		<div class="col-xs-12 col-md-2 large_modal_img">
				      			<h2>Детокс лайт</h2>
				      			<img src="/static/img/stand_man.jpg">
				      		</div>
				      		<div class="col-xs-12 col-md-10 large_modal_content">
				      			<div class="col-xs-12">
				      			<h2>Детокс лайт</h2>
				      			<p>Программа «Детокс Лайт» — настоящая революция в очищении и обновлении организма.Это способ повернуть время вспять, заново запустить стрелку часов жизни.</p>
				      			<div class="just_c">
				      			<span class="large_modal_old_price">6 000 руб.</span>
				      			<span class="large_modal_new_price">3 000 руб.</span>
				      			</div>
				      			</div>
				      			<div class="col-xs-12 col-sm-6 large_modal_l_button"><button>Подробнее</button></div>
				      			<div class="col-xs-12 col-sm-6 large_modal_r_button"><button>Записаться</button></div>
				      		</div>
			      		</div>
			      		<div class="large_modal_item">
				      		<div class="col-xs-12 col-md-2 large_modal_img">
				      			<h2>Детокс лайт</h2>
				      			<img src="/static/img/stand_man.jpg">
				      		</div>
				      		<div class="col-xs-12 col-md-10 large_modal_content">
				      			<div class="col-xs-12">
				      			<h2>Детокс лайт</h2>
				      			<p>Программа «Детокс Лайт» — настоящая революция в очищении и обновлении организма.Это способ повернуть время вспять, заново запустить стрелку часов жизни.</p>
				      			<div class="just_c">
				      			<span class="large_modal_old_price">6 000 руб.</span>
				      			<span class="large_modal_new_price">3 000 руб.</span>
				      			</div>
				      			</div>
				      			<div class="col-xs-12 col-sm-6 large_modal_l_button"><button>Подробнее</button></div>
				      			<div class="col-xs-12 col-sm-6 large_modal_r_button"><button>Записаться</button></div>
				      		</div>
			      		</div>
			      		<div class="large_modal_item">
				      		<div class="col-xs-12 col-md-2 large_modal_img">
				      			<h2>Детокс лайт</h2>
				      			<img src="/static/img/stand_man.jpg">
				      		</div>
				      		<div class="col-xs-12 col-md-10 large_modal_content">
				      			<div class="col-xs-12">
				      			<h2>Детокс лайт</h2>
				      			<p>Программа «Детокс Лайт» — настоящая революция в очищении и обновлении организма.Это способ повернуть время вспять, заново запустить стрелку часов жизни.</p>
				      			<div class="just_c">
				      			<span class="large_modal_old_price">6 000 руб.</span>
				      			<span class="large_modal_new_price">3 000 руб.</span>
				      			</div>
				      			</div>
				      			<div class="col-xs-12 col-sm-6 large_modal_l_button"><button>Подробнее</button></div>
				      			<div class="col-xs-12 col-sm-6 large_modal_r_button"><button>Записаться</button></div>
				      		</div>
			      		</div>
			      		<div class="large_modal_item">
				      		<div class="col-xs-12 col-md-2 large_modal_img">
				      			<h2>Детокс лайт</h2>
				      			<img src="/static/img/stand_man.jpg">
				      		</div>
				      		<div class="col-xs-12 col-md-10 large_modal_content">
				      			<div class="col-xs-12">
				      			<h2>Детокс лайт</h2>
				      			<p>Программа «Детокс Лайт» — настоящая революция в очищении и обновлении организма.Это способ повернуть время вспять, заново запустить стрелку часов жизни.</p>
				      			<div class="just_c">
				      			<span class="large_modal_old_price">6 000 руб.</span>
				      			<span class="large_modal_new_price">3 000 руб.</span>
				      			</div>
				      			</div>
				      			<div class="col-xs-12 col-sm-6 large_modal_l_button"><button>Подробнее</button></div>
				      			<div class="col-xs-12 col-sm-6 large_modal_r_button"><button>Записаться</button></div>
				      		</div>
			      		</div>
			     	</div>
			      </div>
			    </div>
			</div>
			</div>
		</div>
		</div>
		</div>
		</div>
	</section>

	<section class="img-logo-info">
		<div class="main-img-wrap">
			<ul id="smooth-background">
				<li style="background-image: url(/static/img/main_img_1.jpg);"></li>
				<li style="background-image: url(/static/img/main_img_2.jpg);"></li>
				<li style="background-image: url(/static/img/main_img_3.jpg);"></li>
				<li style="background-image: url(/static/img/main_img_4.jpg);"></li>
				<li style="background-image: url(/static/img/main_img_5.jpg);"></li>
			</ul>
			<div class="container logo">
				<img src="/static/img/big_logo.png" alt="">
			</div>
		</div>
		<div class="info">
			<div class="container">
				<h2>Уникальные природные факторы</h2>
				<div class="row">
					<div class="col-sm-4 item-wrap">
						<div class="item">
							<div class="icon">
								<img src="/static/img/nature1.png" alt="">
							</div>
							<div class="text">
								<h3>Уникальный климат</h3>
								<div class="basic-button"><a href="/unikalnyj-klimat.html">подробнее</a></div>
							</div>
						</div>
					</div>
					<div class="col-sm-4 item-wrap">
						<div class="item">
							<div class="icon">
								<img src="/static/img/nature2.png" alt="">
							</div>
							<div class="text">
								<h3>Радоновые воды</h3>
								<div class="basic-button"><a href="/radonovye-vody.html">подробнее</a></div>
							</div>
						</div>
					</div>
					<div class="col-sm-4 item-wrap">
						<div class="item">
							<div class="icon">
								<img src="/static/img/nature3.png" alt="">
							</div>
							<div class="text">
								<h3>Сапропелевые грязи</h3>
								<div class="basic-button"><a href="/sapropelevye-gryazi.html">подробнее</a></div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>

	<?php mod('catalog.action.main_programms') ?>

	<section class="about-order">
		<div class="container">
			<div class="about">
				<?php mod('infoblock.show.about_main') ?>
				<div class="basic-button"><a href="/o-nas.html">о курорте</a></div>
			</div>
			<div class="order">
				<h3>
					<img src="/static/img/order_icon.png" alt="">
					Онлайн<br>Бронирование
				</h3>
				<p>
					<?php mod('infoblock.show.booking_main') ?>
				</p>
				<div class="basic-button"><a href="/bronirovanie.html">забронировать номер</a></div>
			</div>
		</div>
	</section>

</div>

<?php mod('catalog.action.notify') ?>

<?php include "html/templates/footer.tpl.php" ?>