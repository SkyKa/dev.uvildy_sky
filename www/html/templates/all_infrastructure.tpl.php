<?php include "html/templates/header.tpl.php" ?>

<div class="breadcrumbs">
	<div class="container">
		<nav class="breadcrumbs">
			<?php mod('catalog.action.breadcrumbs') ?>
		</nav>
	</div>
</div>


<div class="container">
	<h1><?php mod('pages.show.title') ?></h1>
</div>

<div class="map-page">
	<div id="leaflet-map" class="leaflet-map"></div>
	<a href="/bronirovanie.html" class="to-map">К Бронированию</a>
	<div class="menu-wrap">
		<a href="" class="turn-menu">
			<img src="/static/img/map_menu_arrow.png" alt="">
			<span>меню</span>
		</a>
		<div class="all-infra-menu">
			<?php mod('catalog.action.map_menu') ?>
		</div>
	</div>
</div>

<?php include "html/templates/footer.tpl.php" ?>