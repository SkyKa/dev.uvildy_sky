<?php include "html/templates/header_main.tpl.php" ?>

<div class="main-page">
	<section class="img-logo-info">
		<div class="main-img-wrap">
			<ul id="smooth-background">
				<li style="background-image: url(/static/img/main_img_1.jpg);"></li>
				<li style="background-image: url(/static/img/main_img_2.jpg);"></li>
				<li style="background-image: url(/static/img/main_img_3.jpg);"></li>
				<li style="background-image: url(/static/img/main_img_4.jpg);"></li>
				<li style="background-image: url(/static/img/main_img_5.jpg);"></li>
			</ul>
			<div class="container logo">
				<img src="/static/img/big_logo.png" alt="">
			</div>
		</div>
		<div class="info">
			<div class="container">
				<h2>Уникальные природные факторы</h2>
				<div class="row">
					<div class="col-sm-4 item-wrap">
						<div class="item">
							<div class="icon">
								<img src="/static/img/nature1.png" alt="">
							</div>
							<div class="text">
								<h3>Уникальный климат</h3>
								<div class="basic-button"><a href="/unikalnyj-klimat.html">подробнее</a></div>
							</div>
						</div>
					</div>
					<div class="col-sm-4 item-wrap">
						<div class="item">
							<div class="icon">
								<img src="/static/img/nature2.png" alt="">
							</div>
							<div class="text">
								<h3>Радоновые воды</h3>
								<div class="basic-button"><a href="/radonovye-vody.html">подробнее</a></div>
							</div>
						</div>
					</div>
					<div class="col-sm-4 item-wrap">
						<div class="item">
							<div class="icon">
								<img src="/static/img/nature3.png" alt="">
							</div>
							<div class="text">
								<h3>Сапропелевые грязи</h3>
								<div class="basic-button"><a href="/sapropelevye-gryazi.html">подробнее</a></div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>

	<?php mod('catalog.action.main_programms') ?>

	<section class="about-order">
		<div class="container">
			<div class="about">
				<?php mod('infoblock.show.about_main') ?>
				<div class="basic-button"><a href="/o-nas.html">о курорте</a></div>
			</div>
			<div class="order">
				<h3>
					<img src="/static/img/order_icon.png" alt="">
					Онлайн<br>Бронирование
				</h3>
				<p>
					<?php mod('infoblock.show.booking_main') ?>
				</p>
				<div class="basic-button"><a href="/bronirovanie.html">забронировать номер</a></div>
			</div>
		</div>
	</section>
</div>

<?php include "html/templates/footer.tpl.php" ?>