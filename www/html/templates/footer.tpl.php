</div>

		<?php mod( 'catalog.action.subscribe' )?>
		<footer>
			<div class="container">
				<div class="row">
					<div class="col-md-5 col-sm-6 menu">
						<ul>
							<?php mod('menus.show.footer') ?>
							<?php mod('menus.show.socials') ?>
						</ul>
					</div>
					<div class="clearfix visible-xs"></div>
					<div class="col-md-7 col-sm-6 info">
						<p class="title">© Многопрофильный центр медицины и реабилитации «Курорт Увильды»</p>
						<p class="phones">
							<?php mod( 'catalog.action.yandex_direct_phone', array( 'num' => 1, 'postfix' => ', ' ) )?>
							<?php mod( 'catalog.action.yandex_direct_phone', array( 'num' => 2, 'postfix' => ', ' ) )?>
							<?php mod( 'catalog.action.yandex_direct_phone', array( 'num' => 3, 'postfix' => ', ' ) )?>
							<?php mod( 'catalog.action.yandex_direct_phone', array( 'num' => 4 ) )?>
						</p>
						<p class="address">456890, Челябинская обл., пос. Увильды</p>
						<a href="/kontakty.html"  class="politics">Все контакты</a>
						<a href="/privacy-policy.html" class="politics">Политика конфиденциальности</a>
						<a href="">Напишите нам</a>
					</div>
				</div>
			</div>
			<?php mod( 'catalog.action.cancel_booking' ) ?>
			<?php mod( 'catalog.action.window_offer' ) ?>
		</footer>

		<div class="modal fade callback-modal" id="callback-modal" tabindex="-1" role="dialog" aria-labelledby="callback-modalLabel" aria-hidden="true">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
						<h2>Заказ звонка</h2>
					</div>
					<div class="modal-body">
						<?php mod( 'catalog.action.forms', array( 'alias' => 'zakazat_zvonok' ) ); ?>
					</div>
				</div>
			</div>
		</div>


	</div>

<script src="<?php Utils :: isChange( '/static/js/jquery-2.2.4.min.js' )?>"></script>

<script src="<?php Utils :: isChange( '/static/js/bootstrap.min.js' )?>"></script>
<script src="<?php Utils :: isChange( '/static/js/jquery.fancybox.pack.js' )?>"></script>
<script src="<?php Utils :: isChange( '/static/js/modernizr-custom.js' )?>"></script>
<script src="<?php Utils :: isChange( '/static/js/imagesloaded.pkgd.min.js' )?>"></script>
<script src="<?php Utils :: isChange( '/static/js/lightslider.min.js' )?>"></script>
<script src="<?php Utils :: isChange( '/static/js/jquery-ui.min.js' )?>"></script>
<script src="<?php Utils :: isChange( '/static/js/datepicker-ru.js' )?>"></script>
<script src="<?php Utils :: isChange( '/static/js/jquery.formstyler.min.js' )?>"></script>
<script src="<?php Utils :: isChange( '/static/js/leaflet.js' )?>"></script>
<script src="<?php Utils :: isChange( '/static/js/animatedmodal.min.js' )?>"></script>
<script src="<?php Utils :: isChange( '/static/js/jquery.flip.min.js' )?>"></script>
<script src="/ajax/?mod=catalog.action.forms_js"></script>
<script src="<?php Utils :: isChange( '/static/js/jquery.maskedinput.min.js' )?>"></script>
<script src="<?php Utils :: isChange( '/static/js/jquery.mousewheel.min.js' )?>"></script>
<script src="<?php Utils :: isChange( '/static/js/lightgallery-all.min.js' )?>"></script>
<script src="<?php Utils :: isChange( '/static/js/action.js' )?>"></script>
<script src="<?php Utils :: isChange( '/static/js/jquery.justifiedgallery.min.js' )?>"></script>
<script src="<?php Utils :: isChange( '/static/js/booking.js' )?>"></script>
<script src="<?php Utils :: isChange( '/static/js/leaflet.smoothmarkerbouncing.js' )?>"></script>
<script src="<?php Utils :: isChange( '/static/js/leaflet-map.js' )?>"></script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCxk-OJj0JvQEg7sSWYmLp6ErB5thWBivY"></script>
<!--<script src="<?php Utils :: isChange( '/static/js/bootstrap-notify-master/bootstrap-notify.min.js' )?>"></script>-->
<script src="<?php Utils :: isChange( '/static/js/slick.min.js' )?>"></script>
<script src="<?php Utils :: isChange( '/static/js/landing_action.js' )?>"></script>
<script src="<?php Utils :: isChange( '/static/js/fontawesome-all.js' )?>"></script>

<?php mod( 'banner.show.counters' )?>
</body>
</html>
