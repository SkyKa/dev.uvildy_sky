<?php include "html/templates/header.tpl.php" ?>

	<div class="breadcrumbs">
		<div class="container">
			<nav class="breadcrumbs">
				<?php mod('catalog.action.breadcrumbs_news') ?>
			</nav>
		</div>
	</div>

	<section class="landing_second_slider radon ofline">
		<div class="container-fluid">
			<div class="row landing_second_radon">
				<div class="container news-page"">
					<div class="col-xs-12">
						<?php mod('catalog.action.radon_news') ?>
					</div>
				</div>
			</div>
		</div>
	</section>


<?php include "html/templates/footer.tpl.php" ?>