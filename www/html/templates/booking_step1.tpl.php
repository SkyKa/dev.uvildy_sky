<?php include "html/templates/header.tpl.php" ?>

<div class="booking-map-page booking-page">
	<div class="container">
		<a href="" class="share" >
			<div class="info">
				<p class="title">Акция!</p>
				<p class="text">
					Горящие путевки, 4 корпус<br>
					Стандарт 2-х местный 1-комнатный
				</p>
				<div class="cost">
					<p><span class="number">2200</span> <span class="rouble">a</span> / сутки на человека</p>
					<div class="basic-button blue"><span href="">Подробнее</span></div>
				</div>
			</div>
			<div class="img-wrap"></div>
		</a>
		<nav class="breadcrumbs">
			<?php mod('catalog.action.breadcrumbs') ?>
		</nav>
	</div>

	<div class="container">
		<h1>Выберите здание для бронирования</h1>
		<p class="step">Шаг <span>1</span> из <span>3</span></p>
	</div>

	<section class="map">
		<img src="/static/img/map.jpg" alt="" class="map-img">
		<div class="zoom">
			<a class="up" href="">+</a>
			<a class="down" href="">–</a>
		</div>
	</section>
</div>

<?php include "html/templates/footer.tpl.php" ?>