<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="utf-8">
    <?php 	$title = $registry -> title;
					mod('catalog.action.open_graph', array('title' => $title))
    ?>
    <meta name="description" content="<?php out('description')?>" />
    <meta name="keywords" content="<?php out('keywords')?>" />
	<meta name="yandex-verification" content="ee5c6d79ea87d9f4" />
    <base href="<?php echo $base ?>" />
    <?php mod('catalog.editor.header')?>
    <link rel="shortcut icon" href="<?php Utils :: isChange( '/favicon.ico' )?>">
    <link rel="stylesheet" href="<?php Utils :: isChange( '/static/css/bootstrap.min.css' )?>">
    <link rel="stylesheet" href="<?php Utils :: isChange( '/static/css/kosm-main.css' )?>">
    <link rel="stylesheet" href="<?php Utils :: isChange( '/static/css/lightslider.min.css' )?>">
    <link rel="stylesheet" href="<?php Utils :: isChange( '/static/css/jquery-ui.min.css' )?>">
    <link rel="stylesheet" href="<?php Utils :: isChange( '/static/css/jquery.fancybox.css' )?>">
    <link rel="stylesheet" href="<?php Utils :: isChange( '/static/css/leaflet.css' )?>">
    <link rel="stylesheet" href="<?php Utils :: isChange( '/static/css/animate.css' )?>">
    <link rel="stylesheet" href="<?php Utils :: isChange( '/static/css/forms_js.css' )?>">
    <link rel="stylesheet" href="<?php Utils :: isChange( '/static/css/justifiedgallery.min.css' )?>">
    <link rel="stylesheet" href="<?php Utils :: isChange( '/static/css/lightgallery.min.css' )?>">
    <link rel="stylesheet" href="<?php Utils :: isChange( '/static/css/slick-theme.css' )?>">
    <link rel="stylesheet" href="<?php Utils :: isChange( '/static/css/slick.css' )?>">
    <link rel="stylesheet" href="<?php Utils :: isChange( '/static/css/style.css' )?>">
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
</head>

<body>
	<div class="wrapper">
		<div class="content-inner">


			<header class="header-pages">
				<div class="container">
					<div class="header-top">
						<a class="logo" href="/">
							<img src="/static/img/logo.png" alt="">
						</a>
						<div class="info-wrap">
							<div class="title-phone-time">
								<p class="title">Многопрофильный центр медицины и реабилитации</p>
								<div>
									<?php mod( 'catalog.action.yandex_direct_phone', array( 'num' => 1 ) )?>
								</div>
								<p class="work-time"></a>Прием звонков осуществляется с 7:00 до 16:00 МСК</p>
								
							</div>
							<div class="buttons-wrap">
								<div class="basic-button booking-button">
									<a href="/bronirovanie.html">забронировать номер</a>
								</div>
								<div class="basic-button booking-button">
									<a href="" data-toggle="modal" data-target="#callback-modal">заказать звонок</a>
								</div>

							</div>

							<form action="" id="search" class="collapsed">
								<input type="search" name="q" class="form-control" autocomplete="off">
								<button type="submit">
									<img src="/static/img/loupe.png" alt="">
								</button>
							</form>
						</div>
					</div>
					<div class="et-menu">
						<div class="et-menu-header">
							<div class="et-menu-collapse-btn et-collapsed">
								<div class="et-collapse-line"></div>
								<div class="et-collapse-line"></div>
								<div class="et-collapse-line"></div>
							</div>
						</div>  
						<?php mod('menus.show.main') ?>
						<div class="clearfix"></div>
					</div>
				</div>
			</header>
