<?php include "html/templates/header_offer.tpl.php" ?>

	<!-- BreadCrumbs: Start -->
	<div class="page-nav">
		<div class="container">

			<div class="page-nav-wrap">
				<div class="breadcrumbs">
					<a href="/">Главная</a> &nbsp;|&nbsp; <a href="/programmy.html">Программы</a> &nbsp;|&nbsp; <a href="/programmy/kosmetologiya.html">Косметология</a> &nbsp;|&nbsp; <span>Омоложение и подтяжка бёдер</span>
				</div>
				
				<div class="back-page">
					<a href="/programmy/kosmetologiya.html"><svg role="img"><use xlink:href="/static/svg/svg-sprite.svg#icon-arrows-back"></use></svg></a>
				</div>
			</div>

		</div>
	</div>
	<!-- BreadCrumbs: End -->


	<!-- MAINSCREEN: Start -->
	<section class="sct sct-main-screen">
		<div class="container">
			<h1 class="cst-title-1">Омоложение и подтяжка&nbsp;бёдер</h1>

			<div class="row">
				<div class="col-sm-8 col-sm-push-2 col-md-5 col-md-push-0 col-lg-6 col-lg-push-0">

					<img class="ms-photo" src="/static/img/main-screen-photo.png" alt="lags">

				</div>
				<div class="col-sm-12 col-md-7 col-lg-6">

					<!-- MainScreenForm: Start -->
					<div class="main-screen-form">
						<?php mod( 'catalog.action.forms', array( 'alias' => 'offer_shapka' ) )?>
					</div>
					<!-- MainScreenForm: End -->

				</div>
			</div>

		</div>
	</section>
	<!-- MAINSCREEN: End -->



	<!-- SOLUTIONS: Start -->
	<section class="sct sct-solutions lazy" data-src="/static/img/solutions-bg.jpg">
		<div class="container">

			<div class="solutions-container">

				<!-- JS on 991px -->
				<div class="solutions-header">
					<div class="row">
						<div class="col-sm-4">
							<div class="slt-header slt-prbl">
								<svg role="img"><use xlink:href="/static/svg/svg-sprite.svg#icon-ask"></use></svg>
								<span>Проблема</span>
							</div>
						</div>
						<div class="col-sm-4">
							<div class="slt-header slt-slts">
								<svg role="img"><use xlink:href="/static/svg/svg-sprite.svg#icon-ok"></use></svg>
								<span>Решение</span>
							</div>
						</div>
						<div class="col-sm-4">
							<div class="slt-header slt-res">
								<svg role="img"><use xlink:href="/static/svg/svg-sprite.svg#icon-warning"></use></svg>
								<span>Результат</span>
							</div>
						</div>
					</div>
				</div>

				<!-- FirstRow: Start -->
				<div class="slc-row">
					<div class="row">
						<div class="col-md-4 clearfix">

							<div class="prob-gitem-wrap clearfix">

<!-- 								<div class="slt-header">
									<svg role="img"><use xlink:href="/static/svg/svg-sprite.svg#icon-ask"></use></svg>
									<span>Проблема</span>
								</div> -->

								<div class="prob-item prob-item-first clearfix">
									<div class="pif-icon">
										<img class="lazy" data-src="/static/svg/bodies-parts/troubles-otek.svg" src="/static/img/lazy-bg.png" alt="Отек">
										<noscript><img src="/static/svg/bodies-parts/troubles-otek.svg" alt="Отек"></noscript>
									</div>
									<div class="pif-title">Отек</div>
								</div>

								<div class="prob-item prob-item-second clearfix">
									<div class="pif-icon">
										<img class="lazy" data-src="/static/svg/bodies-parts/troubles-rastyajki.svg" src="/static/img/lazy-bg.png" alt="Растяжки">
										<noscript><img src="/static/svg/bodies-parts/troubles-rastyajki.svg" alt="Растяжки"></noscript>
									</div>
									<div class="pif-title">Растяжки</div>
								</div>

							</div>

						</div>
						<div class="col-md-4 clearfix">

							<div class="slts-gitem-wrap clearfix">

<!-- 								<div class="slt-header slt-slts">
									<svg role="img"><use xlink:href="/static/svg/svg-sprite.svg#icon-ok"></use></svg>
									<span>Решение</span>
								</div> -->

								<div class="solution-item top-icon">
									<div class="slti-icon icon-rotate">
										<svg role="img"><use xlink:href="/static/svg/svg-sprite.svg#icon-laser"></use></svg>
									</div>
									<div class="slti-title">Лазер</div>
								</div>

							</div>

						</div>
						<div class="col-md-4 clearfix">

							<div class="res-gitem-wrap clearfix">

<!-- 								<div class="slt-header slt-res">
									<svg role="img"><use xlink:href="/static/svg/svg-sprite.svg#icon-warning"></use></svg>
									<span>Результат</span>
								</div> -->

								<div class="res-item res-item-first clearfix">
									<div class="pif-icon">
										<img class="lazy" data-src="/static/svg/bodies-parts/result-gladkie-nogi.svg" src="/static/img/lazy-bg.png" alt="result-gladkie-nogi">
										<noscript><img src="/static/svg/bodies-parts/result-gladkie-nogi.svg" alt="result-gladkie-nogi"></noscript>
									</div>
									<div class="pif-title">Гладкие ноги</div>
								</div>

								<div class="res-item res-item-second clearfix">
									<div class="pif-icon">
										<img class="lazy" data-src="/static/svg/bodies-parts/result-uprugie-bedra.svg" src="/static/img/lazy-bg.png" alt="result-uprugie-bedra">
										<noscript><img src="/static/svg/bodies-parts/result-uprugie-bedra.svg" alt="result-uprugie-bedra"></noscript>
									</div>
									<div class="pif-title">Упругие бедра</div>
								</div>

							</div>							

						</div>
					</div>
				</div>
				<!-- FirstRow: End -->

				<!-- SecondRow: Start -->
				<div class="slc-row slcr-center">
					<div class="row">
						<div class="col-md-4 clearfix">

							<div class="prob-gitem-wrap clearfix">

								<div class="prob-item prob-item-first clearfix">
									<div class="pif-icon">
										<img class="lazy" data-src="/static/svg/bodies-parts/troubles-obvisshaya-koja.svg" src="/static/img/lazy-bg.png" alt="troubles-obvisshaya-koja">
										<noscript><img src="/static/svg/bodies-parts/troubles-obvisshaya-koja.svg" alt="troubles-obvisshaya-koja"></noscript>
									</div>
									<div class="pif-title">Обвисшая кожа</div>
								</div>

								<div class="prob-item prob-item-second clearfix">
									<div class="pif-icon">
										<img class="lazy" data-src="/static/svg/bodies-parts/troubles-jirovie-skladki.svg" src="/static/img/lazy-bg.png" alt="troubles-jirovie-skladki">
										<noscript><img src="/static/svg/bodies-parts/troubles-jirovie-skladki.svg" alt="troubles-jirovie-skladki"></noscript>
									</div>
									<div class="pif-title">Жировые складки</div>
								</div>

							</div>

						</div>
						<div class="col-md-4 clearfix">

							<div class="slts-gitem-wrap clearfix">

								<div class="solution-item">
									<div class="slti-title">ЛИПОКОРСЕТ нити PDO</div>
									<p>Бертолетова соль различна. Уравнение стационарно распознает мембранный.</p>
								</div>

							</div>

						</div>
						<div class="col-md-4 clearfix">

							<div class="res-gitem-wrap clearfix">

								<div class="res-item res-item-first clearfix">
									<div class="pif-icon">
										<img class="lazy" data-src="/static/svg/bodies-parts/result-lifting.svg" src="/static/img/lazy-bg.png" alt="result-lifting">
										<noscript><img src="/static/svg/bodies-parts/result-lifting.svg" alt="result-lifting"></noscript>
									</div>
									<div class="pif-title">Лифтинг</div>
								</div>

								<div class="res-item res-item-second clearfix">
									<div class="pif-icon">
										<img class="lazy" data-src="/static/svg/bodies-parts/result-umenshenie-objema.svg" src="/static/img/lazy-bg.png" alt="result-umenshenie-objema">
										<noscript><img src="/static/svg/bodies-parts/result-umenshenie-objema.svg" alt="result-umenshenie-objema"></noscript>
									</div>
									<div class="pif-title">Уменьшение объёма</div>
								</div>

							</div>

						</div>
					</div>
				</div>
				<!-- SecondRow: End -->

				<!-- ThirdRow: Start -->
				<div class="slc-row">
					<div class="row">
						<div class="col-md-4 clearfix">

							<div class="prob-gitem-wrap clearfix">

								<div class="prob-item prob-item-first clearfix">
									<div class="pif-icon">
										<img class="lazy" data-src="/static/svg/bodies-parts/troubles-dryablost.svg" src="/static/img/lazy-bg.png" alt="troubles-dryablost">
										<noscript><img src="/static/svg/bodies-parts/troubles-dryablost.svg" alt="troubles-dryablost"></noscript>
									</div>
									<div class="pif-title">Дряблость</div>
								</div>

								<div class="prob-item prob-item-second clearfix">
									<div class="pif-icon">
										<img class="lazy" data-src="/static/svg/bodies-parts/troubles-cellulit.svg" src="/static/img/lazy-bg.png" alt="troubles-cellulit">
										<noscript><img src="/static/svg/bodies-parts/troubles-cellulit.svg" alt="troubles-cellulit"></noscript>
									</div>
									<div class="pif-title">Целлюлит</div>
								</div>

							</div>

						</div>
						<div class="col-md-4 clearfix">

							<div class="slts-gitem-wrap clearfix">

								<div class="solution-item top-icon">
									<div class="slti-icon">
										<svg role="img"><use xlink:href="/static/svg/svg-sprite.svg#icon-inject"></use></svg>
									</div>
									<div class="slti-title">Инъекции</div>
								</div>

							</div>

						</div>
						<div class="col-md-4 clearfix">

							<div class="res-gitem-wrap clearfix">

								<div class="res-item res-item-first clearfix">
									<div class="pif-icon">
										<img class="lazy" data-src="/static/svg/bodies-parts/result-omologenie.svg" src="/static/img/lazy-bg.png" alt="result-omologenie">
										<noscript><img src="/static/svg/bodies-parts/result-omologenie.svg" alt="result-omologenie"></noscript>
									</div>
									<div class="pif-title">Омоложение</div>
								</div>

								<div class="res-item res-item-second clearfix">
									<div class="pif-icon">
										<img class="lazy" data-src="/static/svg/bodies-parts/result-gladkaya-koja.svg" src="/static/img/lazy-bg.png" alt="result-gladkaya-koja">
										<noscript><img src="/static/svg/bodies-parts/result-gladkaya-koja.svg" alt="result-gladkaya-koja"></noscript>

									</div>
									<div class="pif-title">Гладкая кожа</div>
								</div>

							</div>

						</div>
					</div>
				</div>
				<!-- ThirdRow: End -->

			</div>

			<div class="cta-btn-wrap center">
				<a href="javascript:void(0)" class="btn btn-md btn-red">Получить бесплатную консультацию</a>
			</div>

		</div>
	</section>
	<!-- SOLUTIONS: End -->



	<!-- CORSET: Start -->
	<section class="sct sct-corset">
		<div class="container">

			<h2 class="cst-title-2">Липокорсет</h2>

			<div class="row">
				<div class="col-md-10">

					<div class="corset-descr-block">
						<div class="corset-descr-label">Что такое липокорсет?</div>
						<h3 class="crsd-title">Реальная возможность быстро вернуть былую стройность без операции</h3>
						<p class="crsd-descr">Непосредственно в зону проблемы (в жировую ткань) имплантируется корсет из нитей PDO, который сразу же на один размер утягивает подкожно-жировую клетчатку. Нити с насечками обеспечивают в зоне воздействия механический липолиз.  При ходьбе насечки немного, на доли мм смещаются в тканях, и буквально вытряхивают капли жира из клеток. Кроме того липокорсет препятствует откладыванию нового жира в этой области.</p>
						<div class="crsd-icon">
							<svg role="img"><use xlink:href="/static/svg/svg-sprite.svg#icon-threads"></use></svg>
						</div>
					</div>

				</div>
			</div>

			<div class="corset-bf-af-container">

				<div class="row">
					<div class="col-sm-5 col-md-5">
						
						<div class="crst-before-content lazy" data-src="/static/img/corset-before.jpg">
							<div class="crsth-before">До<br> имплантации</div>
						</div>

					</div>
					<div class="col-sm-2 col-md-2">

						<div class="crst-content">
							<svg role="img"><use xlink:href="/static/svg/svg-sprite.svg#icon-arrow-long-right"></use></svg>
							<svg class="icon-calendar" role="img"><use xlink:href="/static/svg/svg-sprite.svg#icon-calendar"></use></svg>
							<p>эффект<br> за 3 месяца</p>
							<svg role="img"><use xlink:href="/static/svg/svg-sprite.svg#icon-arrow-long-right"></use></svg>
						</div>

					</div>
					<div class="col-sm-5 col-md-5">

						<div class="crst-after-content lazy" data-src="/static/img/corset-after.jpg">
							<div class="crsth-after">После<br> имплантации</div>
						</div>

					</div>
				</div>



			</div>

			<div class="corset-bf-af-info">
				<div class="row">
					<div class="col-md-5">

						<div class="cors-info-item">
							<h3 class="crii-title">К Концу третьего месяца</h3>
							<div class="crii-descr">
								<ul>
									<li>— Контуры тела становятся четкими</li>
									<li>— Избыточный объём уменьшается </li>
									<li>— Локальный жировой обмен нормализуется </li>
									<li>— Новый жир не откладывается </li>
									<li>— Лифтинг данной области становится максимальным</li>
								</ul>
							</div>
							<div class="crii-icon">
								<svg role="img"><use xlink:href="/static/svg/svg-sprite.svg#icon-clock"></use></svg>
							</div>
						</div>	

					</div>

					<div class="col-md-5 col-md-push-1">

						<div class="cors-info-item">
							<h3 class="crii-title">Эффекты (длительность 2-3 года)</h3>
							<div class="crii-descr">
								<ul>
									<li>— Подтяжка фигуры с сохранением индивидуальных форм</li>
									<li>— Подчёркнутая талия</li>
									<li>— Утончение бедер, красивая форма ягодиц</li>
									<li>— Лечение целлюлита</li>
									<li>— Избавление от застоя и отеков ног </li>
								</ul>
							</div>
							<div class="crii-icon">
								<svg role="img"><use xlink:href="/static/svg/svg-sprite.svg#icon-star"></use></svg>
							</div>
							<div class="line-to-top"></div>
						</div>	

					</div>
				</div>
			</div>

		</div>
	</section>
	<!-- CORSET: End -->



	<!-- TIMEQUALITY: Start -->
	<section class="sct sct-time-quality lazy" data-src="/static/img/time-quality-bg.jpg">
		<div class="container">

			<div class="row">
				<div class="col-xs-12 col-sm-10 col-sm-push-3 col-md-8 col-md-push-5 col-lg-6 col-lg-push-6">
					
					<div class="timeq-container">
						<div class="timeq-header">
							<div class="tq-icon"><svg role="img"><use xlink:href="/static/svg/svg-sprite.svg#icon-protect"></use></svg></div>
							<p>Швейцарская технология на рынке Европы более 10 лет</p>
						</div>
						<div class="timeq-content">Мы — в числе тех,<br> кто привёз<br> эту технологию<br> в Россию<br> 6 лет назад</div>	
					</div>

				</div>
			</div>

		</div>
	</section>
	<!-- TIMEQUALITY: End -->



	<!-- ORDER: Start -->
	<section class="sct sct-order">
		<div class="container">
			<div class="row">
				<div class="col-lg-7">

					<div class="order-descr">
						<p>Получите комплексное решение проблем в области бёдер благодаря большоиу опыту в сфере Anti-aging технологий</p>
						<div class="order-icon">
							<svg role="img"><use xlink:href="/static/svg/svg-sprite.svg#icon-arrows-right"></use></svg>
						</div>
					</div>

				</div>
				<div class="col-lg-4 col-lg-push-1">

					<div class="order-form-container">
						<?php
							mod( 'catalog.action.forms', array( 'alias' => 'offer_zakaz_zvonka' ) );
						?>
					</div>

				</div>
			</div>
		</div>
	</section>
	<!-- ORDER: End -->



	<!-- CERTIFICATES: Start -->
	<section class="sct sct-certificates">
		<div class="container">
			<div class="row">
				<div class="col-md-8 col-md-push-2">
					<?php mod( 'catalog.action.offer_certs' )?>
				</div>
			</div>
		</div>
	</section>
	<!-- CERTIFICATES: End -->



	<!-- PROGRAM: Start -->
	<section class="sct sct-program">
		<div class="container">

			<div class="program-wrap">
				<div class="program-descr">
					<p><span>ЛИПОКОРСЕТ «СТРОЙНЫЕ НОГИ» —</span> анатомическая подтяжка бёдер, коленей, голеней для здоровья и красоты. Сразу в процедуре — минус 1 размер.</p>
				</div>
				<h2 class="program-header">Показания к программе «липокорсет»</h2>
				<div class="program-container">

					<div class="row">
						<div class="col-sm-4 col-md-4 col-lg-3 col-lg-push-1">

							<div class="prg-item">
								<p class="prg-descr">Дряблость и избыток кожи на внутренней поверхности бедра</p>
								<div class="prg-img lazy" data-src="/static/img/program/01.jpg"></div>
							</div>

						</div>
						<div class="col-sm-4 col-md-4 col-lg-3 col-lg-push-1">

							<div class="prg-item">
								<p class="prg-descr">Отек и жировая складка над коленом</p>
								<div class="prg-img lazy" data-src="/static/img/program/02.jpg"></div>
							</div>

						</div>
						<div class="col-sm-4 col-md-4 col-lg-3 col-lg-push-1">

							<div class="prg-item">
								<p class="prg-descr">Избыточное отложение жира по лампасной линии бедра</p>
								<div class="prg-img lazy" data-src="/static/img/program/03.jpg"></div>
							</div>

						</div>
					</div>

				</div>
			</div>

		</div>
	</section>
	<!-- PROGRAM: End -->




	<!-- SPECIALISTS: Start -->
	<section class="sct sct-specialists">
		<div class="container">

			<h2 class="cst-title-2">Наши специалисты</h2>

			<div class="specislist-slider owl-carousel">

				<div class="spec-item">
					<div class="row">
						<div class="col-md-4 col-lg-3">

							<div class="spc-img" style="background-image: url('/static/img/specialists/01.jpg')"></div>	

						</div>
						<div class="col-md-8 col-lg-9">

							<div class="spc-content">
								<div class="spcc-title">Матийцо Ольга Васильевна</div>
								<div class="spcc-prof">Врач-косметолог</div>
								<div class="spcc-text">Дамы и господа! Позвольте представить вам новую науку!<br />
В среде ученых о ней заговорили всего пять лет назад, но сегодня она завоевала страны и континенты!<br />
Имя её - РЕКОНСТРУКТИВНАЯ КОСМЕТОЛОГИЯ. Это направление - дитя многих наук. Биология и химия клетки, регенерация тканей, физиология организма, физика квантов и гравитационных полей.</div>
								<div class="spcc-callback"><a href="javascript:void(0)" class="btn btn-red btn-sm">Связаться со специалистом</a></div>

							</div>

						</div>
					</div>
				</div>


			</div>

		</div>
	</section>
	<!-- SPECIALISTS: End -->



	<!-- CONTACTS: Start -->
	<section class="sct sct-contacts">

		<div class="contacts-container">
			
			<!-- GoogleMap -->
			<div id="map"></div>

			<div class="contacts-block">

				<h2 class="cst-title-2">Контакты</h2>

				<address class="cts-address">
					<svg role="img"><use xlink:href="/static/svg/svg-sprite.svg#icon-map-marker"></use></svg>
					<p>456890, Челябинская область<br> Аргаяшский район, посёлок Увильды</p>
					<p>Корпус №2, этаж 4, кабинет 431</p>
				</address>

				<span class="cts-phone"><a href="tel:+73512251616" title="Позвонить"><svg role="img"><use xlink:href="/static/svg/svg-sprite.svg#icon-phone"></use></svg>+7 (351) 225 16 16</a></span>
				<span class="cts-email"><a href="mailto:olga.matico@mail.ru" title="Написать E-mail"><svg role="img"><use xlink:href="/static/svg/svg-sprite.svg#icon-email"></use></svg>olga.matico@mail.ru</a></span>
				
				<div class="cts-socials">
					<span>Давайте дружить:</span>
					<a class="soc-vk" href="javascript:void(0)" target="_blank"><svg role="img"><use xlink:href="/static/svg/svg-sprite.svg#icon-vk-circle"></use></svg></a>
					<a class="soc-fb" href="javascript:void(0)" target="_blank"><svg role="img"><use xlink:href="/static/svg/svg-sprite.svg#icon-fb-circle"></use></svg></a>
					<a class="soc-twitter" href="javascript:void(0)" target="_blank"><svg role="img"><use xlink:href="/static/svg/svg-sprite.svg#icon-twitter-circle"></use></svg></a>
					<a class="soc-instagram" href="javascript:void(0)" target="_blank"><svg role="img"><use xlink:href="/static/svg/svg-sprite.svg#icon-instagram-circle"></use></svg></a>
					<a class="soc-ok" href="javascript:void(0)" target="_blank"><svg role="img"><use xlink:href="/static/svg/svg-sprite.svg#icon-ok-circle"></use></svg></a>
				</div>

			</div>


		</div>

	</section>
	<!-- CONTACTS: End -->




	<!-- CONSULT: Start -->
	<section class="sct sct-consult lazy" data-src="/static/img/consult-bg.jpg">
		<div class="container">

			<div class="row">
				<div class="col-md-10 col-md-push-1">
					
					<h2 class="cst-title-2">Оставьте заявку на бесплатную консультацию врача-косметолога</h2>

				</div>
			</div>
			
			<div class="consult-form-container">
				
						<?php
							mod( 'catalog.action.forms', array( 'alias' => 'offer_konsultaciya' ) );
						?>
				
				<!--form action="/">
					<div class="row">
						<div class="col-md-4"><input type="text" placeholder="ИМЯ"></div>
						<div class="col-md-4"><input type="tel" placeholder="ТЕЛЕФОН"></div>
						<div class="col-md-4"><input type="email" placeholder="E-MAIL"></div>
						<div class="col-md-12 center">
							<label><input type="checkbox"><span class="label-text">Я даю свое согласие на обработку персональных данных и соглашаюсь с условиями и <a href="javascript:void(0)">политикой конфиденциальности</a></span></label>
							<input class="btn btn-red btn-lg" type="submit" value="Получить бесплатную консультацию">
						</div>
					</div>
				</form-->

			</div>

		</div>
	</section>
	<!-- CONSULT: End -->

<?php include "html/templates/footer_offer.tpl.php" ?>