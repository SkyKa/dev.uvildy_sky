<?php include "html/templates/header.tpl.php" ?>

<div class="breadcrumbs">
	<div class="container">
		<nav class="breadcrumbs">
			<?php mod('catalog.action.breadcrumbs') ?>
		</nav>
	</div>
</div>

<div class="container">
	<h1><?php mod('pages.show.title') ?></h1>

	<div class="row">
		<div class="col-lg-6 places">
			<div class="item">
				<h2>курорт</h2>
				<p class="address">456890, Челябинская обл., пос. Увильды.</p>
				<p class="phones">
					<?php mod( 'catalog.action.yandex_direct_phone', array( 'num' => 1, 'postfix' => ', ' ) )?>
					<?php mod( 'catalog.action.yandex_direct_phone', array( 'num' => 2, 'postfix' => ', ' ) )?>
					<?php mod( 'catalog.action.yandex_direct_phone', array( 'num' => 3, 'postfix' => ', ' ) )?>
					<?php mod( 'catalog.action.yandex_direct_phone', array( 'num' => 4 ) )?>
				</p>
				<p class="email"><a href="mailto:sales_uvildy@uvildy.ru">sales_uvildy@uvildy.ru</a></p>
			</div>
			<div class="item">
				<h2>офис в челябинске</h2>
				<p class="address">454000, г. Челябинск, ул. Сони Кривой, 28.</p>
			</div>
			<div class="basic-button big">
				<button data-toggle="modal" data-target="#callback-modal">заказать звонок</button>
			</div>
		</div>
		<div class="col-lg-6">
			<div class="way-to-get-wrap">
				<div class="head">
					<h2>Способы проезда до курорта</h2>
					<p>
						МЦМиР «Курорт Увильды» находится на северо-восточном 
						берегу озера Увильды в Челябинской 
						области, в 90 км к северо-западу от Челябинска.
					</p>
				</div>
				<div class="way-to-get">
					<h2>Как к нам приехать?</h2>
					<!-- <p class="plane" data-travel-type="plane">
						<span class="type">Самолётом</span> — в Челябинск (аэропорт Баландино — 
						<a href="">www.aeroport-74.ru</a>) или Екатеринбург (аэропорт Кольцово — 
						<a href="">www.koltsovo.ru</a>).
					</p>
					<p class="train" data-travel-type="train">
						<span class="type">Поездом</span> — до станции Челябинск-Пассажирский 
						или Ектеринбург-Пассажирский.
					</p>
					<p class="bus" data-travel-type="bus">
						<span class="type">Автобусом</span> — рейс №589 из Екатеринбурга (Южный 
						автовокзал) до Челябинска (автовокзал «Юность»), рейс №670 
						из Челябинска до пос. Увильды.
					</p> -->
					<p class="car selected" data-travel-type="car">
						<a href="" class="type">Автомобилем</a>
					</p>
					<p class="walking" data-travel-type="walking">
						<a href="" class="type">Пешком</a>
					</p>
					<!-- <p class="taxi">
						<span class="type">Заказ такси</span> — по тел.: <a href="tel:+7 (351-31) 2-34-12">+7 (351-31) 2-34-12</a>.
					</p> -->
				</div>
			</div>
		</div>
	</div>
</div>

<div id="map"></div>

<?php include "html/templates/footer.tpl.php" ?>