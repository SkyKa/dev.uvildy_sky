<?php include "html/templates/header.tpl.php" ?>

	<div class="breadcrumbs">
		<div class="container">
			<nav class="breadcrumbs">
				<?php mod('catalog.action.breadcrumbs') ?>
			</nav>
		</div>
	</div>

	<section class="landing_first_slider radon">
		<div class="container-fluid">
			<div class="row landing_slider_radon">
				<?php mod('catalog.action.radon_main_slider') ?>
			</div>
		</div>
	</section>

	<section class="landing_second_slider radon">
		<div class="container-fluid">
			<div class="row landing_second_radon">
				<div class="container">
					<h2>Научные статьи о радоне</h2>
					<div class="science_book">
						<i class="fas fa-book"></i>
					</div>
					<div class="mobile_arrows">						
						<div class="science_slider_left">
							<i class="fas fa-chevron-left"></i>
						</div>
						<div class="science_slider_right">
							<i class="fas fa-chevron-right"></i>
						</div>
					</div>
					<div class="classic_arrows">					
						<div class="science_slider_right">
							<i class="fas fa-chevron-right"></i>
						</div>
						<div class="science_slider_left">
							<i class="fas fa-chevron-left"></i>
						</div>
					</div>					
					<div class="landing_second_slider_radon">
						<?php mod('catalog.action.radon_science_articles_slider') ?>
					</div>
					<div class="col-xs-12 landing_second_under_slider">
						<?php mod('catalog.action.radon_science_articles') ?>
					</div>
					<?php 

					$table = new Table('catalog_section');

					$parent = $table -> select( 'SELECT * FROM `catalog_section` WHERE `alias`=:alias LIMIT 1', array( 'alias' => 'radonovye-vody' ) );
					$parent = end( $parent );

					$childs = $table -> select( 'SELECT * FROM `catalog_section` WHERE `parent_id`=:id ', array( 'id' => $parent[ 'id' ] ) );

					foreach ( $childs as $child ) {
						if ( $child['alias'] == "nauchnye_stati" )
						{
							$rows = $table -> select( 'SELECT * FROM `position_radon_news` WHERE `section_id`=:id AND `public`=1 ', array( 'id' => $child['id'] ) );
							if ( count($rows) > 10 ) {
								echo '<div class="col-xs-12 a_button">
									<a class="landing_second_radon_button" href="/nauchnye-stati-o-radone/">Больше научных статей</a>
								</div>';
							} else {
								echo '
									<div style="display:none;" class="col-xs-12 a_button">
										<a class="landing_second_radon_button" href="/nauchnye-stati-o-radone/">Больше научных статей</a>
									</div>
								';
							}
							
						}
					}

					?>
					
				</div>
			</div>
		</div>
	</section>

	<section class="landing_third_slider radon">
		<div class="container-fluid">
			<div class="row landing_third_radon">
				<div class="container">
					<h2>Новости о радоновом лечении</h2>
					<div class="news_world">
						<i class="fas fa-globe"></i>
					</div>
					<div class="classic_arrows">	
						<div class="news_slider_right">
							<i class="fas fa-chevron-right"></i>
						</div>
						<div class="news_slider_left">
							<i class="fas fa-chevron-left"></i>
						</div>
					</div>
					<div class="mobile_arrows">	
						<div class="news_slider_left">
							<i class="fas fa-chevron-left"></i>
						</div>
						<div class="news_slider_right">
							<i class="fas fa-chevron-right"></i>
						</div>
					</div>
					<div class="landing_third_slider_radon">
						<?php mod('catalog.action.radon_treatment_slider') ?>
					</div>
					<div class="col-xs-12 landing_third_under_slider">
						<?php mod('catalog.action.radon_treatment') ?>
					</div>
					<?php 

					$table = new Table('catalog_section');

					$parent = $table -> select( 'SELECT * FROM `catalog_section` WHERE `alias`=:alias LIMIT 1', array( 'alias' => 'radonovye-vody' ) );
					$parent = end( $parent );

					$childs = $table -> select( 'SELECT * FROM `catalog_section` WHERE `parent_id`=:id ', array( 'id' => $parent[ 'id' ] ) );

					foreach ( $childs as $child ) {
						if ( $child['alias'] == "radonovoe-lechenie" )
						{
							$rows = $table -> select( 'SELECT * FROM `position_radon_news` WHERE `section_id`=:id AND `public`=1 ', array( 'id' => $child['id'] ) );
							if ( count($rows) > 10 ) {
								echo '<div class="col-xs-12 a_button">
									<a class="landing_third_radon_button" href="/novosti-o-radonovom-lechenii/">Все новости</a>
								</div>';
							} else {
								echo '
									<div style="display:none;" class="col-xs-12 a_button">
										<a class="landing_third_radon_button" href="/novosti-o-radonovom-lechenii/">Все новости</a>
									</div>
								';
							}
							
						}
					}

					?>					
				</div>
			</div>
		</div>		
	</section>

	<section class="landing_fourth_slider radon">
		<div class="container-fluid">
			<div class="row landing_fourth_radon">
				<div class="container">
					<h2>Радон в медецине</h2>
					<div class="med_radon">
						<i class="fas fa-medkit"></i>
					</div>
					<div class="classic_arrows">
						<div class="med_slider_right">
							<i class="fas fa-chevron-right"></i>
						</div>
						<div class="med_slider_left">
							<i class="fas fa-chevron-left"></i>
						</div>
					</div>
					<div class="mobile_arrows">
						<div class="med_slider_left">
							<i class="fas fa-chevron-left"></i>
						</div>
						<div class="med_slider_right">
							<i class="fas fa-chevron-right"></i>
						</div>
					</div>
					<div class="landing_fourth_slider_radon">
						<?php mod('catalog.action.radon_med_slider') ?>
					</div>
					<div class="landing_second_under_slider">
						<?php mod('catalog.action.radon_med') ?>					
					</div>
					<?php 

					$table = new Table('catalog_section');

					$parent = $table -> select( 'SELECT * FROM `catalog_section` WHERE `alias`=:alias LIMIT 1', array( 'alias' => 'radonovye-vody' ) );
					$parent = end( $parent );

					$childs = $table -> select( 'SELECT * FROM `catalog_section` WHERE `parent_id`=:id ', array( 'id' => $parent[ 'id' ] ) );

					foreach ( $childs as $child ) {
						if ( $child['alias'] == "radon-v-medecine" )
						{
							$rows = $table -> select( 'SELECT * FROM `position_radon_news` WHERE `section_id`=:id AND `public`=1 ', array( 'id' => $child['id'] ) );
							if ( count($rows) > 10 ) {
								echo '<div class="col-xs-12 a_button">
									<a class="landing_second_radon_button" href="/radon-v-medecine/">Все статьи</a>
								</div>';
							} else {
								echo '
									<div style="display:none;" class="col-xs-12 a_button">
										<a class="landing_second_radon_button" href="/radon-v-medecine/">Все статьи</a>
									</div>
								';
							}
							
						}
					}

					?>					
				</div>
			</div>
		</div>
	</section>

	<section class="landing_fifth_slider radon">
		<div class="container-fluid">
			<div class="row landing_fifth_radon">
				<div class="container">
					<h2>Методика лечения радоном</h2>
					<div class="science_book">
						<i class="fab fa-youtube"></i>
					</div>
					<div class="classic_arrows">
						<div class="metod_slider_right">
							<i class="fas fa-chevron-right"></i>
						</div>
						<div class="metod_slider_left">
							<i class="fas fa-chevron-left"></i>
						</div>
					</div>
					<div class="mobile_arrows">
						<div class="metod_slider_left">
							<i class="fas fa-chevron-left"></i>
						</div>
						<div class="metod_slider_right">
							<i class="fas fa-chevron-right"></i>
						</div>
					</div>
					<div class="landing_fifth_slider_radon">
						<?php mod('catalog.action.radon_videos') ?>
					</div>
					<?php 

					$table = new Table('catalog_section');

					$parent = $table -> select( 'SELECT * FROM `catalog_section` WHERE `alias`=:alias LIMIT 1', array( 'alias' => 'radonovye-vody' ) );
					$parent = end( $parent );

					$childs = $table -> select( 'SELECT * FROM `catalog_section` WHERE `parent_id`=:id ', array( 'id' => $parent[ 'id' ] ) );

					foreach ( $childs as $child ) {
						if ( $child['alias'] == "metodika-lecheniya-radonom" )
						{
							$rows = $table -> select( 'SELECT * FROM `position_radon_news` WHERE `section_id`=:id AND `public`=1 ', array( 'id' => $child['id'] ) );
							if ( count($rows) > 10 ) {
								echo '<div class="col-xs-12 a_button">
									<a class="landing_second_radon_button" href="/metodika-lecheniya-radonom/">Все видеообзоры</a>
								</div>';
							} else {
								echo '
									<div style="display:none;" class="col-xs-12 a_button">
										<a class="landing_second_radon_button" href="/metodika-lecheniya-radonom/">Все видеообзоры</a>
									</div>
								';
							}
							
						}
					}

					?>					
				</div>
			</div>
		</div>
	</section>

	<section class="landing_sixth_slider radon">
		<div class="container-fluid">
			<div class="row landing_sixth_radon">
				<div class="container">
					<h2>Какие болезни лечат радоновые ванны</h2>
					<div class="med_radon">
						<i class="far fa-life-ring"></i>
					</div>
					<div class="classic_arrows">
						<div class="ill_slider_right">
							<i class="fas fa-chevron-right"></i>
						</div>
						<div class="ill_slider_left">
							<i class="fas fa-chevron-left"></i>
						</div>
					</div>
					<div class="mobile_arrows">
						<div class="ill_slider_left">
							<i class="fas fa-chevron-left"></i>
						</div>
						<div class="ill_slider_right">
							<i class="fas fa-chevron-right"></i>
						</div>
					</div>
					<div class="landing_sixth_slider_radon">
						<?php mod('catalog.action.radon_disease') ?>
					</div>
					<?php mod('catalog.action.radon_disease_articles') ?>
					<?php 

					$table = new Table('catalog_section');

					$parent = $table -> select( 'SELECT * FROM `catalog_section` WHERE `alias`=:alias LIMIT 1', array( 'alias' => 'radonovye-vody' ) );
					$parent = end( $parent );

					$childs = $table -> select( 'SELECT * FROM `catalog_section` WHERE `parent_id`=:id ', array( 'id' => $parent[ 'id' ] ) );

					foreach ( $childs as $child ) {
						if ( $child['alias'] == "kakie-bolezni-lechat-radonovye-vanny" )
						{
							$rows = $table -> select( 'SELECT * FROM `position_radon_news` WHERE `section_id`=:id AND `public`=1 ', array( 'id' => $child['id'] ) );
							if ( count($rows) > 10 ) {
								echo '<div class="col-xs-12 a_button">
									<a class="landing_second_radon_button" href="/kakie-bolezni-lechat-radonovye-vanny/">Все статьи</a>
								</div>';
							} else {
								echo '
									<div style="display:none;" class="col-xs-12 a_button">
										<a class="landing_second_radon_button" href="/kakie-bolezni-lechat-radonovye-vanny/">Все статьи</a>
									</div>
								';
							}
							
						}
					}

					?>
				</div>
			</div>
		</div>
	</section>

	<section class="landing_seventh_slider radon">
		<div class="container-fluid">
			<div class="row landing_seventh_radon">
				<div class="container">
					<h2>Высокоактивные мировые источники радоновой воды</h2>
					<div class="news_world">
						<i class="fas fa-tint"></i>
					</div>
					<div class="classic_arrows">
						<div class="worlds_slider_right">
							<i class="fas fa-chevron-right"></i>
						</div>
						<div class="worlds_slider_left">
							<i class="fas fa-chevron-left"></i>
						</div>
					</div>
					<div class="mobile_arrows">
						<div class="worlds_slider_left">
							<i class="fas fa-chevron-left"></i>
						</div>
						<div class="worlds_slider_right">
							<i class="fas fa-chevron-right"></i>
						</div>
					</div>
					<div class="landing_seventh_slider_radon">
						<?php mod('catalog.action.radon_wellspring_slider') ?>
					</div>
					<?php 

					$table = new Table('catalog_section');

					$parent = $table -> select( 'SELECT * FROM `catalog_section` WHERE `alias`=:alias LIMIT 1', array( 'alias' => 'radonovye-vody' ) );
					$parent = end( $parent );

					$childs = $table -> select( 'SELECT * FROM `catalog_section` WHERE `parent_id`=:id ', array( 'id' => $parent[ 'id' ] ) );

					foreach ( $childs as $child ) {
						if ( $child['alias'] == "mirovye-istochniki" )
						{
							$rows = $table -> select( 'SELECT * FROM `position_radon_news` WHERE `section_id`=:id AND `public`=1 ', array( 'id' => $child['id'] ) );
							if ( count($rows) > 10 ) {
								echo '<div class="col-xs-12 a_button">
									<a class="landing_third_radon_button" href="/vysokoaktivnye-mirovye-istochniki-radonovoj-vody/">Все источники</a>
								</div>';
							} else {
								echo '
									<div style="display:none;" class="col-xs-12 a_button">
										<a class="landing_third_radon_button" href="/vysokoaktivnye-mirovye-istochniki-radonovoj-vody/">Все источники</a>
									</div>
								';
							}
							
						}
					}

					?>
				</div>
			</div>
		</div>
	</section>

	<section class="landing_last_slider radon">
		<div class="container-fluid">
			<div class="row landing_last_radon">
				<div class="container">
					<div class="full_width_boy">
						<h2>Вопрос - ответ</h2>
						<div class="med_radon">
							<i class="far fa-question-circle"></i>
						</div>
					</div>
					<div class="question_items_radon">
						<?php mod('catalog.action.radon_questions') ?>	
					</div>
					<?php 

					$table = new Table('catalog_section');

					$rows = $table -> select('SELECT * FROM `position_radon_questions`');
					if ( count($rows) > 10 ) {
						echo '<div class="col-xs-12 a_button">
							<a class="landing_second_radon_button" href="/vopros-otvet/">Все вопросы</a>
						</div>';
					} else {
						echo '
							<div style="display:none;" class="col-xs-12 a_button">
								<a class="landing_second_radon_button" href="/vopros-otvet/">Все вопросы</a>
							</div>
						';
					}
					?>
				</div>
			</div>
		</div>
	</section>



<?php include "html/templates/footer.tpl.php" ?>