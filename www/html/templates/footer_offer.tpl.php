
	<!-- FOOTER: Start -->
	<footer class="sct sct-footer">
		<div class="container">
			
			<div class="footer-container clearfix">
				
				<div class="ftr-item ftr-logo">
					<a href=""><img src="/static/svg/logo.svg" alt="logo"></a>
				</div>
				<div class="ftr-item ftr-phone">
					<a href="tel:+73512251616" title="Позвонить">+7 (351) 225 16 16</a>
				</div>
				<div class="ftr-item ftr-consult">
					<a href="javascript:void(0)" class="btn btn-red btn-sm">Консультация бесплатно</a>
				</div>
				<div class="ftr-item ftr-copyright">
					<p>© Многопрофильный центр медицины и реабилитации «Курорт Увильды», 2017 г.</p>
				</div>

			</div>

		</div>
	</footer>
	<!-- FOOTER: End -->


	<!-- Back to top button -->
	<div id="top-button"></div>

	<!-- build:js -->
	<script src="<?php Utils :: isChange( '/static/js/libs.js' )?>"></script>
	<script src="<?php Utils :: isChange( '/static/js/common.js' )?>"></script>
	<script src="/ajax/?mod=catalog.action.forms_js"></script>
	<!-- endbuild -->
	<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyARHTn4F3lCr0RZE7pAgjyeu3SIJqDdygU&language=ru&callback=initMap" async defer></script>

</body>
</html>