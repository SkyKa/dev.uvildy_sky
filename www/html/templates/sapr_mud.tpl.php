<?php include "html/templates/header.tpl.php" ?>

	<div class="breadcrumbs">
		<div class="container">
			<nav class="breadcrumbs">
				<?php mod('catalog.action.breadcrumbs') ?>
			</nav>
		</div>
	</div>

	<section class="landing_first_slider radon">
		<div class="container-fluid">
			<div class="row landing_slider_radon mud">
				<?php mod('catalog.action.mud_main_slider') ?>
			</div>
		</div>
	</section>

	<section class="landing_second_slider radon">
		<div class="container-fluid">
			<div class="row landing_second_radon mud">
				<div class="container">
					<h2>Научные статьи о применении сапропелевой грязи</h2>
					<div class="science_book">
						<i class="fas fa-book"></i>
					</div>
					<div class="mobile_arrows">						
						<div class="science_slider_left mud">
							<i class="fas fa-chevron-left"></i>
						</div>
						<div class="science_slider_right mud">
							<i class="fas fa-chevron-right"></i>
						</div>
					</div>
					<div class="classic_arrows">					
						<div class="science_slider_right mud">
							<i class="fas fa-chevron-right"></i>
						</div>
						<div class="science_slider_left mud">
							<i class="fas fa-chevron-left"></i>
						</div>
					</div>					
					<div class="landing_second_slider_radon">
						<?php mod('catalog.action.mud_science_articles_slider') ?>
					</div>
					<div class="col-xs-12 landing_second_under_slider">
						<?php mod('catalog.action.mud_science_articles') ?>
					</div>
					<div class="col-xs-12 a_button">
						<a class="landing_second_radon_button mud white" href="/nauchnye-stati-o-gryazi/">Больше научных статей</a>
					</div>
				</div>
			</div>
		</div>
	</section>

	<section class="landing_third_slider radon mud">
		<div class="container-fluid">
			<div class="row landing_third_radon mud">
				<div class="container">
					<h2>Новости о лечении грязью</h2>
					<div class="news_world">
						<i class="fas fa-globe"></i>
					</div>
					<div class="classic_arrows">	
						<div class="news_slider_right mud">
							<i class="fas fa-chevron-right"></i>
						</div>
						<div class="news_slider_left mud">
							<i class="fas fa-chevron-left"></i>
						</div>
					</div>
					<div class="mobile_arrows">	
						<div class="news_slider_left mud">
							<i class="fas fa-chevron-left"></i>
						</div>
						<div class="news_slider_right mud">
							<i class="fas fa-chevron-right"></i>
						</div>
					</div>
					<div class="landing_third_slider_radon">
						<?php mod('catalog.action.mud_treatment_slider') ?>
					</div>
					<div class="col-xs-12 a_button">
						<a class="landing_third_radon_button mud" href="/novosti-o-lechenii-gryazyu/">Все новости</a>
					</div>
				</div>
			</div>
		</div>		
	</section>

	<section class="landing_mud_contraindications">
		<div class="container-fluid">
				<div class="container">
					<div class="col-xs-12 col-sm-6 mud_contraindications_left">
						<h2>Грязелечение: показания</h2>
						<div class="news_world">
							<i class="fas fa-plus-circle"></i>
						</div>
						<?php mod('catalog.action.mud_contraindications') ?>
						<div class="col-xs-12 a_button left">
							<a class="landing_second_radon_button mud flow" href="/pokazaniya/">Все показания</a>
						</div>
					</div>
					<div class="col-xs-12 col-sm-6 mud_contraindications_left">
						<h2>Грязелечение: противопоказания</h2>
						<div class="news_world">
							<i class="fas fa-minus-circle"></i>
						</div>
						<?php mod('catalog.action.mud_indications') ?>
						<div class="col-xs-12 a_button left">
							<a class="landing_second_radon_button mud flow" href="/protivopokazaniya/">Все противопоказания</a>
						</div>
					</div>
				</div>
		</div>
	</section>

	<section class="landing_fourth_slider radon mud">
		<div class="container-fluid">
			<div class="row landing_fourth_radon mud">
				<div class="container">
					<h2>Грязелечение в медецине</h2>
					<div class="med_radon">
						<i class="fas fa-medkit"></i>
					</div>
					<div class="classic_arrows">
						<div class="med_slider_right mud">
							<i class="fas fa-chevron-right"></i>
						</div>
						<div class="med_slider_left mud">
							<i class="fas fa-chevron-left"></i>
						</div>
					</div>
					<div class="mobile_arrows">
						<div class="med_slider_left mud">
							<i class="fas fa-chevron-left"></i>
						</div>
						<div class="med_slider_right mud">
							<i class="fas fa-chevron-right"></i>
						</div>
					</div>
					<div class="landing_fourth_slider_radon">
						<?php mod('catalog.action.mud_med_slider') ?>
					</div>
					<div class="landing_second_under_slider">
						<?php mod('catalog.action.mud_med') ?>					
					</div>
					<div class="col-xs-12 a_button">
						<a class="landing_second_radon_button mud" href="/gryazelechenie-v-medecine/">Все статьи</a>
					</div>
				</div>
			</div>
		</div>
	</section>

	<section class="landing_fifth_slider radon">
		<div class="container-fluid">
			<div class="row landing_fifth_radon mud">
				<div class="container">
					<h2>Методика грязелечения</h2>
					<div class="science_book">
						<i class="fab fa-youtube"></i>
					</div>
					<div class="classic_arrows">
						<div class="metod_slider_right mud">
							<i class="fas fa-chevron-right"></i>
						</div>
						<div class="metod_slider_left mud">
							<i class="fas fa-chevron-left"></i>
						</div>
					</div>
					<div class="mobile_arrows">
						<div class="metod_slider_left mud">
							<i class="fas fa-chevron-left"></i>
						</div>
						<div class="metod_slider_right mud">
							<i class="fas fa-chevron-right"></i>
						</div>
					</div>
					<div class="landing_fifth_slider_radon">
						<?php mod('catalog.action.mud_videos') ?>
					</div>
					<div class="col-xs-12 a_button">
						<a class="landing_second_radon_button mud" href="/metodika-gryazelecheniya/">Все видео</a>
					</div>
				</div>
			</div>
		</div>
	</section>

	<section class="landing_sixth_slider radon mud">
		<div class="container-fluid">
			<div class="row landing_sixth_radon mud">
				<div class="container">
					<h2>Какие болезни лечит сапропелевая грязь</h2>
					<div class="med_radon">
						<i class="far fa-life-ring"></i>
					</div>
					<div class="classic_arrows">
						<div class="ill_slider_right mud">
							<i class="fas fa-chevron-right"></i>
						</div>
						<div class="ill_slider_left mud">
							<i class="fas fa-chevron-left"></i>
						</div>
					</div>
					<div class="mobile_arrows">
						<div class="ill_slider_left mud">
							<i class="fas fa-chevron-left"></i>
						</div>
						<div class="ill_slider_right mud">
							<i class="fas fa-chevron-right"></i>
						</div>
					</div>
					<div class="landing_sixth_slider_radon">
						<?php mod('catalog.action.mud_disease') ?>
					</div>
					<div class="col-xs-12 col-sm-3">
						<div class="sixth_slider_under_item mud">
							<p><a href="#">Лечение панкриотита</a></p>
							<p><a href="#">Лечение гастроантерита</a></p>
							<p><a href="#">Лечение саркомы</a></p>
							<p><a href="#">Лечение аллергии</a></p>
						</div>
					</div>
					<div class="col-xs-12 col-sm-3">
						<div class="sixth_slider_under_item mud">
							<p><a href="#">Лечение астмы</a></p>
							<p><a href="#">Лечение синусита</a></p>
							<p><a href="#">Лечение саркомы</a></p>
							<p><a href="#">Лечение бесплодия</a></p>
						</div>
					</div>
					<div class="col-xs-12 col-sm-3">
						<div class="sixth_slider_under_item mud">
							<p><a href="#">Лечение эндометрита</a></p>
							<p><a href="#">Лечение эпилепсии</a></p>
							<p><a href="#">Лечение слабости</a></p>
							<p><a href="#">Лечение печени</a></p>
						</div>
					</div>
					<div class="col-xs-12 col-sm-3">
						<div class="sixth_slider_under_item mud">
							<p><a href="#">Лечение менингита</a></p>
							<p><a href="#">Лечение инфекции</a></p>
							<p><a href="#">Лечение потливости</a></p>
							<p><a href="#">Лечение дерматита</a></p>
						</div>
					</div>
					<div class="col-xs-12 a_button">
						<a class="landing_second_radon_button mud" href="/kakie-bolezni-lechit-gryaz/">Все статьи</a>
					</div>
				</div>
			</div>
		</div>
	</section>

	<section class="landing_seventh_slider radon mud">
		<div class="container-fluid">
			<div class="row landing_seventh_radon mud">
				<div class="container">
					<h2>Бальнеологические курорты мира</h2>
					<div class="news_world">
						<i class="fas fa-tint"></i>
					</div>
					<div class="classic_arrows">
						<div class="worlds_slider_right mud">
							<i class="fas fa-chevron-right"></i>
						</div>
						<div class="worlds_slider_left  mud">
							<i class="fas fa-chevron-left"></i>
						</div>
					</div>
					<div class="mobile_arrows">
						<div class="worlds_slider_left  mud">
							<i class="fas fa-chevron-left"></i>
						</div>
						<div class="worlds_slider_right  mud">
							<i class="fas fa-chevron-right"></i>
						</div>
					</div>
					<div class="landing_seventh_slider_radon">
						<?php mod('catalog.action.mud_wellspring_slider') ?>
					</div>
					<div class="col-xs-12 a_button">
						<a class="landing_third_radon_button mud" href="/balneologicheskie-kurorty-mira/">Все источники</a>
					</div>
				</div>
			</div>
		</div>
	</section>

	<section class="landing_last_slider radon mud">
		<div class="container-fluid">
			<div class="row landing_last_radon mud">
				<div class="container">
					<div class="full_width_boy">
						<h2>Вопрос - ответ</h2>
						<div class="med_radon">
							<i class="far fa-question-circle"></i>
						</div>
					</div>
					<div class="question_items_radon">
						<?php mod('catalog.action.mud_questions') ?>	
					</div>										
					<div class="col-xs-12 a_button">
						<a class="landing_second_radon_button mud" href="/vopros-otvet2/">Все вопросы</a>
					</div>
				</div>
			</div>
		</div>
	</section>



<?php include "html/templates/footer.tpl.php" ?>