<?php include "html/templates/header.tpl.php" ?>

<div class="breadcrumbs">
	<div class="container">
		<nav class="breadcrumbs">
			<?php mod('catalog.action.breadcrumbs') ?>
		</nav>
	</div>
</div>

<div class="container">
	<h1><?php mod('pages.show.title') ?></h1>
	<div class="content">
		<?php mod('pages.show.content') ?>
	</div>
</div>

<?php include "html/templates/footer.tpl.php" ?>