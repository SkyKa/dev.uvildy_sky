<?php include "html/templates/header_main.tpl.php" ?>

<div class="main-page">

		<section>
		<div class="container-fluid">
			<div class="row">
			<div class="container">
				<div class="form_2_button">
<!-- 				<button type="button" class="btn btn-primary" data-toggle="modal" data-target=".bs-example-modal-lg">ZAPUSAY</button> -->
				<div class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel">
		  <div class="modal-dialog modal-lg" role="document">
		    <div class="modal-content">
		      <div class="modal-header">
		        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true" class="form_2_button_span">X</span></button>
		        <h4 class="modal-title" id="gridSystemModalLabel">Осенняя скидка на люксы - 20% всем гостям</h4>
		      </div>
		      <div class="modal-body">
		      	<div class="row row_glow">
		      		<div class="col-xs-12 col-md-9 left_side">
		      			<div class="col-xs-12 col-md-6 nopadd form_2_img">
		      				<img src="/static/img/foto_back.jpg">
		      			</div>
		      			<div class="col-xs-12 col-md-6 nopadd form_2_prices">
		      				<span class="form_2_oldrice">15 600 руб. / сутки</span>
		      				<span class="form_2_newrice">14 600 руб. / сутки</span>
		      				<button>Подробнее</button>
		      			</div>
		      			<div class="col-xs-12 nopadd">
		      				<div class="col-md-6 col-xs-12 nopadd form_2_h2">
		      				<h2>Акция «-20% на номера люкс» действует с 27 августа по 30 сентября 2017 г.</h2>
		      			</div>
		      			</div>
		      			<div class="col-xs-12 nopadd form_2_p">
		      				<p>Выезд на путевки с проживанием в номерах «люкс», 3-х разовое питание 
по системе «шведский стол» и лечение по программе «Общее оздоровление».  
Скидка 20% на проживание, питание и лечение в номерах данной категории.
Фотографии номерного фонда «люкс». </p>
		      			</div>
		      		</div>
		      		<div class="col-xs-12 col-md-3 right_side nopadd">
		      			<div class="col-xs-12 item-white">
		      				<a role="button" data-toggle="collapse" href="#collapseExample1" aria-expanded="false" aria-controls="collapseExample1">
		      				<img src="/static/img/clossed_message.png">
		      				<p class="form_2_date">17 / 10 / 2017</p>
		      				<p class="form_2_message">Акция «-20% на номера люкс...</p>
		      				</a>		      				
		      				<div class="collapse" id="collapseExample1">
							  <div class="collapse_mobile">
							    <div class="col-xs-12 col-md-9 collapse_mobile_div">
		      			<div class="col-xs-12 col-md-6 nopadd form_2_img">
		      				<img src="/static/img/foto_back.jpg">
		      			</div>
		      			<div class="col-xs-12 col-md-6 nopadd form_2_prices">
		      				<span class="form_2_oldrice">15 600 руб. / сутки</span>
		      				<span class="form_2_newrice">14 600 руб. / сутки</span>
		      				<button>Подробнее</button>
		      			</div>
		      			<div class="col-xs-12 nopadd">
		      				<div class="col-md-6 col-xs-12 nopadd form_2_h2">
		      				<h2>Акция «-20% на номера люкс» действует с 27 августа по 30 сентября 2017 г.</h2>
		      			</div>
		      			</div>
		      			<div class="col-xs-12 nopadd form_2_p">
		      				<p>Выезд на путевки с проживанием в номерах «люкс», 3-х разовое питание 
по системе «шведский стол» и лечение по программе «Общее оздоровление».  
Скидка 20% на проживание, питание и лечение в номерах данной категории.
Фотографии номерного фонда «люкс». </p>
		      			</div>
		      		</div>
							  </div>
							</div>
		      			</div>
		      			<div class="col-xs-12 item-gray">
		      				<a role="button" data-toggle="collapse" href="#collapseExample2" aria-expanded="false" aria-controls="collapseExample2">
		      				<img src="/static/img/opened_message.png">
		      				<p class="form_2_date">17 / 10 / 2017</p>
		      				<p class="form_2_message">Акция «-20% на номера люкс...</p>
		      				</a>
		      				<div class="collapse" id="collapseExample2">
							  <div class="collapse_mobile">
							    <div class="col-xs-12 col-md-9 collapse_mobile_div">
		      			<div class="col-xs-12 col-md-6 nopadd form_2_img">
		      				<img src="/static/img/foto_back.jpg">
		      			</div>
		      			<div class="col-xs-12 col-md-6 nopadd form_2_prices">
		      				<span class="form_2_oldrice">15 600 руб. / сутки</span>
		      				<span class="form_2_newrice">14 600 руб. / сутки</span>
		      				<button>Подробнее</button>
		      			</div>
		      			<div class="col-xs-12 nopadd">
		      				<div class="col-md-6 col-xs-12 nopadd form_2_h2">
		      				<h2>Акция «-20% на номера люкс» действует с 27 августа по 30 сентября 2017 г.</h2>
		      			</div>
		      			</div>
		      			<div class="col-xs-12 nopadd form_2_p">
		      				<p>Выезд на путевки с проживанием в номерах «люкс», 3-х разовое питание 
по системе «шведский стол» и лечение по программе «Общее оздоровление».  
Скидка 20% на проживание, питание и лечение в номерах данной категории.
Фотографии номерного фонда «люкс». </p>
		      			</div>
		      		</div>
							  </div>
							</div>
		      			</div>
		      			<div class="col-xs-12 item-white">
		      				<a role="button" data-toggle="collapse" href="#collapseExample3" aria-expanded="false" aria-controls="collapseExample3">
		      				<img src="/static/img/opened_message.png">
		      				<p class="form_2_date">17 / 10 / 2017</p>
		      				<p class="form_2_message">Акция «-20% на номера люкс...</p>
		      				</a>
		      				<div class="collapse" id="collapseExample3">
							  <div class="collapse_mobile">
							    <div class="col-xs-12 col-md-9 collapse_mobile_div">
		      			<div class="col-xs-12 col-md-6 nopadd form_2_img">
		      				<img src="/static/img/foto_back.jpg">
		      			</div>
		      			<div class="col-xs-12 col-md-6 nopadd form_2_prices">
		      				<span class="form_2_oldrice">15 600 руб. / сутки</span>
		      				<span class="form_2_newrice">14 600 руб. / сутки</span>
		      				<button>Подробнее</button>
		      			</div>
		      			<div class="col-xs-12 nopadd">
		      				<div class="col-md-6 col-xs-12 nopadd form_2_h2">
		      				<h2>Акция «-20% на номера люкс» действует с 27 августа по 30 сентября 2017 г.</h2>
		      			</div>
		      			</div>
		      			<div class="col-xs-12 nopadd form_2_p">
		      				<p>Выезд на путевки с проживанием в номерах «люкс», 3-х разовое питание 
по системе «шведский стол» и лечение по программе «Общее оздоровление».  
Скидка 20% на проживание, питание и лечение в номерах данной категории.
Фотографии номерного фонда «люкс». </p>
		      			</div>
		      		</div>
							  </div>
							</div>
		      			</div>
		      			<div class="col-xs-12 item-gray">
		      				<a role="button" data-toggle="collapse" href="#collapseExample4" aria-expanded="false" aria-controls="collapseExample4">
		      				<img src="/static/img/clossed_message.png">
		      				<p class="form_2_date">17 / 10 / 2017</p>
		      				<p class="form_2_message">Акция «-20% на номера люкс...</p>
		      				</a>
		      				<div class="collapse" id="collapseExample4">
							  <div class="collapse_mobile">
							    <div class="col-xs-12 col-md-9 collapse_mobile_div">
		      			<div class="col-xs-12 col-md-6 nopadd form_2_img">
		      				<img src="/static/img/foto_back.jpg">
		      			</div>
		      			<div class="col-xs-12 col-md-6 nopadd form_2_prices">
		      				<span class="form_2_oldrice">15 600 руб. / сутки</span>
		      				<span class="form_2_newrice">14 600 руб. / сутки</span>
		      				<button>Подробнее</button>
		      			</div>
		      			<div class="col-xs-12 nopadd">
		      				<div class="col-md-6 col-xs-12 nopadd form_2_h2">
		      				<h2>Акция «-20% на номера люкс» действует с 27 августа по 30 сентября 2017 г.</h2>
		      			</div>
		      			</div>
		      			<div class="col-xs-12 nopadd form_2_p">
		      				<p>Выезд на путевки с проживанием в номерах «люкс», 3-х разовое питание 
по системе «шведский стол» и лечение по программе «Общее оздоровление».  
Скидка 20% на проживание, питание и лечение в номерах данной категории.
Фотографии номерного фонда «люкс». </p>
		      			</div>
		      		</div>
							  </div>
							</div>
		      			</div>
		      			<div class="col-xs-12 item-white">
		      				<a role="button" data-toggle="collapse" href="#collapseExample5" aria-expanded="false" aria-controls="collapseExample5">
		      				<img src="/static/img/opened_message.png">
		      				<p class="form_2_date">17 / 10 / 2017</p>
		      				<p class="form_2_message">Акция «-20% на номера люкс...</p>
		      				</a>
		      				<div class="collapse" id="collapseExample5">
							  <div class="collapse_mobile">
							    <div class="col-xs-12 col-md-9 collapse_mobile_div">
		      			<div class="col-xs-12 col-md-6 nopadd form_2_img">
		      				<img src="/static/img/foto_back.jpg">
		      			</div>
		      			<div class="col-xs-12 col-md-6 nopadd form_2_prices">
		      				<span class="form_2_oldrice">15 600 руб. / сутки</span>
		      				<span class="form_2_newrice">14 600 руб. / сутки</span>
		      				<button>Подробнее</button>
		      			</div>
		      			<div class="col-xs-12 nopadd">
		      				<div class="col-md-6 col-xs-12 nopadd form_2_h2">
		      				<h2>Акция «-20% на номера люкс» действует с 27 августа по 30 сентября 2017 г.</h2>
		      			</div>
		      			</div>
		      			<div class="col-xs-12 nopadd form_2_p">
		      				<p>Выезд на путевки с проживанием в номерах «люкс», 3-х разовое питание 
по системе «шведский стол» и лечение по программе «Общее оздоровление».  
Скидка 20% на проживание, питание и лечение в номерах данной категории.
Фотографии номерного фонда «люкс». </p>
		      			</div>
		      		</div>
							  </div>
							</div>
		      			</div>
		      			<div class="col-xs-12 item-gray">
		      				<a role="button" data-toggle="collapse" href="#collapseExample6" aria-expanded="false" aria-controls="collapseExample6">
		      				<img src="/static/img/clossed_message.png">
		      				<p class="form_2_date">17 / 10 / 2017</p>
		      				<p class="form_2_message">Акция «-20% на номера люкс...</p>
		      				</a>
		      				<div class="collapse" id="collapseExample6">
							  <div class="collapse_mobile">
							    <div class="col-xs-12 col-md-9 collapse_mobile_div">
		      			<div class="col-xs-12 col-md-6 nopadd form_2_img">
		      				<img src="/static/img/foto_back.jpg">
		      			</div>
		      			<div class="col-xs-12 col-md-6 nopadd form_2_prices">
		      				<span class="form_2_oldrice">15 600 руб. / сутки</span>
		      				<span class="form_2_newrice">14 600 руб. / сутки</span>
		      				<button>Подробнее</button>
		      			</div>
		      			<div class="col-xs-12 nopadd">
		      				<div class="col-md-6 col-xs-12 nopadd form_2_h2">
		      				<h2>Акция «-20% на номера люкс» действует с 27 августа по 30 сентября 2017 г.</h2>
		      			</div>
		      			</div>
		      			<div class="col-xs-12 nopadd form_2_p">
		      				<p>Выезд на путевки с проживанием в номерах «люкс», 3-х разовое питание 
по системе «шведский стол» и лечение по программе «Общее оздоровление».  
Скидка 20% на проживание, питание и лечение в номерах данной категории.
Фотографии номерного фонда «люкс». </p>
		      			</div>
		      		</div>
							  </div>
							</div>
		      			</div>
		      			<div class="col-xs-12 item-white">
		      				<a role="button" data-toggle="collapse" href="#collapseExample7" aria-expanded="false" aria-controls="collapseExample7">
		      				<img src="/static/img/clossed_message.png">
		      				<p class="form_2_date">17 / 10 / 2017</p>
		      				<p class="form_2_message">Акция «-20% на номера люкс...</p>
		      				</a>
		      				<div class="collapse" id="collapseExample7">
							  <div class="collapse_mobile">
							    <div class="col-xs-12 col-md-9 collapse_mobile_div">
		      			<div class="col-xs-12 col-md-6 nopadd form_2_img">
		      				<img src="/static/img/foto_back.jpg">
		      			</div>
		      			<div class="col-xs-12 col-md-6 nopadd form_2_prices">
		      				<span class="form_2_oldrice">15 600 руб. / сутки</span>
		      				<span class="form_2_newrice">14 600 руб. / сутки</span>
		      				<button>Подробнее</button>
		      			</div>
		      			<div class="col-xs-12 nopadd">
		      				<div class="col-md-6 col-xs-12 nopadd form_2_h2">
		      				<h2>Акция «-20% на номера люкс» действует с 27 августа по 30 сентября 2017 г.</h2>
		      			</div>
		      			</div>
		      			<div class="col-xs-12 nopadd form_2_p">
		      				<p>Выезд на путевки с проживанием в номерах «люкс», 3-х разовое питание 
по системе «шведский стол» и лечение по программе «Общее оздоровление».  
Скидка 20% на проживание, питание и лечение в номерах данной категории.
Фотографии номерного фонда «люкс». </p>
		      			</div>
		      		</div>
							  </div>
							</div>
		      			</div>
		      			<div class="col-xs-12 item-gray">
		      				<a href="">
		      				<img src="/static/img/opened_message.png">
		      				<p class="form_2_date">17 / 10 / 2017</p>
		      				<p class="form_2_message">Акция «-20% на номера люкс...</p>
		      				</a>
		      			</div>
		      			<div class="col-xs-12 item-white">
		      				<a href="">
		      				<img src="/static/img/clossed_message.png">
		      				<p class="form_2_date">17 / 10 / 2017</p>
		      				<p class="form_2_message">Акция «-20% на номера люкс...</p>
		      				</a>
		      			</div>
		      			<div class="col-xs-12 item-gray">
		      				<a href="">
		      				<img src="/static/img/opened_message.png">
		      				<p class="form_2_date">17 / 10 / 2017</p>
		      				<p class="form_2_message">Акция «-20% на номера люкс...</p>
		      				</a>
		      			</div>
		      			<div class="col-xs-12 item-white">
		      				<a href="">
		      				<img src="/static/img/clossed_message.png">
		      				<p class="form_2_date">17 / 10 / 2017</p>
		      				<p class="form_2_message">Акция «-20% на номера люкс...</p>
		      				</a>
		      			</div>
		      			<div class="col-xs-12 item-gray">
		      				<a href="">
		      				<img src="/static/img/clossed_message.png">
		      				<p class="form_2_date">17 / 10 / 2017</p>
		      				<p class="form_2_message">Акция «-20% на номера люкс...</p>
		      				</a>
		      			</div>
		      			<div class="col-xs-12 item-white">
		      				<a href="">
		      				<img src="/static/img/clossed_message.png">
		      				<p class="form_2_date">17 / 10 / 2017</p>
		      				<p class="form_2_message">Акция «-20% на номера люкс...</p>
		      				</a>
		      			</div>
		      			<div class="col-xs-12 item-gray">
		      				<a href="">
		      				<img src="/static/img/clossed_message.png">
		      				<p class="form_2_date">17 / 10 / 2017</p>
		      				<p class="form_2_message">Акция «-20% на номера люкс...</p>
		      				</a>
		      			</div>
		      			<div class="col-xs-12 item-white">
		      				<a href="">
		      				<img src="/static/img/clossed_message.png">
		      				<p class="form_2_date">17 / 10 / 2017</p>
		      				<p class="form_2_message">Акция «-20% на номера люкс...</p>
		      				</a>
		      			</div>
		      			<div class="col-xs-12 item-gray">
		      				<a href="">
		      				<img src="/static/img/clossed_message.png">
		      				<p class="form_2_date">17 / 10 / 2017</p>
		      				<p class="form_2_message">Акция «-20% на номера люкс...</p>
		      				</a>
		      			</div>
		      		</div>
		      	</div>
		      </div>
		    </div>
		  </div>
		</div>
		</div>
		</div>
		</div>
		</div>
	</section>


	<section class="img-logo-info">
		<div class="main-img-wrap">
			<ul id="smooth-background">
				<li style="background-image: url(/static/img/main_img_1.jpg);"></li>
				<li style="background-image: url(/static/img/main_img_2.jpg);"></li>
				<li style="background-image: url(/static/img/main_img_3.jpg);"></li>
				<li style="background-image: url(/static/img/main_img_4.jpg);"></li>
				<li style="background-image: url(/static/img/main_img_5.jpg);"></li>
			</ul>
			<div class="container logo">
				<img src="/static/img/big_logo.png" alt="">
			</div>
		</div>
		<div class="info">
			<div class="container">
				<h2>Уникальные природные факторы</h2>
				<div class="row">
					<div class="col-sm-4 item-wrap">
						<div class="item">
							<div class="icon">
								<img src="/static/img/nature1.png" alt="">
							</div>
							<div class="text">
								<h3>Уникальный климат</h3>
								<div class="basic-button"><a href="/unikalnyj-klimat.html">подробнее</a></div>
							</div>
						</div>
					</div>
					<div class="col-sm-4 item-wrap">
						<div class="item">
							<div class="icon">
								<img src="/static/img/nature2.png" alt="">
							</div>
							<div class="text">
								<h3>Радоновые воды</h3>
								<div class="basic-button"><a href="/radonovye-vody.html">подробнее</a></div>
							</div>
						</div>
					</div>
					<div class="col-sm-4 item-wrap">
						<div class="item">
							<div class="icon">
								<img src="/static/img/nature3.png" alt="">
							</div>
							<div class="text">
								<h3>Сапропелевые грязи</h3>
								<div class="basic-button"><a href="/sapropelevye-gryazi.html">подробнее</a></div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>

	<?php mod('catalog.action.main_programms') ?>

	<section class="about-order">
		<div class="container">
			<div class="about">
				<?php mod('infoblock.show.about_main') ?>
				<div class="basic-button"><a href="/o-nas.html">о курорте</a></div>
			</div>
			<div class="order">
				<h3>
					<img src="/static/img/order_icon.png" alt="">
					Онлайн<br>Бронирование
				</h3>
				<p>
					<?php mod('infoblock.show.booking_main') ?>
				</p>
				<div class="basic-button"><a href="/bronirovanie.html">забронировать номер</a></div>
			</div>
		</div>
	</section>
</div>

<?php mod('catalog.action.notify') ?>


<?php include "html/templates/footer.tpl.php" ?>