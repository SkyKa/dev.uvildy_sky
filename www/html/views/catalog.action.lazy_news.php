<?php 

header( 'content-type: application/json; charset=utf-8' );
usleep(100000);

$res = array(
		's' => false,
		'mess' => '',
		'html' => '',
	);

$number_news = Utils::getVar( 'number_news' );

if( empty( $number_news ) ) {
	$res['s'] = false;
	$res['mess'] = 'Ошибка #' . __LINE__;
	echo json_encode( $res );

	exit();
}

$table = new Table('catalog_section');

$my_alias = Utils :: getVar( 'page_alias' );

$section = $table -> select('SELECT `id` FROM `catalog_section` WHERE `alias`=:alias LIMIT 1', array( 'alias' => $my_alias ) );
if( empty( $section ) ) {
	$res['s'] = false;
	$res['mess'] = 'Ошибка #' . __LINE__;
	echo json_encode( $res );

	exit();
}

// $sql = $table->db->prepare('SELECT * FROM `position_news` WHERE `section_id`=:id AND `public` ORDER BY `datestamp` DESC LIMIT :number_news, 9');
// $sql->bindValue(":id", $section[0]['id']);
// $sql->bindValue(":number_news", (int) $number_news);
// $sql->execute();
// $news_list = $sql->fetchAll(PDO::FETCH_NAMED);

$news_list = $table -> select('SELECT * FROM `position_news` WHERE `section_id`=:id AND `public` ORDER BY `datestamp` DESC LIMIT '. $number_news .', 9', 
	array( 'id' => $section[0]['id'] ) );

// $res['mess'] = $section[0]['id'];
// $res['123'] = $number_news;

$str = '';

foreach ($news_list as $news_item) {
	$str .= '<div class="item-wrap">';

	if( empty( $news_item['content'] ) ) {
		$str .= '<div class="item">';
	}
	else {
		$str .= '<a href="'. $my_alias .'/' . $news_item['alias'] .'.html" class="item">';
	}
			
	$style = '';
	if( !empty($news_item['img']) ) {
		$str .= '<img src="'. get_cache_pic($news_item['img'], 430, 286, true) .'" class="image">';
		
	}
	else {
		$style = 'style="margin-top: 35px;"';
	}

	if( !empty($news_item['is_share']) ) {
	 		$str .= '<img src="/static/img/share.png" class="share-img">';
	}

	$str .=			'<h2 '. $style .'>'. $news_item['title'] .'</h2>';

	$month = date( 'm', $news_item['datestamp']);
	$month = Langvars::replaceMonth($month);

	if( empty($news_item['is_share']) ) {
		$str .=     '<p class="date">'. date( 'd ', $news_item['datestamp']) . $month . date( ' Y', $news_item['datestamp']) .'</p>';
	}
	else {
		$str .=     '<p class="date"></p>';
	}

	$str .=			'<p class="desc">'. $news_item['description'] .'</p>';

	if( !empty( $news_item['content'] ) ) {
		$str .=    '<p class="details">Подробнее</p>
					<div class="clearfix"></div>';
	}

	if( empty( $news_item['content'] ) ) {
		$str .= '</div>';
	}
	else {
		$str .= '</a>';
	}
				
	$str .=		'</div>';
}

$res['s'] = true;
$res['html'] = $str;
echo json_encode( $res );

