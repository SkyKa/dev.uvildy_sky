<?php

	$section = $args -> section;


?>
				<div class="wooman-areas-container clearfix">

					<div class="areas-block-container">

						<div class="areas-block areas-block-start">
							<div class="ars-title">Наведите на&nbsp;проблемные области</div>
							<img class="ars-cursor" src="/static/img/cursor.png" alt="cursor">
							<span class="ars-triangle"></span>
						</div>

						<?php 
						
							$table = new Table( 'catalog_section' );
							$section = $table -> select( 'SELECT * FROM `catalog_section` WHERE `parent_id`=:pid && alias="problemnye-oblasti" ORDER BY `position` LIMIT 1', array( 'pid' => $section[ 'id' ] ) );
							$arr_section_obj = array( );
							if ( count( $section ) ) {
								$section = end( $section );
								$section = $table -> select( 'SELECT * FROM `catalog_section` WHERE `parent_id`=:pid ORDER BY `position`', array( 'pid' => $section[ 'id' ] ) );
								foreach ( $section as $s ) {
									$section_obj = $table -> select( 'SELECT * FROM `section_kosmetolog_obl` WHERE `public`=1 && `id`=:id LIMIT 1', array( 'id' => $s[ 'id' ] ) );
									if ( !count( $section_obj ) ) continue;

									$section_obj = end( $section_obj );
// <a href="/programmy/' . $s[ 'alias' ] . '/' . $section_obj[ 'alias' ] . '.html" class="btn-more">Подробнее</a>
// programm-offer
									echo '
						<div class="areas-block p-area-' . $s[ 'id' ] . '">
							<div class="ars-title">' . $s[ 'title' ] . '</div>
							' . $section_obj[ 'obj_content' ] . '
							<div class="ars-btn">
								<a href="/programm-offer.html" class="btn-more">Подробнее</a>
							</div>
							<span class="ars-triangle"></span>
						</div>';
								$arr_section_obj[ $s[ 'id' ] ] = $section_obj;
								}

							}
							
						?>

					</div>

					<div class="wooman-areas" style="background-image: url('/static/img/wooman.png')">

						<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 315 915" width="315" height="915">

							<!-- Обычное состояние -->
							<g>
								<?php 
								
							if ( count( $section ) ) {
								foreach ( $section as $s ) {
									if ( !isset( $arr_section_obj[ $s[ 'id' ] ] ) ) continue;
									$res = str_replace( '{ID}', $s[ 'id' ], $arr_section_obj[ $s[ 'id' ] ][ 'obj_def' ] );
									echo '
									' . $res . '';
									
								}
							}
								
								?>
							</g>

							<!-- Состояния при наведении-->
							<?php
							
							if ( count( $section ) ) {
								foreach ( $section as $s ) {
									if ( !isset( $arr_section_obj[ $s[ 'id' ] ] ) ) continue;
									
									echo '
									<g class="p-area-' . $s[ 'id' ] . ' p-area-hover">' . $arr_section_obj[ $s[ 'id' ] ][ 'obj_hov' ] . '</g>';
									
								}
							}
							
							?>

						</svg>

					</div>

				</div>
