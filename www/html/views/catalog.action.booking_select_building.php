<div class="booking-map-page booking-page">
	<div class="container">
		<h1>Выберите здание для бронирования</h1>
		<p class="step">Шаг <span>1</span> из <span>3</span></p>
	</div>
	<div class="booking-map">
		<a href="/vsya-infrastruktura.html" class="to-map" target="_blank">Посмотреть всю инфраструктуру</a>
		<a href="" class="booking-icon icon2 hover" id="housing2">
			<div class="flip-container">
				<div class="flip-card shadow">
					<div class="front">
						<img src="/static/img/booking2.png" alt="">
					</div>
					<div class="back face">
						<p>Корпус 1</p>
					</div>
				</div>
			</div>
		</a>
		<a href="" class="booking-icon icon1 hover" id="housing1">
			<div class="flip-card"> 
				<div class="front"> 
					<img src="/static/img/booking1.png" alt="">
				</div> 
				<div class="back">
					<p>Корпус 2</p>
				</div> 
			</div>
		</a>
		<a href="" class="booking-icon icon3 hover" id="cottages">
			<div class="flip-container">
				<div class="flip-card">
					<div class="front">
						<img src="/static/img/booking3.png" alt="">
					</div>
					<div class="back">
						<p>Коттеджи</p>
					</div>
				</div>
			</div>
		</a>
		<a href="" class="booking-icon icon4 hover" id="housing4">
			<div class="flip-container">
				<div class="flip-card">
					<div class="front">
						<img src="/static/img/booking4.png" alt="">
					</div>
					<div class="back">
						<p>Корпус 4</p>
					</div>
				</div>
			</div>
		</a>
	</div>
</div>