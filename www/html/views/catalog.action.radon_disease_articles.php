<?php

$table = new Table('catalog_section');

$parent = $table -> select( 'SELECT * FROM `catalog_section` WHERE `alias`=:alias LIMIT 1', array( 'alias' => 'radonovye-vody' ) );
$parent = end( $parent );

$childs = $table -> select( 'SELECT * FROM `catalog_section` WHERE `parent_id`=:id ', array( 'id' => $parent[ 'id' ] ) );

$disease_separator = 1;


foreach ( $childs as $child ) {
	if ( $child['alias'] == "kakie-bolezni-lechat-napravleniya" )
	{

		$rows = $table -> select( 'SELECT * FROM `position_radon_news` WHERE `section_id`=:id AND `public`=1 ', array( 'id' => $child['id'] ) );

		foreach ($rows as $row) {
			if ( $disease_separator == 1 ) {
				echo '<div class="col-xs-12 col-sm-3">
						<div class="sixth_slider_under_item">';
			}

			echo '<p><a href="/kakie-bolezni-lechat-radonovye-vanny/'. $row['alias'] .'.html">'. $row['title'] .'</a></p>';

			if ( $disease_separator % 4 == 0 ) {
				echo '</div>
					</div>';
				$disease_separator = 0;
			}

			$disease_separator++;
		}
	}
}