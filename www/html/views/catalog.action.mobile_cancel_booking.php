<?php

if( !isset( $_SESSION ) ) {
    session_start();
}

$str = '';

if( $alias == 'bronirovanie' ) {

	if( !isset( $_SESSION[ 'booking' ][ 'is_process' ] ) ) {

		$_SESSION[ 'booking' ][ 'is_process' ] = true;

	}

}
else {

	if( isset( $_SESSION[ 'booking' ][ 'is_process' ] ) && $_SESSION[ 'booking' ][ 'is_process' ] ) {

		$str .= '<a id="open-cancel-booking-modal" href="#cancel-booking-modal" style="display: none;"></a>
				<div id="cancel-booking-modal" class="cancel-booking-modal">

					<div class="basic-button small"><a href="/shag-1.html" style="text-align: center; float: left; margin-top: 2px;">вернуться к бронированию</a></div>
			        
			        <div class="close-cancel-booking-modal"> 
			            <img src="/mobile_static/img/close_btn.png" alt="">
			        </div>
			            
			        <div class="modal-content">
			    		<div class="clearfix"></div>
			        	<h2>Почему вы передумали бронировать номер?</h2>
			            '. val( 'catalog.action.forms', array( 'alias' => 'otmena_bronirovaniya_mobilnye' ) ) .'
			        </div>
			    </div>';

		echo $str;

		$_SESSION[ 'booking' ][ 'is_process' ] = false;

	}

}