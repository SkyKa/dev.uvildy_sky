<?php 

$section_id = Utils :: getvar('section_id');

if( empty($section_id) ) return false;

$table = new Table('catalog_section');

$programm = get_programm( $section_id );

if( empty( $programm ) ) return false;

$section_duration = $table -> select('SELECT `id` FROM `catalog_section` WHERE `parent_id`=:id AND `position_table`="position_curse_duration" LIMIT 1', array( 'id' => $programm ));

if( empty( $section_duration ) ) return false;

$rows = $table -> select('SELECT `id`, `curse_duration` AS `display` FROM `position_curse_duration` WHERE `section_id`=:id ORDER BY `curse_duration`', 
	array( 'id' => $section_duration[0]['id'] ));

echo serialize( $rows );


function get_programm( $id ) {

	$table = new Table('catalog_section');

	$programm = $table -> select('SELECT `id`, `section_table`, `parent_id` FROM `catalog_section` WHERE `id`=:parent_id LIMIT 1', array( 'parent_id' => $id ));

	if( empty( $programm ) ) return false;

	if( $programm[0]['section_table'] != 'section_programms' ) return get_programm( $programm[0]['parent_id'] );
	else return $programm[0]['id'];

}