<?php 

if( $params[1] != 'details' ) return false;

$table = new Table('catalog_section');
$section = $table -> select('SELECT * FROM `catalog_section` WHERE `alias`=:alias LIMIT 1', array( 'alias' => $params[0] ));

if( empty( $section ) ) return false;
$section = end( $section );

$str = '<header>
			<div class="et-menu-collapse-btn et-collapsed">
				<div class="et-collapse-line"></div>
				<div class="et-collapse-line"></div>
				<div class="et-collapse-line"></div>
			</div>
			
			<div class="h1-wrap">
				<h1>'. $section['title'] .': подробная информация</h1>
			</div>

			<a href="/mobile_static/programms.html" class="back-button">
				<img src="/mobile_static/img/back_button.png" alt="">
			</a>

		</header>

		<div class="all-infra-menu" style="visibility: visible;">
			'. val('catalog.action.mobile_map_menu') .'
	    </div>
		
		<div class="content-wrapper">
			<div class="wrapper">
				<div class="content-inner">';

$item = $table -> select('SELECT * FROM `section_programms` WHERE `id`=:id AND `public` LIMIT 1', array( 'id' => $section[ 'id' ] ));

if( empty( $item ) ) return false;
$item = end( $item );

$str .= '<div class="et-container">';

if( count( $item['result'] ) ) {

	$str .= '<div class="content-container inverse">
				<div class="content">
					<div class="h2-wrap">
						<h2>Результат</h2>
					</div>
					<p>
						'. $item['result'] .'
					</p>
				</div>
			</div>';

}

if( count( $item['content'] ) ) {

	$str .= '<div class="content-container">
				<div class="content">
					'. $item['content'] .'
				</div>
			</div>';

}

$str .= '</div>';

echo $str;