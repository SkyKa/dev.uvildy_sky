<?php

$shares = $args -> shares_id;
$programm_title = $args -> programm_title;
if( empty( $shares ) || empty( $programm_title ) ) return false;

$table = new Table('catalog_section');

$section = $table -> select('SELECT `id` FROM `catalog_section` WHERE `alias`=:alias LIMIT 1', array( 'alias' => $alias ) );
if( !count( $section ) ) return false;

$shares_list = $table -> select('SELECT * FROM `position_news` WHERE `section_id`=:id AND `public` AND `is_share` ORDER BY `datestamp` DESC', 
	array( 'id' => $section[0]['id'] ) );
if( !count( $shares_list ) ) return false;

$str = '<div class="breadcrumbs">
			<div class="container">
				<nav class="breadcrumbs">
					'. val('catalog.action.breadcrumbs', array( 'last_link' => true ) ) . $programm_title .'
				</nav>
			</div>
		</div>
		<div class="container news-page">';


$i = 0;
foreach ( $shares_list as $shares_item ) {

	$in_programm = false;
	foreach( $shares as $share ) {
		if( $shares_item['id'] == $share['share_id'] ) {
			$in_programm = true;
			break;
		}
	}

	if( !$in_programm ) continue;

	if( !$i ) {
		$str .= '<h1>Акции: '. $programm_title .'</h1>
				<div class="row news-wrap">';
	}

	$i++;

	$str .= '<div class="item-wrap">';

	if( empty( $shares_item['content'] ) ) {
		$str .= '<div class="item">';
	}
	else {
		$str .= '<a href="/'. $alias .'/' . $shares_item['alias'] .'.html" class="item">';
	}

	$style = '';
	if( !empty($shares_item['img']) ) {
		$str .= '<img src="'. get_cache_pic($shares_item['img'], 430, 286, true) .'" class="image">';
	} 
	else {
		$style = 'style="margin-top: 95px;"';
	}
	
	$str .= '<img src="/static/img/share.png" class="share-img">';

	$str .=	'<h2 '. $style .'>'. $shares_item['title'] .'</h2>';

	$str .=     '<p class="date"></p>';

	$str .=			'<p class="desc">
						'. $shares_item['description'] .'
					</p>';

	if( !empty( $shares_item['content'] ) ) {
		$str .=    '<p class="details">Подробнее</p>
					<div class="clearfix"></div>';
	}

	if( empty( $shares_item['content'] ) ) {
		$str .= '</div>';
	}
	else {
		$str .= '</a>';
	}
				
	$str .=		'</div>';
}

if( $i ) {
	$str .= '</div>';
}



$i = 0;
foreach ( $shares_list as $shares_item ) {

	$in_programm = false;
	foreach( $shares as $share ) {
		if( $shares_item['id'] == $share['share_id'] ) {
			$in_programm = true;
			break;
		}
	}

	if( $in_programm ) continue;

	if( !$i ) {
		$i++;
		$str .=	'<div class="clearfix"></div>
				 <h2 class="h2-to-h1">Другие акции</h2>
					<div class="row news-wrap">';
	}

	$str .= '<div class="item-wrap">';

	if( empty( $shares_item['content'] ) ) {
		$str .= '<div class="item">';
	}
	else {
		$str .= '<a href="/'. $alias .'/' . $shares_item['alias'] .'.html" class="item">';
	}
			
	$style = '';
	if( !empty($shares_item['img']) ) {
		$str .= '<img src="'. get_cache_pic($shares_item['img'], 430, 286, true) .'" class="image">';
	} 
	else {
		$style = 'style="margin-top: 95px;"';
	}
	
	$str .= '<img src="/static/img/share.png" class="share-img">';

	$str .=	'<h2 '. $style .'>'. $shares_item['title'] .'</h2>';
	
	$str .=     '<p class="date"></p>';

	$str .=			'<p class="desc">
						'. $shares_item['description'] .'
					</p>';

	if( !empty( $shares_item['content'] ) ) {
		$str .=    '<p class="details">Подробнее</p>
					<div class="clearfix"></div>';
	}

	if( empty( $shares_item['content'] ) ) {
		$str .= '</div>';
	}
	else {
		$str .= '</a>';
	}
				
	$str .=		'</div>';
}

$str .= '</div>';

$str .= '</div>';

echo $str;
