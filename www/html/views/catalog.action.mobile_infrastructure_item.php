<?php

echo '<div class="et-container infra-content-page">
		<div class="mark-overlay-wrap" data-mark="">
			<div class="mark-overlay">
				<div class="marks-wrap">
					<div class="close-btn">
						<div class="line one"></div>
						<div class="line two"></div>
					</div>
					<p class="do-mark">Поставьте Вашу оценку</p>
					<div class="marks">
						<img src="/mobile_static/img/star.png" alt=""><!-- 
					 --><img src="/mobile_static/img/star.png" alt=""><!-- 
					 --><img src="/mobile_static/img/star.png" alt=""><!-- 
					 --><img src="/mobile_static/img/star.png" alt=""><!-- 
					 --><img src="/mobile_static/img/star.png" alt="">
					</div>
					<p class="done-btn">Готово</p>
				</div>
			</div>
		</div>
		<div class="put-an-assessment">
			<img src="/mobile_static/img/full_star.png" alt=""><!-- 
		 --><img src="/mobile_static/img/full_star.png" alt=""><!-- 
		 --><img src="/mobile_static/img/full_star.png" alt=""><!-- 
		 --><img src="/mobile_static/img/full_star.png" alt=""><!-- 
		 --><img src="/mobile_static/img/star.png" alt="">
			<span>Оценить</span>
		</div>
		<div class="content-container">
			<div class="content">
				<p>
					Курорт «Увильды» — это не только лечение 
					и оздоровление, но ещё и полноценный от-
					дых и всевозможные развлечения на любой 
					вкус и возраст. Частые гости курорта зна-
					ют, что даже несколько дней на Увильдах 
					можно приравнять к настоящему отпуску, 
					проведённому с пользой для души и тела. 
					А развлечься здесь можно, как говорится, 
					по полной программе. 
				</p>
			</div>
		</div>

		<div class="owl-carousel">
			<div class="item">
				<img src="/mobile_static/img/infra_img.jpg" alt="">
				<a href="">Прокат роликовых коньков</a>
			</div>
			<div class="item">
				<img src="/mobile_static/img/infra_img.jpg" alt="">
				<a href="">Прокат лодок</a>
			</div>
		</div>

	</div>';