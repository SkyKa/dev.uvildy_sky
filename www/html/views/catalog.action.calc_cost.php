<?php 

$my_alias = 'programmy';

$table = new Table('catalog_section');
$section = $table -> select('SELECT `id` FROM `catalog_section` WHERE `alias`=:alias LIMIT 1', array( 'alias' => $my_alias ));

if( !count( $section ) ) return false;

$section = end( $section );

$rows = $table -> select('SELECT * FROM `catalog_section` WHERE `parent_id`=:id ORDER BY `position`' , array( 'id' => $section['id'] )); 

if( !count($rows) ) return false;

$str = '<div class="programms-page">
			<div class="breadcrumbs">
				<div class="container">
					'. val('catalog.action.breadcrumbs') .'
				</div>
			</div>
			<div class="container programms">
				<h1>'. val('pages.show.title') .'</h1>
				<div class="row">';


foreach( $rows as $row ) {

	$item = $table -> select('SELECT * FROM `section_programms` WHERE `id`=:id AND `public` LIMIT 1', array( 'id' => $row[ 'id' ] ));

	if( !count( $item ) ) continue;

	$item = end( $item );

	if( empty( $item['is_programm'] ) ) continue; 

	$str .= '<div class="col-md-3 col-sm-4 item-wrap">
				<div class="item" data-programm-id="'. $item['id'] .'">
					<div class="img-wrap" style="background-image: url(/'. get_cache_pic( $item['img'], 345, 439, true ) .');"></div>
					<p class="title">';

					if( $item['is_new'] ) $str .= '<img src="/static/img/new.png" alt="">'; 

					$str .= $row['title'] .'</p>
					<div class="overlay-wrap">
						<div class="overlay">
							<div class="button-wrapper enroll">
								<div class="basic-button"><button style="background-color: #ff6c00;">Узнать стоимость</button></div>
							</div>
						</div>
					</div>
				</div>
			</div>';

}


$str .=        '</div>
			</div>
		</div>';

echo $str;