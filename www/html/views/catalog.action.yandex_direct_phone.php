<?php

	$str = '';

	$num_phone = $args -> num;

	$postfix = $args -> postfix;


	$yandex_direct = ( isset( $_SESSION[ 'yandex_direct' ] ) ) ? $_SESSION[ 'yandex_direct' ] : false;
	$sms = ( isset( $_SESSION[ 'sms' ] ) ) ? true : false;
	$subscribe = ( isset( $_SESSION[ 'subscribe' ] ) ) ? true : false;
	
	
	if ( $sms ) {
//		try {
			$phone = val( 'banner.show.sms_phone' . $num_phone );
//		} catch ( Exception $e ) {

//		}

		if ( $phone ) {
			$str .=	'<a class="phone" href="tel:' . 
					Utils :: phone_number( $phone ) .
					'" data-phone="' . Utils :: phone_number( $phone ) . '" data-id="' . $num_phone . '">' . $phone . '</a>';
		}
	}
	else if ( $subscribe ) {

		$phone = val( 'banner.show.subscribe_phone' . $num_phone );

		if ( $phone ) {
			$str .=	'<a class="phone" href="tel:' . 
					Utils :: phone_number( $phone ) .
					'" data-phone="' . Utils :: phone_number( $phone ) . '" data-id="' . $num_phone . '">' . $phone . '</a>';
		}

	}
	else if ( $yandex_direct ) {

		$phone = val( 'banner.show.yandex_direct_phone' . $num_phone );

		if ( $phone ) {
			$str .=	'<a class="phone" href="tel:' . 
					Utils :: phone_number( $phone ) .
					'" data-phone="' . Utils :: phone_number( $phone ) . '" data-id="' . $num_phone . '">' . $phone . '</a>';
		}
	}
	else {
	
		$phone = val( 'banner.show.phone' . $num_phone );

		if ( $phone ) {
			$str .=	'<a class="phone" href="tel:' . 
					Utils :: phone_number( $phone ) .
					'" data-phone="' . Utils :: phone_number( $phone ) . '" data-id="' . $num_phone . '">' . $phone . '</a>';
		}

	}
	
	echo $str;