<?php

$table = new Table('catalog_section');

$parent = $table -> select( 'SELECT * FROM `catalog_section` WHERE `alias`=:alias LIMIT 1', array( 'alias' => 'sapropelevye-gryazi' ) );
$parent = end( $parent );

$childs = $table -> select( 'SELECT * FROM `catalog_section` WHERE `parent_id`=:id ', array( 'id' => $parent[ 'id' ] ) );

foreach ( $childs as $child ) {
	if ( $child['alias'] == "glavnyj-slajder2" )
	{
		$rows = $table -> select( 'SELECT * FROM `position_mud_news` WHERE `section_id`=:id AND `public`=1 ', array( 'id' => $child['id'] ) );

		foreach ($rows as $row) {
			echo '
				<div>
					<img src="'. $row['img_src'] .'">
					<div class="container">
						<div class="landing_slider_radon_wrap col-xs-12 col-sm-8"> 
							<h2>'. $row['title'] .'</h2>
							<p>'. $row['title2'] .'</p>
							<button>'. $row['button_text'] .'</button>
						</div>
					</div>
				</div>
			';
		}
	}			
}