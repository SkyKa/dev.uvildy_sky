<?php

	$array = array(
		's' => false,
		'error' => '',
		'data' => array()
	);


	header( 'Content-type: application/json; charset=UTF-8' );

	if (
		( isset( $_SERVER[ 'PHP_AUTH_USER' ] ) && ( $_SERVER[ 'PHP_AUTH_USER' ] == 'uvildy' ) ) &&
		( isset( $_SERVER[ 'PHP_AUTH_PW' ] ) && ( md5( $_SERVER[ 'PHP_AUTH_PW' ] ) == 'e855941d7be60f83044fddbabc059b87' ) )
	) {
		// uvildy00_andrey
		$table = new Table( 'position_orders' );
		$rows = $table -> select( 'SELECT * FROM `position_orders` WHERE `pay_status`=2 && `in1c`=0 ORDER BY `id`' );
		$i = 0;
		$sum = 0;
		foreach( $rows as $row ) {
			foreach ( $row as $col => $val ) {
				if ( !$val || $col == 'pay_ip' || $col == 'pay_refund_url' || $col == 'pay_status' ) continue;
				if ( $col == 'pay_amount' ) $sum += $val;
				if ( $col == 'clients' ) {
					$array[ 'data' ][ $i ][ $col ] = json_decode( $val );
				}
				else $array[ 'data' ][ $i ][ $col ] = $val;
			}
			++$i;
		}
		$array[ 's' ] = true;
		//$array[ 'sum' ] = ( $sum / 100 );
	}
	else {
		header( 'WWW-Authenticate: Basic realm="1C protected"' );
		header( 'HTTP/1.0 401 Unauthorized' );
		$array[ 'error' ] = 'Authorisation Error';
	}


	echo json_encode( $array );
	exit;