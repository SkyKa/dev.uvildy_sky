<?php

$table = new Table('catalog_section');

$parent = $table -> select( 'SELECT * FROM `catalog_section` WHERE `alias`=:alias LIMIT 1', array( 'alias' => 'radonovye-vody' ) );
$parent = end( $parent );

$childs = $table -> select( 'SELECT * FROM `catalog_section` WHERE `parent_id`=:id ', array( 'id' => $parent[ 'id' ] ) );


foreach ( $childs as $child ) {
	if ( $child['alias'] == "radon-v-medecine" )
	{
		$rows = $table -> select( 'SELECT * FROM `position_radon_news` WHERE `section_id`=:id AND `public`=1 ORDER BY `datestamp` DESC ', array( 'id' => $child['id'] ) );

		foreach ($rows as $row) {
			echo '
				<div>
					<div class="fourth_slider_item col-xs-12">
						<div class="fourth_slider_item_header">
							<a href="/radon-v-medecine/'. $row['alias'] .'.html"><img src="'. $row['img_src'] .'"></a>
							<div class="fourth_slider_item_header_doc">
								<img src="'. $row['img_doc'] .'">
								<h4>'. $row['title_doc'] .'</h4>
								<p>'. $row['prof_doc'] .'</p>
								<a href="#">Все статьи врача</a>
							</div>
						</div>
						<div class="fourth_slider_item_wrap">
							<h3><a href="/radon-v-medecine/'. $row['alias'] .'.html">'. $row['title'] .'</a></h3>
							<p>'. $row['title2'] .'</p>
							<a href="/radon-v-medecine/'. $row['alias'] .'.html"><button>Подробнее</button></a>
						</div>
					</div>
				</div>
			';
		}
	}
}