<?php 

$my_alias = 'videogalerei';

$table = new Table('catalog_section');

$str = '';

$section_id = $table -> select('SELECT `id` FROM `catalog_section` WHERE `alias`=:alias LIMIT 1', array( 'alias' => $my_alias ));

if( empty( $section_id ) ) return false;
$section_id = end($section_id);

$galleries = $table -> select('SELECT `id`, `title` FROM `catalog_section` WHERE `parent_id`=:id ORDER BY `position`', array( 'id' => $section_id['id'] ));

if( empty( $galleries ) ) return false;

$i = 1;

foreach( $galleries as $gallery ) {
	$str .= '<h2>'. $gallery['title'] .'</h2>';

	$videos = $table -> select('SELECT * FROM `position_video_gallery` WHERE `section_id`=:id AND `public`', array( 'id' => $gallery['id'] ) );
	if( empty($videos) ) continue;

	$str .= '<div class="video-gallery video-gallery-'. $i .'">';

	foreach( $videos as $video ) {

		if( empty( $video['video_link'] ) ) continue;

		$youtube_params = array();

		parse_str( parse_url( $video['video_link'], PHP_URL_QUERY ), $youtube_params );

		if( empty($youtube_params['v']) ) continue; 

		$str .= '
		<a href="'. $video['video_link'] .'" data-sub-html="'. $video['description'] .'" target="_blank" rel="nofollow">
			    	<img src="https://img.youtube.com/vi/'. $youtube_params['v'] .'/sddefault.jpg">
					<div class="gallery-poster">
                        <img src="/static/img/play_button.png">
                    </div>
                    <p class="custom-description">'. $video['description'] .'</p>
			  	</a>';
	}

	$str .= '</div>';
}

echo $str;