<?php

    $alias = Utils :: getVar( 'alias' );
	$items = $data -> getItems( $menu -> id );
    $uri_orig = $_SERVER[ 'REQUEST_URI' ];
    $uris = explode( '?', $uri_orig );
    $uri = $uris[ 0 ];
	
	echo "<ul class='menu-collapsed'>";

// <li class="et-link"><a href="">Главная</a></li>
// <li class="et-link"><a href="">Оставить отзыв</a></li>
// <li class="et-link"><a href="/personalnaya-karta.html">Персональная карта</a></li>
// <li class="et-link"><a href="">Контакты</a></li>
// <li class="et-link"><a href="tel:83512251616">Позвонить</a></li>
// <li class="et-link"><a href="/sluzhba-podderzhki.html">Служба поддержки</a></li>

	// 1 LV
    foreach( $items as $row )
    {

        if ( $row[ 'visible' ] != 1 ) continue;
        $href = SF . $row[ 'type_link' ];
        $class = "";
        if ( $uri == $href || main_active_razdel( $row[ 'type_id' ], $alias ) ) {
            $class = 'selected';
        }

        $subitems = $data -> getItems( $menu -> id, $row[ 'id' ] );
		
		// если подразделы
		if ( count( $subitems ) ) {
			echo "
			<li class='et-dropdown ". $class ."'>";
		}
		// нет подразделов
		else {
			echo "
			<li class='". $class ."'>";
		}

		// если пункт меню не ссылка
		if ( $row[ 'type' ] == '_label' ) {
			echo "
				<a href='' ". "class='et-not-link'" .">" . $row[ 'title' ] . "</a>";
		}
		// если ссылка 
		else {
			echo "
				<a href='" . $row[ 'type_link' ] . "'>" . $row[ 'title' ] . "</a>";
		}
		
		// 2 LV
        if ( count( $subitems ) ) {
            echo "
					<ul class='et-dropdown-menu'>";
            foreach ( $subitems as $subitem ) {

            	if ( $subitem[ 'visible' ] != 1 ) continue;
            	
                echo "
						<li>";
				echo "
							<a href='" . $subitem[ 'type_link' ] . "'>" . $subitem[ 'title' ] . "</a>";
							
				echo "
						</li>";
            }
            echo "
					</ul>";
        }
        echo '</li>';
    }


	echo "
	</ul>";


	function main_active_razdel( $tid, $alias ) {
		$table = new Table( 'pages' );
		$rows = $table -> select( "SELECT `id` FROM `pages` WHERE `alias`=:alias AND (`id`=:tid OR `parent_id`=:tid) LIMIT 1",
			array( 'tid' => $tid, 'alias' => $alias ) );

		if ( count( $rows ) > 0 ) return true;
		else return false;
	}
