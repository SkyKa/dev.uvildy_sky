<?php

if( !empty($params[0]) ) {
	mod('catalog.action.mobile_infrastructure_item');
	return;
}

echo '<div class="et-container infrastructure-page">

	<div class="menu">
		<a href="" class="item item1 selected">Лето</a>
		<a href="" class="item item2">Зима</a>
		<a href="" class="item item3">Круглый год</a>
	</div>

	<div class="infrastructure-list">
		<a href="" class="menu-item mark-done" style="background-image: url(/mobile_static/img/infrastructure1.jpg);">
			<div class="title-wrap">
				<div class="put-an-assessment">
					<img src="/mobile_static/img/full_star.png" alt=""><!-- 
					 --><img src="/mobile_static/img/full_star.png" alt=""><!-- 
					 --><img src="/mobile_static/img/full_star.png" alt=""><!-- 
					 --><img src="/mobile_static/img/full_star.png" alt=""><!-- 
					 --><img src="/mobile_static/img/star.png" alt="">
					<span>Оценить</span>
				</div>
				<div class="title">
					<p>Прокат инвентаря</p>
				</div>
			</div>
			<div class="mark-overlay-wrap" data-mark="">
				<div class="mark-overlay">
					<div class="marks-wrap">
						<div class="close-btn">
							<div class="line one"></div>
							<div class="line two"></div>
						</div>
						<p class="do-mark">Поставьте Вашу оценку</p>
						<div class="marks">
							<img src="/mobile_static/img/star.png" alt=""><!-- 
						 --><img src="/mobile_static/img/star.png" alt=""><!-- 
						 --><img src="/mobile_static/img/star.png" alt=""><!-- 
						 --><img src="/mobile_static/img/star.png" alt=""><!-- 
						 --><img src="/mobile_static/img/star.png" alt="">
						</div>
						<p class="done-btn">Готово</p>
					</div>
				</div>
			</div>
		</a>
		<a href="/mobile_static/select_building.html" class="menu-item" style="background-image: url(/mobile_static/img/infrastructure2.jpg);">
			<div class="title-wrap">
				<div class="put-an-assessment">
					<img src="/mobile_static/img/full_star.png" alt=""><!-- 
					 --><img src="/mobile_static/img/full_star.png" alt=""><!-- 
					 --><img src="/mobile_static/img/full_star.png" alt=""><!-- 
					 --><img src="/mobile_static/img/star.png" alt=""><!-- 
					 --><img src="/mobile_static/img/star.png" alt="">
					<span>Оценить</span>
				</div>
				<div class="title">
					<p>ПЛЯЖНЫЙ ресторан «Шамбала»</p>
				</div>
			</div>
			<div class="mark-overlay-wrap" data-mark="">
				<div class="mark-overlay">
					<div class="marks-wrap">
						<div class="close-btn">
							<div class="line one"></div>
							<div class="line two"></div>
						</div>
						<p class="do-mark">Поставьте Вашу оценку</p>
						<div class="marks">
							<img src="/mobile_static/img/star.png" alt=""><!-- 
						 --><img src="/mobile_static/img/star.png" alt=""><!-- 
						 --><img src="/mobile_static/img/star.png" alt=""><!-- 
						 --><img src="/mobile_static/img/star.png" alt=""><!-- 
						 --><img src="/mobile_static/img/star.png" alt="">
						</div>
						<p class="done-btn">Готово</p>
					</div>
				</div>
			</div>
		</a>
		<a href="/mobile_static/programms.html" class="menu-item" style="background-image: url(/mobile_static/img/infrastructure3.jpg);">
			<div class="title-wrap">
				<div class="put-an-assessment">
					<img src="/mobile_static/img/full_star.png" alt=""><!-- 
					 --><img src="/mobile_static/img/full_star.png" alt=""><!-- 
					 --><img src="/mobile_static/img/full_star.png" alt=""><!-- 
					 --><img src="/mobile_static/img/full_star.png" alt=""><!-- 
					 --><img src="/mobile_static/img/star.png" alt="">
					<span>Оценить</span>
				</div>
				<div class="title">
					<p>Клуб «Увильды»</p>
				</div>
			</div>
			<div class="mark-overlay-wrap" data-mark="">
				<div class="mark-overlay">
					<div class="marks-wrap">
						<div class="close-btn">
							<div class="line one"></div>
							<div class="line two"></div>
						</div>
						<p class="do-mark">Поставьте Вашу оценку</p>
						<div class="marks">
							<img src="/mobile_static/img/star.png" alt=""><!-- 
						 --><img src="/mobile_static/img/star.png" alt=""><!-- 
						 --><img src="/mobile_static/img/star.png" alt=""><!-- 
						 --><img src="/mobile_static/img/star.png" alt=""><!-- 
						 --><img src="/mobile_static/img/star.png" alt="">
						</div>
						<p class="done-btn">Готово</p>
					</div>
				</div>
			</div>
		</a>
		<a href="" class="menu-item" style="background-image: url(/mobile_static/img/infrastructure4.jpg);">
			<div class="title-wrap">
				<div class="put-an-assessment">
					<img src="/mobile_static/img/full_star.png" alt=""><!-- 
					 --><img src="/mobile_static/img/full_star.png" alt=""><!-- 
					 --><img src="/mobile_static/img/full_star.png" alt=""><!-- 
					 --><img src="/mobile_static/img/full_star.png" alt=""><!-- 
					 --><img src="/mobile_static/img/star.png" alt="">
					<span>Оценить</span>
				</div>
				<div class="title">
					<p>Настольный теннис</p>
				</div>
			</div>
			<div class="mark-overlay-wrap" data-mark="">
				<div class="mark-overlay">
					<div class="marks-wrap">
						<div class="close-btn">
							<div class="line one"></div>
							<div class="line two"></div>
						</div>
						<p class="do-mark">Поставьте Вашу оценку</p>
						<div class="marks">
							<img src="/mobile_static/img/star.png" alt=""><!-- 
						 --><img src="/mobile_static/img/star.png" alt=""><!-- 
						 --><img src="/mobile_static/img/star.png" alt=""><!-- 
						 --><img src="/mobile_static/img/star.png" alt=""><!-- 
						 --><img src="/mobile_static/img/star.png" alt="">
						</div>
						<p class="done-btn">Готово</p>
					</div>
				</div>
			</div>
		</a>
		<a href="" class="menu-item" style="background-image: url(/mobile_static/img/infrastructure5.jpg);">
			<div class="title-wrap">
				<div class="put-an-assessment">
					<img src="/mobile_static/img/full_star.png" alt=""><!-- 
					 --><img src="/mobile_static/img/full_star.png" alt=""><!-- 
					 --><img src="/mobile_static/img/full_star.png" alt=""><!-- 
					 --><img src="/mobile_static/img/full_star.png" alt=""><!-- 
					 --><img src="/mobile_static/img/star.png" alt="">
					<span>Оценить</span>
				</div>
				<div class="title">
					<p>БИЛЬЯРД</p>
				</div>
			</div>
			<div class="mark-overlay-wrap" data-mark="">
				<div class="mark-overlay">
					<div class="marks-wrap">
						<div class="close-btn">
							<div class="line one"></div>
							<div class="line two"></div>
						</div>
						<p class="do-mark">Поставьте Вашу оценку</p>
						<div class="marks">
							<img src="/mobile_static/img/star.png" alt=""><!-- 
						 --><img src="/mobile_static/img/star.png" alt=""><!-- 
						 --><img src="/mobile_static/img/star.png" alt=""><!-- 
						 --><img src="/mobile_static/img/star.png" alt=""><!-- 
						 --><img src="/mobile_static/img/star.png" alt="">
						</div>
						<p class="done-btn">Готово</p>
					</div>
				</div>
			</div>
		</a>
		<a href="" class="menu-item" style="background-image: url(/mobile_static/img/infrastructure6.jpg);">
			<div class="title-wrap">
				<div class="put-an-assessment">
					<img src="/mobile_static/img/full_star.png" alt=""><!-- 
					 --><img src="/mobile_static/img/full_star.png" alt=""><!-- 
					 --><img src="/mobile_static/img/full_star.png" alt=""><!-- 
					 --><img src="/mobile_static/img/full_star.png" alt=""><!-- 
					 --><img src="/mobile_static/img/full_star.png" alt="">
					<span>Оценить</span>
				</div>
				<div class="title">
					<p>ТРЕНАЖЁРНЫЙ ЗАЛ</p>
				</div>
			</div>
			<div class="mark-overlay-wrap" data-mark="">
				<div class="mark-overlay">
					<div class="marks-wrap">
						<div class="close-btn">
							<div class="line one"></div>
							<div class="line two"></div>
						</div>
						<p class="do-mark">Поставьте Вашу оценку</p>
						<div class="marks">
							<img src="/mobile_static/img/star.png" alt=""><!-- 
						 --><img src="/mobile_static/img/star.png" alt=""><!-- 
						 --><img src="/mobile_static/img/star.png" alt=""><!-- 
						 --><img src="/mobile_static/img/star.png" alt=""><!-- 
						 --><img src="/mobile_static/img/star.png" alt="">
						</div>
						<p class="done-btn">Готово</p>
					</div>
				</div>
			</div>
		</a>
	</div>

</div>';
