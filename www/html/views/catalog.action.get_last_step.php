<?php 

$table = new Table('catalog_section');

$str = '';

$str .= '<div class="booking-step3-page booking-page">
			<div class="container">
				<h1>Онлайн бронирование номера</h1>
				<p class="step" style="display: inline-block;">
					Шаг <span>3</span> из <span>3</span>
				</p>
				<a href="" class="booking-back"><img src="/static/img/arr_back.png" alt=""></a>
			</div>

			<div class="selected-booking">
				<div class="h2-wrap">
					<h2>Вы выбрали</h2>
				</div>
				<div class="container">';


$room = $table -> select('SELECT `catalog_section`.`id`, `catalog_section`.`parent_id`, `catalog_section`.`title`,
	`section_offers`.`n_rooms`, `section_offers`.`id_1c`, `section_offers`.`n_main_place`, `section_offers`.`n_additional_place` 
	FROM `catalog_section` 
	INNER JOIN 
		`section_offers` 
	ON
		`section_offers`.`id`=`catalog_section`.`id`
	WHERE `catalog_section`.`id`=:id LIMIT 1', array( 'id' => $_SESSION['booking']['number'] ));

$programm_title = 'без лечения';

if($_SESSION['booking']['programm'] != 'without-programm') {

    $programm = $table->select('SELECT `id`, `title` FROM `catalog_section` WHERE `id`=:id LIMIT 1', array( 'id' => $_SESSION['booking']['programm'] ) );

    if( !empty( $programm ) ) {
    	$programm_title = $programm[0]['title'];
    }
}

$main_place = $room[0]['n_main_place'];

$persons = $_SESSION['booking']['persons'];

$end_date = date('Y-m-d', strtotime( "+" . $_SESSION['booking']['duration'] . " days" , strtotime($_SESSION['booking']['start_date'])));

$sum = 0;

// Отправляем взрослых на основные места
foreach( $_SESSION['booking']['persons'] as $key => $person ) {

	if( mb_strtolower($person['age'], 'UTF-8') == 'взрослый' ) {

		if( $main_place == 0 ) break;

		$age_categories = array();

		// Если детская программа, то прайс считаем для взрослого по программе "общее оздоровление"
		if( mb_strtolower($programm_title, 'UTF-8') == 'детская программа' ) {
			$age_categories = $table->select('SELECT `section_number_prices`.`id`, `section_number_prices`.`price`, `section_number_prices`.`programm`, `section_number_prices`.`age`, `section_number_prices`.`food_type`, `section_number_prices`.`end_date`, `section_number_prices`.`place_type`, `section_number_prices`.`start_date`, `section_number_prices`.`offers_id_1c` as id_1c
            FROM 
                `section_number_prices` 
            INNER JOIN 
                `section_offers`
            ON 
                `section_offers`.`id_1c`=`section_number_prices`.`offers_id_1c`
            WHERE 
            (`section_number_prices`.`offers_id_1c`=:id_1c AND
            (DATE(:select_date) <= `section_number_prices`.`end_date`) AND
            (DATE(:end_date) > `section_number_prices`.`start_date`) AND
            `section_number_prices`.`programm`=:programm_title AND
            `section_number_prices`.`age`=:age AND
            `section_number_prices`.`food_type`=:food AND
            `section_number_prices`.`place_type`="Основное место") OR
            (`section_number_prices`.`offers_id_1c`=:id_1c AND
            (DATE(:select_date) <= `section_number_prices`.`end_date`) AND
            (DATE(:end_date) > `section_number_prices`.`start_date`) AND
            `section_number_prices`.`programm`="новогодняя программа" AND
            `section_number_prices`.`age`=:age AND
            `section_number_prices`.`food_type`=:food AND
            `section_number_prices`.`place_type`="Основное место")
            ORDER BY `section_number_prices`.`start_date`', 
            array( 'id_1c' => $room[0]['id_1c'], 'select_date' => $_SESSION['booking']['start_date'], 'end_date' => $end_date, 
            	'programm_title' => 'общее оздоровление', 'age' => $person['age'], 'food' => $person['food'] )); 
		}
		else {
			$age_categories = $table->select('SELECT `section_number_prices`.`id`, `section_number_prices`.`price`, `section_number_prices`.`programm`, `section_number_prices`.`age`, `section_number_prices`.`food_type`, `section_number_prices`.`end_date`, `section_number_prices`.`place_type`, `section_number_prices`.`start_date`, `section_number_prices`.`offers_id_1c` as id_1c
            FROM 
                `section_number_prices` 
            INNER JOIN 
                `section_offers`
            ON 
                `section_offers`.`id_1c`=`section_number_prices`.`offers_id_1c`
            WHERE 
            (`section_number_prices`.`offers_id_1c`=:id_1c AND
            (DATE(:select_date) <= `section_number_prices`.`end_date`) AND
            (DATE(:end_date) > `section_number_prices`.`start_date`) AND
            `section_number_prices`.`programm`=:programm_title AND
            `section_number_prices`.`age`=:age AND
            `section_number_prices`.`food_type`=:food AND
            `section_number_prices`.`place_type`="Основное место") OR
            (`section_number_prices`.`offers_id_1c`=:id_1c AND
            (DATE(:select_date) <= `section_number_prices`.`end_date`) AND
            (DATE(:end_date) > `section_number_prices`.`start_date`) AND
            `section_number_prices`.`programm`="новогодняя программа" AND
            `section_number_prices`.`age`=:age AND
            `section_number_prices`.`food_type`=:food AND
            `section_number_prices`.`place_type`="Основное место")
            ORDER BY `section_number_prices`.`start_date`', 
            array( 'id_1c' => $room[0]['id_1c'], 'select_date' => $_SESSION['booking']['start_date'], 'end_date' => $end_date, 
            	'programm_title' => $programm_title, 'age' => $person['age'], 'food' => $person['food'] ));
		}

		search_period_room( $age_categories, $_SESSION['booking']['start_date'], $end_date );

	    $categories = $age_categories;

	    foreach( $categories as $k => $category ) {
	        if( !isset( $category['in_out'] ) || !$category['in_out'] ) {
	            unset($age_categories[$k]);
	        }
	    }

		$price = calculate_price( $age_categories, $_SESSION['booking']['start_date'], $end_date );

		$sum += $price['price'];
		$_SESSION['booking']['persons'][$key]['price'] = $price['price'];
		$_SESSION['booking']['persons'][$key]['is_custom_calc'] = $price['is_custom_calc'];
		$_SESSION['booking']['persons'][$key]['programm'] = $price['programm'];
		$_SESSION['booking']['persons'][$key]['min_price'] = $price['min_price'];
		$_SESSION['booking']['persons'][$key]['place_type'] = 'Основное место';
		$main_place--;
		unset($persons[$key]);
	}
}

// Раскидываем оставшихся клиентов
foreach( $persons as $key => $person ) {

	if( $main_place != 0 ) {
		$age_categories = $table->select('SELECT `section_number_prices`.`id`, `section_number_prices`.`price`, `section_number_prices`.`programm`, `section_number_prices`.`age`, `section_number_prices`.`food_type`, `section_number_prices`.`end_date`, `section_number_prices`.`place_type`, `section_number_prices`.`start_date`, `section_number_prices`.`offers_id_1c` as id_1c
            FROM 
                `section_number_prices` 
            INNER JOIN 
                `section_offers`
            ON 
                `section_offers`.`id_1c`=`section_number_prices`.`offers_id_1c`
            WHERE 
            (`section_number_prices`.`offers_id_1c`=:id_1c AND
            (DATE(:select_date) <= `section_number_prices`.`end_date`) AND
            (DATE(:end_date) > `section_number_prices`.`start_date`) AND
            `section_number_prices`.`programm`=:programm_title AND
            `section_number_prices`.`age`=:age AND
            `section_number_prices`.`food_type`=:food AND
            `section_number_prices`.`place_type`="Основное место") OR
            (`section_number_prices`.`offers_id_1c`=:id_1c AND
            (DATE(:select_date) <= `section_number_prices`.`end_date`) AND
            (DATE(:end_date) > `section_number_prices`.`start_date`) AND
            `section_number_prices`.`programm`="новогодняя программа" AND
            `section_number_prices`.`age`=:age AND
            `section_number_prices`.`food_type`=:food AND
            `section_number_prices`.`place_type`="Основное место")
            ORDER BY `section_number_prices`.`start_date`', 
            array( 'id_1c' => $room[0]['id_1c'], 'select_date' => $_SESSION['booking']['start_date'], 'end_date' => $end_date, 
            	'programm_title' => $programm_title, 'age' => $person['age'], 'food' => $person['food'] )); 

		search_period_room( $age_categories, $_SESSION['booking']['start_date'], $end_date );

	    $categories = $age_categories;

	    foreach( $categories as $k => $category ) {
	        if( !isset( $category['in_out'] ) || !$category['in_out'] ) {
	            unset($age_categories[$k]);
	        }
	    }

		$price = calculate_price( $age_categories, $_SESSION['booking']['start_date'], $end_date );

		$sum += $price['price'];
		$_SESSION['booking']['persons'][$key]['price'] = $price['price'];
		$_SESSION['booking']['persons'][$key]['is_custom_calc'] = $price['is_custom_calc'];
		$_SESSION['booking']['persons'][$key]['programm'] = $price['programm'];
		$_SESSION['booking']['persons'][$key]['min_price'] = $price['min_price'];
		$_SESSION['booking']['persons'][$key]['place_type'] = 'Основное место';
		$main_place--;
	}
	else {
		$age_categories = $table->select('SELECT `section_number_prices`.`id`, `section_number_prices`.`price`, `section_number_prices`.`programm`, `section_number_prices`.`age`, `section_number_prices`.`food_type`, `section_number_prices`.`end_date`, `section_number_prices`.`place_type`, `section_number_prices`.`start_date`, `section_number_prices`.`offers_id_1c` as id_1c
            FROM 
                `section_number_prices` 
            INNER JOIN 
                `section_offers`
            ON 
                `section_offers`.`id_1c`=`section_number_prices`.`offers_id_1c`
            WHERE 
            (`section_number_prices`.`offers_id_1c`=:id_1c AND
            (DATE(:select_date) <= `section_number_prices`.`end_date`) AND
            (DATE(:end_date) > `section_number_prices`.`start_date`) AND
            `section_number_prices`.`programm`=:programm_title AND
            `section_number_prices`.`age`=:age AND
            `section_number_prices`.`food_type`=:food AND
            `section_number_prices`.`place_type`="Дополнительное место") OR
            (`section_number_prices`.`offers_id_1c`=:id_1c AND
            (DATE(:select_date) <= `section_number_prices`.`end_date`) AND
            (DATE(:end_date) > `section_number_prices`.`start_date`) AND
            `section_number_prices`.`programm`="новогодняя программа" AND
            `section_number_prices`.`age`=:age AND
            `section_number_prices`.`food_type`=:food AND
            `section_number_prices`.`place_type`="Дополнительное место")
            ORDER BY `section_number_prices`.`start_date`', 
            array( 'id_1c' => $room[0]['id_1c'], 'select_date' => $_SESSION['booking']['start_date'], 'end_date' => $end_date, 
            	'programm_title' => $programm_title, 'age' => $person['age'], 'food' => $person['food'] )); 

		search_period_room( $age_categories, $_SESSION['booking']['start_date'], $end_date );

	    $categories = $age_categories;

	    foreach( $categories as $k => $category ) {
	        if( !isset( $category['in_out'] ) || !$category['in_out'] ) {
	            unset($age_categories[$k]);
	        }
	    }

		$price = calculate_price( $age_categories, $_SESSION['booking']['start_date'], $end_date );

		$sum += $price['price'];
		
		$_SESSION['booking']['persons'][$key]['price'] = $price['price'];
		$_SESSION['booking']['persons'][$key]['is_custom_calc'] = $price['is_custom_calc'];
		$_SESSION['booking']['persons'][$key]['programm'] = $price['programm'];
		$_SESSION['booking']['persons'][$key]['min_price'] = $price['min_price'];
		$_SESSION['booking']['persons'][$key]['place_type'] = 'Дополнительное место';
	}
}

$_SESSION['booking']['sum'] = $sum;

$class_room = array();

if( !empty( $room ) ) {
	$class_room = $table -> select('SELECT `id`, `title` FROM `catalog_section` WHERE `id`=:id LIMIT 1', array( 'id' => $room[0]['parent_id'] ));
}

$sum_by_day = 0;
foreach( $_SESSION['booking']['persons'] as $person ) {
	if( $person['is_custom_calc'] ) {
		$sum_by_day = 0;
		break;
	}
	$sum_by_day += $person[ 'price' ] / $_SESSION[ 'booking' ][ 'duration' ];
}

$sum_by_day_text = '';

if( $sum_by_day == 0 ) {
	$sum_by_day_text = 'рассчитывается индивидуально';
}
else {
	$sum_by_day_text = number_format($sum_by_day, 2, ',', ' ') . ' <span class="rouble">c</span> в сутки';
}

$str .= '<p class="selected-item cost"><b>Стоимость: </b>'. $sum_by_day_text .'</p>';

$str .=	'<p class="selected-item room"><b>Номер: </b>' . $class_room[0]['title'] . ' ' . $room[0]['title'] .'</p>';

$str .=	'<p class="selected-item date"><b>Дата заезда: </b>'. date("d.m.Y", strtotime($_SESSION['booking']['start_date'])) .'г.</p>
					<div class="row number-person">';

foreach( $_SESSION['booking']['persons'] as $person ) {

	$programm = $programm_title;

	if( ($programm == 'Детская программа') && ( $person['age'] == 'взрослый' ) ) {
		$programm = 'Общее оздоровление';
	}

	$person_price = '';

	if( $person[ 'price' ] == 0 ) {
		$person_price = 'рассчитывается индивидуально';
	}
	else {
		$person_price = Utils :: price( $person[ 'price' ] / $_SESSION[ 'booking' ][ 'duration' ] ) . ' <span class="rouble">c</span> в сутки';
	}

	$str .= '<div class="col-md-4 col-sm-6 item">
				<h2>'. $person['age'] .' </h2>
				<p class="character">Программа: <span>'. $programm .'</span></p>
				<p class="character">Продолжительность курса (дней): <span>'. $_SESSION['booking']['duration'] .'</span></p>
				<p class="character">Тип места: <span>'. $person['place_type'] .'</span></p>
				<p class="cost"><b>Стоимость: </b>'. $person_price .'</p>
			</div>';

}

$sum_text = '';

if( $sum == 0 ) {
	$sum_text = 'рассчитывается индивидуально';
}
else {
	$sum_text = 'от ' . number_format($sum, 2, ',', ' ');
}

// $sum = $cost_by_day * $_SESSION['booking']['duration'];
						
$str .=				'</div>
					<div class="indicative-cost-block">
						<p>Стоимость проживания:&nbsp; <span>'. $sum_text .' </span><span class="rouble">c</span></p>
					</div>
				</div>

				<div class="formalize-apply">
					<div class="container">' 
						. val( 'catalog.action.forms', array( 'alias' => 'booking_form' ) );

$str .=				'</div>
				</div>
			</div>
		</div>';

echo $str;



function calculate_price($categories, $start_date, $end_date) {
	
	$price = 0; 
	$min_price = 0;
	$num_items = count($categories);
	$i = 0;
	$n_days = 0;
	$programm = '';
	$is_custom_calc = false;

	foreach( $categories as $category ) {

		if( !$i ) {

			$min_price = $category['price'];

			$date1 = date_create($start_date);

			if( $num_items == 1 ) {
				$date2 = date_create( $end_date );
			}
			else {
				$date2 = date_create( $category['end_date'] );
				$n_days++;
			}

			$interval = date_diff($date1, $date2);
			$n_days += (int) $interval->format("%a");

			$programm = $category['programm'];

		}
		else if( $num_items == ($i+1) ) {

			$date1 = date_create($category['start_date']);
			$date2 = date_create($end_date);
			$interval = date_diff($date1, $date2);
			$n_days = (int) $interval->format("%a");

		}
		else {

			$date1 = date_create($category['start_date']);
			$date2 = date_create($category['end_date']);
			$interval = date_diff($date1, $date2);
			$n_days = (int) $interval->format("%a");
			$n_days++;

		}

		// if( $category['programm'] == 'новогодняя программа' ) {
		// 	$n_days = 1;
		// }

		if( $min_price > $category['price'] ) {
			$min_price = $category['price'];
		}

		$price += $n_days * $category['price'];

	// Если в выбранном прайсе нет цены, то рассчитываем индивидуально
		if( $category['price'] == 0 ) {
			$is_custom_calc = true;
		}

		$i++;

	}

	$array = array(
		'price' => $price,
		'min_price' => $min_price,
		'programm' => $programm,
		'is_custom_calc' => $is_custom_calc
		);

	return $array;

}


function search_period_room( &$age_categories, $select_date, $end_date ) {


    foreach( $age_categories as $key => $category ) {

        if( isset($age_categories[$key]['in_out']) && $age_categories[$key]['in_out'] ) continue; 

        if( (strtotime("+1 day", strtotime( $category['end_date'])) >= strtotime( $end_date )) && (strtotime( $category['start_date'] ) <= strtotime( $select_date )) ) { 
            $age_categories[$key]['in_out'] = true;
            continue;            
        }
        else {
            if( strtotime( $category['start_date'] ) <= strtotime( $select_date ) ) {

                $indexes = find_req_period_room( $age_categories, $select_date, $end_date, $category, $key );

                foreach( $indexes as $index ) {
                    $age_categories[$index]['in_out'] = true; 
                }
            }
        }

    }

}

function find_req_period_room( &$age_categories, $select_date, $end_date, $category, $key, $index=array() ) {

   	$first_day = strtotime("+1 day", strtotime($category['end_date']));
    $search_param = array();

    $search_param['age'] = $category['age'];
    $search_param['food_type'] = $category['food_type'];
    $search_param['place_type'] = $category['place_type'];
    $search_param['id_1c'] = $category['id_1c'];

    $index[] = $key;

    foreach( $age_categories as $k => $cat ) {
        if( ($first_day == strtotime($cat['start_date']) ) 
            && ($search_param['age'] == $cat['age']) 
            && ($search_param['food_type'] == $cat['food_type']) 
            && ($search_param['place_type'] == $cat['place_type'])
            && ($search_param['id_1c'] == $cat['id_1c']) ) {
            
            if( strtotime($cat['end_date']) >= strtotime("-1 day", strtotime($end_date)) ) {
                $index[] = $k;
                return $index;
            }
            else {
                return find_req_period_room($age_categories, $select_date, $end_date, $cat, $k, $index);
            }
        }
    }

    return array();

}