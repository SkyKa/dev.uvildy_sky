<?php

	header( 'content-type: application/json; charset=utf-8' );

	usleep(100000);

	if( !isset( $_SESSION ) ) {
	    session_start();
	}

    if( isset( $_SESSION['booking']['failed'] ) && $_SESSION['booking']['failed'] ) {
        unset( $_SESSION['booking'] );
    }

	$json = array(	
				's' => false, 
				'mess' => '',
				);

	if( !isset( $_SESSION['booking']['step'] ) ) {
		$_SESSION['booking']['step'] = 1;
	}

	$method = Utils :: getVar( 'method' );

	if( empty( $method ) ) {
		$json[ 'mess' ] = 'Ошибка #' . __LINE__;
		echo json_encode( $json );
		exit();
	}

    switch( $method ) {

        case 'select_building': 
        	select_building(); 
        	break;

        case 'add_person': 
        	add_person(); 
        	break;

        case 'write_to_programm': 
            write_to_programm(); 
            break;

        case 'remove_person': 
            remove_person(); 
            break;

        case 'get_step': 
        	get_step(); 
        	break;

         case 'get_step_2': 
            get_step_2(); 
            break;

        case 'get_previous_step': 
        	get_previous_step(); 
        	break;

        case 'get_humans_form': 
            get_humans_form(); 
            break;

        case 'get_last_step': 
            get_last_step();   
            break;

       	default: 
       		$json[ 'mess' ] = 'Ошибка #' . __LINE__;
			echo json_encode( $json );
			exit();

    }

    function select_building() {

    	$building = Utils :: getVar( 'building' );

    	if( empty( $building ) ) {
    		$json[ 'mess' ] = 'Ошибка #' . __LINE__;
			echo json_encode( $json );
			exit();
    	}

    	$buildings = array( 'housing1', 'housing2', 'housing4', 'cottages' );

    	if( !in_array( $building, $buildings ) ) {
    		$json[ 'mess' ] = 'Ошибка #' . __LINE__;
			echo json_encode( $json );
			exit();
    	}

    	$_SESSION[ 'booking' ][ 'step' ] = 2;
    	$_SESSION[ 'booking' ][ 'building' ] = $building;

    	get_step();
    }

    function add_person() {

        $json = array(  
                's' => false, 
                'mess' => '',
                );

        $number_persons = 0;

        if( !empty( $_SESSION['booking']['persons'] ) ) {
    	   $number_persons = count( $_SESSION['booking']['persons'] );
        }

        $select_date = $_SESSION['booking']['start_date'];

        $end_date = date('Y-m-d', strtotime( "+" . $_SESSION['booking']['duration'] . " days" , strtotime($_SESSION['booking']['start_date'])));

        if( empty( $select_date ) ) {
            $json[ 'mess' ] = 'Ошибка #' . __LINE__;
            echo json_encode( $json );
            exit();
        }

    	$table = new Table('catalog_section');

    	$age = Utils::getVar( 'age' );

        $programm_id = $_SESSION['booking']['programm'];

        $programm = $table->select('SELECT `id`, `title` FROM `catalog_section` WHERE `id`=:id LIMIT 1', 
            array( 'id' => $programm_id ) );

        if(empty($programm) && ($programm_id != 'without-programm')) {
            $res['s'] = false;
            $res['mess'] = 'Ошибка #' . __LINE__;
            echo json_encode( $res );
            exit();
        }

        $programm_title = '';

        if( $programm_id == 'without-programm' ) {
            $programm_title = 'без лечения';
        }
        else {
            $programm_title = mb_strtolower($programm[0]['title'], 'UTF-8');
        }

        $food = Utils::getVar( 'food' );

        if( $food == 'без питания' ) {
            $food = '';
        }

        $food = html_entity_decode( $food );

        $age_categories = array();
        $room = get_room();

        if( !empty( $room ) ) {

            $res['programm_title'] = $programm_title;

            $age_categories = $table->select('SELECT `section_number_prices`.`id`, `section_number_prices`.`age`, `section_number_prices`.`price`, `section_offers`.`n_main_place`, `section_offers`.`n_additional_place` 
                FROM 
                    `section_number_prices` 
                INNER JOIN 
                    `section_offers`
                ON 
                    `section_offers`.`id_1c`=`section_number_prices`.`offers_id_1c`
                WHERE 
                `section_number_prices`.`offers_id_1c`=:id_1c AND
                (DATE(:select_date) <= `section_number_prices`.`end_date`) AND
                (DATE(:end_date) > `section_number_prices`.`start_date`) AND
                (`section_number_prices`.`programm`=:programm_title OR
                `section_number_prices`.`programm`="новогодняя программа") AND
                `section_number_prices`.`food_type`=:food AND
                `section_number_prices`.`age`=:age
                GROUP BY `section_number_prices`.`age` 
                ORDER BY `section_number_prices`.`age` 
                LIMIT 1', 
                array( 'id_1c' => $room[0]['id_1c'], 'select_date' => $select_date, 'end_date' => $end_date, 'programm_title' => $programm_title, 'age' => $age, 'food' => $food ));

            if ( !count( $age_categories ) ) {
                $res['s'] = false;
                $res['mess'] = 'Ошибка #' . __LINE__;
                echo json_encode( $res );
                exit();
            }
        }


        if ( ($age_categories[0]['n_main_place'] + $age_categories[0]['n_additional_place']) <= $number_persons ) {
            $json[ 'error' ][] = '! Нельзя добавить более '. $number_persons .' человек';
        }

		////////////////////////////////
		// Костыль для детской программы, поскольку мы прибавляли взрослого человека
		/*
        if( $programm_title == 'детская программа' ) {
            if( $age_categories[0]['n_additional_place'] < $number_persons) {
                $json[ 'error' ][] = '! Нельзя больше добавить людей в бронирование';
            }
        }
		*/
		////////////////////////////////
		
        if( empty($json['error']) ) {
            $key = md5( microtime().rand() );
        	$_SESSION['booking']['persons'][ $key ]['age'] = $age;
            $_SESSION['booking']['persons'][ $key ]['food'] = $food;

            $json[ 'added_persons' ] = val('catalog.action.get_persons');
        	$json[ 's' ] = true;
        }

        echo json_encode( $json );
        exit();

    }


    function write_to_programm() {

        unset( $_SESSION['booking']['persons'] );

        $table = new Table('catalog_section');

        $json = array(  
                    's' => false, 
                    'mess' => '',
                    );


        $programm_id = ( !empty( $_REQUEST[ 'programm' ] ) ) ? $_REQUEST[ 'programm' ] : $_SESSION[ 'booking' ][ 'programm' ];

        $programm = array();

        if($programm_id != 'without-programm') {
            $programm = $table->select('SELECT `id` FROM `catalog_section` WHERE `id`=:id LIMIT 1', array( 'id' => $programm_id ) );
        }

        if( empty( $programm ) && ( $programm_id != 'without-programm' ) ) {
            $json[ 'mess' ] = 'Ошибка #' . __LINE__;
            echo json_encode( $json );
            exit();
        }

        $duration = ( !empty( $_REQUEST[ 'duration' ] ) ) ? $_REQUEST[ 'duration' ] : $_SESSION['booking']['duration'];

        if( $programm_id == 'without-programm' ) {
            if( !(is_int( +$duration ) && ( $duration <= 120 ) && ( $duration > 0 )) ) {
                $json[ 'mess' ] = 'Ошибка #' . __LINE__;
                $json[ 'error' ][] = 'Необходимо ввести продолжительность курса';
                echo json_encode( $json );
                exit();
            }
        }
        else {

            $section_curse_duration = $table -> select('SELECT `id` FROM `catalog_section` WHERE `parent_id`=:id and 
                `position_table`="position_curse_duration" LIMIT 1', array( 'id' => $programm_id ) );

            if( empty( $section_curse_duration ) ) {
                $json[ 'mess' ] = 'Ошибка #' . __LINE__;
                echo json_encode( $json );
                exit();
            }

            if( $duration == 'default' ) {
                $curse_durations = $table -> select('SELECT MIN(`curse_duration`) AS `min_curse_duration` FROM `position_curse_duration` WHERE `section_id`=:id GROUP BY `section_id`', array( 'id' => $section_curse_duration[ 0 ][ 'id' ]) );

				$duration = ( isset( $curse_durations[0]['min_curse_duration'] ) ) ? $curse_durations[0]['min_curse_duration'] : 1;
            }
            else {
/*
                $curse_durations = $table -> select('SELECT `id` FROM `position_curse_duration` WHERE `section_id`=:id AND `curse_duration`=:duration', 
                    array( 'id' => $section_curse_duration[0]['id'], 'duration' => $duration ) );

                if( empty( $curse_durations ) ) {
                    $json[ 'mess' ] = 'Ошибка #' . __LINE__;
                    echo json_encode( $json );
                    exit();
                }
*/
            }

        }

        if( empty( $json['error'] ) ) {

            $_SESSION['booking']['programm'] = $programm_id;
            $_SESSION['booking']['duration'] = $duration;

            $json['s'] = true;

        }
        
        echo json_encode( $json );
        exit();

    }


    function get_step() {

    	switch( $_SESSION['booking']['step'] ) {
    		case 1:
    			$json['html'] = val('catalog.action.booking_select_room');
    			$json[ 's' ] = true;
				echo json_encode( $json );
    			exit();

    		case 2: 
                get_step_2();
                exit();

            case 3: 
                get_last_step();
                exit();

    		default: 
	       		$json[ 'mess' ] = 'Ошибка #' . __LINE__;
				echo json_encode( $json );
				exit();
    	}

    }


    function get_previous_step() {

    	switch( $_SESSION['booking']['step'] ) {

    		case 2: 
    			$json['html'] = val('catalog.action.booking_select_room');
                $_SESSION['booking']['step'] = 1;

                unset( $_SESSION['booking']['persons'] );
                unset( $_SESSION['booking']['number'] );

    			$json[ 's' ] = true;
				echo json_encode( $json );
    			exit();

            case 3: 
                $_SESSION['booking']['step'] = 2;
                get_step_2();
                exit();

    		default: 
	       		$_SESSION['booking']['step'] = 1;
                get_step();
				exit();

    	}

    }


    function remove_person() {

        $person_id = Utils::getVar('person_id');

        if( isset( $_SESSION['booking']['persons'][$person_id] ) ) {
		////////////////////////////////////////
		// Костыль для детской программы, нельзя удалить взрослого
		/*
            if( ($_SESSION['booking']['programm'] == '23') 
                && ($_SESSION['booking']['persons'][$person_id]['age'] == 'взрослый' ) ) {
                $json['error'][] = '! Нельзя удалить взрослого человека, дети не могут заехать в номер одни';
                $json[ 'added_persons' ] = val('catalog.action.get_persons');
                $json[ 's' ] = false;
                echo json_encode( $json );
                exit();
            }
            else {
		*/
		////////////////////////////////////////
                unset($_SESSION['booking']['persons'][$person_id]);
                $json[ 'added_persons' ] = val('catalog.action.get_persons');
                $json[ 's' ] = true;
                echo json_encode( $json );
		/*
            }
		*/
        }

        exit();

    }

    function get_last_step() {
        
        if( !isset($_SESSION['booking']['persons']) ) {
            $json['error'][] = 'Нужно добавить людей в бронирование';
        }

        if( empty( $json['error'] ) ) {

            $json['html'] = val('catalog.action.get_last_step');

            if( !empty( $json['html'] ) ) {
                $_SESSION['booking']['step'] = 3;
                $json['s'] = true;
            }
        }
        
        echo json_encode( $json );
        exit();

    }



function get_step_2() {

    unset( $_SESSION['booking']['persons'] );
    $table = new Table('catalog_section');

    $json = array(  
                's' => false, 
                'mess' => '',
                );

    $start_date = Utils::getVar('start_date');

    if( empty( $start_date ) && empty( $_SESSION['booking']['start_date'] ) ) {
        $json['error'][] = 'Необходимо выбрать дату заезда';
    }

    $date = '';

    if( !empty( $start_date ) ) {

        $d = DateTime::createFromFormat('d.m.Y', $start_date);

        if( !($d && $d->format('d.m.Y') === $start_date) ) {
            $json['error'][] = 'Неправильный формат даты, выберите дату в календаре';
        }
        else {
            $date = date('Y-m-d', strtotime($start_date));
            $date_now = date("Y-m-d");

            if( $date < $date_now ) {
                $json['error'][] = 'Невозможно выбрать дату в прошлом, выберите дату в календаре';
            }
        }

    }

    if( empty( $start_date ) && !empty( $_SESSION['booking']['start_date'] ) ) {
        $date = $_SESSION['booking']['start_date'];
    }

    $programm_id = ( !empty( $_REQUEST[ 'programm' ] ) ) ? $_REQUEST[ 'programm' ] : $_SESSION[ 'booking' ][ 'programm' ];

    $programm = array();

    if($programm_id != 'without-programm') {
        $programm = $table->select('SELECT `id`, `alias` FROM `catalog_section` WHERE `id`=:id LIMIT 1', array( 'id' => $programm_id ) );
    }

    if( empty( $programm ) && ( $programm_id != 'without-programm' ) ) {
        $json[ 'mess' ] = 'Ошибка #' . __LINE__;
        echo json_encode( $json );
        exit();
    }

    $duration = 0;

    if( !empty( $_REQUEST[ 'duration' ] ) ) {
        $duration = $_REQUEST[ 'duration' ];
    }
    else {
        if( !empty( $_SESSION['booking']['duration'] ) ) {
            $duration = $_SESSION['booking']['duration']; 
        }
    }

    if( $programm_id == 'without-programm' ) {
        if( !(is_int( +$duration ) && ( $duration <= 120 ) && ( $duration > 0 )) ) {
            $json[ 'mess' ] = 'Ошибка #' . __LINE__;
            $json[ 'error' ][] = 'Необходимо ввести продолжительность курса';
            echo json_encode( $json );
            exit();
        }
    }
    else {

        if( $programm[0]['alias'] == 'putevka-vyhodnogo-dnya' ) {
            $weekday = (int) date('w', strtotime($date));
            if( ($weekday != 0) && ($weekday != 5) && ($weekday != 6) ) {
                $json[ 'mess' ] = 'Ошибка #' . __LINE__;
                echo json_encode( $json );
                exit();
            }
            else {
                if( ($weekday == 0 && $duration > 1) || ($weekday == 5 && $duration > 3) || ($weekday == 6 && $duration > 2) ) {
                    $json[ 'mess' ] = 'Ошибка #' . __LINE__;
                    echo json_encode( $json );
                    exit();
                }
            }
        }

        $section_curse_duration = $table -> select('SELECT `id` FROM `catalog_section` WHERE `parent_id`=:id and 
            `position_table`="position_curse_duration" LIMIT 1', array( 'id' => $programm_id ) );

        if( empty( $section_curse_duration ) ) {
            $json[ 'mess' ] = 'Ошибка #' . __LINE__;
            echo json_encode( $json );
            exit();
        }

        if( $duration == 'default' ) {
            $curse_durations = $table -> select('SELECT MIN(`curse_duration`) AS `min_curse_duration` FROM `position_curse_duration` WHERE `section_id`=:id GROUP BY `section_id`', array( 'id' => $section_curse_duration[ 0 ][ 'id' ]) );

            $duration = ( isset( $curse_durations[0]['min_curse_duration'] ) ) ? $curse_durations[0]['min_curse_duration'] : 1;
        }
        else {
/*
            $curse_durations = $table -> select('SELECT `id` FROM `position_curse_duration` WHERE `section_id`=:id AND `curse_duration`=:duration', 
                array( 'id' => $section_curse_duration[0]['id'], 'duration' => $duration ) );

            if( empty( $curse_durations ) ) {
                $json[ 'mess' ] = 'Ошибка #' . __LINE__;
                echo json_encode( $json );
                exit();
            }
*/
        }

    }

    if( empty( $json['error'] ) ) {

        $_SESSION['booking']['start_date'] = $date;
        $_SESSION['booking']['programm'] = $programm_id;
        $_SESSION['booking']['duration'] = $duration;

        $json['html'] = val('catalog.action.step_2');

        if( !empty( $json['html'] ) ) {
            $_SESSION['booking']['step'] = 2;
            $json['s'] = true;
        }
    }
    
    echo json_encode( $json );
    exit();

}


function get_humans_form() {

    unset( $_SESSION['booking']['persons'] );

    $table = new Table('catalog_section');

    $json = array(  
                's' => false, 
                'mess' => '',
                );

    $select_date = $_SESSION['booking']['start_date'];

    if( empty( $select_date ) ) {
        $json[ 'mess' ] = 'Ошибка #' . __LINE__;
        echo json_encode( $json );
        exit();
    }


    $age_categories = array();

    $table = new Table('catalog_section');

    $room = get_room();
    if( empty( $room ) ) {
        $json['error'][] = 'Выбранный номер не существует';
    }
    else {

        $programm_id = $_SESSION['booking']['programm'];

        $programm = $table->select('SELECT `id`, `title` FROM `catalog_section` WHERE `id`=:id LIMIT 1', 
            array( 'id' => $programm_id ) );

        if(empty($programm) && ($programm_id != 'without-programm')) {
            $res['s'] = false;
            $res['mess'] = 'Ошибка #' . __LINE__;
            echo json_encode( $res );
            exit();
        }

        $programm_title = '';

        if( $programm_id == 'without-programm' ) {
            $programm_title = 'без лечения';
        }
        else {
            $programm_title = mb_strtolower($programm[0]['title'], 'UTF-8');
        }

        $end_date = date('Y-m-d', strtotime( "+" . $_SESSION['booking']['duration'] . " days" , strtotime($_SESSION['booking']['start_date'])));
        
        $age_categories = $table->select(
            'SELECT `section_number_prices`.`id`, `section_number_prices`.`age`, `section_number_prices`.`programm`, `section_number_prices`.`food_type`, `section_number_prices`.`end_date`, `section_number_prices`.`place_type`, `section_number_prices`.`start_date`
            FROM 
                `section_number_prices` 
            INNER JOIN 
                `section_offers`
            ON 
                `section_offers`.`id_1c`=`section_number_prices`.`offers_id_1c`
            WHERE 
            (`section_number_prices`.`offers_id_1c`=:id_1c AND
            (DATE(:select_date) <= `section_number_prices`.`end_date`) AND
            (DATE(:end_date) > `section_number_prices`.`start_date`) AND
            `section_number_prices`.`programm`=:programm_title) OR
            (`section_number_prices`.`offers_id_1c`=:id_1c AND
            (DATE(:select_date) <= `section_number_prices`.`end_date`) AND
            (DATE(:end_date) > `section_number_prices`.`start_date`) AND
            `section_number_prices`.`programm`="новогодняя программа")
            ORDER BY `section_number_prices`.`start_date`', 
            array( 'id_1c' => $room[0]['id_1c'], 'select_date' => $select_date, 'end_date' => $end_date, 'programm_title' => $programm_title ));

        search_period( $age_categories, $select_date, $end_date );

        $categories = $age_categories;

        $json['categories'] = $age_categories;

        foreach( $categories as $key => $category ) {
            if( !isset( $category['in_out'] ) || !$category['in_out'] ) {
                unset($age_categories[$key]);
            }
        }

        $json['categories2'] = $age_categories;

		////////////////////////////////////
        // Костыль для детской программы, добавляем взрослого
		/*
        if( $programm_title == 'детская программа' ) {
            $key = md5( microtime().rand() );
            $_SESSION['booking']['persons'][ $key ]['age'] = 'взрослый';
            $_SESSION['booking']['persons'][ $key ]['food'] = 'шведский стол';
        }
		*/
		////////////////////////////////////
        if( empty($age_categories) ) {
            $json['error'][] = 'Не найдено свободных номеров с выбранными параметрами, попробуйте найти номер с другими параметрами';
        }
        else {
            $json['humans_form'] = val('catalog.action.get_humans_form', array( 'age_categories' => $age_categories ));
        }
    }

    if( empty( $json['error'] ) ) {
        if( !empty( $json['humans_form'] ) ) {
            $_SESSION['booking']['number'] = Utils :: getVar( 'number' );
            $json['s'] = true;
        }
    }
    
    echo json_encode( $json );
    exit();

}


function get_room() {
    $table = new Table('catalog_section');

    $number = ( empty( $_REQUEST[ 'number' ]	) ) ? $_SESSION[ 'booking' ][ 'number' ] : $_REQUEST[ 'number' ];

    if( !empty( $number ) ) {

        $room = $table->select('SELECT * FROM `section_offers` WHERE `id`=:id LIMIT 1', array( 'id' => $number ));

        return $room;
    }
    else {
        return false;
    }
}


function search_period( &$age_categories, $select_date, $end_date ) {

    foreach( $age_categories as $key => $category ) {

        if( isset($age_categories[$key]['in_out']) && $age_categories[$key]['in_out'] ) continue; 

        if( (strtotime("+1 day", strtotime( $category['end_date'])) >= strtotime( $end_date )) && (strtotime( $category['start_date'] ) <= strtotime( $select_date )) ) { 
            $age_categories[$key]['in_out'] = true;
            continue;            
        }
        else {
            if( strtotime( $category['start_date'] ) <= strtotime( $select_date ) ) {

                $indexes = find_req_period( $age_categories, $select_date, $end_date, $category, $key );

                foreach( $indexes as $index ) {
                    $age_categories[$index]['in_out'] = true; 
                }
            }
        }

    }

}

function find_req_period( &$age_categories, $select_date, $end_date, $category, $key, $index=array() ) {

    $first_day = strtotime("+1 day", strtotime($category['end_date']));
    $search_param = array();

    $search_param['age'] = $category['age'];
    $search_param['food_type'] = $category['food_type'];
    $search_param['place_type'] = $category['place_type'];

    $index[] = $key;

    foreach( $age_categories as $k => $cat ) {
        if( ($first_day == strtotime($cat['start_date']) ) 
            && ($search_param['age'] == $cat['age']) 
            && ($search_param['food_type'] == $cat['food_type'])
            && ($search_param['place_type'] == $cat['place_type']) ) {

            // Костыль поиска для новогодней программы
            // if( $cat['food_type'] == 'Ресторан "Акватория"' ) {
            //     $cat['food_type'] = $search_param['food_type'];
            // }
            
            if( strtotime($cat['end_date']) >= strtotime("-1 day", strtotime($end_date)) ) {
                $index[] = $k;
                return $index;
            }
            else {
                return find_req_period($age_categories, $select_date, $end_date, $cat, $k, $index);
            }
        }
    }

    return array();

}