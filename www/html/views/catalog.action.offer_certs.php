<?php

	$myalias = 'kosmetologiya';

	$table = new Table( 'catalog_section' );
	$section = $table -> select( 'SELECT * FROM `catalog_section` WHERE alias=:alias LIMIT 1', array( 'alias' => $myalias ) );
	if ( !count( $section ) ) return false;
	$section = end( $section );

	$certs_section = $table -> select( 'SELECT * FROM `catalog_section` WHERE `parent_id`=:pid && `position_table`=:position_table LIMIT 1',
		array( 'pid' => $section[ 'id' ], 'position_table' => 'position_photo_gallery' ) );
	if ( !count( $certs_section ) ) return false;
	$certs_section = end( $certs_section );

	$certs = $table -> select( 'SELECT * FROM `position_photo_gallery` WHERE `section_id`=:sid && `public`=1 ORDER BY `position`',
		array( 'sid' => $certs_section[ 'id' ] ) );

	if ( !count( $certs ) ) return false;

	$str = '
<h2 class="cst-title-2">Всё оборудование и препараты имеют международные сертификаты</h2>
<div class="certificates-slider owl-carousel clearfix">';

	foreach( $certs as $cert ) {
			$str .= '
		<a href="/'. get_cache_pic( $cert[ 'img' ], 1000, 800, true) . '"><img src="/'. get_cache_pic( $cert[ 'img' ], 270, 270, true ) . '" alt="' . $cert[ 'description' ] . '"></a>';
	}

	$str .= '
</div>';
	
	
	echo $str;