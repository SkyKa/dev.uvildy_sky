<?php 

	$table = new Table('catalog_section');

	$parent = $table -> select( 'SELECT * FROM `catalog_section` WHERE `alias`=:alias LIMIT 1', array( 'alias' => 'radonovye-vody' ) );
	$parent = end( $parent );

	$childs = $table -> select( 'SELECT * FROM `catalog_section` WHERE `parent_id`=:id ', array( 'id' => $parent[ 'id' ] ) );

	$video_page = false;
	$questions = false;
	$i = 0;	

	if ($alias == 'nauchnye-stati-o-radone') {
		foreach ( $childs as $child ) {
			if ( $child['alias'] == "nauchnye_stati" )
			{
				$rows = $table -> select( 'SELECT * FROM `position_radon_news` WHERE `section_id`=:id AND `public`=1 ORDER BY `datestamp` DESC ', array( 'id' => $child['id'] ) );
			}
		}
	}

	if ($alias == 'novosti-o-radonovom-lechenii') {
 		foreach ( $childs as $child ) {
			if ( $child['alias'] == "radonovoe-lechenie" )
			{
				$rows = $table -> select( 'SELECT * FROM `position_radon_news` WHERE `section_id`=:id AND `public`=1 ORDER BY `datestamp` DESC ', array( 'id' => $child['id'] ) );
			}
		}
	}

	if ($alias == 'radon-v-medecine') {
 		foreach ( $childs as $child ) {
			if ( $child['alias'] == "radon-v-medecine" )
			{
				$rows = $table -> select( 'SELECT * FROM `position_radon_news` WHERE `section_id`=:id AND `public`=1 ORDER BY `datestamp` DESC ', array( 'id' => $child['id'] ) );
			}
		}
	}

	if ($alias == 'metodika-lecheniya-radonom') {
 		foreach ( $childs as $child ) {
			if ( $child['alias'] == "metodika-lecheniya-radonom" )
			{
				$rows = $table -> select( 'SELECT * FROM `position_radon_news` WHERE `section_id`=:id AND `public`=1 ORDER BY `datestamp` DESC ', array( 'id' => $child['id'] ) );
			}
		}
 		$video_page = true;
	}

	if ($alias == 'kakie-bolezni-lechat-radonovye-vanny') {
 		foreach ( $childs as $child ) {
			if ( $child['alias'] == "kakie-bolezni-lechat-radonovye-vanny" || $child['alias'] == "kakie-bolezni-lechat-napravleniya" )
			{
				$rows = $table -> select( 'SELECT * FROM `position_radon_news` WHERE `section_id`=:id AND `public`=1 ORDER BY `datestamp` DESC ', array( 'id' => $child['id'] ) );
			}
		}
	}

	if ($alias == 'vysokoaktivnye-mirovye-istochniki-radonovoj-vody') {
 		foreach ( $childs as $child ) {
			if ( $child['alias'] == "mirovye-istochniki" )
			{
				$rows = $table -> select( 'SELECT * FROM `position_radon_news` WHERE `section_id`=:id AND `public`=1 ORDER BY `datestamp` DESC ', array( 'id' => $child['id'] ) );
			}
		}
	}

	if ($alias == 'vopros-otvet') {
 		foreach ( $childs as $child ) {
			if ( $child['alias'] == "vopros-otvet" )
			{
				$rows = $table -> select( 'SELECT * FROM `position_radon_questions` WHERE `section_id`=:id AND `public`=1 ', array( 'id' => $child['id'] ) );
			}
		}
		$questions = true;
	}

	if ( !$params[ 0 ] ) {
		

		echo '<div class="col-xs-12 "><h2>'. val('pages.show.title') .'</h2></div>';

		echo '<div class="row news-wrap jscroll">';

		foreach ($rows as $row) {

			if ($video_page){			
				echo '
						<div class="item-wrap radon">
							<a class="item" href="/'. $alias .'/'. $row["alias"] .'.html">
								<iframe class="image" src="'. $row['video_link'] .'"></iframe>
								<h3>'. $row["title"] .'</h3>
								<p class="date">'. date( 'd', $row['datestamp']) .' / '. date( 'm', $row['datestamp']) .' / '. date( 'Y', $row['datestamp']) .'</p>
								<p class="desc">'. $row["title2"] .'</p>
								<p class="details">Подробнее</p>
								<div class="clearfix"></div>
							</a>
						</div>
					';			
			} elseif ($questions) {
					echo '
						<div class="question_item full">
							<div class="question_item_header">
								<a role="button" data-toggle="collapse" href="#collapseExample'.$i.'" aria-expanded="false" aria-controls="collapseExample'.$i.'"><h4>'. $row['title'] .'</h4></a>
								<div class="question_item_header_drop" role="button" data-toggle="collapse" href="#collapseExample'.$i.'" aria-expanded="false" aria-controls="collapseExample'.$i.'">
									<i class="fas fa-chevron-down"></i>
								</div>
							</div>
							<div class="collapse" id="collapseExample'.$i.'">
								<p>'. $row['title2'] .'</p>
							</div>
						</div>
					';
					$i++;
				} else {
					echo '
						<div class="item-wrap radon">
							<a class="item" href="/'. $alias .'/'. $row["alias"] .'.html">
								<img class="image" src="'. $row["img_src"] .'">
								<h3>'. $row["title"] .'</h3>
								<p class="date">'. date( 'd', $row['datestamp']) .' / '. date( 'm', $row['datestamp']) .' / '. date( 'Y', $row['datestamp']) .'</p>
								<p class="desc">'. $row["title2"] .'</p>
								<p class="details">Подробнее</p>
								<div class="clearfix"></div>
							</a>
						</div>
					';
				}
			


		}

		echo '</div>';


	} else {

		if ($video_page){
			foreach ($rows as $row) {
				if ($row['alias'] == $params[0] ) {
					echo '
						<h2>'. $row["title"] .'</h2>
							<div class="radon_all_news_item_inner">
								<div class="col-xs-12 col-sm-6"><iframe src="'. $row['video_link'] .'"></iframe></div>	
								<p>'. $row["title3"] .'</p>
							</div>
						';
				}
			}

		} else {

			foreach ($rows as $row) {
				if ($row['alias'] == $params[0] ) {
					if (!$row["img_src"]) {
						echo '
							<h2>'. $row["title"] .'</h2>
							<div class="radon_all_news_item_inner">
								<p>'. $row["title3"] .'</p>
							</div>
						';
					} else {
						echo '
							<h2>'. $row["title"] .'</h2>
							<div class="radon_all_news_item_inner">
								<div class="col-xs-12 col-sm-3 no_pad_l"><img src="'. $row["img_src"] .'"></div>	
								<p>'. $row["title3"] .'</p>
							</div>
						';
					}
				}				
			}
		}
	}
	

?>