<?php

$table = new Table('catalog_section');

$parent = $table -> select( 'SELECT * FROM `catalog_section` WHERE `alias`=:alias LIMIT 1', array( 'alias' => 'sapropelevye-gryazi' ) );
$parent = end( $parent );

$childs = $table -> select( 'SELECT * FROM `catalog_section` WHERE `parent_id`=:id ', array( 'id' => $parent[ 'id' ] ) );


foreach ( $childs as $child ) {
	if ( $child['alias'] == "kakie-bolezni-lechit" )
	{
		$rows = $table -> select( 'SELECT * FROM `position_mud_news` WHERE `section_id`=:id AND `public`=1 ORDER BY `datestamp` DESC ', array( 'id' => $child['id'] ) );

		foreach ($rows as $row) {
			echo '
				<div class="col-xs-12 sixth_slider_item mud">
					<h3><i class="far fa-clock"></i> '. date( 'd', $row['datestamp']) .' / '. date( 'm', $row['datestamp']) .' / '. date( 'Y', $row['datestamp']) .'</h3>
					<h4><a href="/kakie-bolezni-lechit-gryaz/'. $row['alias'] .'.html">'. $row['title'] .'</a></h4>
					<p>'. $row['title2'] .'</p>
					<a href="/kakie-bolezni-lechit-gryaz/'. $row['alias'] .'.html"><button>Подробнее</button></a>
				</div>
			';
		}
	}
}