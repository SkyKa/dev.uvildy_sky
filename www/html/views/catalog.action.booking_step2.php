<div class="booking-step2-page booking-page">
	<div class="container">
		<h1>Онлайн бронирование номера<a href="" class="back-button"><img src="/static/img/back_button.png" alt=""></a></h1>
		<p class="step">Шаг <span>2</span> из <span>3</span></p>

		<div class="modal fade room-modal" id="room-modal" tabindex="-1" role="dialog" aria-labelledby="room-modalLabel" aria-hidden="true">
			<div class="modal-dialog">
				<div class="modal-content">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
					<div class="modal-body">
						<div id="preloader-ajax">
							<div id="preloader-ajax-center">
								<div id="preloader-ajax-center-absolute">
									<div id="preloader-ajax-center-absolute-one">
										<div class="preloader-ajax-object-one"></div>
										<div class="preloader-ajax-object-one"></div>
										<div class="preloader-ajax-object-one"></div>
										<div class="preloader-ajax-object-one"></div>
										<div class="preloader-ajax-object-one"></div>
										<div class="preloader-ajax-object-one"></div>
									</div>
									<div id="preloader-ajax-center-absolute-two">
										<div class="preloader-ajax-object-two"></div>
										<div class="preloader-ajax-object-two"></div>
										<div class="preloader-ajax-object-two"></div>
										<div class="preloader-ajax-object-two"></div>
										<div class="preloader-ajax-object-two"></div>
										<div class="preloader-ajax-object-two"></div>
									</div>
								</div>
							</div>
						</div>
						<div class="room-content-wrap">
							
						</div>
					</div>
				</div>
			</div>
		</div>

		<div class="row added-humans">
			<?php 

				$table = new Table('catalog_section');

				if( !isset( $_SESSION['booking']['persons'] ) ) {
					$_SESSION['booking']['persons'][ 0 ]['programm_id'] = 15;
			    	$_SESSION['booking']['persons'][ 0 ]['age'] = 1;
			    	$_SESSION['booking']['persons'][ 0 ]['duration'] = 21;
				}

				foreach( $_SESSION['booking']['persons'] as $key => $person ) {
					if( $person['age'] == '1' ) {

						$programm_title = $table -> select('SELECT `id`, `title` FROM `catalog_section` WHERE `id`=:id LIMIT 1',
							array( 'id' => $person['programm_id'] ));

						if( empty( $programm_title ) ) continue;

						echo '<div class="col-md-4 col-sm-6 item" data-person-id="'. $key .'">
								<img class="icon" src="/static/img/adult_icon.png" alt="">
								<h2>Взрослый</h2>
								<p class="character">Программа: <span>'. $programm_title[0]['title'] .'</span></p>
								<p class="character">Продолжительность курса: <span>'. $person['duration'] .' день</span></p>
								<button type="button" class="delete"><span>Удалить</span></button>
							</div>';

					}
					else if( $person['age'] == '2' ) {

						echo '<div class="col-md-4 col-sm-6 item">
								<img class="icon" src="/static/img/child_icon.png" alt="">
								<h2>Детский <span>(6-12 лет)</span></h2>
								<p class="character">Программа: <span>'. $programm_title[0]['title'] .'</span></p>
								<p class="character">Продолжительность курса: <span>'. $person['duration'] .' день</span></p>
								<button type="button" class="delete"><span>Удалить</span></button>
							</div>';

					}
					else if( $person['age'] == '3' ) {

						echo '<div class="col-md-4 col-sm-6 item">
								<img class="icon" src="/static/img/small_child_icon.png" alt="">
								<h2>Детский <span>(3-5 лет)</span></h2>
								<button type="button" class="delete"><span>Удалить</span></button>
							</div>';

					}
				}
			?>
		</div>

		<form action="">
			<div class="add-man basic-button big"><button type="button">добавить человека</button></div>

				<div class="add-person-form-wrap">
					<div class="add-person-form clearfix">
						<div class="old">
							<h2>
								Возраст проживающего
								<p>(полных лет)</p>
							</h2>
							<input type="text" class="form-control">
						</div>
						<div class="select-programm-wrap">
							<h2>Выберите программу</h2>
							<div class="custom-radio">
							<?php
								$programm_alias = 'programmy';

								$section = $table -> select('SELECT * FROM `catalog_section` WHERE `alias`=:alias LIMIT 1', 
									array( 'alias' => $programm_alias ));

								if( empty( $section ) ) return false;
								$section = end( $section );

								$items = $table -> select('SELECT * FROM `catalog_section` WHERE `parent_id`=:id', array( 'id' => $section[ 'id' ] ));

								if( empty( $items ) ) return false;	

								$i = 0;
								foreach( $items as $item ) {

									$section_curse_duration = $table -> select('SELECT `id` FROM `catalog_section` WHERE `parent_id`=:id and 
										`position_table`="position_curse_duration" LIMIT 1', array( 'id' => $item['id'] ) );

									if( empty( $section_curse_duration ) ) continue;

									$checked = '';
									if( !$i ) {
										$checked = 'checked';
									}

									echo '<div class="form-group">
											<input type="radio" id="prog-'. $item[ 'alias' ] .'" name="radio" '. $checked .'>
										    <label for="prog-'. $item[ 'alias' ] .'">
										    	<div class="square"></div>
										    </label>
										    <a href="/'. $programm_alias . '/' . $item[ 'alias' ] . '.html" target="_blank" class="title">'. $item[ 'title' ] .'</a>
										</div>';

									$i++;

								}

							?>
							</div>
						</div>
						<div class="select-during clearfix">
							<h2>Выберите продолжительность курса</h2>
							<?php
								$i = 0;
								foreach( $items as $item ) {

									$programm = $table -> select('SELECT * FROM `section_programms` WHERE `id`=:id', 
										array( 'id' => $item[ 'id' ] ));

									if( empty( $programm ) ) continue;

									$programm = end( $programm );

									$section_curse_duration = $table -> select('SELECT `id` FROM `catalog_section` WHERE `parent_id`=:id and 
										`position_table`="position_curse_duration" LIMIT 1', array( 'id' => $item['id'] ) );

									if( empty( $section_curse_duration ) ) continue;

									$section_curse_duration = end( $section_curse_duration );

									$curse_durations = $table -> select('SELECT * FROM `position_curse_duration` WHERE `section_id`=:id ORDER BY `curse_duration`', array( 'id' => $section_curse_duration['id'] ) );

									if( empty( $curse_durations ) ) continue;

									if( !$i ) {
										echo '<div class="custom-radio" data-programm-id="prog-'. $item[ 'alias' ] .'">';
									}
									else {
										echo '<div class="custom-radio" style="display:none;" data-programm-id="prog-'. $item[ 'alias' ] .'">';
									}

									$j = 0;
									foreach( $curse_durations as $curse_duration ) {

										$checked = '';

										if( !$j ) {
											$checked = 'checked';
										}

										echo '<input type="radio" value="'. $curse_duration['curse_duration'] .'" id="number-'. $curse_duration['curse_duration'] .'-'. $i .'" name="radio-duration-'. $i .'" 
												'. $checked .'><label for="number-'. $curse_duration['curse_duration'] .'-'. $i .'" class="days">'. $curse_duration['curse_duration'] .'</label><br>';
										$j++;

									}

									echo '</div>';

									$i++;
								}

							?>
						</div>
					</div>
					<div class="basic-button big"><button>сохранить</button></div>
					<div class="basic-button big gray close-form"><button>закрыть форму</button></div>
				</div>
		
			<?php mod('catalog.action.rooms') ?>
			
			<h2>ВВЕДИТЕ ДАТУ ПРОЖИВАНИЯ</h2>
			<div class="dates">
				<div class="from-to">
					<label for="from">Дата заезда</label>
					<input type="text" id="from" name="from" class="form-control">
				</div>
				<div class="from-to">
					<label for="to">Дата отъезда</label>
					<input type="text" id="to" name="to" class="form-control">
				</div>
			</div>
		</form>
	</div>
	
	<div class="container indicative-cost-wrap">
		<div class="indicative-cost">
			<div class="basic-button big"><a href="">далее</a></div>
			<p class="title">Ориентировочная стоимость проживания: <span>15 000</span> <span class="rouble">a</span></p>
			<div class="clearfix"></div>
			<p class="description">
				Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quasi numquam eum distinctio aliquid. Accusantium consectetur, possimus odio! Aut nam adipisci incidunt itaque, iusto, magni. Ratione ipsam, voluptatum in delectus nam velit atque praesentium ducimus vel veniam sapiente cumque est itaque reiciendis deleniti non repudiandae soluta quae commodi minus cum officiis. Atque in accusantium soluta porro, pariatur quaerat quo corporis? Delectus, quia, aspernatur.
			</p>
		</div>
	</div>
</div>