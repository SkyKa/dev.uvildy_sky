<?php

if( !isset( $_SESSION ) ) {
    session_start();
}

if( isset($_SESSION[ 'order_booking' ]) ) return false;

$str = '';

if( $alias == 'bronirovanie' ) {

	if( !isset( $_SESSION[ 'booking' ][ 'is_process' ] ) ) {

		$_SESSION[ 'booking' ][ 'is_process' ] = true;

	}

}
else {

	if( isset( $_SESSION[ 'booking' ][ 'is_process' ] ) && $_SESSION[ 'booking' ][ 'is_process' ] ) {

		$str .= '<div class="modal fade cancel-booking-modal" id="cancel-booking-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
						<h2>По какой причине вы передумали бронировать номер?</h2>
					</div>
					<div class="modal-body">'.
						val( 'catalog.action.forms', array( 'alias' => 'otmena_bronirovaniya_mobilnye' ) ) 
					.'</div>
				</div>
			</div>
		</div>';

		echo $str;

		$_SESSION[ 'booking' ][ 'is_process' ] = false;

	}

}