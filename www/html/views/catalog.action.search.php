<?php 
	$request = Utils :: getVar( 'q' );

	$request = trim($request); 
    $request = htmlspecialchars($request);
    $request = strip_tags($request);

    if( mb_strlen( $request, 'utf-8' ) < 4 ) {
		echo '<h4>Задан слишком общий критерий. Введите не менее 4 символов!</h4>';
		return false;
	}

	$table = new Table('catalog_section');

	$rel_pages = $table->select('SELECT * FROM `pages` WHERE MATCH (`title`, `content`, `announcement`) AGAINST (:request)', 
		array( 'request' => $request ) );
