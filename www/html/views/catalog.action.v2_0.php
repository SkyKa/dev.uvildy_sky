<?php

	$sole = 'GQL#f3yyR&F9T&NK0N4MTMT$J(^)@)Y0#@$G%F@#%0&*)@)@&*==&*@&&#JH0JU(H#';
	$method = 'undefined';
	$code = 200;

	$method = $_SERVER[ 'REQUEST_METHOD' ];
	$format = ( ( isset( $_GET[ 'format' ] ) ) ? strtoupper( $_GET[ 'format' ] ) : 'JSON' );
	
	
	$rt =  $_REQUEST;
	unset( $rt[ 'uri' ] );
	unset( $rt[ 'mod' ] );
	unset( $rt[ 'alias' ] );

	$input = file_get_contents( 'php://input' );
	
	$cmspath = realpath( dirname( __FILE__ ) . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . '..' ) . DIRECTORY_SEPARATOR;
	$fo = fopen( $cmspath . 'verbose.log', 'a+' );
	fwrite( $fo, date( 'm-d-Y H:i:s' ) . " START REQUEST\n", 3000 );
	fwrite( $fo, '---------------------------------' . "\n\n", 3000 );
	fwrite( $fo, 'REQUEST < ' . var_export( $rt, true ) . "\n", 100000 );
	fwrite( $fo, '---------------------------------' . "\n\n", 3000 );
	fwrite( $fo, 'BODY < ' . "\n" . var_export( $input, true ) . "\n\n", 100000 );
	fwrite( $fo, '---------------------------------' . "\n\n", 3000 );


	
	
	$conf = array(
		'params' => $params,
		'crypt' => false,
		'format' => $format,
		'route' => array(
		/*
			'#^\/catalog/parts\/#i' => '/pt/position_cats/',
			'#^\/catalog/brands\/#i' => '/katalog/',
			'#^\/catalog/([^/]{1,})\/#i' => '/katalog/$1/_items/',
			'#^\/orders\/#i' => '/zakazy/_extended/',
		*/
			// скидки
            '#^\/discounts\/#i' => '/akcii-i-novosti/discounts/_items/',
			// новости
            '#^\/news\/#i' => '/akcii-i-novosti/news/_items/',
			// статьи
            '#^\/publications\/#i' => '/akcii-i-novosti/publications/_items/',
			
			// лицензии
            '#^\/license\/#i' => '/documents/licenzii/_items/',
			// биоклиматический паспорт
            '#^\/biopassport\/#i' => '/documents/bioklimaticheskij-pasport/_items/',
			// договор с физическими лицами
            '#^\/contract_for_individuals\/#i' => '/documents/dogovor-s-fizicheskimi-licami/_items/',
			// договор с юридическими лицами
            '#^\/contract_for_companies\/#i' => '/documents/dogovor-s-yuridicheskimi-licami/_items/',
			
			// подписка на email рассылку
            '#^\/subscribe\/#i' => '/sub5cribe_module/subscribe_client_base/subscribe-clients/_items/',
			
			// запросы на бронь
			'#^\/booking\/#i' => '/bronir0vanie-nomerov/bronirovanie/_items/',
			
			// инфраструктура - карта
			'#^\/map\/#i' => '/vsya-infrastruktura/_items/',
			// инфраструктура - где покушать
			'#^\/food\/#i' => '/vsya-infrastruktura/restaurants/_items/',
			// инфраструктура - развлечение
			'#^\/games\/#i' => '/vsya-infrastruktura/amusement/_items/',			
			// инфраструктура - спорт
			'#^\/sport\/#i' => '/vsya-infrastruktura/sport/_items/',
			// инфраструктура - SPA-Wellness
			'#^\/spa\/#i' => '/vsya-infrastruktura/spa-wellness/_items/',

			// лечебная сапропеливая грязь - слайдер
			'#^\/dirt\/slider\/#i' => '/sapropelevye-gryazi/glavnyj-slajder2/_items/',
			// лечебная сапропеливая грязь - научные статьи
			'#^\/dirt\/science\/#i' => '/sapropelevye-gryazi/nauchnye-stati-o-gryazi/_items/',
			// лечебная сапропеливая грязь - новости
			'#^\/dirt\/news\/#i' => '/sapropelevye-gryazi/novosti-o-lechenii/_items/',
			// лечебная сапропеливая грязь - медицина
			'#^\/dirt\/medical\/#i' => '/sapropelevye-gryazi/gryazelechenie-v-medecine/_items/',
			// лечебная сапропеливая грязь - методики лечения
			'#^\/dirt\/methods\/#i' => '/sapropelevye-gryazi/metodika-gryazelecheniya/_items/',
			// лечебная сапропеливая грязь - какие болезни лечит
			'#^\/dirt\/heal\/#i' => '/sapropelevye-gryazi/kakie-bolezni-lechit/_items/',
			// лечебная сапропеливая грязь - бальнеологические курорты
			'#^\/dirt\/resorts\/#i' => '/sapropelevye-gryazi/balneologicheskie-kurorty/_items/',
			// лечебная сапропеливая грязь - вопрос / ответ
			'#^\/dirt\/help\/#i' => '/sapropelevye-gryazi/vopros-otvet2/_items/',
			// лечебная сапропеливая грязь - показания
			'#^\/dirt\/indications\/#i' => '/sapropelevye-gryazi/pokazaniya/_items/',	
			// лечебная сапропеливая грязь - противопоказания
			'#^\/dirt\/contraindication\/#i' => '/sapropelevye-gryazi/protivopokazaniya/_items/',

			// лечебная радоновая вода - слайдер
			'#^\/water\/slider\/#i' => '/radonovye-vody/glavnyj-slajder/_items/',
			// лечебная радоновая вода - научные статьи
			'#^\/water\/science\/#i' => '/radonovye-vody/nauchnye_stati/_items/',
			// лечебная радоновая вода - новости
			'#^\/water\/news\/#i' => '/radonovye-vody/radonovoe-lechenie/_items/',
			// лечебная радоновая вода - медицина
			'#^\/water\/medical\/#i' => '/radonovye-vody/radon-v-medecine/_items/',
			// лечебная радоновая вода - методики лечения
			'#^\/water\/methods\/#i' => '/radonovye-vody/metodika-lecheniya-radonom/_items/',
			// лечебная радоновая вода - статьи по лечению болезней
			'#^\/water\/healnews\/#i' => '/radonovye-vody/kakie-bolezni-lechat-radonovye-vanny/_items/',
			// лечебная радоновая вода - какие болезни лечит
			'#^\/water\/heal\/#i' => '/radonovye-vody/kakie-bolezni-lechat-napravleniya/_items/',
			// лечебная радоновая вода - мировые источники радоновой воды
			'#^\/water\/resorts\/#i' => '/radonovye-vody/mirovye-istochniki-radonovoy-vody/_items/',
			// лечебная радоновая вода - вопрос / ответ
			'#^\/water\/help\/#i' => '/radonovye-vody/vopros-otvet/_items/',

			// ФОТОГАЛЕРЕЯ
			

			// ВИДИОГАЛЕРЕЯ
			

			// ПРОГРАММЫ
			

			// ЛЕЧЕБНО-ДИАГНОСТИЧЕСКИЕ УСЛУГИ
			

		),
		'auth' => array(
			'method' => 'Basic Auth',
			'users' => array( 'api' => 'LRI1Hgf003fj_o_3FDdd739' ),
			'realm' => 'Uvildy API Service'
		),
		'debug' => true,
		'method' => $method
	);


	
	$path_rsa_public = '';
	$pk = '';
	
	$crypt = ( isset( $conf[ 'crypt' ] ) && $conf[ 'crypt' ] ) ? true : false;
	
	

	if ( $crypt ) {
		// получаем открытый ключ
		$path_rsa_public = realpath( dirname( __FILE__ ) . '/../..' ) . DS . 'cms' . DS . 'rsa' . DS . 'pck';
		$pk = file_get_contents( $path_rsa_public );
		$pk = openssl_get_publickey( $pk );
	}


	try {

		$api = new Api_Base_Server( $conf );

	}
	catch ( Exception $e ) {
		//ob_start( 'ob_gzhandler' );
		header( 'HTTP/1.1 400 Bad Request' );
		Api_Base_Server :: __header( $format );
		$r[ 's' ] = false;
		$r[ 'code' ] = 400;
		$r[ 'errors' ][ 0 ] = 'invalid request ' . __LINE__;
		$r[ 'message' ] = $e -> getMessage( );
		$r[ 'location' ] = $e -> getLine( );

		///////////////////////////////////////
		fwrite( $fo, 'RESPONSE > ' . "\n" . Api_Base_Server :: __encode( $r, $format ) . "\n\n", 100000 );
		fwrite( $fo, "\n\n\n\n", 8500 );
		fclose( $fo );
		///////////////////////////////////////

		echo Api_Base_Server :: __encode( $r, $format );
		//ob_end_flush( );
		exit( 1 );
	}
	

	$section = $api -> _section;
	$type = $api -> _type;

	$body = file_get_contents( 'php://input' );
	$the_request = $_GET;




	try {


		
		switch ( $method ) {

			// GET — получить
			case 'GET':

				$c = Config :: getConfig( 'catalog' );

				if ( $type == 'section' ) {
					$r = $api -> get_sections( $body, $the_request );
				}
				if ( $type == 'section_extended' ) {
					$r = $api -> get_sections( $body, $the_request, true );
				}
				else if ( $type == 'position' ) {
					$r = $api -> get_positions( $body, $the_request );
				}
				else if ( $type == 'position_table' ) {
					$r = $api -> get_position_table( $body, $the_request );
				}
				else if ( $type == 'section_table' ) {
					$r = $api -> get_section_table( $body, $the_request );
				}
				//////////////////////////////////////////////////////////////////////////
				// ДОШИФРОВКА ИНФОРМАЦИИ ПЕРЕД ВЫВОДОМ
				$encrypt_filter = $api -> make_encrypt_filter( );
				$r[ 'filter' ] = $encrypt_filter;
				if ( !isset( $r[ 'data' ] ) ) $r[ 'data' ] = array( );
				foreach ( $r[ 'data' ] as $data_key => $row ) {
					foreach( $row as $col => $val ) {

						if ( $col != 'positions' ) {

							$val = trim( $val );
							if ( isset( $encrypt_filter[ $col ] ) ) continue;
							if ( !$val ) {
								unset( $r[ 'data' ][ $data_key ][ $col ] );
								continue;
							}
							/////////////////////////////////////////////////////////////
							// шифруем открытым ключем незашифрованные поля
							if ( $crypt ) {
								$encrypted = $api -> ssl_encrypt( $val, 'public', $pk );
								$encrypted = base64_encode( $encrypted );
								$r[ 'data' ][ $data_key ][ $col ] = $encrypted;
							}
						}
						// разширенные сессии
						else {

							foreach( $val as $position_key => $position ) {
								foreach( $position as $position_col => $position_val ) {

									$position_val = trim( $position_val );
									if ( isset( $encrypt_filter[ $position_col ] ) ) continue;
									if ( !$position_val ) {
										unset( $r[ 'data' ][ $data_key ][ 'positions' ][ $position_col ] );
										continue;
									}
									/////////////////////////////////////////////////////////////
									// шифруем открытым ключем незашифрованные поля
									if ( $crypt ) {
										$encrypted = $api -> ssl_encrypt( $position_val, 'public', $pk );
										$encrypted = base64_encode( $encrypted );
										$r[ 'data' ][ $data_key ][ 'positions' ][ $position_col ] = $encrypted;
									}
								}
							}

						}

					}
				}
				break;

			
			// POST — создать
			case 'POST':

				if ( $type == 'section' ) {
					$r = $api -> post_sections( $body, $the_request );
				}
				else if ( $type == 'position' ) {
					$r = $api -> post_positions( $body, $the_request );
				}
				else if ( $type == 'position_table' ) {
					$r = $api -> post_position_table( $body, $the_request );
				}
				/*
				else if ( $type == 'section_table' ) {
					$r = $api -> post_section_table( $body, $the_request );
				}
				*/
				/////////////////////////////////////////////////////////
				// ДОШИФРОВЫВАЕМ ИНФОРМАЦИЮ
				if ( !isset( $r[ 'data' ] ) ) $r[ 'data' ] = array( );
				foreach ( $r[ 'data' ] as $data_key => $row ) {
					foreach( $row as $col => $val ) {
						$val = trim( $val );
						if ( isset( $encrypt_filter[ $col ] ) ) continue;
						if ( !$val ) {
							unset( $r[ 'data' ][ $data_key ][ $col ] );
							continue;
						}
						/////////////////////////////////////////////////////////////
						// шифруем открытым ключем незашифрованные поля
						if ( $crypt ) {
							$encrypted = $api -> ssl_encrypt( $val, 'public', $pk );
							$encrypted = base64_encode( $encrypted );
							$r[ 'data' ][ $data_key ][ $col ] = $encrypted;
						}
						
					}
				}
				break;


			// PUT — изменить
			case 'PUT':

				if ( $type == 'section' ) {
					$r = $api -> put_sections( $body, $the_request );
				}
				else if ( $type == 'position' ) {
					$r = $api -> put_positions( $body, $the_request );

				}
				else if ( $type == 'position_table' ) {
					$r = $api -> put_position_table( $body, $the_request );
				}
				else if ( $type == 'section_table' ) {
					$r = $api -> put_section_table( $body, $the_request );
				}
				/////////////////////////////////////////////////////////
				// ДОШИФРОВЫВАЕМ ИНФОРМАЦИЮ
				if ( !isset( $r[ 'data' ] ) ) $r[ 'data' ] = array( );
				foreach ( $r[ 'data' ] as $data_key => $row ) {
				     foreach( $row as $col => $val ) {
				      $val = trim( $val );
				      if ( isset( $encrypt_filter[ $col ] ) ) continue;
				      if ( !$val ) {
				       unset( $r[ 'data' ][ $data_key ][ $col ] );
				       continue;
				      }
				      /////////////////////////////////////////////////////////////
				      // шифруем открытым ключем незашифрованные поля
					  if ( $crypt ) {
						$encrypted = $api -> ssl_encrypt( $val, 'public', $pk );
						$encrypted = base64_encode( $encrypted );
						$r[ 'data' ][ $data_key ][ $col ] = $encrypted;
					  }
				     }
				    }
				    break;


			// DELETE — удалить
			case 'DELETE':

				if ( $type == 'section' ) {
					$r = $api -> delete_sections( $body, $the_request );
				}
				else if ( $type == 'position' ) {
					$r = $api -> delete_positions( $body, $the_request );
				}
				/////////////////////////////////////////////////////////
				// ДОШИФРОВЫВАЕМ ИНФОРМАЦИЮ
                
                
				break;

				default:
		}

		if ( isset( $r[ 's' ] ) && $r[ 's' ] ) {
			$r[ 'code' ] = 200;
			$r[ 'message' ] = 'Успешый запрос';
		}
		///////////////////////////////////////
		fwrite( $fo, 'RESPONSE > ' . "\n" . Api_Base_Server :: __encode( $r, $format ) . "\n\n", 100000 );
		fwrite( $fo, "\n\n\n\n", 8500 );
		fclose( $fo );
		///////////////////////////////////////
		$api -> strout( $r );


	}
	catch ( Exception $e ) {
		header( 'HTTP/1.1 400 Bad Request' );
		Api_Base_Server :: __header( $format );
		$r[ 's' ] = false;
		$r[ 'code' ] = 400;
		$r[ 'errors' ][ 0 ] = 'invalid request ' . __LINE__;
		$r[ 'message' ] = $e -> getMessage( );
		echo Api_Base_Server :: __encode( $r, $format );
		//ob_end_flush( );
		///////////////////////////////////////
		fwrite( $fo, 'RESPONSE > ' . "\n" . Api_Base_Server :: __encode( $r, $format ) . "\n\n", 100000 );
		fwrite( $fo, "\n\n\n\n", 8500 );
		fclose( $fo );
		///////////////////////////////////////
		exit( 1 );
	}
	
	

