<?php 

//$my_alias = 'fotogalerei';
$my_alias = $alias;

$table = new Table('catalog_section');

$str = '';

$section_id = $table -> select('SELECT `id` FROM `catalog_section` WHERE `alias`=:alias LIMIT 1', array( 'alias' => $my_alias ));

if( empty( $section_id ) ) return false;
$section_id = end($section_id);

$galleries = $table -> select('SELECT `id`, `title` FROM `catalog_section` WHERE `parent_id`=:id ORDER BY `position`', array( 'id' => $section_id['id'] ));

if( empty( $galleries ) ) return false;

$i = 1;

foreach( $galleries as $gallery ) {

	$str .= '<h2>'. $gallery['title'] .'</h2>';

	$images = $table -> select('SELECT * FROM `position_photo_gallery` WHERE `section_id`=:id', array( 'id' => $gallery['id'] ) );
	
	// var_dump( $images );
	
	if( empty($images) ) continue;

	$str .= '<div class="lightgalleries lightgalleries-'. $i .'">';

	foreach( $images as $image ) {
		$str .= '<a href="/'. $image['img'] .'" data-sub-html="'. $image['description'] .'">
					    <img class="img-responsive" src="/'. get_cache_pic( $image['img'], 150, 150, false ) .'" />
					    <div class="gallery-poster">
                            <img src="/static/img/zoom.png">
                        </div>
					</a>';
	}

	$str .= '</div>';
}

echo $str;
