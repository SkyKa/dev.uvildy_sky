<?php

$table = new Table('catalog_section');

$parent = $table -> select( 'SELECT * FROM `catalog_section` WHERE `alias`=:alias LIMIT 1', array( 'alias' => 'radonovye-vody' ) );
$parent = end( $parent );

$childs = $table -> select( 'SELECT * FROM `catalog_section` WHERE `parent_id`=:id ', array( 'id' => $parent[ 'id' ] ) );

$news_separator = 0;

foreach ( $childs as $child ) {
	if ( $child['alias'] == "nauchnye_stati" )
	{
		$rows = $table -> select( 'SELECT * FROM `position_radon_news` WHERE `section_id`=:id AND `public`=1 ORDER BY `datestamp` DESC LIMIT 8 ', array( 'id' => $child['id'] ) );

		foreach ($rows as $row) {

			if ( $news_separator < 2 ) {
				$news_separator++;
				continue;
			}
			echo '
				<div class="landing_second_under_slider_item col-xs-12 col-sm-3">
					<div class="landing_second_under_slider_item_date">
						<i class="far fa-clock"></i> '. date( 'd', $row['datestamp']) .' / '. date( 'm', $row['datestamp']) .' / '. date( 'Y', $row['datestamp']) .'
					</div>
					<div class="landing_second_under_slider_item_wrap">
						<a href="/nauchnye-stati-o-radone/'. $row['alias'] .'.html">'. $row['title'] .'</a>
					</div>
				</div>
			';			
		}
	}
}