<?php 

if( !empty( $params[0] ) ) {
	mod('catalog.action.mobile_news_item');
	return;
}

echo 	'<header>
			<div class="et-menu-collapse-btn et-collapsed">
				<div class="et-collapse-line"></div>
				<div class="et-collapse-line"></div>
				<div class="et-collapse-line"></div>
			</div>
			
			<div class="h1-wrap">
				<h1>'. val('pages.show.title') .'</h1>
			</div>

			<a href="/mobile_static/programms.html" class="back-button">
				<img src="/mobile_static/img/back_button.png" alt="">
			</a>

		</header>
		
		<div class="content-wrapper">
			<div class="wrapper">
				<div class="content-inner">';

$table = new Table('catalog_section');

$section = $table -> select('SELECT `id` FROM `catalog_section` WHERE `alias`=:alias LIMIT 1', array( 'alias' => $alias ) );
if( !count( $section ) ) return false;


$news_list = $table -> select('SELECT * FROM `position_news` WHERE `section_id`=:id AND `public` ORDER BY `datestamp` DESC', array( 'id' => $section[0]['id'] ) );
if( !count( $news_list ) ) return false;

$str = '<div class="et-container news-page">';

foreach ($news_list as $news_item) {
	$str .= '<div class="item-wrap">';

	if( empty( $news_item['content'] ) ) {
		$str .= '<div class="item">';
	}
	else {
		$str .= '<a href="/'. $alias .'/' . $news_item['alias'] .'.html" class="item">';
	}
			
	$style = '';
	if( !empty($news_item['img']) ) {
		$str .= '<img src="'. get_cache_pic($news_item['img'], 430, 286, true) .'" class="image">';
		
	}
	else {
		$style = 'style="margin-top: 35px;"';
	}

	if( !empty($news_item['is_share']) ) {
	 		$str .= '<img src="/static/img/share.png" class="share-img">';
	}

	$str .=			'<h2 '. $style .'>'. $news_item['title'] .'</h2>';

	$month = date( 'm', $news_item['datestamp']);
	$month = Langvars::replaceMonth($month);

	if( empty($news_item['is_share']) ) {
		$str .=     '<p class="date">'. date( 'd ', $news_item['datestamp']) . $month . date( ' Y', $news_item['datestamp']) .'</p>';
	}
	else {
		$str .=     '<p class="date"></p>';
	}

	$str .=			'<p class="desc">'. $news_item['description'] .'</p>';

	if( !empty( $news_item['content'] ) ) {
		$str .=    '<p class="details">Подробнее</p>';
	}

	if( empty( $news_item['content'] ) ) {
		$str .= '</div>';
	}
	else {
		$str .= '</a>';
	}
				
	$str .=		'</div>';
}

$str .=    '</div>';

echo $str;