<?php

echo replace_short_codes(val('pages.show.content'));

function replace_short_codes( $content ) {

	$matches = Utils::search_short_codes( $content );
	if( !$matches ) return $content;

	foreach( $matches as $match ) {

		$end_entity = strpos( $match, ' ' );
		$entity = substr( $match, 0, $end_entity );

		$entity_alias_start = strpos( $match, 'alias=&quot;' );

		$entity_alias = substr( $match, $entity_alias_start + 12 );

		$entity_alias = rtrim( $entity_alias, '&quot;' );

		switch( $entity ) {
			case 'gallery':
				$gallery =  build_gallery( $entity_alias );

				if( empty( $gallery ) ) continue;

				return str_replace( '__' . $match . '__', $gallery, $content );

				break;
		}

	}

	return $content;
}

function build_gallery( $entity_alias ) {

	$table = new Table('catalog_section');

	$section = $table -> select('SELECT `id` FROM `catalog_section` WHERE `alias`=:alias LIMIT 1', array( 'alias' => $entity_alias ) );
	if( empty($section) ) return false;

	$images = $table -> select('SELECT * FROM `position_photo_gallery` WHERE `section_id`=:id', array( 'id' => $section[0]['id'] ) );
	if( empty($images) ) return false;

	$gallery = '<div id="lightgallery-in-content">';

	foreach( $images as $image ) {
		$gallery .= '<a href="/'. $image['img'] .'" data-sub-html="'. $image['description'] .'">
					    <img class="img-responsive" src="/'. get_cache_pic( $image['img'], 150, 150, false ) .'" />
					    <div class="gallery-poster">
                            <img src="/static/img/zoom.png">
                        </div>
					</a>';
	}

	$gallery .= '</div>';

	return $gallery;

}