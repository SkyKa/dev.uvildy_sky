<?php

if( !isset( $_SESSION ) ) {
    session_start();
}

$table = new Table( 'position_window_offer' );

$offer = $table -> select(
		'SELECT * FROM `position_window_offer` WHERE datestamp<=:timestamp && enddatestamp>:timestamp && `public`=1 LIMIT 1',
		array( 'timestamp' => time() )
);


if ( !count( $offer ) ) return false;
$offer = end( $offer );
if ( isset( $_SESSION[ 'window_offer' ] ) && $_SESSION[ 'window_offer' ] == $offer[ 'id' ] ) return false;

$str = '';

$content = $offer[ 'content' ];
$content = str_replace( '{PHONE}', val( 'catalog.action.yandex_direct_phone', array( 'num' => 1 ) ), $content );


$str .= '<div class="modal fade window-offer-modal" id="window-offer-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
				<h2>' . $offer[ 'title' ] . '</h2>
			</div>
			<div class="modal-body">' . $content . '</div>
		</div>
	</div>
</div>';

echo $str;

$_SESSION[ 'window_offer' ] = $offer[ 'id' ];
