<?php

$my_alias = 'vsya-infrastruktura';

$table = new Table('catalog_section');
$section = $table -> select('SELECT `id` FROM `catalog_section` WHERE `alias`=:alias LIMIT 1', array( 'alias' => $my_alias ) );

if( !count( $section ) ) return false;

$rows = $table -> select('SELECT * FROM `position_map_objects` WHERE `section_id`=:id AND `public` ORDER BY `position` DESC', array( 'id' => $section[0]['id'] ) );

if( !count( $rows ) ) return false;

$str = '<ul>';

foreach( $rows as $row ) {

	$str .= '<li>
				<a class="'. $row[ 'alias' ] .'" href="" style="background-image: url(/'. $row[ 'icon_menu' ] .');" 
					data-small-icon="/'. get_cache_pic( $row[ 'icon_map' ], 45, 61, false ) .'" data-x="'. $row[ 'map_x' ] .'" data-y="'. $row[ 'map_y' ] .'"  data-popup-text="'. htmlspecialchars($row[ 'popup_text' ]) .'">
						'. $row[ 'title' ] .'
				</a>';
	$str .=	'</li>';

}

$str .= '</ul>';

echo $str;