<?php $title = $args -> title; ?>
<title><?php  echo $title; ?></title>
<?php 

$op = '';
$table = new Table ('position_news');

// "Акции и новости"
if ( $alias == 'akcii-i-novosti' && !empty( $params[0] ) && empty( $params[1] ) ) {
	
	$article = $table -> select ('SELECT * FROM `position_news` WHERE `alias`=:alias LIMIT 1', array('alias' => $params[0]));
	if ( !count( $article ) ) return false;
	$article = end ( $article );
	
	$article[ 'title' ] = preg_replace('#"(.*?)"#', '«$1»', $article[ 'title' ]);
	$article[ 'description' ] = preg_replace('#"(.*?)"#', '«$1»', $article[ 'description' ]);
	
	$op = '	<meta property="og:type" content="article" />
			<meta property="og:title" content="' . $article[ 'title' ] . '" />
			<meta property="og:description" name="description" content="' . $article[ 'description' ] . '" />
			<meta property="og:url" content="http://www.uvildy.ru/akcii-i-novosti/' . $article[ 'alias' ] . '.html" />
			<meta property="og:image" content="http://www.uvildy.ru/' . $article[ 'img' ] . '" />';

}
// "Программы"
if ( $alias == 'programmy' && !empty( $params[0] ) && empty( $params[1] ) ) {

	$section = $table -> select ('SELECT `id`, `title`, `alias` FROM `catalog_section` WHERE `alias`=:alias LIMIT 1', array('alias' => $params[0]) );
	if ( !count( $section ) ) return false;
	$section = end ( $section );

	$programm = $table -> select ('SELECT * FROM `section_programms` WHERE `id`=:id', array('id' => $section['id']));
	if ( !count( $programm ) ) return false;
	$programm = end ( $programm );

	$section[ 'title' ] = preg_replace('#"(.*?)"#', '«$1»', $section[ 'title' ]);
	$programm[ 'description' ] = preg_replace('#"(.*?)"#', '«$1»', $programm[ 'description' ]);

	$op = '	<meta property="og:type" content="article" />
			<meta property="og:title" content="' . $section[ 'title' ] . '" />
			<meta property="og:description" name="description" content="' . $programm[ 'description' ] . '" />
			<meta property="og:url" content="http://www.uvildy.ru' . $_SERVER['REQUEST_URI'] . '" />
			<meta property="og:image" content="http://www.uvildy.ru/' . $programm[ 'img' ] . '" />';

}
// "Процедуры"
if ( $alias == 'procedury' && !empty( $params[1] )  ) {

	$section = $table -> select ('SELECT `id`, `title`, `alias` FROM `catalog_section` WHERE `alias`=:alias LIMIT 1', array('alias' => $params[1]) );
	if ( !count( $section ) ) return false;
	$section = end ( $section );

	$procedury = $table -> select ('SELECT * FROM `section_separate_procedures` WHERE `id`=:id', array('id' => $section['id']));
	if ( !count( $procedury ) ) return false;
	$procedury = end ( $procedury );

	$text = strip_tags($procedury[ 'description' ]);

	$section[ 'title' ] = preg_replace('#"(.*?)"#', '«$1»', $section[ 'title' ]);
	$text = preg_replace('#"(.*?)"#', '«$1»', $text);

	$op = '	<meta property="og:type" content="article" />
			<meta property="og:title" content="' . $section[ 'title' ] . '" />
			<meta property="og:description" name="description" content="' . $text . '" />
			<meta property="og:url" content="http://www.uvildy.ru' . $_SERVER['REQUEST_URI'] . '" />
			<meta property="og:image" content="http://www.uvildy.ru/' . $procedury[ 'img' ] . '" />';

}
// нет микроразметки
else {

}

echo $op;


