		
<a href="/">Главная</a>&nbsp;&nbsp;|&nbsp;&nbsp;
<?php

    $last_link = $args -> last_link;

    $table = new Table( 'pages' );
    $id = $table->select( 'SELECT * FROM `pages` WHERE alias=:alias LIMIT 1', array( 'alias' => $alias ) );
    if( count( $id ) > 0 )
    {
      if( $id[ 0 ][ 'parent_id' ] != 0 ) {
          echo show_Breadcrumbs( $id[ 0 ][ 'parent_id' ], '' );
      }
    }
  
	  
function show_Breadcrumbs( $id, $str )
{
     $table = new Table( 'pages' );
     $rows = $table->select( 'SELECT * FROM `pages` WHERE id=:id LIMIT 1', array( 'id' => $id ) );
     if( count( $rows ) > 0 )
     {
          $str = "<a href='/" . $rows[ 0 ][ 'alias' ] . ".html'>" . $rows[ 0 ][ 'title' ] . "</a>&nbsp;&nbsp;|&nbsp;&nbsp;" . $str;
          if( $rows[ 0 ][ 'parent_id' ] != 0 )
          {
              $str = show_Breadcrumbs( $rows[ 0 ][ 'parent_id' ], $str );
          }
     }
   return $str;
}
if ( !$last_link ) {
  echo "<a href='/" . $alias . ".html'>" . val( 'pages.show.title' ) . "</a>";
}
else {
  echo "<a href='/" . $alias . ".html'>" . val( 'pages.show.title' ) . "</a>&nbsp;&nbsp;|&nbsp;&nbsp;";
}

?>
