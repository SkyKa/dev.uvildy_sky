<?php 

	$myalias = 'subscribe-clients';

	$table = new Table( 'catalog_section' );

	$parent = $table -> select( 'SELECT * FROM `catalog_section` WHERE `alias`=:myalias LIMIT 1', array( 'myalias' => $myalias ) );
	if ( !count( $parent ) ) return false;
	$parent = end( $parent );
	
	$section = $table -> select( 'SELECT * FROM `section_page` WHERE `id`=:sid LIMIT 1', array( 'sid' => $parent[ 'id' ] ) );
	if ( !count( $section ) ) return false;
	$section = end( $section );	
	if ( !$section[ 'visible' ] ) return false;

	$str = '
<div class="new-sale-wrapper">
					<div class="container ">
						<div class="row">				
							<div class="container">
								<div class="row">
									<div class="col-md-8">
										<p class="title-sale">
											хочу подписаться на новые акции и скидки
											от курорта «Увильды»
										</p>
										<p class="description-sale">
											Если желаете быть в курсе всех новостей, укажите ваш e-mail.
										</p>
									</div>
									<div class="col-md-4">';

	$str .= val( 'catalog.action.forms', array( 'alias' => 'subscribe_form' ) );

	$str .= '
									</div>
								</div>
							</div>
						</div>
					</div>	
				</div>';

	echo $str;

