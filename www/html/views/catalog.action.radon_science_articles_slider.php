<?php


$table = new Table('catalog_section');

$parent = $table -> select( 'SELECT * FROM `catalog_section` WHERE `alias`=:alias LIMIT 1', array( 'alias' => 'radonovye-vody' ) );
$parent = end( $parent );

$childs = $table -> select( 'SELECT * FROM `catalog_section` WHERE `parent_id`=:id ', array( 'id' => $parent[ 'id' ] ) );


foreach ( $childs as $child ) {
	if ( $child['alias'] == "nauchnye_stati" )
	{
		$rows = $table -> select( 'SELECT * FROM `position_radon_news` WHERE `section_id`=:id AND `public`=1 ORDER BY `datestamp` DESC ', array( 'id' => $child['id'] ) );

		foreach ($rows as $row) {
			echo '
			<div>
				<div class="col-xs-12 science_slider_item">
					<div class="science_slider_item_header">
						<a href="/nauchnye-stati-o-radone/'. $row['alias'] .'.html"><img src="'. $row['img_src'] .'"></a>
						<div class="science_slider_item_header_date">
							<p><i class="far fa-clock"></i></p>
							<p>'. date( 'd', $row['datestamp']) .' / '. date( 'm', $row['datestamp']) .'</p>
							<p>'. date( 'Y', $row['datestamp']) .'</p>
						</div>
					</div>
					<div class="science_slider_item_wrap">
						<h3><a href="/nauchnye-stati-o-radone/'. $row['alias'] .'.html">'. $row['title'] .'</a></h3>
						<p>'. $row['title2'] .'</p>
						<a href="/nauchnye-stati-o-radone/'. $row['alias'] .'.html"><button>Подробнее</button></a>
					</div>
				</div>
			</div>
		';
		}
	}
}