<?php

$table = new Table('catalog_section');
$section = $table -> select('SELECT `id` FROM `catalog_section` WHERE `alias`=:alias LIMIT 1', array( 'alias' => $alias ) );

if( !count( $section ) ) return false;


$history = $table -> select('SELECT * FROM `position_history` WHERE `section_id`=:id ORDER BY `year` ASC', array( 'id' => $section[0]['id'] ) );

if( !count( $history ) ) return false;

$str = '';

foreach( $history as $item ) {

	$str .= '<div class="item clearfix">
				<div class="col-sm-6 img-wrap">
					<a href="/'. get_cache_pic( $item['img'], 800, 600, true ) .'" class="fancybox" rel="item">
						<img src="/'. get_cache_pic( $item['img'], 583, 391, true ) .'" alt="">
					</a>
				</div>
				<div class="col-sm-6 info-wrap">
					<div class="info">
						<div class="year-wrap">
							<p>'. $item['year'] .'</p>
						</div>
						<h2>'. $item['title'] .'</h2>
						<div class="content">'. $item['description'] .'</div>
					</div>
				</div>
			</div>';

}

echo $str;