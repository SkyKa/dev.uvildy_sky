<?php

$table = new Table('catalog_section');

$parent = $table -> select( 'SELECT * FROM `catalog_section` WHERE `alias`=:alias LIMIT 1', array( 'alias' => 'radonovye-vody' ) );
$parent = end( $parent );

$childs = $table -> select( 'SELECT * FROM `catalog_section` WHERE `parent_id`=:id ', array( 'id' => $parent[ 'id' ] ) );


foreach ( $childs as $child ) {
	if ( $child['alias'] == "metodika-lecheniya-radonom" )
	{
		$rows = $table -> select( 'SELECT * FROM `position_radon_news` WHERE `section_id`=:id AND `public`=1 ORDER BY `datestamp` ', array( 'id' => $child['id'] ) );

		foreach ($rows as $row) {
			echo '
				<div class="col-xs-12 radon_fifth_slider_item">
					<div class="radon_fifth_slider_item_header">
						<p><i class="far fa-clock"></i> '. date( 'd', $row['datestamp']) .' / '. date( 'm', $row['datestamp']) .' / '. date( 'Y', $row['datestamp']) .'</p>
						<iframe src="'. $row['video_link'] .'"></iframe>
					</div>
					<div class="radon_fifth_slider_item_wrap">
						<h3><a href="/metodika-lecheniya-radonom/'. $row['alias'] .'.html">'. $row['title'] .'</a></h3>
						<p>'. $row['title2'] .'</p>
					</div>							
				</div>
			';
		}
	}
}