<?php 

$my_alias = 'programmy';

$table = new Table('catalog_section');
$section = $table -> select('SELECT `id` FROM `catalog_section` WHERE `alias`=:alias LIMIT 1', array( 'alias' => $my_alias ));

if( !count( $section ) ) return false;

$section = end( $section );

$rows = $table -> select('SELECT * FROM `catalog_section` WHERE `parent_id`=:id ORDER BY `position`' , array( 'id' => $section['id'] )); 

if( !count($rows) ) return false;

$str = '<section class="programms">
			<div class="container">
				<div class="row">';


foreach( $rows as $row ) {

	$item = $table -> select('SELECT * FROM `section_programms` WHERE `id`=:id AND `public` LIMIT 1', array( 'id' => $row[ 'id' ] ));

	if( !count( $item ) ) continue;

	$item = end( $item );

	$str .= '<div class="col-md-3 col-sm-6 item-wrap">
				<div class="item">
					<div class="img-wrap" style="background-image: url(/'. get_cache_pic( $item['img'], 345, 231, true ) .');"></div>
					<p class="title">';

					if( $item['is_new'] ) $str .= '<img src="/static/img/new.png" alt="">'; 

					$str .= $row['title'] .'</p>
					<div class="overlay-wrap">
						<div class="overlay">
							<div class="button-wrapper">
								<div class="basic-button"><button>заказать</button></div>
								<div class="basic-button"><a href="/'. $my_alias . '/' . $row['alias'] .'.html">подробнее</a></div>
								<div class="basic-button"><button>акции</button></div>
							</div>
						</div>
					</div>
				</div>
			</div>';

}


$str .=        '</div>
			</div>
		</section>';

echo $str;