<?php

$table = new Table('catalog_section');

$news = $table -> select('SELECT * FROM `position_news` WHERE `alias`=:alias AND `public`', array( 'alias' => $params[0] ) );
if( !count( $news ) ) return false;

$news = end( $news );

$new_date = explode( ' ', date( "d m Y", $news[ 'datestamp' ] ) );
$date = $new_date[ 0 ] . ' ' . Langvars :: replaceMonth( $new_date[ 1 ] ) . ' ' . $new_date[ 2 ];

$str = '<div class="breadcrumbs">
			<div class="container">
				<nav class="breadcrumbs">
					'. val('catalog.action.breadcrumbs', array( 'last_link' => true ) ) . '<span>'. $news['title'] .'</span>' .'
				</nav>
			</div>
		</div>
		<div class="container news-item-page">';

if( empty($news['is_share']) ) {
	$str .= '<p class="date">'. $date .'</p>';
}

$str .=	'<h1>'. $news['title'] .'</h1>';

$str .= '<div class="content"><img src="/'. get_cache_pic($news['img'], 540, 351, true) .'" alt="">' . $news['content'] .'</div>';

$str .= '</div>';

echo $str;