<?php 

if( !empty( $params[0] ) && !$params[ 1 ] ) {
	mod('catalog.action.programm');
	return;
}
else if ( !empty( $params[ 1 ] ) ) {
	mod('catalog.action.programm_offer');
	return;
}


$table = new Table('catalog_section');
$section = $table -> select('SELECT `id` FROM `catalog_section` WHERE `alias`=:alias LIMIT 1', array( 'alias' => $alias ));

if( !count( $section ) ) return false;

$section = end( $section );

$rows = $table -> select('SELECT * FROM `catalog_section` WHERE `parent_id`=:id ORDER BY `position`' , array( 'id' => $section['id'] )); 

if( !count($rows) ) return false;

$str = '<div class="programms-page">
			<div class="breadcrumbs">
				<div class="container">
					'. val('catalog.action.breadcrumbs') .'
				</div>
			</div>
			<div class="container programms">
				<h1>'. val('pages.show.title') .'</h1>
				<div class="row">';


foreach( $rows as $row ) {

	$item = $table -> select('SELECT * FROM `section_programms` WHERE `id`=:id AND `public` LIMIT 1', array( 'id' => $row[ 'id' ] ));

	if( !count( $item ) ) continue;

	$item = end( $item );

	$str .= '<div class="col-md-3 col-sm-6 item-wrap">
				<div class="item" onclick="window.location.href=\'/'. $alias . '/' . $row['alias'] .'.html\'" data-programm-id="'. $item['id'] .'">
					<div class="img-wrap" style="background-image: url(/'. get_cache_pic( $item['img'], 345, 439, true ) .');"></div>
					<p class="title">';

					if( $item['is_new'] ) $str .= '<img src="/static/img/new.png" alt="">'; 

					$str .= $row['title'] .'</p>
					<div class="overlay-wrap">
						<div class="overlay">
							<div class="button-wrapper">
								<div class="basic-button details"><a href="/'. $alias . '/' . $row['alias'] .'.html">подробнее</a></div>';
					if( is_shares_in_programm( $row['id'] ) ) {
						$str .= '<div class="basic-button"><a href="/akcii-i-novosti.html?programm='. $row['alias'] .'">акции</a></div>';
					}
					if( !empty($item['is_programm']) ) {
						$str .=	 '<div class="basic-button"><a href="" class="enroll">записаться</a></div>';
					}
					$str .=	 '</div>
						</div>
					</div>
				</div>
			</div>';

}


$str .=        '</div>
			</div>
		</div>';

echo $str;


function is_shares_in_programm( $id ) {

	$table = new Table('catalog_section');

	$shares_section = $table -> select('SELECT `id` FROM `catalog_section` WHERE `parent_id`=:id and `position_table`="position_select_shares" LIMIT 1', 
		array( 'id' => $id ) );
	if( empty( $shares_section ) ) return false;

	$shares_section = end( $shares_section );

	$shares = $table -> select('SELECT `id` FROM `position_select_shares` WHERE `section_id`=:id', array( 'id' => $shares_section['id'] ) );
	if( empty( $shares ) ) return false;

	return true;

}