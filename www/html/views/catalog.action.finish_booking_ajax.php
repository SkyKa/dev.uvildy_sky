<?php 
header( 'content-type: application/json; charset=utf-8' );

usleep(100000);

$json = array(	
			's' => false, 
			'mess' => '',
			);

$name = Utils :: getVar( 'name' );
$email = Utils :: getVar( 'email' );
$phone = Utils :: getVar( 'phone' );
$comment = Utils :: getVar( 'comment' );

$table = new Table('catalog_section');

$start_date = date('d.m.Y', strtotime($_SESSION['booking']['start_date']));
$end_date = date('d.m.Y', strtotime( "+" . $_SESSION['booking']['duration'] . " days" , strtotime($_SESSION['booking']['start_date'])));

$period = $start_date . '-' . $end_date;

$programm_title = 'без лечения';

if($_SESSION['booking']['programm'] != 'without-programm') {

    $programm = $table->select('SELECT `id`, `title` FROM `catalog_section` WHERE `id`=:id LIMIT 1', array( 'id' => $_SESSION['booking']['programm'] ) );

    if( !empty( $programm ) ) {
    	$programm_title = $programm[0]['title'];
    }
}

$room = $table -> select('SELECT `catalog_section`.`id`, `catalog_section`.`parent_id`, `catalog_section`.`title`,
	`section_offers`.`n_rooms`, `section_offers`.`n_main_place`, `section_offers`.`n_additional_place` 
	FROM `catalog_section` 
	INNER JOIN 
		`section_offers` 
	ON
		`section_offers`.`id`=`catalog_section`.`id`
	WHERE `catalog_section`.`id`=:id LIMIT 1', array( 'id' => $_SESSION['booking']['number'] ));

$class_room_title = '';
$class_room_id = '';

if( !empty( $room ) ) {
	$class_room = $table -> select('SELECT `catalog_section`.`id`, `catalog_section`.`title`, `section_number_class`.`id_1c` FROM `catalog_section` 
		INNER JOIN `section_number_class` 
		ON (`section_number_class`.`id`=`catalog_section`.`id`)
		WHERE `catalog_section`.`id`=:id LIMIT 1', array( 'id' => $room[0]['parent_id'] ));

	$class_room_title = $class_room[0]['title'];
	$class_room_id = $class_room[0]['id_1c'];
}

$n_rooms = $room[0]['n_rooms'];
$n_main_place = $room[0]['n_main_place'];
$n_additional_place = $room[0]['n_additional_place'];

$class_number_all_number = $table -> select('SELECT `section_number_class`.`id` FROM `section_number_class` INNER JOIN `catalog_section` WHERE `catalog_section`.`parent_id`=806 AND `section_number_class`.`id_1c`=:id_1c LIMIT 1', 
	array( 'id_1c' => $class_room_id ));

$number = $table -> select('SELECT `catalog_section`.`id`, `catalog_section`.`title`, `section_rooms`.`id_1c` 
	FROM `catalog_section` 
	INNER JOIN `section_rooms`
	ON (`catalog_section`.`id`=`section_rooms`.`id`)
	WHERE 
	`catalog_section`.`parent_id`=:id AND
	`section_rooms`.`n_rooms`=:n_rooms AND
	`section_rooms`.`n_main_place`=:n_main_place AND
	`section_rooms`.`n_additional_place`=:n_additional_place
	LIMIT 1',
	array( 'id' => $class_number_all_number[0]['id'], 'n_rooms' => $n_rooms, 'n_main_place' => $n_main_place, 'n_additional_place' => $n_additional_place ));

$number = end( $number );


$number_title = $number['title'];
$number_id = $number['id_1c'];

$sharing = 0;
if( ($n_main_place + $n_additional_place) < $_SESSION['booking']['persons'] ) {
	$sharing = 1;
}

$price = $_SESSION['booking']['cost_by_day'];
$sum = $_SESSION['booking']['cost_by_day'] * $_SESSION['booking']['duration'];

foreach( $_SESSION['booking']['persons'] as $key => $person ) {
	$age = $table -> select('SELECT `id`, `age_id_1c` FROM `section_number_prices` WHERE `age`=:age LIMIT 1', array( 'age' => $person['age'] ) );

	$_SESSION['booking']['persons'][$key]['age_id'] = $age[0]['age_id_1c'];
}

$clients = json_encode($_SESSION['booking']['persons']);

$table->execute('INSERT INTO `position_orders` (`date_create`, `period`, `programm`, `num_1cid`, `num_type`, `num_rooms`, `num_place`, `num_dopplace`, `num_title`, `price`, `summ`, `sharing`, `client_name`, `client_email`, `client_phone`, `client_comm`, `clients`) VALUES (now(), :period, :programm, :num_1cid, :num_type, :num_rooms, :num_place, :num_dopplace, :num_title, :price, :summ, :sharing, :client_name, :client_email, :client_phone, :client_comm, :clients)',
	array( 'period' => $period, 'programm' => $programm_title, 'num_1cid' => $number_id, 'num_type' => $class_room_title, 'num_rooms' => $n_rooms, 'num_place' =>
		$n_main_place, 'num_dopplace' => $n_additional_place, 'num_title' => $number_title, 'price' => $price, 'summ' => $sum, 'sharing' => $sharing, 'client_name' => $name, 'client_email' => $email, 'client_phone' => $phone, 'client_comm' => $comment, 'clients' => $clients
		) );

$json['html'] = '<div class="container"><h2 style="font-size: 27px; margin-top: 67px;">Номер успешно забронирован, спасибо! Мы свяжемся с Вами в ближайшее время.</h2></div>';
$json['s'] = true;
echo json_encode( $json );
exit();