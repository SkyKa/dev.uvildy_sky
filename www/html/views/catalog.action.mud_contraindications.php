<?php

$table = new Table('catalog_section');

$parent = $table -> select( 'SELECT * FROM `catalog_section` WHERE `alias`=:alias LIMIT 1', array( 'alias' => 'sapropelevye-gryazi' ) );
$parent = end( $parent );

$childs = $table -> select( 'SELECT * FROM `catalog_section` WHERE `parent_id`=:id ', array( 'id' => $parent[ 'id' ] ) );


foreach ( $childs as $child ) {
	if ( $child['alias'] == "pokazaniya" )
	{
		$rows = $table -> select( 'SELECT * FROM `position_mud_news` WHERE `section_id`=:id AND `public`=1 ORDER BY `datestamp` DESC LIMIT 8 ', array( 'id' => $child['id'] ) );

		foreach ($rows as $row) {
			echo '
				<div class="mud_contraindications_item col-xs-12 col-sm-6">
					<div class="mud_contraindications_item_date">
						<i class="far fa-clock"></i> '. date( 'd', $row['datestamp']) .' / '. date( 'm', $row['datestamp']) .' / '. date( 'Y', $row['datestamp']) .'
					</div>
					<div class="mud_contraindications_item_wrap">
						<a href="/pokazaniya/'. $row['alias'] .'.html">'. $row['title'] .'</a>
					</div>
				</div>
			';
		}
	}
}