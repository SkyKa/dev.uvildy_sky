<?php

$table = new Table('catalog_section');

$parent = $table -> select( 'SELECT * FROM `catalog_section` WHERE `alias`=:alias LIMIT 1', array( 'alias' => 'sapropelevye-gryazi' ) );
$parent = end( $parent );

$childs = $table -> select( 'SELECT * FROM `catalog_section` WHERE `parent_id`=:id ', array( 'id' => $parent[ 'id' ] ) );


foreach ( $childs as $child ) {
	if ( $child['alias'] == "gryazelechenie-v-medecine" )
	{
		$rows = $table -> select( 'SELECT * FROM `position_mud_news` WHERE `section_id`=:id AND `public`=1 ORDER BY `datestamp` DESC ', array( 'id' => $child['id'] ) );

		foreach ($rows as $row) {
			echo '
				<div>
					<div class="fourth_slider_item mud col-xs-12">
						<div class="fourth_slider_item_header">
							<a href="/gryazelechenie-v-medecine/'. $row['alias'] .'.html"><img src="'. $row['img_src'] .'"></a>
							<div class="fourth_slider_item_header_doc mud">
								<img src="'. $row['img_doc'] .'">
								<h4>'. $row['title_doc'] .'</h4>
								<p>'. $row['prof_doc'] .'</p>
								<a href="#">Все статьи врача</a>
							</div>
						</div>
						<div class="fourth_slider_item_wrap mud">
							<h3><a href="/gryazelechenie-v-medecine/'. $row['alias'] .'.html">'. $row['title'] .'</a></h3>
							<p>'. $row['title2'] .'</p>
							<a href="/gryazelechenie-v-medecine/'. $row['alias'] .'.html"><button>Подробнее</button></a>
						</div>
					</div>
				</div>
			';
		}
	}
}