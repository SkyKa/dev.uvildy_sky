<?php
 /**
  * Author:		Startcev PV / Fedotov VA
  * Email:		cefar@mail.ru / vottiv@gmail.com
  * Phone:		+7(912)323-84-94
  * Company:	IT-FACTORY
  */
class Tinkoff_rest {
	


	// логин банка
	protected $_username;
	// пароль банка
	protected $_password;
	// Подпись запроса
	protected $_token;
	// URL Адрес сайта
	protected $_siteurl;
	// Адрес, на который надо перенаправить пользователя в случае успешной оплаты
	protected $_return_url;	
	// Адрес, на который надо перенаправить пользователя в случае неуспешной оплаты
	protected $_fail_url;
	// урл REST API
	protected $_gatewey_url = 'https://rest-api-test.tcsbank.ru/rest';
	// таблица содержащая транзакции
	protected $_transaction_table = 'position_transaction';	
	// плаьежные поля в таблице
	

	/*
	 Только для Tinkoff
	*/
	// Ошибка запроса
	protected $_error;
	// ссылка на ридерект урл
	protected $_paymentUrl; 
	// статус запроса
	protected $_status; 
	
 
	public $order_status =	array(
								'NEW' => 'Платеж зарегистрирован, но его обработка не начата',
								'CANCELED' => 'Платеж отменен Продавцом',
								'PREAUTHORIZING' => 'Проверка платежных данных Покупателя',
								'FORMSHOWED' => 'Покупатель переправлен на страницу оплаты',
								'AUTHORIZING' => 'Покупатель начал аутентификацию',
								'3DS_CHECKING' => 'Покупатель начал аутентификацию по протоколу 3-D Secure',
								'3DS_CHECKED' => 'Покупатель завершил аутентификацию по протоколу 3-D Secure',
								'AUTHORIZED' => 'Средства заблокированы, но не списаны',
								'REVERSING' => 'Начало отмены блокировки средств',
								'REVERSED' => 'Денежные средства разблокированы',
								'CONFIRMING' => 'Начало списания денежных средств',
								'CONFIRMED' => 'Денежные средства списаны',
								'REFUNDING' => 'Начало возврата денежных средств',
								'REFUNDED' => 'Произведен возврат денежных средств',
								'PARTIAL_REFUNDED' => 'Произведен частичный возврат денежных средств',
								'REJECTED' => 'Платеж отклонен Банком',
								'UNKNOWN' => 'Статус не определен'
								);


	public static $table_rows =	array(
										// идентификатор товара на сайте
										'id' => 'id',
										// Номер заказа в платежной системе. Уникален в пределах системы. Отсутствует если регистрация заказа
										// не удалась по причине ошибки, детализированной в errorCode.
										'orderId' => 'pay_order_id',
										// URL платежной формы, на который надо перенаправить броузер клиента. Не возвращается если
										// регистрация заказа не удалась по причине ошибки, детализированной в errorCode.
										'formUrl' => 'pay_form_url',
										// статус платежа
										'OrderStatus' => 'pay_status',
										// Описание статуса платежа
										'pay_status_message' => 'pay_status_message',
										// Код ошибки
										'pay_error' => 'pay_error',
										// Описание ошибки на языке, переданном в параметре language в запросе.
										'pay_error_message' => 'pay_error_message',
										// урл возврата средств
										'pay_refund_url' => 'pay_refund_url',
										// Код ошибки возврата средств
										'pay_refund_error' => 'pay_refund_error',
										// Описание ошибки на языке, переданном в параметре language в запросе.
										'pay_refund_message' => 'pay_refund_message',
										// Сумма возврата
										'pay_refund_amount' => 'pay_refund_amount',
										// Маскированный номер карты, которая использовалась для оплаты. Указан только после оплаты заказа.
										'Pan' => 'pay_pan',
										// Срок истечения действия карты в формате YYYYMM. Указан только после оплаты заказа.
										'expiration' => 'pay_expiration',
										// Имя держателя карты. Указан только после оплаты заказа.
										'cardholderName' => 'pay_cardholder_name',
										// Код валюты платежа ISO 4217. Если не указан, считается равным 810 (российские рубли).
										'currency' => 'pay_currency',
										// Сумма платежа в копейках или центах
										'Amount' => 'pay_amount',
										// AN6 нет Код авторизации МПС. Поле фиксированной длины (6 символов), может содержать цифры и латинские буквы.
										'approvalCode' => 'pay_approval_сode',
										// IP адрес пользователя, который оплачивал заказ
										'Ip' => 'pay_ip',
										// Дата регистрации заказа
										'date' => 'pay_date',
										// Описание заказа, переданное при его регистрации
										'orderDesctiption' => 'pay_description',
										// Расшифровка кода ответа на языке, переданном в параметре Language в запросе.
										'actionCodeDesctiption' => 'pay_action_code_desctiption',
										// Номер (идентификатор) клиента в системе магазина, переданный при регистрации заказа. Присутствует только если магазину разрешено создание связок.
										'clientId' => 'pay_client_id',
										// Идентификатор связки созданной при оплате заказа или использованной для оплаты. Присутствует только если магазину разрешено создание связок.
										'bindingId' => 'pay_binding_id'

									);
	// Объкт коннекта к БД
	protected $_db;

	// Код валюты платежа ISO 4217. Если не указан, считается равным 810 (российские рубли).
	protected $_currency = 810;

	public function __construct( $conf ) {

		if ( empty( $conf[ 'siteurl' ] ) || !$conf[ 'siteurl' ] ) {
			throw new Exception( 'params siteurl not found' );
		}
		if ( empty( $conf[ 'username' ] ) || !$conf[ 'username' ] ) {
			throw new Exception( 'params username not found' );
		}
		if ( empty( $conf[ 'password' ] ) || !$conf[ 'password' ] ) {
			throw new Exception( 'params password not found' );
		}
		// if ( empty( $conf[ 'token' ] ) || !$conf[ 'token' ] ) {
		// 	throw new Exception( 'params token not found' );
		// }
		// Адрес, на который надо перенаправить пользователя в случае успешной оплаты
		if ( empty( $conf[ 'return_url' ] ) || !$conf[ 'return_url' ] ) {
			throw new Exception( 'params return_url not found' );
		}
		// Адрес, на который надо перенаправить пользователя в случае неуспешной оплаты
		if ( empty( $conf[ 'fail_url' ] ) || !$conf[ 'fail_url' ] ) {
			throw new Exception( 'params fail_url not found' );
		}
		
		$this -> _username = $conf[ 'username' ];
		$this -> _password = $conf[ 'password' ];
		// $this -> _token = $conf[ 'token' ];
		$this -> _siteurl = $conf[ 'siteurl' ];
		$this -> _return_url = $conf[ 'return_url' ];
		$this -> _fail_url = $conf[ 'fail_url' ];

		if ( isset( $conf[ 'session_timeout' ] ) ) {
			$this -> _session_timeout = $conf[ 'session_timeout' ];
		}

		$this -> _db = new Table( $this -> _transaction_table );

	}

	public function __destruct( ) {
	
	}

	// получение заказа по ид сайта
	public function get_transaction( $cols=array(), $orderby=array() ) {
		
		$pdo_where = '';
		$pdo_orderby = '';
		if ( count( $cols ) ) {
			$pdo_where .= ' WHERE ';
			foreach ( $cols as $key => $c ) {
				$pdo_where .= '`' . $key . '`=:' . $key . ' && ';
			}
			$pdo_where = substr( $pdo_where, 0, -4 );
		}

		if ( count( $orderby ) ) {
			$pdo_orderby .= ' ORDER BY ';
			$pdo_orderby .= implode( ',', $orderby );
		}

		$select = $this -> _db -> select( 'SELECT * FROM `' . $this -> _transaction_table . '`' . $pdo_where . $pdo_orderby, $cols );
		return $select;
	
	}


	public function register( $site_order_id, $amount, $description='', $add_params=null ) {
		
		if ( empty( $site_order_id ) || !$site_order_id ) {
			throw new Exception( 'params site_order_id not found' );
		}

		if ( empty( $amount ) || !$amount ) {
			throw new Exception( 'params amount not found' );
		}

		$data = array(
			'TerminalKey' => $this -> _username,
			'Amount' => urlencode( $amount ),
			'OrderId' => urlencode( $site_order_id ),
			//'IP' => $_SERVER['REMOTE_ADDR'],
			//'Currency' => $this -> _currency,
			// 'Token' => $this -> _token,
			//'PayForm' => NULL,
			//'CustomerKey' => NULL,
			//'Recurrent' => NULL,

	
			// 'returnUrl' => $this -> _return_url,
		);
		var_dump($data);
		// Продолжительность жизни заказа в секундах.
		if ( isset( $this -> _session_timeout ) ) {
			$data[ 'sessionTimeoutSecs' ] = $this -> _session_timeout;
		}
		
		if ( $description != '' ) $data[ 'description' ] = $description;
		if ( isset( $add_params ) ) $data[ 'DATA' ] = $add_params;

		$response = $this -> gateway( 'Init', $data );
		if ( gettype( $response ) != 'array' ) return $response;

		// в случае, если запрос улетел вникуда
		if ( !$this->_response ) {

			throw new Exception( "invalid response" );
			
		}

		// В случае ошибки вывести ее
		if ( !$response[ 'Success' ] ) {

			// МЕНЯЕМ ЗАКАЗ
			$this -> _db -> execute(
				'UPDATE `' . $this -> _transaction_table . '`
				 SET `' . ( self :: $table_rows[ 'pay_error' ] ) . '`=' . $response[ 'ErrorCode' ] . ',
					 `' . ( self :: $table_rows[ 'pay_status_message' ] ) . '`="' . $response[ 'Message' ] . '|' . $response[ 'Details' ] . '"
				 WHERE `' . ( self :: $table_rows[ 'id' ] ) . '` = "' . $site_order_id . '";'
			);

			/*
				echo 'Ошибка #' . $response[ 'errorCode' ] . ': ' . $response[ 'errorMessage' ];
			*/
		}
		// В случае успеха перенаправить пользователя на плетжную форму
		else if ( $response[ 'Success' ] && isset( $response[ 'PaymentURL' ] ) ) {

			// МЕНЯЕМ ЗАКАЗ
			$this -> _db -> execute(
				'UPDATE `' . $this -> _transaction_table . '`
				 SET `' . ( self :: $table_rows[ 'formUrl' ] ) . '`="' . $response[ 'PaymentURL' ] . '",
					 `' . ( self :: $table_rows[ 'orderId' ] ) . '`="' . $response[ 'OrderId' ] . '"
				 WHERE `' . ( self :: $table_rows[ 'id' ] ) . '` = "' . $site_order_id . '";'
			);
		}

		return $response;
	}

	public function status( $pay_order_id ) {
		
		$data = array(
					'TerminalKey' => $this -> _username,			
					'PaymentId' => $pay_order_id,
					'IP' => $_SERVERS['REMOTE_ADD'],
					// 'Token' => $this -> _token,
			);

		$response = $this -> gateway( 'GetState', $data );
		if ( gettype( $response ) != 'array' ) return $response;
		
		// МЕНЯЕМ ЗАКАЗ
		$sql = 'UPDATE `' . $this -> _transaction_table . '` SET 
				 `' . ( self :: $table_rows[ 'OrderStatus' ] ) . '`="' . $response[ 'Status' ] . '"
				 ,`' . ( self :: $table_rows[ 'Amount' ] ) . '`="' . $response[ 'Amount' ] . '"
				 ,`' . ( self :: $table_rows[ 'pay_status_message' ] ) . '`="' . $this -> order_status[ $response[ 'Status' ] ] . '" ';
		 
		 /*
		 	,`' . ( self :: $table_rows[ 'pay_status_message' ] ) . '`="' . $response[ 'Message' ] . '|' . $response[ 'Details' ] . '" ';
		 */

		if ( isset( $response[ 'ErrorCode' ] ) ) {
			$sql .= ',`' . ( self :: $table_rows[ 'pay_error' ] ) . '`="' . $response[ 'ErrorCode' ] . '" ';
		}
		if ( isset( $response[ 'ErrorMessage' ] ) ) {
				 ',`' . ( self :: $table_rows[ 'pay_error_message' ] ) . '`="' . $response[ 'ErrorMessage' ] . '" ';
		}
		
			$sql .= 'WHERE `' . ( self :: $table_rows[ 'orderId' ] ) . '` = "' . $pay_order_id . '";';

		$this -> _db -> execute( $sql );

	}

	public function refund( $pay_order_id, $amount ) {
		return false;
	}

	public function cashback_url( $pay_order_id ) {
		return false;
	}

	public function gateway( $method, $data ) {

        $url = $this-> _gatewey_url;

        if (is_array($data)) {
            if (! array_key_exists( 'TerminalKey', $data ) ) {
                $data[ 'TerminalKey' ] = $this -> _username;
            }
            if (! array_key_exists('Token', $data)) {
                $data[ 'Token' ] = $this -> _genToken( $data );
            }
        }

        $url = $this->_combineUrl($url, $method);

        return $this->_sendRequest($url, $data);

    }



	public function getSections( ) {
		$sections = $this -> _db -> select( 'SELECT * FROM `catalog_section` WHERE `position_table`=:position_table LIMIT 1',
				array( 'position_table' => $this -> _transaction_table ) );
		if ( count( $sections ) ) return end( $sections );
		return $sections;
	}

	public function getTable( ) {
		return $this -> _transaction_table;
	}
/*  -----------------------------
	Инициирует платежную сессию и регистрирует заказ в системе Банка. В ответ на вызов метода Init продавец получает ссылку, которая ведет на форму ввода реквизитов банковской карты. Продавец должен перенаправить клиента на полученную ссылку.

	Параметры запроса:

	Наименование	Тип			Обязательный	Описание
	TerminalKey		String(20)	Да				Идентификатор терминала, выдается Продавцу Банком
	Amount			Number(10)	Да				Сумма в копейках
	OrderId			String(50)	Да				Номер заказа в системе Продавца
	IP				String(40)	Нет				IP-адрес клиента
	Description		String(250)	Нет				Краткое описание
	Currency		Number(3)	Нет				Код валюты ISO 4217 (например, 643). Если передан Currency, и он разрешен для 
												Продавца, то транзакция будет инициирована в переданной валюте. Иначе 
												будет использована валюта по умолчанию для данного терминала
	Token			String		Да				Подпись запроса
	PayForm			String(20)	Нет				Название шаблона формы оплаты продавца. При использовании стандартного шаблона 
												формы оплаты, оставьте поле пустым.
	CustomerKey		String(36)	Нет				Идентификатор покупателя в системе Продавца. Если передается, то для данного покупателя 
												будет осуществлена привязка карты к данному идентификатору клиента 	CustomerKey. В 
												нотификации на AUTHORIZED будет передан параметр CardId, подробнее см. метод GetGardList
	Recurrent		String(1)	Нет				Если передается и установлен в Y, то регистрирует платеж как рекуррентный. В этом 
												случае после оплаты в нотификации на AUTHORIZED будет передан параметр RebillId для использования в методе Charge
	DATA			String		Да				Ключ=значение дополнительных параметров через “|”, например Email=a@test.ru|Phone=+71234567890, если ключи или значения содержат в себе 
												спец символы, то получившееся значение должно быть закодировано функцией urlencode. При этом, обязательным является наличие дополнительного параметра Email. Прочие можно добавлять по желанию. Данные параметры будут переданы на страницу оплаты (в случае ее кастомизации). Максимальная длина для каждого передаваемого параметра: Ключ – 20 знаков, Значение – 100 знаков. Максимальное количество пар «ключ-значение» не может превышать 20. (*)

	Параметры ответа:

	Наименование	Тип			Обязательный	Описание
	TerminalKey		String(20)	Да				Идентификатор терминала, выдается Продавцу Банком
	Amount			Number(10)	Да				Сумма в копейках
	OrderId			String(20)	Да				Номер заказа в системе Продавца
	Success			bool		Да				Успешность операции
	Status			String(20)	Да				Статус транзакции
	PaymentId		Number(20)	Да				Уникальный идентификатор транзакции в системе Банка
	ErrorCode		String(20)	Да				Код ошибки, «0» - если успешно
	PaymentURL		String(100)	Нет				Ссылка на страницу оплаты. По умолчанию ссылка доступна в течении 24 часов.
	Message			String		Нет				Краткое описание ошибки
	Details			String		Нет				Подробное описание ошибки

	Статус платежа:
					при успешном сценарии: NEW
					при неуспешном: REJECTED

*/
	public function Init ( $args ) {

		return $this->buildQuery('Init', $args);

	}


/*  -----------------------------
	Данный метод используется, если продавец обладает сертификацией PCI DSS и использует свою собственную платежную форму вместо формы банка. Метод FinishAuthorize подтверждает инициированный платеж передачей карточных данных. При использовании одностадийного проведения осуществляет списание денежных средств с карты покупателя. При двухстадийном проведении осуществляет блокировку указанной суммы на карте покупателя.

	Статус платежа:
					при успешном сценарии и одностадийном проведении платежа: CONFIRMED
					при успешном сценарии и двухстадийном проведении платежа: AUTHORIZED
					при неуспешном: REJECTED
	Переадресация покупателя:
					В случае успешного проведения платежа на Success URL
					В случае не успешного проведения платежа на Fail URL
*/
	public function FinishAuthorize () {
		


	}


/*	-----------------------------
	Подтверждает платеж и осуществляет списание заблокированных ранее денежных средств. Используется при двухстадийном проведении платежа (при одностадийном проведении платежа вызывается автоматически). Применим только к платежам в статусе AUTHORIZED. Сумма подтверждения может быть меньше или равна сумме авторизации. Если сумма подтверждения меньше суммы платежа, то будет выполнено частичное подтверждение.

	Параметры запроса:

	Наименование	Тип			Обязательный	Описание
	TerminalKey		String(20)	Да				Идентификатор терминала, выдается Продавцу Банком
	PaymentId		Number(20)	Да				Уникальный идентификатор транзакции в системе Банка
	IP				String(40)	Нет				IP-адрес клиента
	Amount			Number(10)	Нет				Сумма в копейках
	Token			String		Да				Подпись запроса

	Параметры ответа:

	Наименование	Тип			Обязательный	Описание
	TerminalKey		String(20)	Да				Идентификатор терминала, выдается Продавцу Банком
	OrderId			String(20)	Да				Номер заказа в системе Продавца
	Success			bool		Да				Успешность операции (true/false)
	Status			String(20)	Да				Статус транзакции
	PaymentId		Number(20)	Да				Уникальный идентификатор транзакции в системе Банка
	ErrorCode		String(20)	Да				Код ошибки, «0» - если успешно
	Message			String		Нет				Краткое описание ошибки
	Details			String		Нет				Подробное описание ошибки

	Пример ответа:
				{"Success":true,"ErrorCode":"0","TerminalKey":"TestB","Status":"CONFIRMED","PaymentId":"63100","OrderId":"100668","Amount":444}
*/	
	public function Confirm () {
		
		return $this->buildQuery('Confirm', $args);

	}


/*  -----------------------------
	Осуществляет рекуррентный (повторный) платеж — безакцептное списание денежных средств со счета банковской карты Покупателя.

	Параметры запроса:
	
	Наименование	Тип			Обязательный	Описание
	TerminalKey		String(20)	Да				Идентификатор терминала, выдается Продавцу Банком
	PaymentId		Number(20)	Да				Уникальный идентификатор транзакции в системе Банка, полученный в ответе на вызов метода Init
	IP				String(40)	Нет				IP-адрес клиента
	RebillId		Number(20)	Да				Идентификатор рекуррентного платежа (см. параметр Recurrent в методе Init)
	Token			String		Да				Подпись запроса

	Параметры ответа:
	
	Наименование	Тип			Обязательный	Описание
	TerminalKey		String(20)	Да				Идентификатор терминала, выдается Продавцу Банком
	OrderId			String(20)	Да				Номер заказа в системе Продавца
	Success			bool		Да				Успешность операции (true/false)
	Status			String(20)	Да				Статус транзакции
	PaymentId		Number(20)	Да				Уникальный идентификатор транзакции в системе Банка
	ErrorCode		String(20)	Да				Код ошибки, «0» - если успешно
	Amount			Number(10)	Да				Сумма списания в копейках
	Message			String		Нет				Краткое описание ошибки
	Details			String		Нет				Подробное описание ошибки

	Пример ответа:
				{"Success":true,"ErrorCode":"0","TerminalKey":"TestB","Status":"CONFIRMED", "PaymentId":"10063","OrderId":"21054"}
*/	
	public function Charge () {
		
		return $this->buildQuery('Charge', $args);

	}


/*  -----------------------------
	Отменяет платежную сессию. В зависимости от статуса платежа переводит его в следующие состояния:

	Начальный статус	Статус после проведения операции
	NEW					CANCELED
	AUTHORIZED			REVERSED
	CONFIRMED			PARTIAL_REFUNDED – если отмена не на полную сумму
	CONFIRMED			REFUNDED – если отмена на полную сумму

	Параметры запроса:
	Наименование	Тип			Обязательный	Описание
	TerminalKey		String(20)	Да				Идентификатор терминала, выдается Продавцу Банком
	PaymentId		Number(20)	Да				Уникальный идентификатор транзакции в системе Банка
	IP				String(40)	Нет				IP-адрес клиента
	Reason			String(100)	Нет				Причина отмены
	Token			String		Да				Подпись запроса
	Amount			Number(10)	Нет (*)			Сумма отмены в копейках (**)

	(*) в случае отмены платежа в статусах NEW или AUTHORIZED поле Amount, даже если оно проставлено, игнорируется. Отмена из статусов NEW или AUTHORIZED всегда производится на полную сумму.
	(**) в случае отмены платежа в статусе CONFIRMED, клиент может указать сумму отмены явно. Если сумма отмены меньше суммы платежа, будет произведена частичная отмена. Частичную отмену можно производить до тех пор, пока платеж не будет полностью отменен. На каждую отмену на Notifcation URL будет отправляться нотификация CANCEL.

	Параметры ответа:

	Наименование	Тип			Обязательный	Описание
	TerminalKey		String(20)	Да				Идентификатор терминала, выдается Продавцу Банком
	Success			bool		Да				Успешность операции (true/false)
	Status			String(20)	Да				Статус транзакции
	PaymentId		Number(20)	Да				Уникальный идентификатор транзакции в системе Банка
	ErrorCode		String(20)	Да				Код ошибки, «0» - если успешно
	OrderId			String(20)	Да				Номер заказа в системе Продавца
	OriginalAmount	Number(10)	Да				Сумма в копейках до операции отмены
	NewAmount		Number(10)	Да				Сумма в копейках после операции отмены
	Message			String		Нет				Краткое описание ошибки
	Details			String		Нет				Подробное описание ошибки
*/	
	public function Cancel () {

		return $this->buildQuery('Cancel', $args);

	}


/*	-----------------------------
	Возвращает текуший статус платежа.

	Параметры запроса:

	Наименование	Тип			Обязательный	Описание
	TerminalKey		String(20)	Да				Идентификатор терминала, выдается Продавцу Банком
	PaymentId		Number(20)	Да				Уникальный идентификатор транзакции в системе Банка
	IP				String(40)	Нет				IP-адрес клиента
	Token			String		Да				Подпись запроса

	Параметры ответа:

	Наименование	Тип			Обязательный	Описание
	TerminalKey		String(20)	Да				Идентификатор терминала, выдается Продавцу Банком
	OrderId			String(20)	Да				Номер заказа в системе Продавца
	Success			bool		Да				Успешность операции (true/false)
	Status			String(20)	Да				Статус транзакции
	PaymentId		Number(20)	Да				Уникальный идентификатор транзакции в системе Банка
	ErrorCode		String(20)	Да				Код ошибки, «0» - если успешно
	Message			String		Нет				Краткое описание ошибки
	Details			String		Нет				Подробное описание ошибки
	
	Пример ответа:
				{"Success":true,"ErrorCode":"0","TerminalKey":"TestB","Status":"NEW","PaymentId":"10063","OrderId":"21057"}
*/	
	public function GetState () {
		
		return $this->buildQuery('GetState', $args);

	}


/*	-----------------------------
	Данный метод регистрирует покупателя в терминале Продавца. 

	Параметры запроса:

	Наименование	Тип			Обязательный	Описание
	TerminalKey		String(20)	Да				Идентификатор терминала, выдается Продавцу Банком
	CustomerKey		String(36)	Да				Идентификатор покупателя в системе Продавца
	IP				String(40)	Нет				IP-адрес запроса
	Email			String(100)	Нет				Email клиента
	Phone			String(15)	Нет				Телефон клиента (+71234567890)
	Token			String		Да				Подпись запроса

	Параметры ответа:

	Наименование	Тип			Обязательный	Описание
	TerminalKey		String(20)	Да				Платежный ключ, выдается Продавцу при заведении терминала
	CustomerKey		String(32)	Да				Идентификатор покупателя в системе Продавца
	Success			bool		Да				Успешность операции (true/false)
	ErrorCode		String(20)	Да				Код ошибки, «0» - если успешно
	Message			String		Нет				Краткое описание ошибки
	Details			String		Нет				Подробное описание ошибки

	Пример ответа:
				{"Success":true,"ErrorCode":"0","TerminalKey":"TestB","CustomerKey":"Customer1"}
*/	
	public function AddCustomer () {
		
		return $this->buildQuery('AddCustomer', $args);

	}


/*	-----------------------------
	Данный метод возвращает данные покупателя сохраненные для терминала Продавца.

	Параметры запроса:

	Наименование	Тип			Обязательный	Описание
	TerminalKey		String(20)	Да				Идентификатор терминала, выдается Продавцу Банком
	CustomerKey		String(36)	Да				Идентификатор покупателя в системе Продавца
	IP				String(40)	Нет				IP-адрес запроса
	Token			String		Да				Подпись запроса

	Параметры ответа:

	Наименование	Тип			Обязательный	Описание
	TerminalKey		String(20)	Да				Платежный ключ, выдается Продавцу при заведении терминала
	CustomerKey		String(32)	Да				Идентификатор покупателя в системе Продавца
	Success			bool		Да				Успешность операции (true/false)
	ErrorCode		String(20)	Да				Код ошибки, «0» - если успешно
	Email			String(100)	Нет				Email клиента
	Phone			String(15)	Нет				Телефон клиента (+71234567890)
	Message			String		Нет				Краткое описание ошибки
	Details			String		Нет				Подробное описание ошибки

	Пример ответа:
				{"Success":true,"ErrorCode":"0","TerminalKey":"TestB","CustomerKey":"Customer1"}
*/
	public function GetCustomer () {
		
		return $this->buildQuery('GetCustomer', $args);

	}


/*	-----------------------------
	Удаляет данные покупателя.

	Параметры запроса:

	Наименование	Тип			Обязательный	Описание
	TerminalKey		String(20)	Да				Идентификатор терминала, выдается Продавцу Банком
	CustomerKey		String(36)	Да				Идентификатор покупателя в системе Продавца
	IP				String(40)	Нет				IP-адрес запроса
	Token			String		Да				Подпись запроса


	Параметры ответа:

	Наименование	Тип			Обязательный	Описание
	TerminalKey		String(20)	Да				Платежный ключ, выдается Продавцу при заведении терминала
	CustomerKey		String(32)	Да				Идентификатор покупателя в системе Продавца
	Success			bool		Да				Успешность операции (true/false)
	ErrorCode		String(20)	Да				Код ошибки, «0» - если успешно
	Message			String		Нет				Краткое описание ошибки
	Details			String		Нет				Подробное описание ошибки

	Пример ответа:
				{"Success":true,"ErrorCode":"0","TerminalKey":"TestB","CustomerKey":"Customer1"}
*/
	public function RemoveCustomer () {
		
		return $this->buildQuery('RemoveCustomer', $args);

	}


/*	-----------------------------
	Возвращает список привязанных карт у покупателя.

	Параметры запроса:

	Наименование	Тип			Обязательный	Описание
	TerminalKey		String(20)	Да				Идентификатор терминала, выдается Продавцу Банком
	CustomerKey		String(36)	Да				Идентификатор покупателя в системе Продавца
	IP				String(40)	Нет				IP-адрес запроса
	Token			String		Да				Подпись запроса

	Параметры ответа:

	Наименование	Тип			Обязательный	Описание
	Pan				String(20)	Да				Номер карты 411111******1111
	CardId			String(32)	Да				Идентификатор карты в системе Банка
	Status			String(1)	Да				Статус карты: A – активная, I – не активная

	Пример ответа:
				[{"CardId":"4750","Pan":"543211******4773","Status":"A"},{"CardId":"5100","Pan":"411111******1111","Status":"I"}]
*/
	public function GetCardList () {
		
		return $this->buildQuery('GetCardList', $args);

	}


/*	-----------------------------
	Удаляет привязанную карту у покупателя.

	Параметры запроса:

	Наименование	Тип			Обязательный	Описание
	TerminalKey		String(20)	Да				Идентификатор терминала, выдается Продавцу Банком
	CardId			Number(20)	Да				Идентификатор карты в системе Банка
	CustomerKey		String(36)	Да				Идентификатор покупателя в системе Продавца
	IP				String(40)	Нет				IP-адрес запроса
	Token			String		Да				Подпись запроса

	Параметры ответа:

	Наименование	Тип	Обязательный	Описание
	TerminalKey		String(20)	Да		Платежный ключ, выдается Продавцу при заведении терминала
	CardId			Number(20)	Да		Идентификатор карты в системе Банка
	CustomerKey		String(32)	Да		Идентификатор покупателя в системе Продавца
	Status			String(1)	Да		Статус карты: D – удалена
	Success			bool		Да		Успешность операции (true/false)
	ErrorCode		String(20)	Да		Код ошибки, «0» - если успешно
	Message			String		Нет		Краткое описание ошибки
	Details			String		Нет		Подробное описание ошибки
*/
	public function RemoveCard () {
		
		return $this->buildQuery('RemoveCard', $args);

	}

	public function buildQuery($path, $args) {

        $url = $this-> _gatewey_url;

        if (is_array($args)) {
            if (! array_key_exists( 'TerminalKey', $args )) {
                $args[ 'TerminalKey' ] = $this-> _terminalKey;
            }
            if (! array_key_exists( 'Token', $args )) {
                $args[ 'Token' ] = $this-> _genToken($args);
            }
        }

        $url = $this-> _combineUrl($url, $path);

        return $this-> _sendRequest($url, $args);

    }

    private function _genToken($args) {
        
        $token = '';
        $args[ 'Password' ] = $this-> _password;
        ksort($args);
        foreach ($args as $arg) {
            $token .= $arg;
        }
        $token = hash('sha256', $token);

        return $token;

    }

    private function _combineUrl() {

        $args = func_get_args();
        $url = '';
        foreach ($args as $arg) {
            if (is_string($arg)) {
                if ($arg[strlen($arg) - 1] !== '/') {
                    $arg .= '/';
                }
                $url .= $arg;
            } else {
                continue;
            }
        }

        return $url;

    }

    private function _sendRequest($api_url, $args) {
       
        $this -> _error = '';
        //todo add string $args support
        //$proxy = 'http://192.168.5.22:8080';
        //$proxyAuth = '';
        if (is_array($args)) {
            $args = http_build_query($args);
        }
        // Debug :: trace($args);
        if ($curl = curl_init()) {
            curl_setopt ( $curl, CURLOPT_URL, $api_url );
            curl_setopt ( $curl, CURLOPT_FOLLOWLOCATION, true );
            curl_setopt ( $curl, CURLOPT_RETURNTRANSFER, true );
            curl_setopt ( $curl, CURLOPT_SSL_VERIFYPEER, false );
            curl_setopt ( $curl, CURLOPT_POST, true );
            curl_setopt ( $curl, CURLOPT_POSTFIELDS, $args );
            $out = curl_exec ( $curl );

            $this->_response = $out;
            $json = json_decode($out);
            if ($json) {
                if (@$json->ErrorCode !== "0") {
                    $this->_error = @$json->Details;
                } else {
                    $this->_paymentUrl = @$json->PaymentURL;
                    $this->_paymentId = @$json->PaymentId;
                    $this->_status = @$json->Status;
                }
            }

            curl_close($curl);

            return (array) $json;

        } else {
            throw new HttpException(
                'Can not create connection to ' . $api_url . ' with args '
                . $args, 404
            );
        }
    }
	
}
