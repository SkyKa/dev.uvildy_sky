<?php


	class Unisender {
	
	
		private $_api_key = '';
		private $_format = 'json';
		private $_url = 'https://api.unisender.com/ru/api/';
	
		/**
		 * conaf = array(
		 *    'format' => 'json|xml',
		 *    'api_key' => unisender api key
		 * );
		 */
		public function __construct( $conf ) {
			if ( !isset( $conf[ 'api_key' ] ) ) throw new Exception( 'key api_key is empty' );
			if ( isset( $conf[ 'format' ] ) && $conf[ 'format' ] == 'xml' ) $this -> _format = 'xml';
			$this -> _api_key = $conf[ 'api_key' ];
		}

		
		/**
		 * $list_ids - ������������� ������
		 * $email
		 * $name
		 * $double_optin - "0" � �������������� ��������, "3" - ��� �������������
		 * $overwrite - 0 �� ��������������, ���� � ������ ������ ��� �������
		 */
		public function subscribe( $list_ids, $email, $name='', $double_optin=3, $overwrite=1 ) {

			if ( !isset( $list_ids ) ) throw new Exception( 'arg list_ids is empty' );
			if ( !isset( $email ) ) throw new Exception( 'arg email is empty' );
			
			$post = array(
				'api_key' => $this -> _api_key,
				'list_ids' => $list_ids,
				'fields[email]' => $email,
				'double_optin' => $double_optin,
				'overwrite' => $overwrite
			);

			if ( $name != '' ) $post[ 'fields[Name]' ] = $name;

			$content = $this -> _request( 'subscribe', $post );

			return $content;

		}




		private function _request( $method, $post ) {
			$url = $this -> _url . $method . '?format=' . $this -> _format;
			$ch = curl_init( );
			curl_setopt( $ch, CURLOPT_SSL_VERIFYPEER, false );
			curl_setopt( $ch, CURLOPT_SSL_VERIFYHOST, false );
			curl_setopt( $ch, CURLOPT_HEADER, false );
			curl_setopt( $ch, CURLOPT_POST, 1 );
			curl_setopt( $ch, CURLOPT_POSTFIELDS, $post );
			curl_setopt( $ch, CURLOPT_TIMEOUT, 10 );
			curl_setopt( $ch, CURLOPT_URL, $url );
			curl_setopt( $ch, CURLOPT_RETURNTRANSFER, true );
			$content = curl_exec( $ch );
			$info = curl_getinfo( $ch );
			curl_close( $ch );
			if ( $this -> _format == 'json' ) $content = json_decode( $content, true );
			return $content;
		}


	
	}

	
