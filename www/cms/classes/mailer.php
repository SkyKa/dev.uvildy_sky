<?php

// МАЙЛЕР от service mailgun

class Mailer {

	public $From = 'postmaster@mail.xeya.ru';
	public $FromName = 'IT-FACTORY sender';
	public $Subject;
	public $Body;

	protected $_api = 'key-59c4c21dcb9ffd0352589b6556f605fe';
	protected $_domen = 'mail.xeya.ru';

	protected $_is_html = false;
	protected $_to = array( );
	protected $_attachments = array( );
	protected $_inline = array( );
	
	public $errorInfo;

	public function __construct( ) {
		;
	}

	public function IsHTML( $f=false ) {
		$this -> _is_html = ( bool ) $f;
	}

	public function AddAddress( $to ) {
		array_push( $this -> _to, $to );
	}

	public function Attachments( $arr_files ) {
		$this -> _attachments = $arr_files;
	}

	public function AddAttachment( ) {
		$args = func_get_args( );
		foreach ( $args as $arg ) {
			array_push( $this -> _attachments, $arg );
		}
	}

	public function EmbeddedImages( $arr_files ) {
		$this -> _inline = $arr_files;
	}

	public function AddEmbeddedImage( ) {
		$args = func_get_args( );
		foreach ( $args as $arg ) {
			array_push( $this -> _inline, '@' . $arg );
		}
	}

	public function MsgHTML( $body ) {
		$this -> IsHTML( true );
		$this -> Body = $body;
	}
	
	
	
	

	public function Send( ) {

		$images = $this -> _inline;
    	$att = $this -> _attachments;
		$ch = curl_init( );

		curl_setopt( $ch, CURLOPT_SSL_VERIFYPEER, false );
		curl_setopt( $ch, CURLOPT_SSL_VERIFYHOST, false );
		
		curl_setopt( $ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC );
		curl_setopt( $ch, CURLOPT_USERPWD, 'api:' . $this -> _api );

		curl_setopt( $ch, CURLOPT_RETURNTRANSFER, 1 );
		curl_setopt( $ch, CURLOPT_CUSTOMREQUEST, 'POST' );
		curl_setopt( $ch, CURLOPT_URL, 'https://api.mailgun.net/v2/' . $this -> _domen . '/messages' );

		$option = array(	'from'		=> 'IT-FACTORY sender <postmaster@mail.xeya.ru>',
							'subject'	=> $this -> Subject, //'IT-FACTORY sender',
							'to'		=> implode( ',', $this -> _to )
							);

		if ( $this -> _is_html ) {
			$option[ 'html' ] = $this -> Body;
		}
		else {
			$option[ 'text' ] = $this -> Body;
		}

		//////////////////////////////////////////
		// attachments
	    if ( count( $att ) ) {
		    $x = 1;
		    foreach( $att as $a ) {
		        $option[ "attachment[$x]" ] = curl_file_create( $a );
		        ++$x;
		    }
		    // relevant cURL parameter, $msgArray also contains to, from, subject  parameters etc.
	    	curl_setopt( $ch, CURLOPT_HTTPHEADER, array( "Content-type: multipart/form-data" ) );
	    }
	    /////////////////////////////////////////
		// EmbeddedImages
	    if ( count( $images ) ) {
		    $x = 0;
		    foreach( $images as $image ) {
		        $option[ "inline[$x]" ] = $image;
		        ++$x;
		    }
		    // relevant cURL parameter, $msgArray also contains to, from, subject  parameters etc.
	    	// curl_setopt( $ch, CURLOPT_HTTPHEADER, array( "Content-type: multipart/form-data" ) );
	    }
		
		/////////////////////////////////////////


		curl_setopt( $ch, CURLOPT_POSTFIELDS, $option );

 		$this -> errorInfo = curl_getinfo( $ch, CURLINFO_HTTP_CODE );

		$result = curl_exec( $ch );

		curl_close( $ch );

		return $result;

	}

	
	
	



	public function getFailedMessages( ) {
		$module = 'events';
		$option = array(
			'event' => 'rejected OR failed',
			'limit' => 300,
			'subject' => '\u0423\u0432\u0438\u043b\u044c\u0434\u044b\\'
		);
		$result = $this -> _request( 'GET', $module, $option );
		return $result;
	}


	protected function _request( $method, $module, $option=array() ) {
		$ch = curl_init( );
		curl_setopt( $ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC );
		curl_setopt( $ch, CURLOPT_USERPWD, 'api:' . $this -> _api );
		curl_setopt( $ch, CURLOPT_SSL_VERIFYPEER, false );
		curl_setopt( $ch, CURLOPT_SSL_VERIFYHOST, false );
		curl_setopt( $ch, CURLOPT_RETURNTRANSFER, 1 );
		curl_setopt( $ch, CURLOPT_CUSTOMREQUEST, $method );
		/////////////////////
		// POST
		if ( $method == 'POST' ) curl_setopt( $ch, CURLOPT_POSTFIELDS, $option );
		// GET
		else $module .= '?' . http_build_query( $option );
		/////////////////////
		curl_setopt( $ch, CURLOPT_URL, 'https://api.mailgun.net/v3/' . $this -> _domen . '/' . $module );
		$result = curl_exec( $ch );
 		$this -> errorInfo = curl_getinfo( $ch, CURLINFO_HTTP_CODE );
		curl_close( $ch );
		return $result;
	}

	
	
}


if ( !function_exists( 'curl_file_create' ) ) {
    function curl_file_create( $filename, $mimetype = '', $postname = '' ) {
        return "@$filename;filename="
            . ( $postname ?: basename( $filename ) )
            . ( $mimetype ? ";type=$mimetype" : '' );
    }
}

