<?php

	class Smsru {
	
		private $_api_id		= '85E44A4D-481F-D069-413F-4EF017C1EEAD';
		private $_from			= 'Uvildy.ru';
		private $_partner_id;


		public $test			= false;
		public $url				= 'http://sms.ru/sms/send';
		public $method			= 'POST';


		public $code = array(	100 => 'Сообщение принято к отправке. На следующих строчках вы найдете идентификаторы отправленных сообщений в том же порядке, в котором вы указали номера, на которых совершалась отправка.',
								200 => 'Неправильный api_id',
								201 => 'Не хватает средств на лицевом счету',
								202 => 'Неправильно указан получатель',
								203 => 'Нет текста сообщения',
								204 => 'Имя отправителя не согласовано с администрацией',
								205 => 'Сообщение слишком длинное (превышает 8 СМС)',
								206 => 'Будет превышен или уже превышен дневной лимит на отправку сообщений',
								207 => 'На этот номер (или один из номеров) нельзя отправлять сообщения, либо указано более 100 номеров в списке получателей',
								208 => 'Параметр time указан неправильно',
								209 => 'Вы добавили этот номер (или один из номеров) в стоп-лист',
								210 => 'Используется GET, где необходимо использовать POST',
								211 => 'Метод не найден',
								212 => 'Текст сообщения необходимо передать в кодировке UTF-8 (вы передали в другой кодировке)',
								220 => 'Сервис временно недоступен, попробуйте чуть позже.',
								230 => 'Сообщение не принято к отправке, так как на один номер в день нельзя отправлять более 250 сообщений.',
								300 => 'Неправильный token (возможно истек срок действия, либо ваш IP изменился)',
								301 => 'Неправильный пароль, либо пользователь не найден',
								302 => 'Пользователь авторизован, но аккаунт не подтвержден (пользователь не ввел код, присланный в регистрационной смс)' );


		public function __construct( $conf=array() ) {

			// изменить api_id
			if ( isset( $conf[ 'api_id' ] ) )			$this -> _api_id				= $conf[ 'api_id' ];
			// изменить отправителя
			if ( isset( $conf[ 'from' ] ) )			$this -> _from				= $conf[ 'from' ];
			// изменить дебаг режим
			if ( isset( $conf[ 'test' ] ) )			$this -> test					= $conf[ 'test' ];
			// изменить партнерскую программу
			if ( isset( $conf[ 'partner_id' ] ) )	$this -> _partner_id		= $conf[ 'partner_id' ];

		}


		// метод отправки SMS
		public function send( $to, $text, $translit=false ) {
			
			$params = array( );
			$params[ 'to' ] = $to;
			$params[ 'text' ] = $text;
			$params[ 'translit' ] = $translit;
			$params[ 'multi' ] = null;
			$params[ 'from' ] = $this -> _from;
			$params[ 'time' ] = time( );
			$params[ 'api_id' ] = $this -> _api_id;
			$params[ 'partner_id' ] = 40043;

			$this->request( $params );
		}
		
		
								
		// запрос на сервер
		public function request( $params=array() ) {
			
			$acurl = new Acurl( );
			$acurl -> set_option( 'url', $this -> url );
			$acurl -> set_option( 'port', 80 );
			$acurl -> set_option( 'headers', false );
			$acurl -> set_option( 'Content-Type', 'application/x-www-form-urlencoded' );
			$acurl -> set_option( 'charset', 'utf-8' );
			$acurl -> set_option( 'Connection', 'close' );
			$answer = $acurl -> http_post_request( $params );
			$answer = iconv( 'windows-1251', 'utf-8', $answer );
			//var_dump( $answer );
			
		}
	
	
	
	
	
	}