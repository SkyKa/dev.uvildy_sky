<?php

class Form {

	public $errorInfo;
	protected static $_instance;

	protected $_section = array( );
	protected $_rows = array( );


	public static function getInstance( ) {
		if ( null === self :: $_instance ) {
			self :: $_instance = new self( );
		}
		return self :: $_instance;
	}


	public function __construct( $alias=null ) {
		;
	}

	public function __destruct( ) {
		;
	}


	// Установить поля формы по алиасу
	public function setRows( $alias=null ) {

		$table = new Table( 'section_forms' );
		$catalog_section = $table -> select( "SELECT * FROM `catalog_section` WHERE `alias`=:alias LIMIT 1",
								array( 'alias' => $alias ) );

		if ( !count( $catalog_section ) ) {
			$this -> errorInfo = 'alias does not exist';
			return false;
		} else $sid = $catalog_section[ 0 ][ 'id' ];

		$table = new Table( 'section_forms' );
		$sections = $table -> select( "SELECT * FROM `section_forms` WHERE `id`=:id LIMIT 1",
								array( 'id' => $sid ) );
		if ( !count( $sections ) ) {
			$this -> errorInfo = 'section_form does not exist';
			return false;
		}
		$this -> _section = $sections[ 0 ];

		$rows = $table -> select( "SELECT * FROM `position_forms` WHERE `section_id`=:sid ORDER BY `position`",
							array( 'sid' => $sid ) );
		if ( !count( $sections ) ) {
			$this -> errorInfo = 'position_forms does not exist';
			return false;
		}
		$this -> _rows = $rows;
	}
	

	public function getRows( ) {
		return $this -> _rows;
	}

	public function getSection( ) {
		return $this -> _section;
	}
	


	// получить аргументы формы
	public function getArgs( ) {
		$rows = $this -> getRows( );
		if ( !count( $rows ) ) throw new Exception( 'this method not access, setRows cant be used' );
		$args = array( );
		foreach ( $rows as $row ) {
			if (	$row[ 'type_id' ] == 'link' ||
					$row[ 'type_id' ] == 'submit' ) continue;

			$key = ( string ) $row[ 'nameid' ];
			$args[ $key ] = Utils :: getVar( $key );
		}
		return $args;
	}
	
	// валидация формы
	public function valide( ) {
		$rows = $this -> getRows( );
		if ( !count( $rows ) ) throw new Exception( 'this method not access, setRows cant be used' );
		$args = $this -> getArgs( );
		$errors = array( );
		$pwd = array( );
		foreach ( $rows as $row ) {
			if ( $row[ 'type_id' ] == 'pwd' ) {
				if ( mb_strlen( $args[ $row[ 'nameid' ] ] ) < 5 ) $errors[ $row[ 'nameid' ] ][ ] = "Пароль не должен быть менее 5ти символов";
				$pwd[ ] = $row[ 'nameid' ]; // скидываем поля с паролями в массив
			}
			if ( !$row[ 'valid_empty' ] ) continue; // если поле не обязательное, мимо
			if ( !$args[ $row[ 'nameid' ] ] ) { // если значение поля не было передано с формы, обижаемся
				$errors[ $row[ 'nameid' ] ][ ] = "Введите &laquo;" . $row[ 'name' ] . "&raquo;"; // копим злость в массиве ошибок!
				// если нужна валидация на e-mail
				if ( $row[ 'valid_email' ] ) {
					// ищем подвох
					if ( !preg_match( "/[0-9a-zA-Z_\-]+@[0-9a-zA-Z_\.\-]+\.[a-zA-Z_]{2,3}/i", $row[ 'email' ] ) )
						$errors[ $row[ 'nameid' ] ][ ] .= "Адрес электронной почты введен неверно";
				}
			}
		}
		// 2 поля с паролем - регистрация
		$cnt = count( $pwd );
		if ( $cnt == 2 ) {
			if ( $pwd[ 0 ] != $pwd[ 1 ] ) {
				$errors[ 'system' ][ ] .= "Пароль и подтверждение пароля не совпадают";
			}
		}
		// 3 поля с паролем - сменить пароль в ЛК
		else if ( $cnt == 3 ) {
			if ( $pwd[ 1 ] != $pwd[ 2 ] ) {
				$errors[ 'system' ][ ] .= "Пароль и подтверждение пароля не совпадают";
			}
		}
		return $errors;
	}


	// отрисовка формы
	public function display( $row, $section, $values=array( ) ) {

		$str = '';

		$key = mb_strtolower( $row[ 'nameid' ] );

		if ( !isset( $values[ $key ] ) ) {
			$val = ( !isset( Registry :: __instance( ) -> FormArgs[ $key ] ) ) ? NULL : Registry :: __instance( ) -> FormArgs[ $key ];
		} else $val = $values[ $key ];

		$row[ 'name' ]  = ( $row[ 'valid_empty' ] ) ? $row[ 'name' ] . '*' : $row[ 'name' ];

		$str .= $row[ 'html_before' ];
/*

						array(14) {
  ["id"]=>
  string(4) "1919"
  ["section_id"]=>
  string(1) "0"
  ["email"]=>
  string(29) "cefar@mail.ru,cefar@yandex.ru"
  ["efrom"]=>
  NULL
  ["efromname"]=>
  NULL
  ["esubject"]=>
  string(68) "Косметология "Увильды", главная форма"
  ["captcha"]=>
  string(1) "0"
  ["header_mail"]=>
  NULL
  ["title_form"]=>
  NULL
  ["success_message"]=>
  string(0) ""
  ["html"]=>
  string(0) ""
  ["html_id"]=>
  string(12) "offer_header"
  ["method"]=>
  string(9) "ajax-post"
  ["action"]=>
  string(10) "send_email"
}
array(14) {
  ["id"]=>
  string(4) "1919"
  ["section_id"]=>
  string(1) "0"
  ["email"]=>
  string(29) "cefar@mail.ru,cefar@yandex.ru"
  ["efrom"]=>
  NULL
  ["efromname"]=>
  NULL
  ["esubject"]=>
  string(68) "Косметология "Увильды", главная форма"
  ["captcha"]=>
  string(1) "0"
  ["header_mail"]=>
  NULL
  ["title_form"]=>
  NULL
  ["success_message"]=>
  string(0) ""
  ["html"]=>
  string(0) ""
  ["html_id"]=>
  string(12) "offer_header"
  ["method"]=>
  string(9) "ajax-post"
  ["action"]=>
  string(10) "send_email"
}
array(14) {
  ["id"]=>
  string(4) "1919"
  ["section_id"]=>
  string(1) "0"
  ["email"]=>
  string(29) "cefar@mail.ru,cefar@yandex.ru"
  ["efrom"]=>
  NULL
  ["efromname"]=>
  NULL
  ["esubject"]=>
  string(68) "Косметология "Увильды", главная форма"
  ["captcha"]=>
  string(1) "0"
  ["header_mail"]=>
  NULL
  ["title_form"]=>
  NULL
  ["success_message"]=>
  string(0) ""
  ["html"]=>
  string(0) ""
  ["html_id"]=>
  string(12) "offer_header"
  ["method"]=>
  string(9) "ajax-post"
  ["action"]=>
  string(10) "send_email"
}
array(14) {
  ["id"]=>
  string(4) "1919"
  ["section_id"]=>
  string(1) "0"
  ["email"]=>
  string(29) "cefar@mail.ru,cefar@yandex.ru"
  ["efrom"]=>
  NULL
  ["efromname"]=>
  NULL
  ["esubject"]=>
  string(68) "Косметология "Увильды", главная форма"
  ["captcha"]=>
  string(1) "0"
  ["header_mail"]=>
  NULL
  ["title_form"]=>
  NULL
  ["success_message"]=>
  string(0) ""
  ["html"]=>
  string(0) ""
  ["html_id"]=>
  string(12) "offer_header"
  ["method"]=>
  string(9) "ajax-post"
  ["action"]=>
  string(10) "send_email"
}
array(14) {
  ["id"]=>
  string(4) "1919"
  ["section_id"]=>
  string(1) "0"
  ["email"]=>
  string(29) "cefar@mail.ru,cefar@yandex.ru"
  ["efrom"]=>
  NULL
  ["efromname"]=>
  NULL
  ["esubject"]=>
  string(68) "Косметология "Увильды", главная форма"
  ["captcha"]=>
  string(1) "0"
  ["header_mail"]=>
  NULL
  ["title_form"]=>
  NULL
  ["success_message"]=>
  string(0) ""
  ["html"]=>
  string(0) ""
  ["html_id"]=>
  string(12) "offer_header"
  ["method"]=>
  string(9) "ajax-post"
  ["action"]=>
  string(10) "send_email"
}

	
*/
		//var_dump($section  );
		
		switch ( $row[ 'type_id' ] ) {


			case 'label':

				$str .= "
					<p class='form-control'>" . $row[ 'name' ] . "</p>";

			break;


			case 'radiobox':

				$str .= "
					<input type='radio' class='form-control radio' name='" . $row[ 'nameid' ] . "' value='" . $row[ 'name' ] . "' id='radiobox-" . $row[ 'id' ] . "' />";

			break;


			case 'text':

				// val( 'catalog.editor.btn_position', array( 'section_id' => $section[ 'id' ], 'position_id' => $row[ 'id' ] ) );
				if ( $row[ 'select_options' ] ) {
				}

				$placeholder = $row[ 'name' ];
				if($row[ 'name' ] == '*') {
					$placeholder = "";
				}

				$str .= "
						<input type='text' required='' class='form-control' name='" . $row[ 'nameid' ] . "' placeholder='" . $placeholder . "' id='" . $row[ 'nameid' ] . '_' . $row['section_id'] . "' />";

			break;


			case 'memo':

				if ( $row[ 'select_options' ] ) {
				}

				$str .= "
					<textarea placeholder='" . $row[ 'name' ] . "' name='" . $row[ 'nameid' ] . "' class='form-control' required='' id='" . $row[ 'nameid' ] . '_' . $row['section_id'] . "'></textarea>";

			break;

				
			case 'select':

				$options = explode( "\n", $row[ 'select_options' ] );
				$str .= "
						<select class='form-control' name='" . $row[ 'nameid' ] . "'>
							<option value='' disabled selected hidden>" . $row[ 'name' ] . "</option>";
				if ( count( $options ) ) {
					foreach ( $options as $option ) {
						$str .= "
							<option>" . $option . "</option>";
					}
				}
				$str .= "
						</select>";
			break;


			case 'upload':

				$str .= "
						<input type='file' name='" . $row[ 'nameid' ] . "' />
						<div class='general-button inverse-button'>
							<button>" . $row[ 'name' ] . "</button>
						</div>";
			break;


			case 'date':

				if ( $row[ 'select_options' ] ) {
					$str .= "
						<div class='b_afb_box'>" . $row[ 'select_options' ] . "</div>";
				}

			break;


			case 'pwd':

				if ( $row[ 'select_options' ] ) {
				}

				$str .= "
					<input type='password' class='form-control' placeholder='" . $row[ 'name' ] . "' name='" . $row[ 'nameid' ] . "' />";

			break;


			case 'check':

				if ( $val ) $check = ' CHECKED';
				else $check = '';
				
				if ( strpos( $section[ 'html_id' ], 'offer_' ) !== false ) {
				$str .= "
						<input required type='checkbox' class='checkbox form-control offer-field' name='" . $row[ 'nameid' ] . "' id='" . $row[ 'nameid' ] . "' />";
				}
				else {
				$str .= "
						<input required type='checkbox' class='checkbox form-control hidden-field' name='" . $row[ 'nameid' ] . "' id='" . $row[ 'nameid' ] . "' />";
				}
				break;

			case 'link':
				$str .= "
				<a data-toggle='modal' data-target='#" . $row[ 'nameid' ] . "' class='" . $row[ 'nameid' ] . "' href='#' data-dismiss='modal' aria-hidden='true'>" . $row[ 'name' ] . "</a>";

			break;


			case 'hidden':
				$str .= "
					<input type='hidden' name='" . $row[ 'nameid' ] . "' value='" . $row[ 'name' ] . "' />";
			break;


			case 'submit':
			
			if ( strpos( $section[ 'html_id' ], 'offer_' ) !== false ) {
				$str .= "
					<button type='submit' class='btn btn-md btn-red btn-wide'>" . $row[ 'name' ] . "</button>";
			}
			else {
				$str .= "
					<button type='submit'>" . $row[ 'name' ] . "</button>";
			}
			
				break;

		}

		$str .= $row[ 'html_after' ];
		
		return $str;

	}

	
	
	public static function parse_mail_tpl( $tpl='', $args=array(), $debug=false ) {

		if ( !count( $args ) ) return $tpl;

		if ( $debug ) {
			$args[ 'server' ] = self :: server_tpl();
			$args[ 'context' ] = self :: context_tpl();
			$args[ 'sms' ] = self :: sms_tpl();
			$args[ 'subscribe' ] = self :: subscribe_tpl();
		}

		$pattern = "/{[^}]*}/";
		preg_match_all( $pattern, $tpl, $matches );

		$matches = end( $matches );
		$arr_key = array( );

		foreach ( $matches as $key ) {

			$key = str_replace( '{', '', $key );
			$key = str_replace( '}', '', $key );
			$key_lw = mb_strtolower( $key );

			if ( !isset( $args[ $key_lw ] ) ) $replace = '';
			else $replace = $args[ $key_lw ];

			$tpl = str_replace( '{' . $key . '}', $replace, $tpl );

		}

		return $tpl;

	}

	
	
	
	
	
	
	
	

	/*
		
	*/
	public static function server_tpl( ) {

		$source = 'ТЕХНИЧЕСКАЯ ИНФОРМАЦИЯ';

		$arr_src = array( );
		$arr_src[ ] = '{SERVER.SOURCE}';
		$arr_src[ ] = '{SERVER.HTTP_USER_AGENT}';
		$arr_src[ ] = '{SERVER.REMOTE_ADDR}';
		$arr_src[ ] = '{SERVER.HTTP_X_FORWARDED_FOR}';
		$arr_src[ ] = '{SERVER.REQUEST_TIME}';

		$arr_rps = array( );
		$arr_rps[ ] = $source;
		$arr_rps[ ] = ( ( isset( $_SERVER[ 'HTTP_USER_AGENT' ] ) ) ? $_SERVER[ 'HTTP_USER_AGENT' ] : '' );
		$arr_rps[ ] = ( ( isset( $_SERVER[ 'REMOTE_ADDR' ] ) ) ? $_SERVER[ 'REMOTE_ADDR' ] : '' );
		$arr_rps[ ] = ( ( isset( $_SERVER[ 'HTTP_X_FORWARDED_FOR' ] ) ) ? $_SERVER[ 'HTTP_X_FORWARDED_FOR' ] : '' );
		$arr_rps[ ] = ( ( isset( $_SERVER[ 'REQUEST_TIME' ] ) ) ? $_SERVER[ 'REQUEST_TIME' ] : '' );

		$str = '

			<table bgcolor="#fff" width="400" style="padding:10px;padding-top:30px;" align="center">
				<tbody>
					<tr>
						<td align="center" style="padding: 10px;color: #dcdcdc;" width="100%" colspan="2">----------------------------------------</td>
					</tr>
					<tr>
						<td align="center" style="padding: 10px;color: #dcdcdc;" width="100%" colspan="2">{SERVER.SOURCE}</td>
					</tr>
					<tr>
						<td align="" style="padding: 5px;color: #dcdcdc;" width="50%">Устройство</td>
						<td style="padding: 5px;color: #dcdcdc;">{SERVER.HTTP_USER_AGENT}</td>
					</tr>
					<tr>
						<td align="" style="padding: 5px;color: #dcdcdc;" width="50%">IP</td>
						<td style="padding: 5px;color: #dcdcdc;">{SERVER.REMOTE_ADDR}</td>
					</tr>
					<tr>
						<td align="" style="padding: 5px;color: #dcdcdc;" width="50%">Подсеть</td>
						<td style="padding: 5px;color: #dcdcdc;">{SERVER.HTTP_X_FORWARDED_FOR}</td>
					</tr>
					<tr>
						<td align="" style="padding: 5px;color: #dcdcdc;" width="50%">Время</td>
						<td style="padding: 5px;color: #dcdcdc;">{SERVER.REQUEST_TIME}</td>
					</tr>
				</tbody>
			</table>';

		return str_replace( $arr_src, $arr_rps, $str );
	}



	/*
		
	*/
	public static function context_tpl() {

		if ( empty( $_SESSION[ 'yandex_direct' ] ) ) return false;

		$source = 'КОНТЕКСТНАЯ РЕКЛАМА';

		if ( $_SESSION[ 'yandex_direct' ][ 'utm_source' ] == 'yandex_poisk' ) {
			$source = 'КОНТЕКСТНАЯ РЕКЛАМА YANDEX DIRECT';
			$clid = 'yclid';
		}
		else {
			$source = 'КОНТЕКСТНАЯ РЕКЛАМА GOOGLE ADWORDS';
			$clid = 'gclid';
		}


		$arr_src = array( );
		$arr_src[ ] = '{CONTEXT.UTM_SOURCE}';
		$arr_src[ ] = '{CONTEXT.UTM_CAMPAIGN}';
		$arr_src[ ] = '{CONTEXT.UTM_CONTENT}';
		$arr_src[ ] = '{CONTEXT.UTM_TERM}';
		$arr_src[ ] = '{CONTEXT.CLID}';


		$arr_rps = array( );
		$arr_rps[ ] = $source;
		$arr_rps[ ] = ( ( isset( $_SESSION[ 'yandex_direct' ][ 'utm_campaign' ] ) ) ? $_SESSION[ 'yandex_direct' ][ 'utm_campaign' ] : '' );
		$arr_rps[ ] = ( ( isset( $_SESSION[ 'yandex_direct' ][ 'utm_content' ] ) ) ? $_SESSION[ 'yandex_direct' ][ 'utm_content' ] : '' );
		$arr_rps[ ] = ( ( isset( $_SESSION[ 'yandex_direct' ][ 'utm_term' ] ) ) ? $_SESSION[ 'yandex_direct' ][ 'utm_term' ] : '' );
		$arr_rps[ ] = ( ( isset( $_SESSION[ 'yandex_direct' ][ $clid ] ) ) ? $_SESSION[ 'yandex_direct' ][ $clid ] : '' );

		$str = '

			<table bgcolor="#fff" width="400" style="padding:10px;padding-top:30px;" align="center">
				<tbody>
					<tr>
						<td align="center" style="padding: 10px;color: #dcdcdc;" width="100%" colspan="2">----------------------------------------</td>
					</tr>
					<tr>
						<td align="center" style="padding: 10px;color: #dcdcdc;" width="100%" colspan="2">{CONTEXT.UTM_SOURCE}</td>
					</tr>
					<!--tr>
						<td align="" style="padding: 5px;color: #dcdcdc;" width="50%">Компания</td>
						<td style="padding: 5px;color: #dcdcdc;">{CONTEXT.UTM_CAMPAIGN}</td>
					</tr>
					<tr>
						<td align="" style="padding: 5px;color: #dcdcdc;" width="50%">Содержимое</td>
						<td style="padding: 5px;color: #dcdcdc;">{CONTEXT.UTM_CONTENT}</td>
					</tr>
					<tr-->
						<td align="" style="padding: 5px;color: #dcdcdc;" width="50%">Ключевое слово</td>
						<td style="padding: 5px;color: #dcdcdc;">{CONTEXT.UTM_TERM}</td>
					</tr>
					<tr>
						<td align="" style="padding: 5px;color: #dcdcdc;" width="50%">Клиент</td>
						<td style="padding: 5px;color: #dcdcdc;">{CONTEXT.CLID}</td>
					</tr>
				</tbody>
			</table>';

		return str_replace( $arr_src, $arr_rps, $str );
	}


	public static function subscribe_tpl() {
	
		if ( empty( $_SESSION[ 'subscribe' ] ) ) return false;

		$source = 'E-MAIL РАССЫЛКА';

		$arr_src = array( );
		$arr_src[ ] = '{SUBSCRIBE.SOURCE}';

		$arr_rps = array( );
		$arr_rps[ ] = $source;


		$str = '
			<table bgcolor="#fff" width="400" style="padding:10px;padding-top:30px;" align="center">
				<tbody>
					<tr>
						<td align="center" style="padding: 10px;color: #dcdcdc;" width="100%" colspan="2">----------------------------------------</td>
					</tr>
					<tr>
						<td align="center" style="padding: 10px;color: #dcdcdc;" width="100%" colspan="2">{SUBSCRIBE.SOURCE}</td>
					</tr>
				</tbody>
			</table>';

		return str_replace( $arr_src, $arr_rps, $str );
	}


	public static function sms_tpl() {

		if ( empty( $_SESSION[ 'sms' ] ) ) return false;

		$source = 'SMS РАССЫЛКА';

		$arr_src = array( );
		$arr_src[ ] = '{SMS.SOURCE}';

		$arr_rps = array( );
		$arr_rps[ ] = $source;


		$str = '
			<table bgcolor="#fff" width="400" style="padding:10px;padding-top:30px;" align="center">
				<tbody>
					<tr>
						<td align="center" style="padding: 10px;color: #dcdcdc;" width="100%" colspan="2">----------------------------------------</td>
					</tr>
					<tr>
						<td align="center" style="padding: 10px;color: #dcdcdc;" width="100%" colspan="2">{SMS.SOURCE}</td>
					</tr>
				</tbody>
			</table>';

		return str_replace( $arr_src, $arr_rps, $str );
	}
	
	
	
	
	
	
}

