<?php 

class Import1c {

	const GOODS_PATH = '1c_upload/webdata/000000001/goods/1/';

	const PROPERTIES_PATH = '1c_upload/webdata/000000001/properties/1/';

	public function getGoods() {

		$file_name = 'import___83069f00-62ee-4643-86c2-486c958769f5.xml';

		if( file_exists( self::GOODS_PATH . $file_name ) ) {
			return simplexml_load_file( self::GOODS_PATH . $file_name );
		}
		else {
			return false;
		}

	}

	public function getNumberTypes() {

		$file_name = 'offers___d4177b97-711c-4425-81fc-7dd68c6389dd.xml';

		if( file_exists( self::PROPERTIES_PATH . $file_name ) ) {
			return simplexml_load_file( self::PROPERTIES_PATH . $file_name );
		}
		else {
			return false;
		}

	}

	public function getPrices() {

		$file_name = 'prices___a8322c88-e2d7-4158-ac21-53f2b5899da5.xml';

		if( file_exists( self::GOODS_PATH . $file_name ) ) {
			return simplexml_load_file( self::GOODS_PATH . $file_name );
		}
		else {
			return false;
		}

	}

	public function getRests() {

		$file_name = 'rests___918a958c-df9e-4024-ab1d-7d798adc9ea9.xml';

		if( file_exists( self::GOODS_PATH . $file_name ) ) {
			return simplexml_load_file( self::GOODS_PATH . $file_name );
		}
		else {
			return false;
		}

	}

}