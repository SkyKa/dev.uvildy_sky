<?php

  // http://www.robokassa.ru/ru/Doc/Ru/Interface.aspx#223



  class Pay_robokassa {

    private $_path_to_log = '/billing/log.php';

  
    private $_mrh_login   = 'test';         // your login here
    private $_inv_id      = 5;              // shop's invoice number

    private $_mrh_pass1   = "securepass1";  // merchant pass1 here
    private $_mrh_pass2   = "securepass2";  // merchant pass2 here
    private $_culture     = "ru-RU";

    private $_root_id     = 20000;          // ид пользователя, на счет которого
                                            // будут приходить деньги
                                            // в исключительных ситуациях

    private $_shp_uid;

    // статусы оплаты
    const S_CLEAR    = 'clear';
    const S_ENABLED  = 'enabled';
    const S_DISABLED = 'disabled';


    public function __construct( $conf ) {
    
      $this -> _path_to_log = $_SERVER[ 'DOCUMENT_ROOT' ] . $this -> _path_to_log; 
    
      $this -> _mrh_login  = $conf[ 'LOGIN' ];
      $this -> _mrh_pass1  = $conf[ 'PASS1' ];
      $this -> _mrh_pass2  = $conf[ 'PASS2' ];
    }
    
    
    // получить uid пользователя
    public function getUserId( ) {
          if ( !$this -> _shp_uid ) {
            return ( !isset( $_SESSION[ 'user_id' ] ) ) ? $this -> _root_id : $_SESSION[ 'user_id' ];
          }
          return $this -> _shp_uid;
    }
    
    
    // изменить uid пользователя 
    public function setUserId( $uid ) {
      $this -> _shp_uid = $uid;
    }


    // получить name пользователя
    public function getUserName( ) {
    	$uid = $this -> getUserId( );
		$table = new Table( 'catalog_section' );
		$rows =	$table -> select( "SELECT * FROM `catalog_section` WHERE `id`=:id LIMIT 1",
				array( 'id' => $uid ) );
		if ( $table -> errorInfo ) throw new Exception( $table -> errorInfo );
		if ( !count( $rows ) || !isset( $rows[ 0 ][ 'title' ] ) ) throw new Exception( 'user not found' );
		return $rows[ 0 ][ 'title' ];
    }


    // Формирование URL переадресации пользователя на оплату
    // $our_summ ( float ) - сумма заказа
    // $inv_desc ( string ) - описание заказа
    public function PayLink( $out_summ, $inv_desc, $shp_uid=null ) {

      $this->_log(  'Передача параметров out_sum : ' . $out_summ . ', inv_desc : ' .
                    $inv_desc . ', shp_uid : ' . $shp_uid );


      $shp_uid_param = ( isset( $shp_uid ) && !is_null( $shp_uid ) ) ? "&Shp_uid=" . $shp_uid : NULL;
      $shp_uid_crc   = ( isset( $shp_uid ) && !is_null( $shp_uid ) ) ? ":Shp_uid=" . $shp_uid : NULL;

      // build CRC value
      $crc  = md5( $this -> _mrh_login . ":" .
                   $out_summ . ":" .
                   $this -> _inv_id . ":" .
                   $this -> _mrh_pass1 .
                   $shp_uid_crc );

      // build URL
      $url =  "https://auth.robokassa.ru/Merchant/Index.aspx?MrchLogin=" .
              $this -> _mrh_login . "&OutSum=" . $out_summ . "&InvId=" .
              $this -> _inv_id . "&Desc=" . $inv_desc . $shp_uid_param .
              "&SignatureValue=" . $crc;

      return $url;
    }


    // формирование crc hash
    public function PayCrc( $our_summ, $shp_uid=null ) {


      $shp_uid   = ( isset( $shp_uid ) && !is_null( $shp_uid ) ) ? ":Shp_uid=" . $shp_uid : NULL;
      $inv_id    = $this -> _clearRecord( $our_summ, "request" );

      $crc  = md5(  $this -> _mrh_login . ":" .
                    $our_summ . ":" .
                    $inv_id . ":" .
                    $this -> _mrh_pass1 .  
                    $shp_uid );
      return $crc;
    }


    // Получение уведомления об исполнении операции (ResultURL)  
    public function ResultURL( ) {

      // HTTP parameters:
      $out_summ   = $_REQUEST[ "OutSum" ];
      $inv_id     = $_REQUEST[ "InvId" ];
      $shp_uid    = ( isset( $_REQUEST[ "Shp_uid" ] ) ) ? ":Shp_uid=" . $_REQUEST[ "Shp_uid" ] : NULL;
      $culture    = $this -> _culture;
      $crc        = $_REQUEST[ "SignatureValue" ];

      $crc = strtoupper( $crc );   // force uppercase

	  
      // build own CRC
      $my_crc = strtoupper( md5(  $out_summ . ":" .
                                  $inv_id . ":" .
                                  $this -> _mrh_pass2 . $shp_uid ) );

      if ( $my_crc != $crc ) {
        $this -> _log( 'crc not valid : post -> ' .  $my_crc  . ' != ' . $crc );
        $this->_disableRecord( );
        return false;
      }

      return true;

    }

    
    // Проверка параметров в скрипте завершения операции ( SuccessURL )
    public function SuccessURL( ) {

      // HTTP parameters:
      $out_summ = $_REQUEST[ "OutSum" ];
      $inv_id   = $_REQUEST[ "InvId" ];
      $shp_uid   = ( isset( $_REQUEST[ "Shp_uid" ] ) ) ? ":Shp_uid=" . $_REQUEST[ "Shp_uid" ] : NULL;
      $crc      = $_REQUEST[ "SignatureValue" ];

      $crc = strtoupper( $crc );  // force uppercase

      // build own CRC
      $my_crc = strtoupper( md5(  $out_summ . ":" .
                                  $inv_id . ":" .
                                  $this -> _mrh_pass1 . $shp_uid ) );

      if ( $my_crc != $crc ) {
        return false;
      }

  		$this -> _enabledRecord( $out_summ, $inv_id );
      $this -> _log( 'Транзация оплачена'  . "\n"  );
      return true;

    }

	
	public function Fail( ) {
    $this -> _disableRecord( );
    echo "Отмена платежа<br />\n";
  }
	
	
	

	// установка текущего времени
	// current date	
	private function _date( ) {

		$tm = getdate( time( ) + 9 * 3600 );

		$date = $tm[ 'year' ] . "-" . $tm[ 'mon' ] . "-" .
			  $tm[ 'mday' ] . " " .
			  $tm[ 'hours' ] . ":" .
			  $tm[ 'minutes' ] . ":" .
			  $tm[ 'seconds' ];

		return $date;
	}




	// отменено ( Закрываем запись )
	private function _disableRecord( ) {
	
    $inv_id = ( !isset( $_POST[ 'inv_id' ] ) ) ? null : $_POST[ 'inv_id' ]; 
	
		$uid = $this -> getUserId( );
	
		$table = new Table( 'position_pay' );
		$rows = $table -> select( "SELECT id FROM `position_pay` WHERE `id`=:id AND `status`='" . self :: S_CLEAR . "' LIMIT 1",
            array( 'id' => $inv_id ) );
 
		if ( $table -> errorInfo ) throw new Exception( $table -> errorInfo );

		if ( count( $rows ) ) {

		  $res = $table -> execute( "UPDATE `position_pay` SET `status`=:st, `enddatestamp`=:dt
                                WHERE `id`=:id LIMIT 1",
                        array(  'st' => self :: S_DISABLED,
                                'id' => $rows[ 0 ][ 'id' ],
                                'dt' => $this -> _date( ) ) );

		  if ( $table -> errorInfo ) throw new Exception( $table -> errorInfo );
      $this -> _log( 'Транзация отменена' . "\n" );
		}
	}


	// оплачено ( Упдейт записи )
	private function _enabledRecord( $out_summ=null, $inv_id=null ) {

		$uid = $this -> getUserId( );
	
		$table = new Table( 'position_pay' );
		$rows = $table -> select( "SELECT id FROM `position_pay` WHERE `id`=:id AND `status`='" . self :: S_CLEAR . "' LIMIT 1",
            array( 'id' => $inv_id ) );
 
		if ( $table -> errorInfo ) throw new Exception( $table -> errorInfo );

		if ( count( $rows ) ) {

		  $res = $table -> execute( "UPDATE `position_pay` SET `status`=:st, `summ`=:summ, `enddatestamp`=:dt
                                WHERE `id`=:id LIMIT 1",
                        array(  'st' => self :: S_ENABLED,
                                'summ' => $out_summ,
                                'id' => $rows[ 0 ][ 'id' ],
                                'dt' => $this -> _date( ) ) );
		  if ( $table -> errorInfo ) throw new Exception( $table -> errorInfo );

      $this -> _log( 'Транзация оплачена'  . "\n"  );

		  $this -> _upMoney( $uid, $out_summ );
    }

		return true;
	}

	// создание новой дефолтной записи
	// она же возвращает Ид
	public function clearRecord( $out_summ=null, $desc=null, $user_id=null ) {

    $table = new Table( 'catalog_section' );

    $uid = $this -> getUserId( );

    $section = $table -> getEntity( $uid );
    $section = ( !$section ) ? $table -> getEntity( $this -> _root_id ) : $section;

    $user_name = $section -> title;

    $table = new Table( 'position_pay' );

    $item = $table -> select( 'SELECT * FROM `position_pay` WHERE `section_id`=:sid && `status`=:status LIMIT 1',
    			             array(  'sid' => $section -> id,
    					             'status' => self :: S_CLEAR ) );

    if ( count( $item ) ) {
      $id = $item[ 0 ][ 'id' ];
      $this -> _inv_id = $id;
      return $id;
    }

    if ( $table -> errorInfo ) {
      throw new Exception( $table -> errorInfo );
    }

    // установка текущего времени
    $date = $this -> _date( );

    $pay = new stdClass;

    $pay -> section_id = $section -> id;
    $pay -> title = $desc;
    $pay -> summ = 0;
    $pay -> sign = '+';
    $pay -> date = $date;
    $pay -> status = self :: S_CLEAR;

    $lid = $table -> save( $pay );
    if ( $table -> errorInfo ) throw new Exception( $table -> errorInfo );
    $this -> _inv_id = $lid;

    $this -> _log( '' );
    $this -> _log( 'Новая транзакция [ ' . $lid . ' -> ' . $user_name . ' ]' );
    
    return $lid;

	}


	// метод зачисления средств пользователю
	private function _upMoney( $uid, $out_summ ) {

		$table = new Table( 'section_users' );
		$user = ( !$table -> getEntity( $uid ) ) ? new stdClass( ) : $table -> getEntity( $uid );
		$user -> pay += $out_summ;
		$table -> save( $user );
		if ( $table -> errorInfo ) throw new Exception( $table -> errorInfo );
		$this -> _log( 'Зачисляем деньги на счет +' . $out_summ );
	}

	// метод списания средств пользователю
	private function _downMoney( $uid, $out_summ ) {

		$table = new Table( 'section_users' );
		$user = ( !$table -> getEntity( $uid ) ) ? new stdClass( ) : $table -> getEntity( $uid );
		$user -> pay -= $out_summ;
		$table -> save( $user );
		if ( $table -> errorInfo ) throw new Exception( $table -> errorInfo );
		$this -> _log( 'Списываем деньги со счета -' . $out_summ );
	}
	
	
	
	// метод логирования
  private function _log( $write ) {
    $file = $this -> _path_to_log;
    $f = @fopen( $file, 'a+' );
    if ( !$f ) throw new Exception( 'not create billing loging file' );
    fwrite( $f, "#" . $write . "\n" );
    fclose( $f );
    
    
  }	
	

}