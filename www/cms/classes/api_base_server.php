<?php

class Api_Base_Server {

	private $_debug = false;

	private $_key;
	private $_permissions;

	private $_route = array( );

	private $_db;
	private $_table = 'catalog_section';

	private $_params;

	public $_type;
	public $_section;
	public $_data_tables = array( );
	
	private $_header = 200;
	
	private $_operations = array(
		'.' => 'CONCAT',
		'+' => 'SUM',
		'-' => 'DIF',
		'/' => 'DIV',
		'*' => 'MULTI'
	);

/*
200 OK
400 Bad Request (некорректный запрос)
500 Internal Server Error (внутренняя ошибка сервера)

backend_error (503)	Ошибка сервера.
invalid_parameter (400)	Неверно задан параметр.
not_found (404)	Указанный объект не найден.
missing_parameter (400)	Не указан необходимый параметр.
access_denied (403)	Доступ запрещен.
unauthorized (401)	Неавторизованный пользователь.
quota (429)	Превышен лимит количества запросов к API.
query_error (400)	Запрос слишком сложный.
conflict (409)	Нарушение целостности данных
invalid_json (400)	Переданный JSON имеет неверный формат.
*/

	private $_headers = array(
		200 => 'HTTP/1.1 OK',
		400 => 'HTTP/1.1 400 Bad Request',
		401 => 'HTTP/1.1 401 Unauthorized',
		403 => 'HTTP/1.0 403 Forbidden',
		409 => 'HTTP/1.1 409 Conflict',
		429 => 'HTTP/1.1 429 Too Many Requests',
		500 => 'HTTP/1.1 500 Internal Server Error',
		503 => 'HTTP/1.1 503 Service Unavailable'
	);


	private $_auth_methods = array(
		'Basic Auth',
//		'Digest Auth',
//		'OAuth 1.0',
//		'OAuth 2.0',
//		'Hawk Authenticate',
//		'AWS Signature'
	);
	
	
	private $_access_methods = array(
		'GET',
		'POST',
		'PUT',
		'DELETE'
		);

	private $_access_format = array(
		'JSON',
		'XML',
//		'CSV'
	);

	private $_method = 'GET';
	private $_format = 'JSON';
	private $_body = false;

	/**
		'params'
		'format'
		'route'
		'debug'
	*/
	
	
	private $_encrypt_key = '';
	private $_encrypt_type = 'public';
	
	
	
	
	
	
	public function __construct( $conf=array( ) ) {

		if ( !isset( $conf[ 'params' ] ) ) throw new Exception( 'invalid request ' . __LINE__ );
		if ( !isset( $conf[ 'method' ] ) ) throw new Exception( 'invalid request ' . __LINE__ );
		if ( array_search( $conf[ 'method' ], $this -> _access_methods ) === false ) throw new Exception( 'invalid request ' . __LINE__ );
		$this -> _method = $conf[ 'method' ];
		if ( isset( $conf[ 'route' ] ) && !is_array( $conf[ 'route' ] ) ) throw new Exception( 'invalid request ' . __LINE__ );

		$this -> _route = ( isset( $conf[ 'route' ] ) ) ? $conf[ 'route' ] : array( );
		$this -> _format = ( isset( $conf[ 'format' ] ) && array_search( $conf[ 'format' ], $this -> _access_format ) !== false ) ? $conf[ 'format' ] : 'JSON';
		$this -> _debug = ( isset( $conf[ 'debug' ] ) && $conf[ 'debug' ] ) ? true : false;
		//$this -> _body = $this -> { '_decode' . $this -> _format }( file_get_contents( 'php://input' ) );

		$path_rsa_public = realpath( dirname( __FILE__ ) . '/../..' ) . DS . 'cms' . DS . 'rsa' . DS . 'pck';
		$pk = file_get_contents( $path_rsa_public );
		$this -> _encrypt_key = openssl_get_publickey( $pk );

		$this -> _db = new Table( 'catalog_section' );

		//////////////////////////////////////////////////////////
		if ( isset( $conf[ 'auth' ][ 'method' ] ) && isset( $conf[ 'auth' ][ 'users' ] ) && 
			 isset( $conf[ 'auth' ][ 'realm' ] ) && 
			 $conf[ 'auth' ][ 'method' ] == 'Basic Auth' ) {
				$this -> auth_basic( $conf[ 'auth' ][ 'users' ], $conf[ 'auth' ][ 'realm' ] );
		}
		
		//////////////////////////////////////////////////////////////
		// МЕТОД ВЫВОДИТ СТРУКТУРУ ПРОЕТА 
		if ( $conf[ 'params' ][ 0 ] == 'get_chain' ) {
			//ob_start( 'ob_gzhandler' );
			$this -> header( );
			$r[ 's' ] = true;
			$r[ 'code' ] = 200;
			$r[ 'message' ] = 'Успешый запрос';
			$r[ 'data' ] = $this -> get_chain( );
			$r[ 'route' ] = $this -> _route;

			//////////////////////////////////////////////////////////////////////////
			// ДОШИФРОВКА ИНФОРМАЦИИ ПЕРЕД ВЫВОДОМ
			$encrypt_filter = $this -> make_encrypt_filter( );
			$r[ 'filter' ] = $encrypt_filter;
			echo $this -> encode( $r );
			exit( 1 );
		}

		$p = $this -> _router_map_params( $conf[ 'params' ] );
		$this -> _params = $p[ 'params' ];
		$this -> _type = $p[ 'type' ];
		$this -> _section = $this -> _router_check_params( );

	}

	
	// собирает названия зашифрованных полей
	public function make_encrypt_filter( ) {
		$crypt_fields = array( );
		if ( count( $this -> _data_tables ) ) {
			$c = Config :: getConfig( 'catalog' );
			foreach( $this -> _data_tables as $table => $b ) {
			if ( !isset( $c -> { $table } ) ) continue;
			$cols = $c -> { $table };
				foreach ( $cols as $col => $v ) {
					if ( $col == 'id' ) continue;
					if ( !isset( $c -> { $table }[ $col ] ) ) continue;
					if ( $c -> { $table }[ $col ][ 'type' ] == 'encode' ) {
						$crypt_fields[ $col ] = $table;
					}
				}
			}
		}
		return $crypt_fields;
	}



	// шифруем данные
	private function _encrypt( $source ) {
		$key_details = openssl_pkey_get_details( $this -> _encrypt_key );
		$decrypt_chunk_size = ceil( $key_details[ 'bits' ] / 8 );
		$maxlength = $decrypt_chunk_size;
		$output = '';
		while ( $source ) {
		  $input = substr( $source, 0, $maxlength );
		  $source = substr( $source, $maxlength );
		  if ( $this -> _encrypt_type == 'private' ) {
			$ok = openssl_private_encrypt( $input, $encrypted, $this -> _encrypt_key );
		  } else {
			$ok = openssl_public_encrypt( $input, $encrypted, $this -> _encrypt_key );
		  }
		  $output .= $encrypted;
		}
		return $output;
	}
	
	
	
	
	
	
	
	
	
	
	//////////////////////////////////////////////////////////////////
	// INDEXED
	//////////////////////////////////////////////////////////////////

	/**
		ПОЛУЧАЕТ СПИСОК СЕКЦИЙ
	*/
	public function get_chain( ) {
		$id_parent = array( );
		$parent_id = array( );
		$items = array( );
		$array = array( );
		$return = array( );
		$chains = $this -> _chain_flat_array( );
		foreach( $chains as $key => $chain ) {
			$a = $this -> _chain_build_tree( $chain ); // плоский массив в дерево
			foreach ( $chain as $c ) {
				$items[ $c[ 'alias' ] ] = $c;
			}
			$a = end( $a );  // убираем многомерный массив
			$this -> _chain_tree_to_path( $a, 'alias', 'children', '/', $array ); // пишим в array склееные параметры
		}
		unset( $a, $key, $chain, $last );
		//var_dump( '11111111111111', $items, '11111111111111' );
		//var_dump( '22222222222222', $array, '22222222222222' );
		foreach ( $array as $path ) {
			$ex = explode( '/', $path );
			$last = array_pop( $ex );
			if ( $last == '_items' ) {
				$last2 = array_pop( $ex );
				$last = $last2 . '/' . $last;
			}
			$items[ $last ][ 'alias' ] = str_replace( '/_items', '', $items[ $last ][ 'alias' ] );
			$items[ $last ][ 'alias' ] = str_replace( '/_extended', '', $items[ $last ][ 'alias' ] );
			$return[ $path . '/' ] = $items[ $last ];
		}
		
		return $return;
	}

	
	/**
		ИЗВЛЕКАЕТ ПУТЬ УЗЛОВ АПИ ИЗ ДЕРЕВА
	*/
	public function _chain_tree_to_path( $a, $keyname, $parent, $glue, & $rtn, $pre="" ) {
	  $_pre = "";
	  if ( $a[ $keyname ] ) {
		$rtn[ ] = $_pre = $pre . $glue . $a[ $keyname ];
	  }
	  if ( $a[ $parent ] ) {
		if( is_array($a[ $parent ] ) ) {
		  foreach( $a[ $parent ] as $c )
			$this -> _chain_tree_to_path($c, $keyname, $parent, $glue, $rtn, $_pre );
		} else {
		  $this -> _chain_tree_to_path( $a[ $parent ], $keyname, $parent, $glue, $rtn, $_pre );
		}
	  }
	  $qtd = count( $rtn );
	  return ( isset( $rtn[ -1 ] ) ) ? $rtn[ -1 ] : null;
	}



	/**
		ПРЕОБРАЗОВЫВАЕТ ПЛОСКИЙ МАССИВ СЕКЦИЙ В МАССИВ ДЕРЕВО
	*/
	protected function _chain_build_tree( $tree, $root = null ) {
		$return = array( );
		foreach( $tree as $child => $parent ) {
			if ( isset( $parent[ 'parent_id' ] ) && $parent[ 'parent_id' ] == $root ) {
				unset( $tree[ $child ] );
				$parent[ 'children' ] = $this -> _chain_build_tree( $tree, $child );
				$return[ $parent[ 'id' ] ] = $parent;
			}
		}
		return empty( $return ) ? null : $return;    
	}


	/**
		ФОРМИРУЕТ ПЛОСКИЙ МАССИВ СЕКЦИЙ
	*/
	protected function _chain_flat_array( $parents=array( ), $index=0, $main='', $ret=array( ) ) {
		$str = '';
		$parent_cnt = count( $parents );
		if ( !$parent_cnt ) {
			$rows = $this -> _db -> select( 'SELECT * FROM `catalog_section` WHERE isNull(`parent_id`) ORDER BY `position`' );
			++$index;
			$ret = $this -> _chain_flat_array( $rows, $index, $main, $ret );
			--$index;
		}
		else {

			foreach ( $parents as $parent ) {

				if ( $index == 1 ) {
					$main = $parent[ 'alias' ];
					$parent_id = 0;
				}
				else {
					$parent_id = $parent[ 'parent_id' ];
				}

				if ( !isset( $ret[ $main ][ $parent[ 'id' ] ] ) ) $ret[ $main ][ $parent[ 'id' ] ] = array( );

				$ret[ $main ][ $parent[ 'id' ] ][ 'parent_id' ] = $parent_id;				
				$ret[ $main ][ $parent[ 'id' ] ][ 'id' ] = $parent[ 'id' ];
				$ret[ $main ][ $parent[ 'id' ] ][ 'alias' ] = $parent[ 'alias' ];

				$tpl = $parent[ 'children_tpl' ];
				$tpl = str_replace( array( "\n","\r","\t"," " ), '', $tpl );
				$ret[ $main ][ $parent[ 'id' ] ][ 'tpl' ] = $tpl;

				//$ret[ $main ][ $parent[ 'id' ] ][ 'level_tree' ] = $index;
				
				if ( $parent[ 'position_table' ] ) {
					$ret[ $main ][ $parent[ 'id' ] ][ 'alias' ] = $ret[ $main ][ $parent[ 'id' ] ][ 'alias' ] . '/_items';
					$ret[ $main ][ $parent[ 'id' ] ][ 'position' ] = $parent[ 'position_table' ];
					$position_cols = $this -> _db -> select( 'SHOW COLUMNS FROM `' . $parent[ 'position_table' ] . '`' );
					if ( count( $position_cols ) ) {
						foreach ( $position_cols as $p_key => $p_col ) {
							$ret[ $main ][ $parent[ 'id' ] ][ 'position_c' ][ $p_key ] = $p_col[ 'Field' ];
							$ret[ $main ][ $parent[ 'id' ] ][ 'position_ctype' ][ $p_key ] = $p_col[ 'Type' ];
							$ret[ $main ][ $parent[ 'id' ] ][ 'position_cnull' ][ $p_key ] = ( ( $p_col[ 'Null' ] == 'YES' ) ? true : false );
							$ret[ $main ][ $parent[ 'id' ] ][ 'position_cdeft' ][ $p_key ] = $p_col[ 'Default' ];
						}
					}
				}

				if ( $parent[ 'section_table' ] ) {
					$ret[ $main ][ $parent[ 'id' ] ][ 'section' ] = $parent[ 'section_table' ];
					$section_cols = $this -> _db -> select( 'SHOW COLUMNS FROM `' . $parent[ 'section_table' ] . '`' );
					if ( count( $section_cols ) ) {
						foreach ( $section_cols as $s_key => $s_col ) {
							$ret[ $main ][ $parent[ 'id' ] ][ 'section_c' ][ $s_key ] = $s_col[ 'Field' ];
							$ret[ $main ][ $parent[ 'id' ] ][ 'section_ctype' ][ $s_key ] = $s_col[ 'Type' ];
							$ret[ $main ][ $parent[ 'id' ] ][ 'section_cnull' ][ $s_key ] = ( ( $s_col[ 'Null' ] == 'YES' ) ? true : false );
							$ret[ $main ][ $parent[ 'id' ] ][ 'section_cdeft' ][ $s_key ] = $s_col[ 'Default' ];
						}
					}
				}

				$rows = $this -> _db -> select( 'SELECT * FROM `catalog_section` WHERE `parent_id`=:parent_id ORDER BY `position`', array( 'parent_id' => $parent[ 'id' ] ) );
				$cnt = count( $rows );
				if ( $cnt ) {
					++$index;
					$ret = $this -> _chain_flat_array( $rows, $index, $main, $ret );
					--$index;
				}
			}
		}
		return $ret;
	}


	//////////////////////////////////////////////////////////////////
	// POSITION_TABLE
	//////////////////////////////////////////////////////////////////

	/**
		ПОЛУЧАЕТ СПИСОК ВСЕХ ЗАПИСЕЙ ИЗ ТАБЛИЦЫ ПОЗИЦИЙ
	*/
	public function get_position_table( $data = array( ), $fields=array( ) ) {
		if ( $this -> _method != 'GET' ) throw new Exception( 'invalid request ' . __LINE__ );
		if ( $this -> _type != 'position_table' ) throw new Exception( 'invalid request ' . __LINE__ );

		$r = array( );
		$r[ 'data' ] = array( );

		if ( isset( $fields[ 'uri' ] ) ) unset( $fields[ 'uri' ] );
		if ( isset( $fields[ 'mod' ] ) ) unset( $fields[ 'mod' ] );
		if ( isset( $fields[ 'alias' ] ) ) unset( $fields[ 'alias' ] );
		
		$data = $this -> decode( $data );
		$fields = ( isset( $fields[ 'fields' ] ) && $fields[ 'fields' ] ) ? $fields[ 'fields' ] : '';
		$filter = ( isset( $data[ 'filter' ] ) && is_array( $data[ 'filter' ] ) ) ? $data[ 'filter' ] : array( );
		$filter = $this -> _filter( $filter );
		$sql_filter = $filter[ 'sql_prepare' ];
		$params = $filter[ 'params' ];
		unset( $filter );
		$sql_fields = $this -> _fields( $fields );

		if ( !isset( $this -> _data_tables[ $this -> _section[ 'position_table' ] ] ) )
			$this -> _data_tables[ $this -> _section[ 'position_table' ] ] = true;

		$child_positions = $this -> _db -> select(
			'SELECT ' . $sql_fields . 
			' FROM `' . $this -> _section[ 'position_table' ] . 
			'` WHERE `id` > 0' . $sql_filter . ' ORDER BY `id`'
		);


		if ( $this -> _db -> errorInfo ) {
			$r[ 's' ] = false;
			$r[ 'code' ] = 500;
			$r[ 'errors' ][ 0 ][ 'error_type' ] = 'transaction_error';
			$r[ 'errors' ][ 0 ][ 'message' ] = $this -> _db -> errorInfo;
			//$r[ 'errors' ][ 0 ][ 'sql' ] = $sql;
			$r[ 'errors' ][ 0 ][ 'location' ] = __LINE__;
			$r[ 'message' ] = 'Ошибка добавления';
			$r[ 'data' ] = array( );
			return $r;
		}
		else {
			$r[ 's' ] = true;
			$r[ 'code' ] = 200;
			$r[ 'data' ] = $child_positions;
		}
	
		return $r;
	}


	/**
		ИЗМЕНЯЕТ СПИСОК ВСЕХ ЗАПИСЕЙ ОПРЕДЕЛЕННОГО ТИПА `position_table`
	*/
	public function put_position_table( $data = array( ), $fields=array( ) ) {

		if ( $this -> _method != 'PUT' ) throw new Exception( 'invalid request ' . __LINE__ );
		if ( $this -> _type != 'position_table' ) throw new Exception( 'invalid request ' . __LINE__ );

		$data = $this -> decode( $data );

		$items = ( isset( $data[ 'items' ] ) && is_array( $data[ 'items' ] ) ) ? $data[ 'items' ] : array( );
		if ( !count( $items ) ) throw new Exception( 'invalid request ' . __LINE__ );

		$filter = ( isset( $data[ 'filter' ] ) && is_array( $data[ 'filter' ] ) ) ? $data[ 'filter' ] : array( );

		$filter = $this -> _filter( $filter );
		$sql_filter = $filter[ 'sql' ];
		unset( $filter );


		$r = array( );
		$r[ 'errors' ] = array( );
		$sql = '';

		foreach( $items as $key => $item ) {

			$col_sql = '';
			$operation = '';

			foreach ( $item as $col => $val ) {
				$operation = substr( $col, -1 );
				if ( isset( $this -> _operations[ $operation ] ) ) {
					$col = substr( $col, 0, -1 );
					$operation = $this -> _operations[ $operation ];
				} else $operation = '';

				if ( $col == 'alias_prefix' ) continue;
				if ( $col == 'id' ) continue;

				if ( $col == 'title' ) {
					$items[ $key ][ 'alias' ] = ( ( isset( $items[ $key ][ 'alias_prefix' ] ) ) ? $items[ $key ][ 'alias_prefix' ] : '' ) .
						( ( isset( $items[ $key ][ 'title' ] ) && $items[ $key ][ 'title' ] ) ? Utils :: translit( $items[ $key ][ 'title' ] ) : '' );
				}
				$gettype = gettype( $val );
				// !!!!!!!!!!!!!!!!
				if ( $gettype == 'array' || $gettype == 'object' ) $val = ( addslashes( json_encode( $val, JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES ) ) );

				switch( $operation ) {
					case 'CONCAT': $col_sql .= ' `' . $col . '`=CONCAT(COALESCE(`' . $col . '`,""),"' . $val . '"),'; break;
					case 'SUM': $col_sql .= ' `' . $col . '`=(COALESCE(`' . $col . '`,0)+' . $val . '),'; break;
					case 'DIF':	$col_sql .= ' `' . $col . '`=(COALESCE(`' . $col . '`,0)-' . $val . '),'; break;
					case 'MULTI': $col_sql .= ' `' . $col . '`=(COALESCE(`' . $col . '`,0)*' . $val . '),'; break;
					case 'DIV': $col_sql .= ' `' . $col . '`=(COALESCE(`' . $col . '`,0)/' . $val . '),'; break;
					default:
						$col_sql .= ' `' . $col . '`="' . $val . '",';
					break;
				}

			}
			$col_sql = substr( $col_sql, 0, -1 );
			$sql .= 'UPDATE `' . $this -> _section[ 'position_table' ] . '` SET' . $col_sql . ' WHERE `id`>0' . $sql_filter . ';';

		}

		if ( count( $r[ 'errors' ] ) ) {
			$r[ 's' ] = false;
			$r[ 'code' ] = 500;
			$r[ 'errors' ][ 0 ][ 'error_type' ] = 'transaction_error';
			$r[ 'errors' ][ 0 ][ 'message' ] = $this -> _db -> errorInfo;
			$r[ 'errors' ][ 0 ][ 'sql' ] = $sql;
			$r[ 'errors' ][ 0 ][ 'location' ] = __LINE__;
			$r[ 'message' ] = 'Ошибка редактирования';
			return $r;
		}

		$this -> _db -> execute( $sql );
		if ( $this -> _db -> errorInfo ) {
			$r[ 's' ] = false;
			$r[ 'code' ] = 500;
			$r[ 'errors' ][ 0 ][ 'error_type' ] = 'transaction_error';
			$r[ 'errors' ][ 0 ][ 'message' ] = $this -> _db -> errorInfo;
			$r[ 'errors' ][ 0 ][ 'sql' ] = $sql;
			$r[ 'errors' ][ 0 ][ 'location' ] = __LINE__;
			$r[ 'message' ] = 'Ошибка редактирования';
			return $r;
		}
		else {
			$r[ 'sql' ] = $sql;
			$r[ 's' ] = true;
			$r[ 'code' ] = 200;
			return $r;
		}
	}


	/**
		ИЗМЕНЯЕТ СПИСОК ВСЕХ ЗАПИСЕЙ ОПРЕДЕЛЕННОГО ТИПА `position_table`
	*/
	public function post_position_table( $data = array( ), $fields=array( )  ) {
		return $this -> post_positions( $data, $fields );
	}


	//////////////////////////////////////////////////////////////////
	// SECTION_TABLE
	//////////////////////////////////////////////////////////////////

	/**
		ПОЛУЧАЕТ СПИСОК ВСЕХ ЗАПИСЕЙ ОПРЕДЕЛЕННОГО ТИПА `section_table`
	*/
	public function get_section_table( $data = array( ), $fields=array( ) ) {
		
	}
	
	/**
		ИЗМЕНЯЕТ СПИСОК ВСЕХ ЗАПИСЕЙ ОПРЕДЕЛЕННОГО ТИПА `section_table`
	*/
	public function put_section_table( $data = array( ), $fields=array( ) ) {
		
	}


	
	

	//////////////////////////////////////////////////////////////////
	// SECTIONS
	//////////////////////////////////////////////////////////////////

	/**
		ПОЛУЧАЕТ СПИСОК СЕКЦИЙ
	*/
	public function get_sections( $data = array( ), $fields=array( ), $extended=false ) {

		if ( $this -> _method != 'GET' ) throw new Exception( 'invalid request ' . __LINE__ );
		if ( $this -> _type != 'section' && $this -> _type != 'section_extended' ) throw new Exception( 'invalid request ' . __LINE__ );

		$r = array( );
		$r[ 'data' ] = array( );

		if ( isset( $fields[ 'uri' ] ) ) unset( $fields[ 'uri' ] );
		if ( isset( $fields[ 'mod' ] ) ) unset( $fields[ 'mod' ] );
		//if ( isset( $fields[ 'alias' ] ) ) unset( $fields[ 'alias' ] );

		$data = $this -> decode( $data );

		$filter = ( isset( $data[ 'filter' ] ) && is_array( $data[ 'filter' ] ) ) ? $data[ 'filter' ] : array( );
		$fields = ( isset( $fields[ 'fields' ] ) && $fields[ 'fields' ] ) ? $fields[ 'fields' ] : '';

		$filter = $this -> _filter( $filter );
		$sql_filter = $filter[ 'sql_prepare' ];
		$params = $filter[ 'params' ];
		unset( $filter );
		$sql_fields = $this -> _fields( $fields );

		$child_sections = $this -> _db -> select( 'SELECT ' . $sql_fields . ' FROM `catalog_section` WHERE `parent_id`=:pid ORDER BY `position`',
			array( 'pid' => $this -> _section[ 'id' ] ) );

		if ( count( $child_sections ) ) {
			$i = 0;
			foreach ( $child_sections as $child ) {

				if ( !isset( $this -> _data_tables[ $child[ 'section_table' ] ] ) && $child[ 'section_table' ] != '' ) {
					$this -> _data_tables[ $child[ 'section_table' ] ] = true;
				}
				$property = $this -> _db -> select(
					'SELECT ' . $sql_fields . 
					' FROM `' . $child[ 'section_table' ] . 
					'` WHERE `id`=:id' . $sql_filter . ' LIMIT 1',
					array_merge( $params, array( 'id' => $child[ 'id' ] ) )
				);
				if ( !count( $property ) ) continue;
				$property = array_diff( end( $property ), array( '' ) );
				if ( !count( $property ) ) continue;
				$r[ 'data' ][ $i ] = $property;
				// LEFT JOIN `catalog_section` AS `cs` ON `cs`.`id`=`' . $child[ 'section_table' ] . '`.`id` 
				$r[ 'data' ][ $i ][ 'alias' ] = $child[ 'alias' ];
				$r[ 'data' ][ $i ][ 'title' ] = $child[ 'title' ];

				// если запрос расширенный
				if ( $extended && $this -> _type == 'section_extended' && $child[ 'position_table' ] ) {
					// ищем и выводим так же и позиции в секциях
					$positions = $this -> _db -> select(
						'SELECT * FROM `' . $child[ 'position_table' ] . 
						'` WHERE `section_id`=:id ORDER BY `id`', // !!!!!!!! сделать сортировку
						array( 'id' => $child[ 'id' ] )
					);
					if ( $cnt = count( $positions ) ) {
						$r[ 'data' ][ $i ][ 'positions' ] = $positions; // !!!!!!!!! сделать фильтр полей
					}
				}

				++$i;
			}
			$r[ 's' ] = true;
			//$r[ 'filter' ] = $sql_filter;
			$r[ 'code' ] = 200;
		}
		return $r;
	}



	/**
		ДОБАВЛЯЕТ СЕКЦИЮ
	*/
	public function post_sections( $data = array( ) ) {

		if ( $this -> _method != 'POST' ) throw new Exception( 'invalid request ' . __LINE__ );
		if ( $this -> _type != 'section' ) throw new Exception( 'invalid request ' . __LINE__ );

		$data = $this -> decode( $data );
		$items = ( isset( $data[ 'items' ] ) && is_array( $data[ 'items' ] ) ) ? $data[ 'items' ] : array( );
		if ( !count( $items ) ) throw new Exception( 'invalid request ' . __LINE__ );

		$sql = array( );
		$sql_positions = '';

		if ( !isset( $items[ 0 ][ 'section' ] ) || !$items[ 0 ][ 'section' ] ) {
			$result_sql = 'SELECT `cs`.`parent_id`, `cs`.`title`, `cs`.`alias`, `cs`.`position_table` AS `position`, `cs`.`section_table` as `section` from `catalog_section` AS `cs` WHERE `id` IN (';
		}
		else {
			$result_sql = 'SELECT `cs`.`parent_id`, `cs`.`title`, `cs`.`alias`, `cs`.`position_table` AS `position`, `cs`.`section_table` as `section`, `' . $items[ 0 ][ 'section' ] . '`.* from `' . $items[ 0 ][ 'section' ] . '` LEFT JOIN `catalog_section` AS `cs` ON `cs`.`id`=`' . $items[ 0 ][ 'section' ] . '`.`id` WHERE `cs`.`alias` IN (';
		}

		$result_arr = array( );
		foreach( $items as $key => $item ) {
			$new_alias = ( ( isset( $item[ 'alias_prefix' ] ) ) ? $item[ 'alias_prefix' ] : '' ) . Utils :: translit( $item[ 'title' ] );
			$sql[ 'catalog_section|' . $new_alias ] = "
INSERT INTO `catalog_section` (`id`,`parent_id`,`title`,`alias`,`position_table`,`section_table`,`children_tpl`,`leaf`) VALUES (NULL," . $this -> _section[ 'id' ] . ",'" . addslashes( $item[ 'title' ] ) . "','" . $new_alias . "','" . ( ( isset( $item[ 'position' ] ) ) ? addslashes( $item[ 'position' ] ) : '' ) . "','" . ( ( isset( $item[ 'section' ] ) && $item[ 'section' ] ) ? addslashes( $item[ 'section' ] ) : '' ) . "','" . ( ( isset( $item[ 'tpl' ] ) ) ? addslashes( $item[ 'tpl' ] ) : '' ) . "',NULL);
SET @last_id" . $key . ":=LAST_INSERT_ID( );";
			$result_arr[ $new_alias ] = '"' . $new_alias . '"';
			if ( isset( $item[ 'section' ] ) && $item[ 'section' ] ) {
				$col_sql = '';
				$val_sql = '';
				foreach ( $item as $col => $val ) {
					// !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
					// REQURSIVE ADDED STRUCTURE
					////////////////////////////////////////////
					if ( $col == 'children_tpl' ) {
						;
					}
					if ( $col == 'section' || $col == 'position' ) continue;
					if ( $col == 'alias_prefix' ) continue;
					if ( $col == 'title' ) continue;

					
					////////////////////////////////////////////
					// ADD POSITIONS ITEMS
					if ( $col == 'positions' && isset( $val[ 'item' ] ) ) {
						$val[ 'item' ] = ( isset( $val[ 'item' ][ 0 ] ) ) ? $val[ 'item' ] : array( $val[ 'item' ] );
						foreach ( $val[ 'item' ] as $position_key => $position ) {
							$position_col_sql = '';
							$position_val_sql = '';
							foreach ( $position as $position_col => $position_val ) {
								if ( is_array( $position_val ) ) $position_val = ''; // <xml/> - is array
								$position_col_sql .= '`' . $position_col . '`,';
								$position_val_sql .= '"' . addslashes( $position_val ) . '",';
							}
							$position_col_sql = substr( $position_col_sql, 0, -1 );
							$position_val_sql = substr( $position_val_sql, 0, -1 );
							$sql_positions .= "
INSERT INTO `" . $item[ 'position' ] . "` (`id`,`section_id`," . $position_col_sql . ") VALUES (NULL,@last_id" . $key . "," . $position_val_sql . ");";
						}
						break;
					}
					///////////////////////////////////////////
					
					
					$col_sql .= '`' . $col . '`,';
					$val_sql .= '"' . addslashes( $val ) . '",';
				}
				$col_sql = substr( $col_sql, 0, -1 );
				$val_sql = substr( $val_sql, 0, -1 );

				$sql[ $new_alias ] = "
INSERT INTO `" . $item[ 'section' ] . "` (`id`," . $col_sql . ") VALUES (@last_id" . $key . "," . $val_sql . ");" . $sql_positions;
			}
		}
		

		$err = false;
		$this -> _db -> begin_transaction( );
		foreach ( $sql as $k => $s ) {
			$this -> _db -> execute( $s );
			if ( $this -> _db -> errorInfo ) {
				$err = true;
				break;
			}
		}

		if ( $err ) {
			$this -> _db -> roll_back( );
			$r[ 's' ] = false;
			$r[ 'code' ] = 500;
			$r[ 'errors' ][ 0 ][ 'error_type' ] = 'transaction_error';
			$r[ 'errors' ][ 0 ][ 'message' ] = $this -> _db -> errorInfo;
			$r[ 'errors' ][ 0 ][ 'alias' ] = $k;
			//$r[ 'errors' ][ 0 ][ 'sql' ] = $sql;
			$r[ 'errors' ][ 0 ][ 'location' ] = __LINE__;
			$r[ 'message' ] = 'Не удалось создать записи';
			$r[ 'data' ] = array( );
			return $r;
		}
		else {
			$commit = $this -> _db -> commit( );
			$r[ 'commit' ] = $commit;
			$r[ 'errors' ][ 0 ][ 'sql' ] = $sql;
		}


		$result_sql .= implode( ',', $result_arr ) . ') ORDER BY `cs`.`id`;';
		$r[ 'data' ] = $this -> _db -> select( $result_sql );
		if ( $this -> _db -> errorInfo || !count( $r[ 'data' ] ) ) {
			$r[ 's' ] = false;
			$r[ 'code' ] = 500;
			$r[ 'errors' ][ 0 ][ 'error_type' ] = 'transaction_result_error';
			$r[ 'errors' ][ 0 ][ 'message' ] = $this -> _db -> errorInfo;
			//$r[ 'errors' ][ 0 ][ 'sql' ] = $sql;
			$r[ 'errors' ][ 0 ][ 'location' ] = __LINE__;
			$r[ 'message' ] = 'Ошибка результата добавления';
			$r[ 'sql' ] = $result_sql;
			return $r;
		}
		else {
			$r[ 's' ] = true;
			$r[ 'code' ] = 200;
			$r[ 'sql' ] = $sql;
			return $r;
		}

	}



	/**
		РЕДАКТИРУЕТ СЕКЦИЮ
	*/
	public function put_sections( $data = array( ) ) {

		if ( $this -> _method != 'PUT' ) throw new Exception( 'invalid request ' . __LINE__ );
		if ( $this -> _type != 'section' ) throw new Exception( 'invalid request ' . __LINE__ );

		$r = array( );
		$data = $this -> decode( $data );
		$items = ( isset( $data[ 'items' ] ) && is_array( $data[ 'items' ] ) ) ? $data[ 'items' ] : array( );
		if ( !count( $items ) ) throw new Exception( 'invalid request ' . __LINE__ );

		$r[ 'errors' ] = array( );

		$sql = '';
		$col_sql = '';
		$id = $this -> _section[ 'id' ];
		$find_where = 'id';
		foreach( $items as $key => $item ) {

			if ( isset( $item[ 'id' ] ) && $item[ 'id' ] && preg_match( "/^[0-9]{1,}$/i", $item[ 'id' ] ) ) {
				$id = $item[ 'id' ];
			}
			else if ( isset( $item[ 'alias' ] ) ) {
				$id = $item[ 'alias' ];
				$find_where = 'alias';
			}
			else $id = $this -> _section[ 'id' ];
		
			$find = $this -> _db -> select( 'SELECT `id`,`section_table`,`position_table` FROM `catalog_section` WHERE `' . $find_where . '`=:id LIMIT 1', array( 'id' => $id ) );
			if ( !count( $find ) ) {
				$r[ 'errors' ][ $key ] = 'item[ ' . $key . ' ] dont find catalog section';
				continue;
			}
			$find = end( $find );
			$id = $find[ 'id' ];
			$set_sql = '';
			$i = 0;
			foreach ( $item as $col => $val ) {
				$operation = substr( $col, -1 );
				if ( isset( $this -> _operations[ $operation ] ) ) {
					$col = substr( $col, 0, -1 );
					$operation = $this -> _operations[ $operation ];
				} else $operation = '';

				if ( $col == 'alias_prefix' ) continue;
				if ( $col == 'id' ) {
					continue;
				}
				if ( $col == 'title' || $col == 'children_tpl' ) {
					continue;
				}
				if ( $i ) $set_sql .= ' &&';
				$gettype = gettype( $val );
				// !!!!!!!!
				if ( $gettype == 'array' || $gettype == 'object' ) $val = ( addslashes( json_encode( $val, JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES ) ) );

				switch( $operation ) {
					case 'CONCAT': $set_sql .= ' `' . $col . '`=CONCAT(COALESCE(`' . $col . '`,""),"' . $val . '"),'; break;
					case 'SUM': $set_sql .= ' `' . $col . '`=(COALESCE(`' . $col . '`,0)+' . $val . '),'; break;
					case 'DIF':	$set_sql .= ' `' . $col . '`=(COALESCE(`' . $col . '`,0)-' . $val . '),'; break;
					case 'MULTI': $set_sql .= ' `' . $col . '`=(COALESCE(`' . $col . '`,0)*' . $val . '),'; break;
					case 'DIV': $set_sql .= ' `' . $col . '`=(COALESCE(`' . $col . '`,0)/' . $val . '),'; break;
					default: $set_sql .= ' `' . $col . '`="' . $val . '",'; break;
				}

			}
			$set_sql = substr( $set_sql, 0, -1 );

			$set_cs_sql = '';
			$set_cs_sql .= ( isset( $item[ 'title' ] ) && $item[ 'title' ] ) ? ' `title`="' . $item[ 'title' ] . '",' : '';
			$set_cs_sql .= ( isset( $item[ 'children_tpl' ] ) && $item[ 'children_tpl' ] ) ? ' `children_tpl`="' . $item[ 'children_tpl' ] . '",' : '';
			if ( strlen( $set_cs_sql ) > 0 ) {
				$set_cs_sql = substr( $set_cs_sql, 0, -1 );
				$sql .= "
UPDATE `catalog_section` SET " . $set_cs_sql . " WHERE `id`='" . $id . "';";
			}

			$sql .= "
UPDATE `" . $find[ 'section_table' ] . "` SET" . $set_sql . " WHERE `id`='" . $id . "';";
			++$i;
		}


		if ( count( $r[ 'errors' ] ) ) {
			$r[ 's' ] = false;
			$r[ 'code' ] = 500;
			$r[ 'errors' ][ 0 ][ 'error_type' ] = 'transaction_error';
			$r[ 'errors' ][ 0 ][ 'message' ] = $this -> _db -> errorInfo;
			$r[ 'errors' ][ 0 ][ 'sql' ] = $sql;
			$r[ 'errors' ][ 0 ][ 'location' ] = __LINE__;
			$r[ 'message' ] = 'Ошибка редактирования';
			return $r;
		}

		$this -> _db -> execute( $sql );
		if ( $this -> _db -> errorInfo ) {
			$r[ 's' ] = false;
			$r[ 'code' ] = 500;
			$r[ 'errors' ][ 0 ][ 'error_type' ] = 'transaction_error';
			$r[ 'errors' ][ 0 ][ 'message' ] = $this -> _db -> errorInfo;
			$r[ 'errors' ][ 0 ][ 'data' ] = var_export( $find, true );
			$r[ 'errors' ][ 0 ][ 'sql' ] = $sql;
			$r[ 'errors' ][ 0 ][ 'location' ] = __LINE__;
			$r[ 'message' ] = 'Ошибка редактирования';
			return $r;
		}
		else {
			$r[ 's' ] = true;
			$r[ 'code' ] = 200;
			return $r;
		}

	}



	/**
		УДАЛЯЕТ СЕКЦИЮ

	public function delete_sections( $data = array( ) ) {
		if ( $this -> _method != 'DELETE' ) throw new Exception( 'invalid request ' . __LINE__ );
		if ( $this -> _type != 'section' ) throw new Exception( 'invalid request ' . __LINE__ );
		$r = array( );
		
		
		
		return $r;
	}
	*/
	
	
	//////////////////////////////////////////////////////////////////
	// POSITIONS
	//////////////////////////////////////////////////////////////////

	
	/**
		ПОЛУЧАЕТ СПИСОК ПОЗИЦИЙ
	*/
	public function get_positions( $data = array( ), $fields = array( ) ) {
		if ( $this -> _method != 'GET' ) throw new Exception( 'invalid request ' . __LINE__ );
		if ( $this -> _type != 'position' ) throw new Exception( 'invalid request ' . __LINE__ );

		$r = array( );
		$r[ 'data' ] = array( );

		if ( isset( $fields[ 'uri' ] ) ) unset( $fields[ 'uri' ] );
		if ( isset( $fields[ 'mod' ] ) ) unset( $fields[ 'mod' ] );
		if ( isset( $fields[ 'alias' ] ) ) unset( $fields[ 'alias' ] );
		
		$data = $this -> decode( $data );
		$fields = ( isset( $fields[ 'fields' ] ) && $fields[ 'fields' ] ) ? $fields[ 'fields' ] : '';
		$filter = ( isset( $data[ 'filter' ] ) && is_array( $data[ 'filter' ] ) ) ? $data[ 'filter' ] : array( );
		$filter = $this -> _filter( $filter );
		$sql_filter = $filter[ 'sql_prepare' ];
		$params = $filter[ 'params' ];
		unset( $filter );
		$sql_fields = $this -> _fields( $fields );

		if ( !isset( $this -> _data_tables[ $this -> _section[ 'position_table' ] ] ) )
			$this -> _data_tables[ $this -> _section[ 'position_table' ] ] = true;

		$child_positions = $this -> _db -> select(
			'SELECT ' . $sql_fields . 
			' FROM `' . $this -> _section[ 'position_table' ] . 
			'` WHERE `section_id`=:id' . $sql_filter . ' ORDER BY `id`',
			array_merge( $params, array( 'id' => $this -> _section[ 'id' ] ) )
		);

		if ( $this -> _db -> errorInfo ) {
			$r[ 's' ] = false;
			$r[ 'code' ] = 500;
			$r[ 'errors' ][ 0 ][ 'error_type' ] = 'transaction_error';
			$r[ 'errors' ][ 0 ][ 'message' ] = $this -> _db -> errorInfo;
			//$r[ 'errors' ][ 0 ][ 'sql' ] = $sql;
			$r[ 'errors' ][ 0 ][ 'location' ] = __LINE__;
			$r[ 'message' ] = 'Ошибка добавления';
			$r[ 'data' ] = array( );
			return $r;
		}
		else {
			$r[ 's' ] = true;
			$r[ 'code' ] = 200;
			$r[ 'data' ] = $child_positions;
		}
	
		return $r;
	}



	/**
		ДОБАВЛЯЕТ СПИСОК ПОЗИЦИЙ
	*/
	public function post_positions( $data = array( ), $fields=array( ) ) {
		if ( $this -> _method != 'POST' ) throw new Exception( 'invalid request ' . __LINE__ );
		if ( $this -> _type != 'position' && $this -> _type != 'position_table' ) throw new Exception( 'invalid request ' . __LINE__ );

		$r = array( );
		$data = $this -> decode( $data );
		$items = ( isset( $data[ 'items' ] ) && is_array( $data[ 'items' ] ) ) ? $data[ 'items' ] : array( );
		if ( !count( $items ) ) throw new Exception( 'invalid request ' . __LINE__ );

		$sql = '';
/*
		$sql = "
START TRANSACTION;"; !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
*/

		$result_arr = array( );
		foreach( $items as $key => $item ) {
			$col_sql = '';
			$val_sql = '';
			$update_sql = '`id`=LAST_INSERT_ID(`id`),';
			foreach ( $item as $col => $val ) {
				$lower_val = mb_strtolower( $val, 'UTF-8' );
				// if ( $col == 'title' || $col == 'id' ) continue;
				if ( $lower_val == 'false' ) $val = 0;
				if ( $lower_val == 'true' ) $val = 1;
				$col_sql .= '`' . $col . '`,';
				$val_sql .= '"' . addslashes( $val ) . '",';
				$update_sql .= '`' . $col . '`="' . addslashes( $val ) . '",';
			}
			$col_sql = substr( $col_sql, 0, -1 );
			$val_sql = substr( $val_sql, 0, -1 );
			$update_sql = substr( $update_sql, 0, -1 );

			if ( $this -> _type == 'position' ) {
				$col_sql = '`section_id`,' . $col_sql;
				$val_sql = "'" . $this -> _section[ 'id' ] . "'," . $val_sql;
			}

			$sql .= "
INSERT INTO `" . $this -> _section[ 'position_table' ] . "` (" . $col_sql . ") VALUES (" . $val_sql . ") ON DUPLICATE KEY UPDATE " . $update_sql . ";
SET @last_id" . $key . ":=LAST_INSERT_ID( );";
			$result_arr[ ] = '@last_id' . $key;

		}

		$this -> _db -> execute( $sql );
		if ( $this -> _db -> errorInfo ) {
			$r[ 's' ] = false;
			$r[ 'code' ] = 500;
			$r[ 'errors' ][ 0 ][ 'error_type' ] = 'transaction_error';
			$r[ 'errors' ][ 0 ][ 'message' ] = $this -> _db -> errorInfo;
			$r[ 'errors' ][ 0 ][ 'sql' ] = $sql;
			$r[ 'errors' ][ 0 ][ 'location' ] = __LINE__;
			$r[ 'message' ] = 'Ошибка добавления';
			$r[ 'data' ] = array( );
			return $r;
		}

		$result_sql = 'SELECT * from `' . $this -> _section[ 'position_table' ] . '` WHERE `id` IN (';
		$result_sql .= implode( ',', $result_arr ) . ') ORDER BY `id`;';
		$result = $this -> _db -> select( $result_sql );
		$r[ 'data' ] = $result;
		if ( $this -> _db -> errorInfo || !count( $r[ 'data' ] ) ) {
			$r[ 's' ] = false;
			$r[ 'code' ] = 500;
			$r[ 'errors' ][ 0 ][ 'error_type' ] = 'transaction_result_error';
			$r[ 'errors' ][ 0 ][ 'message' ] = $this -> _db -> errorInfo;
			$r[ 'errors' ][ 0 ][ 'sql' ] = $sql;
			$r[ 'errors' ][ 0 ][ 'location' ] = __LINE__;
			$r[ 'message' ] = 'Ошибка результата добавления';
			return $r;
		}
		else {
			$r[ 's' ] = true;
			$r[ 'code' ] = 200;
			// $r[ 'sql' ] = $sql;
			return $r;
		}

	}


	/**
		ИЗМЕНЯЕТ ПОЗИЦИИ
	*/
	public function put_positions( $data = array( ) ) {
		if ( $this -> _method != 'PUT' ) throw new Exception( 'invalid request ' . __LINE__ );
		if ( $this -> _type != 'position' ) throw new Exception( 'invalid request ' . __LINE__ );

		$data = $this -> decode( $data );

		$items = ( isset( $data[ 'items' ] ) && is_array( $data[ 'items' ] ) ) ? $data[ 'items' ] : array( );
		if ( !count( $items ) ) throw new Exception( 'invalid request ' . __LINE__ );

		$filter = ( isset( $data[ 'filter' ] ) && is_array( $data[ 'filter' ] ) ) ? $data[ 'filter' ] : array( );

		$filter = $this -> _filter( $filter );
		$sql_filter = $filter[ 'sql' ];
		unset( $filter );


		$r = array( );
		$r[ 'errors' ] = array( );
		$sql = '';

		foreach( $items as $key => $item ) {

			if ( isset( $item[ 'id' ] ) && $item[ 'id' ] && preg_match( "/^[0-9]{1,}$/i", $item[ 'id' ] ) ) $id = $item[ 'id' ];
			else $id = 0;

			if ( !$id && !$sql_filter ) {
				 throw new Exception( 'invalid request: item[ id ] || filter[ ] must be exist ' . __LINE__ );
			}

			$col_sql = '';
			$operation = '';

			foreach ( $item as $col => $val ) {
				$operation = substr( $col, -1 );
				if ( isset( $this -> _operations[ $operation ] ) ) {
					$col = substr( $col, 0, -1 );
					$operation = $this -> _operations[ $operation ];
				} else $operation = '';

				if ( $col == 'alias_prefix' ) continue;
				if ( $col == 'id' ) continue;

				if ( $col == 'title' ) {
					$items[ $key ][ 'alias' ] = ( ( isset( $items[ $key ][ 'alias_prefix' ] ) ) ? $items[ $key ][ 'alias_prefix' ] : '' ) .
						( ( isset( $items[ $key ][ 'title' ] ) && $items[ $key ][ 'title' ] ) ? Utils :: translit( $items[ $key ][ 'title' ] ) : '' );
				}
				$gettype = gettype( $val );
				// !!!!
				if ( $gettype == 'array' || $gettype == 'object' ) $val = ( addslashes( json_encode( $val, JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES ) ) );

				switch( $operation ) {
					case 'CONCAT': $col_sql .= ' `' . $col . '`=CONCAT(COALESCE(`' . $col . '`,""),"' . $val . '"),'; break;
					case 'SUM': $col_sql .= ' `' . $col . '`=(COALESCE(`' . $col . '`,0)+' . $val . '),'; break;
					case 'DIF':	$col_sql .= ' `' . $col . '`=(COALESCE(`' . $col . '`,0)-' . $val . '),'; break;
					case 'MULTI': $col_sql .= ' `' . $col . '`=(COALESCE(`' . $col . '`,0)*' . $val . '),'; break;
					case 'DIV': $col_sql .= ' `' . $col . '`=(COALESCE(`' . $col . '`,0)/' . $val . '),'; break;
					default:
						$col_sql .= ' `' . $col . '`="' . $val . '",';
					break;
				}

			}
			$col_sql = substr( $col_sql, 0, -1 );

			if ( $id ) $sql_filter .= ' && `id`=' . $id;
			$sql .= 'UPDATE `' . $this -> _section[ 'position_table' ] . '` SET' . $col_sql . ' WHERE `section_id`=' . $this -> _section[ 'id' ] . $sql_filter . ';';

		}

		if ( count( $r[ 'errors' ] ) ) {
			$r[ 's' ] = false;
			$r[ 'code' ] = 500;
			$r[ 'errors' ][ 0 ][ 'error_type' ] = 'transaction_error';
			$r[ 'errors' ][ 0 ][ 'message' ] = $this -> _db -> errorInfo;
			$r[ 'errors' ][ 0 ][ 'sql' ] = $sql;
			$r[ 'errors' ][ 0 ][ 'location' ] = __LINE__;
			$r[ 'message' ] = 'Ошибка редактирования';
			return $r;
		}

		$this -> _db -> execute( $sql );
		if ( $this -> _db -> errorInfo ) {
			$r[ 's' ] = false;
			$r[ 'code' ] = 500;
			$r[ 'errors' ][ 0 ][ 'error_type' ] = 'transaction_error';
			$r[ 'errors' ][ 0 ][ 'message' ] = $this -> _db -> errorInfo;
			$r[ 'errors' ][ 0 ][ 'sql' ] = $sql;
			$r[ 'errors' ][ 0 ][ 'location' ] = __LINE__;
			$r[ 'message' ] = 'Ошибка редактирования';
			return $r;
		}
		else {
			$r[ 'sql' ] = $sql;
			$r[ 's' ] = true;
			$r[ 'code' ] = 200;
			return $r;
		}
	}



	/**
		УДАЛЯЕТ ПОЗИЦИИ
	*/
	public function delete_positions( $data = array( ) ) {
		if ( $this -> _method != 'DELETE' ) throw new Exception( 'invalid request ' . __LINE__ );
		if ( $this -> _type != 'position' ) throw new Exception( 'invalid request ' . __LINE__ );

		$r = array( );
		$data = $this -> decode( $data );
		$items = ( isset( $data[ 'items' ] ) && is_array( $data[ 'items' ] ) ) ? $data[ 'items' ] : array( );
		if ( !count( $items ) ) throw new Exception( 'invalid request ' . __LINE__ );

		$r[ 'errors' ] = array( );
		$sql = '';

		$sql = '';

		$ids = array( );
		foreach( $items as $key => $item ) {
			if ( !isset( $item[ 'id' ] ) || !preg_match( "/^[0-9]{1,}$/i", $item[ 'id' ] ) ) {
				$r[ 'errors' ][ $key ] = 'param item[ id ] must be int';
				continue;
			}
			$ids[ ] = $item[ 'id' ];
		}
        
		$sql .= "
DELETE FROM `" . $this -> _section[ 'position_table' ] . "` WHERE `id` IN (" . implode( ',', $ids ) . ");";

		if ( count( $r[ 'errors' ] ) ) {
			$r[ 's' ] = false;
			$r[ 'code' ] = 500;
			$r[ 'errors' ][ 0 ][ 'error_type' ] = 'transaction_error';
			$r[ 'errors' ][ 0 ][ 'message' ] = $this -> _db -> errorInfo;
			$r[ 'errors' ][ 0 ][ 'sql' ] = $sql;
			$r[ 'errors' ][ 0 ][ 'location' ] = __LINE__;
			$r[ 'message' ] = 'Ошибка удаления';
			return $r;
		}

		$this -> _db -> execute( $sql );
		if ( $this -> _db -> errorInfo ) {
			$r[ 's' ] = false;
			$r[ 'code' ] = 500;
			$r[ 'errors' ][ 0 ][ 'error_type' ] = 'transaction_error';
			$r[ 'errors' ][ 0 ][ 'message' ] = $this -> _db -> errorInfo;
			$r[ 'errors' ][ 0 ][ 'sql' ] = $sql;
			$r[ 'errors' ][ 0 ][ 'location' ] = __LINE__;
			$r[ 'message' ] = 'Ошибка удаления';
			return $r;
		}
		else {
			$r[ 's' ] = true;
			$r[ 'code' ] = 200;
			return $r;
		}
	}


	
	
	
	
	
	/**
		strout to browser
	*/
	public function strout( $ret ) {

		// var_dump( $ret );
	
		if ( !isset( $ret[ 'code' ] ) || !isset( $this -> _headers[ $ret[ 'code' ] ] ) ) throw new Exception( 'invalid request ' . __LINE__ );

		$this -> _header = $ret[ 'code' ];
		if ( $this -> _header != 200 && ( !isset( $ret[ 'errors' ] ) || !is_array( $ret[ 'errors' ] ) ) ) {
			 throw new Exception( 'invalid request param[ errors ] cant be array ' . __LINE__ );
		}

		if ( isset( $ret[ 'query' ][ 'uri' ] ) ) unset( $ret[ 'query' ][ 'uri' ] );
		if ( isset( $ret[ 'query' ][ 'mod' ] ) ) unset( $ret[ 'query' ][ 'mod' ] );
		if ( isset( $ret[ 'query' ][ 'alias' ] ) ) unset( $ret[ 'query' ][ 'alias' ] );


		$ret[ 'code' ] = $this -> _header;
		$ret[ 'errors' ] = ( isset( $ret[ 'errors' ] ) && is_array( $ret[ 'errors' ] ) ) ? $ret[ 'errors' ] : array( );
		$ret[ 'message' ] = ( isset( $ret[ 'message' ] ) && $ret[ 'message' ] ) ? $ret[ 'message' ] : '';
		$ret[ 'data' ] = ( isset( $ret[ 'data' ] ) && $ret[ 'data' ] ) ? $ret[ 'data' ] : array( );


		if ( $this -> _debug == true ) {
			$ret[ 'route' ] = $_SERVER[ 'REQUEST_URI' ];
			$ret[ 'method' ] = $this -> _method;
			$ret[ 'header' ] = $this -> _header;
			if ( isset( $ret[ 'body' ] ) && $ret[ 'body' ] ) {
				// $this -> decode( file_get_contents( 'php://input' ) )
			}
			// $ret[ 'body' ] = $this -> _body;
			// $ret[ 'query' ] = $query;
		}


		//exit;
		//ob_start( 'ob_gzhandler' );
		$this -> header( );
		//var_dump( $this -> { '_encode' . $this -> _format }( $ret ) );
		// $a = $this -> { '_encode' . $this -> _format }( $ret );
		$ret = $this -> encode( $ret );
		echo( $ret );
		//ob_end_flush( );
		exit( ( $this -> _header == 200 ) ? 0 : 1 );
	}


	
	// STATIC
	static function __header( $format ) {
		if ( !method_exists( 'Api_base_server', '_header' . $format ) ) return self :: _headerJSON( );
		return self :: { '_header' . $format }( );
	}

	static function __encode( $data, $format ) {
		if ( !method_exists( 'Api_base_server', '_encode' . $format ) ) return self :: _encodeJSON( $data );
		return self :: { '_encode' . $format }( $data );
	}
	
	static function __decode( $str, $format='' ) {
		if ( !method_exists( 'Api_base_server', '_decode' . $format ) ) return self :: _decodeJSON( $str );
		return self :: { '_decode' . $format }( $str );
	}
	
	

	// PUBLIC
	public function header( $format='' ) {
		$format = ( $format ) ? $format : $this -> _format;
		if ( !method_exists( $this, '_header' . $format ) ) return self :: _headerJSON( $str );
		return self :: { '_header' . $format }( );
	}

	public function encode( $data, $format='' ) {
		$format = ( $format ) ? $format : $this -> _format;
		if ( !method_exists( $this, '_encode' . $format ) ) return self :: _encodeJSON( $data );
		return self :: { '_encode' . $format }( $data );
	}

	public function decode( $str, $format='' ) {
		$format = ( $format ) ? $format : $this -> _format;
		if ( !method_exists( $this, '_decode' . $format ) ) return self :: _decodeJSON( $str );
		return self :: { '_decode' . $format }( $str );
	}


	/////////////////
	// XML
	static function _headerXML( ) { header( 'Content-type: text/xml; charset=utf-8' ); }

	static function _encodeXML( $data ) {
		$xml = new \SimpleXMLElement( '<response/>' );
		self :: _array_to_xml( $data, $xml );
		$res = $xml -> asXML( );
		$res = self :: _xml_entity_decode( $res );
		return $res;
	}
	
	static function _decodeXML( $str ) {
		$str = self :: _xml_to_array( $str );
		return $str;
	}


/*
GET_CHAIN BUGFIX
<#^\___catalog___parts\___#i>/pt/position_cats/</#^\___catalog___parts\___#i>
<#^\___catalog___brands\___#i>/katalog/</#^\___catalog___brands\___#i>
<#^\___catalog___([^___]{1,})\___#i>/katalog/$1/_items/</#^\___catalog___([^___]{1,})\___#i>

string(13) "#^\/users\/#i"
string(21) "#^\/catalog/parts\/#i"
string(22) "#^\/catalog/brands\/#i"
string(26) "#^\/catalog/([^/]{1,})\/#i"
string(16) "#^\/storages\/#i"
string(15) "#^\/markets\/#i"
string(24) "#^\/delivery_methods\/#i"
string(19) "#^\/pay_methods\/#i"
string(20) "#^\/order_status\/#i"
string(14) "#^\/orders\/#i"
string(12) "#^\/docs\/#i"

<users>/polzovateli/_extended/</users>
<catalog___parts>/pt/position_cats/</catalog___parts>
<catalog___brands>/katalog/</catalog___brands>
<catalog___([^___]{1,})>/katalog/$1/_items/</catalog___([^___]{1,})>
<storages>/spravochniki/sklady/_items/</storages>
<markets>/spravochniki/tochki-samovyvoza/_items/</markets>
<delivery_methods>/spravochniki/metody-dostavki/_items/</delivery_methods>
<pay_methods>/spravochniki/metody-oplaty/_items/</pay_methods>
<order_status>/spravochniki/statusy-zakaza/_items/</order_status>
<orders>/zakazy/_extended/</orders>
<docs>/lichnyj-kabinet/dokumentooborot/_items/</docs>

*/
	static function _array_to_xml( $data, &$xmlData ) {

		$replace = array( '#^\/', '\/#i', '/' );
		$replaced = array( '', '', '___' );
	
        foreach( $data as $key => $value ) {
			//var_dump( $key );
			$key = str_replace( $replace, $replaced, $key );
            if ( is_array( $value ) ) {
                if ( !is_numeric( $key ) ) $subnode = $xmlData -> addChild( $key );
                else $subnode = $xmlData -> addChild( 'item' );
				self :: _array_to_xml( $value, $subnode );
            }
            else {
				$value = htmlspecialchars( $value );
                if ( !is_numeric( $key ) ) $xmlData -> addChild( $key, $value );
				else $xmlData -> addChild( 'item', $value );
			}
        }
		return $data;
	}

	static function _xml_to_array( $str, $recursive = false ) {
		if ( !$recursive ) {
			$array = simplexml_load_string( $str );
			$array = json_encode( $array );
			$array = json_decode( $array, true );
		}
		else $array = $str;
		$new_array = array( );
		$array = ( array ) $array;
		foreach ( $array as $key => $value ) {
			if ( $key == 'item' ) continue;
			if ( $key == 'items' && isset( $value[ 'item' ] ) ) $value = $value[ 'item' ];
			if ( is_array( $value ) ) {
				if ( isset( $value[ 0 ] ) ) $new_array[ $key ] = $value;
				else $new_array[ $key ][ 0 ] = $value;
			}
			else $new_array[ $key ] = self :: _xml_to_array( $value, true );
		}
		return $new_array;
	}

	static function _xml_entity_decode( $s ) {
		$xentities = array( '&amp;', '&gt;', '&lt;', '&#xD;' );
		$xsafentities = array( '#_x_amp#;', '#_x_gt#;', '#_x_lt#;', '' );
		$s = str_replace( $xentities, $xsafentities, $s ); 
		$s = html_entity_decode( $s, ENT_HTML5 | ENT_NOQUOTES, 'UTF-8' ); // PHP 5.3+
		$s = str_replace( $xsafentities, $xentities, $s );
		return $s;
	 }



	/////////////////
	// JSON
	static function _headerJSON( ) { header( 'Content-type: application/json; charset=utf-8' ); }
	static function _encodeJSON( $data ) { return json_encode( $data ); }
	static function _decodeJSON( $str ) { return json_decode( $str, true ); }


	/////////////////
	// CSV
	static function _headerCSV( ) { header( 'Content-Type: text/plain; charset=utf-8' ); }
	static function _encodeCSV( $data ) { return $data; }
	static function _decodeCSV( $str ) { return $str; }
	
	






	

	private function _fields( $fields='' ) {
		if ( !$fields ) return '*';
		if ( strlen( $fields ) > 0 ) {
			$fields = explode( ',', $fields );
		}
		return ( ( count( $fields ) ) ? implode( ',', $fields ) : '*' );
	}


	private function _filter( $filter = null ) {

		$sql = '';
		$sql_prepare = '';
		$params = array( );

		if ( isset( $filter ) && is_array( $filter ) ) {
			if ( count( $filter ) ) {

				$sql_prepare .= ' && ';
				$sql .= ' && ';

				foreach( $filter as $k => $f ) {

					if ( is_string( $f ) ) {

						$sql_prepare .= '`' . $k . '`=:' . $k . ' && ';

						$sql .= '`' . $k . '`=';
						$sql .= ( !preg_match( "/^[0-9]{1,}$/i", $f ) ) ? '"' . addslashes( $f ) . '"' : $f;
						$sql .= ' && ';
					}
					else if ( is_array( $f ) ) {

						$sql_prepare .= ' `' . $k . '` IN (';
						$sql .= ' `' . $k . '` IN (';
	
						foreach ( $f as $f_one ) {

							$f_one = addslashes( $f_one );
						
							if ( !preg_match( "/^[0-9]{1,}$/i", $f_one ) ) {
								$sql_prepare .= '"' . $f_one . '",';
								$sql .= '"' . $f_one . '",';
							}
							else {
								$sql_prepare .= $f_one . ',';
								$sql .= $f_one . ',';
							}

						}

						$sql_prepare = substr( $sql_prepare, 0, -1 );
						$sql_prepare .= ') && ';

						$sql = substr( $sql, 0, -1 );
						$sql .= ') && ';

						unset( $filter[ $k ] );
					}
				}
			}
			$sql_prepare = substr( $sql_prepare, 0, -3 );
			$sql = substr( $sql, 0, -3 );
		}

		return array(
			'sql_prepare' => $sql_prepare,
			'sql' => $sql,
			'params' => $filter
		);

	}



	/**
		вернет массив 
		$ret[ 'type' ] = 'section' or 'position';
		$ret[ 'params' ] = array route;
	*/
	private function _router_map_params( $params ) {
		if ( !$params[ 0 ] ) {
			throw new Exception( 'invalid request' );
		}

		$ret = array( );

		if ( count( $this -> _route ) ) {
			$params = array_diff( $params, array( '' ) );
			$real_params = '/' . implode( '/', $params ) . '/';
			foreach( $this -> _route as $k_r => $r ) {
				if ( !$r ) continue;
				$real_params = preg_replace( $k_r, $r, $real_params, 1 );
			}
			$params = explode( '/', $real_params );
			array_shift( $params );
			array_pop( $params );
		}

		$ret[ 'type' ] = 'section';
		foreach ( $params as $k => $param ) {
			if ( !$param ) continue;
			if ( $param == '_items' ) {
				$ret[ 'type' ] = 'position';
				break;
			}
			if ( $param == '_extended' ) {
				if ( $this -> _method == 'GET' ) $ret[ 'type' ] = 'section_extended';
				break;
			}
			$ret[ 'params' ][ ] = $param;
		}

		/////////////////////////////////////////
		// РАБОТА С ТАБЛИЦЕЙ ПОЗИЦИЙ
		if ( $ret[ 'params' ][ 0 ] == 'position_table' || $ret[ 'params' ][ 0 ] == 'pt' ) {
			$ret[ 'type' ] = 'position_table';
			$ret[ 'params' ] = $params;
			return $ret;
		}

		/////////////////////////////////////////
		// РАБОТА С ТАБЛИЦЕЙ СЕКЦИЙ
		if ( $ret[ 'params' ][ 0 ] == 'section_table' || $ret[ 'params' ][ 0 ] == 'st' ) {
			$ret[ 'type' ] = 'section_table';
			$ret[ 'params' ] = $params;
			return $ret;
		}

		return $ret;
	}



	/**
		recursive function
		parent_id = id родительского элемента catalog_section
	*/
	private function _router_check_params( $parent_id = null, $r = 0 ) {

		////////////////////////////////////////////
		// РАБОТА С ТАБЛИЦАМИ ПОЗИЦИЙ
		if (  $this -> _type == 'position_table' ) {
			if ( !isset( $this -> _params[ 1 ] ) ) throw new Exception( 'NF _params[ 1 ] invalid request ' . __LINE__ );
			$this -> _section[ 'position_table' ] = $this -> _params[ 1 ];
			return $this -> _section;
		}
		////////////////////////

		$sql = '';
		$pdo_params = array( );
		$pdo_params[ 'alias' ] = $this -> _params[ $r ];

		if ( !$r ) {
			$sql .= ' && ( isNull( `parent_id` ) || `parent_id` = 0 )';
		}
		else {
			$sql .= ' && `parent_id`=:parent_id';
			$pdo_params[ 'parent_id' ] = $parent_id;
		}

		$section = $this -> _db -> select( 'SELECT * FROM `' . $this -> _table . '` WHERE `alias`=:alias' . $sql . ' LIMIT 1', $pdo_params );

		/*
		echo "<pre>";
		var_dump( 'SELECT * FROM `' . $this -> _table . '` WHERE `alias`=:alias' . $sql . ' LIMIT 1' );
		var_dump( $pdo_params);
		var_dump( $section );
		echo "</pre>";
		*/

		if ( !count( $section ) ) {
			throw new Exception( 'invalid request: ' . $pdo_params[ 'alias' ] . ' : ' . $r . ' : ' . 'SELECT * FROM `' . $this -> _table . '` WHERE `alias`=:alias' . $sql . ' LIMIT 1, ' . implode( ',', $pdo_params ) );
		}
		$section = end( $section );
		if ( isset( $this -> _params[ $r + 1 ] ) ) {
			$section = $this -> _router_check_params( $section[ 'id' ], $r + 1 );
		}
		return $section;
	}
	





	/*
		$users = array( 'login1' => 'password1', 'login2' => 'password2' ... );
	*/
	public function auth_basic( $users = array( ), $realm="Base API Realm" ) {
		//var_dump( $_SERVER );
		//var_dump( getallheaders( ) );
		//exit;
		$validated = false;
		$valid_passwords = $users;
		$valid_users = array_keys( $valid_passwords );
		$redirect_auth = ( isset( $_SERVER[ 'REDIRECT_HTTP_AUTHORIZATION' ] ) ) ? $_SERVER[ 'REDIRECT_HTTP_AUTHORIZATION' ] : '';
		$auth = ( isset( $_SERVER[ 'HTTP_AUTHORIZATION' ] ) ) ? $_SERVER[ 'HTTP_AUTHORIZATION' ] : $redirect_auth;
		$user = ( isset( $_SERVER[ 'PHP_AUTH_USER' ] ) ) ? $_SERVER[ 'PHP_AUTH_USER' ] : '';
		$pass = ( isset( $_SERVER[ 'PHP_AUTH_PW' ] ) ) ? $_SERVER[ 'PHP_AUTH_PW' ] : '';
		if ( $auth ) {
			list( $user, $pass ) = explode( ':', base64_decode( substr( $auth, 6 ) ) );
			$validated = ( in_array( $user, $valid_users ) ) && ( $pass == $valid_passwords[ $user ] );
		}
		if ( !$validated ) {
			header( 'WWW-Authenticate: Basic realm="' . $realm . '"' );
			header( 'HTTP/1.1 401 Unauthorized' );
			$r[ 's' ] = false;
			$r[ 'code' ] = 401;
			$r[ 'errors' ][ 0 ] = 'Not authorized';
			$r[ 'message' ] = 'Not authorized';
			$r[ 'location' ] = __LINE__;
			$this -> strout( $r );
		}
		return true;	
	}


	public function auth_digest( ) {
		
		
		
	}




	public function ssl_encrypt( $source, $type, $key ) {
		$private_key_details = openssl_pkey_get_details( $key );
		$decrypt_chunk_size = ceil( $private_key_details[ 'bits' ] / 8 ) - 11;
		$maxlength = $decrypt_chunk_size;

		$output = '';
		while( $source ) {
		  $input = substr( $source, 0, $maxlength );
		  $source = substr( $source, $maxlength );
		  if( $type == 'private' ) {
			$ok = openssl_private_encrypt( $input, $encrypted, $key );
		  } else {
			$ok = openssl_public_encrypt( $input, $encrypted, $key );
		  }
		  $output .= $encrypted;
		}
		return $output;
	}


	public function ssl_decrypt( $source, $type, $key ) {
		$private_key_details = openssl_pkey_get_details( $key );
		$decrypt_chunk_size = ceil( $private_key_details[ 'bits' ] / 8 );
		$maxlength = $decrypt_chunk_size;
		$output = '';
		while( $source ) {
		  $input = substr( $source, 0, $maxlength );
		  $source = substr( $source, $maxlength );
		  if( $type == 'private' ) {
			$ok = openssl_private_decrypt( $input, $out, $key );
		  } else {
			$ok = openssl_public_decrypt( $input, $out, $key );
		  }
		  $output.=$out;
		}
		return $output;
	}
	
	
	
}

