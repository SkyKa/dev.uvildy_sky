Библиотека для определения типа и модели устройства на PHP

Mobile Detect это легковесный PHP класс для определения типа мобильного устройства, (включая и планшеты). Для определения устройства происходит анализ опции User-Agent и HTTP заголовков.
Общие возможности

    isMobile() - определяет является ли устройство мобильным

    isTablet() - определяет является ли устройство планшетом

Возможности библиотеки

Для проверки мы можем заюзать следующие методы:

Определение телефона

    isiPhone()
    isBlackBerry()
    isHTC()
    isNexus()
    isDellStreak()
    isMotorola()
    isSamsung()
    isSony()
    isAsus()
    isPalm()
    isGenericPhone()

Определение планшета

    isBlackBerryTablet()
    isiPad()
    isKindle()
    isSamsungTablet()
    isHTCtablet()
    isMotorolaTablet()
    isAsusTablet()
    isNookTablet()
    isAcerTablet()
    isYarvikTablet()
    isGenericTablet()

Определение операционной системы

    isAndroidOS()
    isBlackBerryOS()
    isPalmOS()
    isSymbianOS()
    isWindowsMobileOS()
    isiOS()
    isFlashLiteOS()
    isJavaOS()
    isNokiaOS()
    iswebOS()
    isbadaOS()
    isBREWOS()

Определение мобильного браузера

    isChrome()
    isDolfin()
    isOpera()
    isSkyfire()
    isIE()
    isFirefox()
    isBolt()
    isTeaShark()
    isBlazer()
    isSafari()
    isMidori()
    isGenericBrowser()

Примеры