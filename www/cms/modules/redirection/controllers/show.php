<?php
/*
 * Контроллер show баннера
 */

class RedirectionController_Show extends Controller_Base
{

    private function show($alias)
    {      
        $table = new Table('redirection');
        $redirection = $table->getEntityAlias($alias);
        if($redirection) $this->setContent("/go/".$redirection->id.".html");
    }

    public function __call($name, $args)
    {
       $this->show($name);
    }

    public function index()
    {

    }


}

?>
