<?php
/**
 *
 */
class mysiteController_Login Extends Controller_Base
{
    public function index()
    {
//        $_SESSION['user'] = true;
        $res = array();
        
        $name = Utils::getPost('name');
        $password = Utils::getPost('password');

        //$res[ 'n' ] = $name;
        //$res[ 'p' ] = $password;

        $login = val("security.login.login", array("name"=>$name,"password"=>$password)) && (val("security.inrole.admin") || val("security.inrole.user") || val("security.inrole.manager"));

        //$res[ 'login' ] = $login;

        if($login)
        {
            $res['success'] = true;
            $res['msg'] = '/mysite/';
        }
        else
        {
            $res['success'] = false;
            $res['msg'] = 'Неверное имя или пароль';
        }

        $this->setContent(json_encode($res));
    }

    public function logout()
    {
        val("security.login.logout");

        $res = array();
        $res['success'] = true;
        $res['msg'] = '/mysite/';

        $this->setContent(json_encode($res));
    }

    public function form()
    {
        $template = $this->createTemplate();

        $template->render();
    }

    public function form_login()
    {
        $template = $this->createTemplate();

        $template->render();
    }

    public function form_logout()
    {
        $template = $this->createTemplate();

        $template->render();
    }
}
