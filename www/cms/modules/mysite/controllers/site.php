<?php
/*
 *
 */

class mysiteController_Site Extends Controller_Base
{
    public function index()
    {
    }

    public function template()
    {
        $is_login = val("security.login") && (val("security.inrole.admin") || val("security.inrole.user") || val("security.inrole.manager") );
        
        if($is_login)
        {
            $this->setContent($this->config->template);
        }
        else
        {
            $this->setContent($this->config->template_login);
        }
    }
}
?>