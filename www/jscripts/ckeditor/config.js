/**
 * @license Copyright (c) 2003-2014, CKSource - Frederico Knabben. All rights reserved.
 * For licensing, see LICENSE.md or http://ckeditor.com/license
 */

CKEDITOR.editorConfig = function( config ) {
	// Define changes to default configuration here. For example:
	// config.language = 'fr';
	// config.uiColor = '#AADC6E';
	 config.filebrowserUploadUrl = ''+ CKEDITOR.basePath+'filemanager/connectors/php/upload.php';
	// config.filebrowserBrowseUrl =''+ CKEDITOR.basePath+'filemanager/connectors/php/connector.php';
 //    config.filebrowserImageBrowseUrl = ''+ CKEDITOR.basePath+'filemanager/browser/default/browser.html?Type=Image&amp;Command=GetFoldersAndFiles&amp;Connector='+ CKEDITOR.basePath+'filemanager/connectors/php/connector.php';
 //    config.filebrowserFlashBrowseUrl =''+ CKEDITOR.basePath+'filemanager/browser/default/browser.html?Type=Flash&amp;Command=GetFoldersAndFiles&amp;Connector='+ CKEDITOR.basePath+'filemanager/connectors/php/connector.php&amp;Command=GetFoldersAndFiles';

	config.allowedContent = true;
	config.fillEmptyBlocks = false;
	
    var roxyFileman = 'jscripts/ckeditor/plugins/fileman/index.html';

    config.filebrowserBrowseUrl = 'files/pages/files';
    config.filebrowserImageBrowseUrl=roxyFileman;
    config.removeDialogTabs= 'link:upload;image:upload';




};

CKEDITOR.config.allowedContent = true;
CKEDITOR.dtd.$removeEmpty['i'] = false
