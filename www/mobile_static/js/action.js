'use strict';

;(function($, undefined) {

	$(document).on('ready', function() {

		$(document).imagesLoaded( function() {
			$('.et-container').css('margin-top', $('header').outerHeight() - 1);

			if($('.map-page').length) {
				setMapSize('.leaflet-map'); 
				initLeafletMap();
			}

			if($('#map').length) {
				setMapSize('#map');
				var routeBuilder = new createRouteBuilder();
			}

			function setMapSize(selector) {
				var height = $(window).outerHeight() - ($('header').outerHeight() + $('footer').outerHeight());

				$(selector).height(height);
			}


		  	$('.et-preloader').fadeOut(500);

			setTimeout( function() {
				$("#open-cancel-booking-modal").click();
			}, 1000);

		});

		$('.et-menu-collapse-btn').on('click', function() {
			if($('body').hasClass('et-menu-open-left')) closeMenu();
			else openMenu();
		});

		$('.site-overlay').on('click', function() {
			closeMenu();
		});

    	function openMenu() {
    		$('body').addClass('et-menu-open-left');
    		$('html, body').css('height', '100%');
			$('.et-menu-collapse-btn').removeClass('et-collapsed');
    	}
    	function closeMenu() {
    		$('body').removeClass('et-menu-open-left all-infra-menu-open-left');
    		$('html, body').css('height', '');
			$('.et-menu-collapse-btn').addClass('et-collapsed');
    	}

    	$('.all-infra-menu ul li a').on('click', function(e) {
    		e.preventDefault();
    		closeMenu();
    	});

		Waves.init({
			duration: 500,
			delay: 100
		});
		
    	Waves.attach('.menu-item', ['waves-block', 'waves-light']);
    	Waves.attach('.procedures .item', ['waves-block', 'waves-light']);
    	Waves.attach('.et-menu .et-link', ['waves-block']);
    	Waves.attach('.basic-button button', ['waves-block', 'waves-light']);
    	Waves.attach('.basic-button a', ['waves-block', 'waves-light']);
    	Waves.attach('.all-infra-menu > li', ['waves-block']);

    // Animated thumbnails
	    var $animThumb = $('#lightgallery-in-content');
	    if ($animThumb.length) {
	        $animThumb.justifiedGallery({
	            border: 6
	        }).on('jg.complete', function() {
	        	$('#lightgallery-in-content > a .gallery-poster').show();
	            $animThumb.lightGallery({
	                thumbnail: true,
	                loop: false,
	            });
	        });
	    }


    // Mobiscroll
    	if($('#start-date').length) {
	    	$("#start-date").mobiscroll().date({
	    		display: 'inline',
	    		theme: 'mobiscroll',
	    		showLabel: false,
	    		height: 24,
	    		dateOrder: 'ddmmyy',
	        	dateFormat: 'dd mm yy',
	        	minDate: new Date(),
	        	onChange: function (valueText, inst) {
	        		var date = valueText.split(' ');
	        		$('#end-date').mobiscroll('option', 'minDate', new Date(date[2], date[1] - 1, date[0]));
    			}
	    	});
	    }

	    if($('#end-date').length) {
	    	$("#end-date").mobiscroll().date({
	    		display: 'inline',
	    		theme: 'mobiscroll',
	    		showLabel: false,
	    		height: 24,
	    		dateOrder: 'ddmmyy',
	        	dateFormat: 'dd mm yy',
	        	minDate: new Date()
	    	});
	    }

	    if($('#select-age').length) {
		    $('#select-age').mobiscroll().select({
		        theme: 'mobiscroll',
		        display: 'inline',
		        showLabel: false,
		        height: 24,
		    });
		}

    // Custom select fields (jQuery UI)
		if($('.custom-select-room').length) {

			$('.custom-select-room').selectmenu({ 
				width: '100%',
				icons: { button: "select-dropdown-btn" }
			}).data("ui-selectmenu")._renderItem = function(ul, item) {
			    var li = $( "<li>" ).css( "background-color", item.value );
				 
			 	if ( item.disabled ) {
			    	li.addClass( "ui-state-disabled" );
			  	}
			 
			  	this._setText( li, item.label );
			  	li.append('<div class="stars"></div>');

			  	switch( item.value ) {

			  		case 'president':
			  			setStars( 5, false );
			  			break;

			  		case 'luxe':
			  			setStars( 4, true );
			  			break;

			  		case 'self-luxe':
			  			setStars( 4, false );
			  			break;

			  		case 'cott-luxe':
			  			setStars( 3, true );
			  			break;

			  		case 'comfort':
			  			setStars( 3, false );
			  			break;

			  		case 'cott-base':
			  			setStars( 2, true );
			  			break;

			  		case 'base':
			  			setStars( 2, false );
			  			break;

			  	}

			  	function setStars( numberFull, isSelf ) {

			  		for(var i = 0; i < numberFull; i++) {
		  				li.find('.stars').append('<img src="/mobile_static/img/room_star.png" alt="">');
		  			}

		  			if( isSelf ) {

		  				li.find('.stars').append('<img src="/mobile_static/img/room_self_star.png" alt="">');
		  				numberFull++;
		  			} 

	  				for(var i = 0; i < (5 - numberFull); i++) {
		  				li.find('.stars').append('<img src="/mobile_static/img/room_empty_star.png" alt="">');
		  			}	  			

			  	}
			 
			  	return li.appendTo( ul );

			}

		}


	// Custom select fields (jQuery UI)
		if($('.custom-select').length) {

			$('.custom-select').selectmenu({ 

				width: '100%',
				icons: { button: "select-dropdown-btn" }		
			});

		}


	// Custom radio
		if( $("#select-number-days").length ) {
			$("#select-number-days").buttonset();
		}

		if( $(".custom-radio").length ) {
			$(".custom-radio").buttonset();
		}


    // Swipe menu
    	if( !$('#leaflet-map').length ) {

	    	;(function() {

	    		var touchsurface = document.getElementsByTagName('body')[0];
	    		var started = false;
	    		var touch, x, y;

	    		touchsurface.addEventListener('touchstart', function(e) {

	    			if (e.touches.length != 1 || started){
						return;
					}

					// Запоминаем текущее касание и его координаты
					touch = e.changedTouches[0];
					x = touch.pageX;
					y = touch.pageY;

					if( (x > 40) && !$('body').hasClass('et-menu-open-left') ) return;

					started = true;

	    		});

	    		touchsurface.addEventListener('touchend', function(e) {

	    			if( !started ) return;

	    			started = false;
	    			if((e.changedTouches.length > 1) || (touch.identifier != e.changedTouches[0].identifier)) return;

					var newX = e.changedTouches[0].pageX;
					var newY = e.changedTouches[0].pageY;

					if (Math.abs(x - newX) < Math.abs(y - newY)) return false;

					var delta = x - newX; 

					var swipeTo = delta < 0 ? 'left' : 'right';

					delta = Math.abs(delta);

					if( delta > 40 ) {
						if(swipeTo == 'right') {
							closeMenu();
						} else {
							openMenu();
						}
					}

	    		});

	    	})()
	    }


    // Phone mask for inputs
    	if($('.phone input').length) {
			$(".phone input").mask("?+7 (999) 999-99-99");
		}
		if($('form input[name="phone"]').length) {
			$('form input[name="phone"]').mask("?+7 (999) 999-99-99");
		}

	// Do mark (list-page)
		$('.et-container .menu-item .title-wrap .put-an-assessment').click( function(e) {
			e.preventDefault();

			$(this).parent().animate({
				right: '-100%'
			}, 400);

			$(this).closest('.menu-item').find('.mark-overlay-wrap').animate({
				bottom: '0%'
			}, 400);

		});

		$('.et-container .menu-item .mark-overlay .close-btn').click( function(e) {

			$(this).closest('.menu-item').find('.title-wrap').animate({
				right: '0'
			}, 400);

			$(this).closest('.menu-item').find('.mark-overlay-wrap').animate({
				bottom: '100%'
			}, 400);

		});

		$('.et-container .menu-item .mark-overlay').click( function(e) {
			e.preventDefault();
		});

		$('.mark-overlay .marks').on('click', 'img', function() {

			var elem = $(this).parent();

			var mark = $(this).index() + 1;

			elem.find('img').remove();

			for(var i = 0; i < 5; i++) {
				if(i < mark) {
					elem.append('<img src="/mobile_static/img/full_star.png" alt="">');
				}
				else {
					elem.append('<img src="/mobile_static/img/star.png" alt="">');
				}
			}

			elem.closest('.mark-overlay-wrap').attr('data-mark', mark);
		});

		$('.et-container .menu-item .mark-overlay .done-btn').click( function(e) {

			$(this).closest('.menu-item').find('.title-wrap').animate({
				right: 0
			}, 400);			

			$(this).closest('.menu-item').find('.mark-overlay-wrap').animate({
				bottom: '100%'
			}, 400);

			var mark = $(this).closest('.mark-overlay-wrap').attr('data-mark');

			if( mark && (mark >= 1) && (mark <= 5)) {
				// Здесь ajax запрос


			}

		});

	// Do mark (item-page)
		$('.infra-content-page .put-an-assessment').click( function(e) {
			e.preventDefault();

			$(this).animate({
				left: '-100%'
			}, 400);

			$(this).closest('.infra-content-page').find('.mark-overlay-wrap').animate({
				right: 0
			}, 400);

		});

		$('.infra-content-page .mark-overlay .close-btn').click( function(e) {

			$(this).closest('.infra-content-page').find('.put-an-assessment').animate({
				left: 0
			}, 400);

			$(this).closest('.infra-content-page').find('.mark-overlay-wrap').animate({
				right: '-100%'
			}, 400);

		});

		$('.infra-content-page .mark-overlay .done-btn').click( function(e) {

			$(this).closest('.infra-content-page').find('.put-an-assessment').animate({
				left: 0
			}, 400);			

			$(this).closest('.infra-content-page').find('.mark-overlay-wrap').animate({
				right: '-100%'
			}, 400);

			var mark = $(this).closest('.mark-overlay-wrap').attr('data-mark');

			if( mark && (mark >= 1) && (mark <= 5)) {
				// Здесь ajax запрос


			}

		});


	// Owl-carousel on infra-page
		if($('.infra-content-page .owl-carousel').length) {
			$('.infra-content-page .owl-carousel').owlCarousel({
				items: 1,
	    		autoplay: true,
				autoplayTimeout: 5000,
				nav: true,
				navText: ['<img src="/mobile_static/img/arrow_left.png"/>  <img src="/mobile_static/img/arrow_left_active.png"/>', 
						  '<img style="transform: rotate(180deg);" src="/mobile_static/img/arrow_left.png"/> <img style="transform: rotate(180deg);" src="/mobile_static/img/arrow_left_active.png"/>']
			});
		}


	// All infrastructure menu
    	$('.map-page .all-infra').click( function(e) {
    		e.preventDefault();

    		$('body').addClass('all-infra-menu-open-left');
    	});

		// $('.infra-map-wrapper .zoom .down').click( function(e) {
		// 	e.preventDefault();

		// 	$('.infra-map-wrapper').children('.add-info').remove();

		// 	var centerX = ($('.infra-map').scrollLeft() + $('.infra-map').width()/2)/$('.infra-map .map-img').width();
		// 	var centerY = ($('.infra-map').scrollTop() + $('.infra-map').height()/2)/$('.infra-map .map-img').height();

		// 	if( $('.infra-map .map-img').height()*0.7 > $('.infra-map').height()) {
		// 		$('.infra-map .map-img').css('height', $('.infra-map .map-img').height()*0.7);
		// 	} else {
		// 		$('.infra-map .map-img').css('height', $('.infra-map').height());
		// 	}

		// 	$('.infra-map').scrollLeft(centerX*$('.infra-map .map-img').width()-$('.infra-map').width()/2);
		// 	$('.infra-map').scrollTop(centerY*$('.infra-map .map-img').height()-$('.infra-map').height()/2);

		// 	$('.infra-map-wrapper .img-icons-wrap').css('height', 'auto');	

		// });

		// $('.infra-map-wrapper .zoom .up').click( function(e) {

		// 	e.preventDefault();

		// 	$('.infra-map-wrapper').children('.add-info').remove();

		// 	var centerX = ($('.infra-map').scrollLeft() + $('.infra-map').width()/2)/$('.infra-map .map-img').width();
		// 	var centerY = ($('.infra-map').scrollTop() + $('.infra-map').height()/2)/$('.infra-map .map-img').height();

		// 	if( $('.infra-map .map-img').height()*1.3 < $('.infra-map .map-img').prop('naturalHeight')) {
		// 		$('.infra-map .map-img').css('height', $('.infra-map .map-img').height()*1.3);
		// 	} else {
		// 		$('.infra-map .map-img').css('height',$('.infra-map .map-img').prop('naturalHeight'));
		// 	}

		// 	$('.infra-map').scrollLeft(centerX*$('.infra-map .map-img').width()-$('.infra-map').width()/2);
		// 	$('.infra-map').scrollTop(centerY*$('.infra-map .map-img').height()-$('.infra-map').height()/2);

		// 	$('.infra-map-wrapper .img-icons-wrap').css('height', 'auto');

		// }); 
		

	// Move to selected object
		// $('.infra-map .objects .item').click( function(e) {
		// 	moveToObject(this);
		// });


		// $('.all-infra-menu ul li a').on('click', function(e) {

		// 	e.preventDefault();
		// 	var item = $(this).attr('class');

		// 	moveToObject($('.infra-map .objects').find('.' + item));

		// 	closeMenu();

		// });

		// function moveToObject(item) {

		// 	$('.infra-map-wrapper .img-icons-wrap').css('height', 'auto');

		// 	$('.small-icon').fadeIn(0);
		// 	$(item).find('.small-icon').add($('.big-icon, .name')).fadeOut(0);
		// 	$('.infra-map-wrapper').children('.add-info').remove();

		// 	$('.infra-map .map-img').css('height', $('.infra-map .map-img').prop('naturalHeight'));

		// 	$(item).find('.big-icon, .name').css('display', 'block');

		// 	var posX = $(item).position().left - 20;
		// 	$('.infra-map').scrollLeft(posX);

		// 	var posY = $(item).position().top - 120;
		// 	$('.infra-map').scrollTop(posY);

		// 	$(item).find('.add-info').each( function( i, elem ) {
		// 		$(this).clone().appendTo('.infra-map-wrapper');
		// 	});

		// 	$('.infra-map').off('scroll');

		// 	setTimeout( function() {
		// 		$('.infra-map').on('scroll', function(e) {
		// 			$('.infra-map-wrapper').children('.add-info').remove();
		// 		});
		// 	}, 50);	

		// }

		$("#open-cancel-booking-modal").animatedModal({
			modalTarget: 'cancel-booking-modal',
			color: '#fff',
		});

	// Inteview custom option
		$(".cancel-booking-modal form input").change( function() {
			if($('.cancel-booking-modal .custom-radio #radiobox-42').is(':checked'))  {
				$('.cancel-booking-modal .custom-option-textarea').show(0);
				$('.cancel-booking-modal .custom-option-textarea textarea').focus();
			}
			else {
				$('.cancel-booking-modal .custom-option-textarea').hide(0);
			}
		});

		$('.back-button').click( function(e) {
			e.preventDefault();

			window.history.back();
		});


	//Карта
		function createRouteBuilder() {
			var self = this;

			var directionsService = new google.maps.DirectionsService();
			var directionsDisplay = new google.maps.DirectionsRenderer({suppressMarkers: true});
			var destination = new google.maps.LatLng(55.533046, 60.571168);

			var mapOptions = {
				zoom: 10,
				center: destination
			}

			var map = new google.maps.Map(document.getElementById("map"), mapOptions);

			directionsDisplay.setMap(map);

			var end_icon = new google.maps.MarkerImage(
					// URL
					'/static/img/mapicon.png',
					new google.maps.Size( 113, 73 ),
					// The origin point (x,y)
					new google.maps.Point( 0, 0 ),
					// The anchor point (x,y)
					new google.maps.Point( 66, 73 )
				);

			var start_icon = new google.maps.MarkerImage(
					'/static/img/start_location_icon.png',
					new google.maps.Size( 50, 73 ),
					new google.maps.Point( 0, 0 ),
					new google.maps.Point( 25, 73 )
				);

			function makeMarker( position, icon, title ) {
				var infowindow = new google.maps.InfoWindow({
			      content: title
			    });
				var marker = new google.maps.Marker({
					position: position,
					map: map,
					icon: icon,
					title: title
				});
				google.maps.event.addListener(marker, 'click', function() {
			      infowindow.open(map,marker);
			    });
			}

			var startPosition;

			if ("geolocation" in navigator) {

				navigator.geolocation.getCurrentPosition( function(position) {
					self.startPosition = new google.maps.LatLng(position.coords.latitude,  position.coords.longitude);
					self.calcRoute('car');
				});

			} else {
				alert('Не удалось определить Ваше местоположение, геолокация недоступна для этого браузера.');
			}

			this.directionsService = directionsService;
			this.directionsDisplay = directionsDisplay;
			this.destination = destination;
			this.map = map;

			this.calcRoute = function(mode) {

				var travelMode;
				var transitOptions;

				switch(mode) {
					case 'car':
						travelMode = google.maps.TravelMode.DRIVING;
						break;

					// case 'bus':
					// 	travelMode = google.maps.TravelMode.TRANSIT;
					// 	transitOptions = {
					// 		modes: [google.maps.TransitMode.BUS],
					// 	}

					// 	break;
				}

				var request = {
					origin: this.startPosition,
					destination: this.destination,
					travelMode: travelMode,
					transitOptions: transitOptions,
					provideRouteAlternatives: true,
				};

				directionsService.route(request, function(result, status) {
					if (status == google.maps.DirectionsStatus.OK) {
						directionsDisplay.setDirections(result);
						var leg = result.routes[ 0 ].legs[ 0 ];
						makeMarker( leg.end_location, end_icon, 'Увильды' );
						makeMarker( leg.start_location, start_icon, 'Ваше текущее расположение' );
					}
					else if( status == google.maps.DirectionsStatus.ZERO_RESULTS ) {
						alert('Этот вид проезда не поддерживается');
					}
				});

			};
		}

		$('.content table').wrap('<div class="table-responsive"></div>');


	});

}(jQuery))