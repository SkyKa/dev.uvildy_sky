'use strict';

;(function($, undefined) {

	$(document).on('ready', function() {

		var booking = {

			building: '',
			number: '',
			humans: [],

			setBuildingHandler: function() {

				$('.select-building .select-building-inner .mark').on('click', function(e) {

					var link = $(this).attr('href');

					e.preventDefault();

					$.ajax({

						url: '/ajax/?mod=catalog.action.booking_ajax',
						dataType: 'json',
						method: 'POST',
						data: {
							method: 'setBuilding',
							building: $(this).attr('data-alias'),
						}

					}).done( function(data) {
							
						if(!data.s) {
							console.log('error');
							return false;
						}

						window.location.href = link;

					});

				});

			},

			addHumanHandler: function() {
				
			},

			getBookingObj: function() {

				$.ajax({

					url: '/ajax/?mod=catalog.action.booking_get_ajax',
					dataType: 'json'

				}).done( function(data) {
						
					if(!data.s) {
						console.log('error');
						return false;
					}

					var booking = data.booking;

					this.building = booking.building;
					

				});

			},


		}

		booking.setBuildingHandler();
		booking.getBookingObj();


	var addHumanForm = {

		init: function() {
			this.openFormHandler();
			this.saveHumanHandler();
		},

		openFormHandler: function() {
			$('.basic-button a.add-human').on( 'click', function(e) {

				e.preventDefault();
				$('#add-human-form').show();

				$('html, body').animate({

					scrollTop: $('#add-human-form').offset().top - $('header').outerHeight() - 10,

				}, 300);

			});
		},

		saveHumanHandler: function() {

			$('.add-human-form .save button').on( 'click', function(e) {

				$('.add-human-form').hide();

			});

		},

	}
		

	// Booking step 1, building to session
	/*	$('.select-building .select-building-inner .mark').on('click', function(e) {

			// e.preventDefault();

			$.ajax({

				url: '/ajax/?mod=catalog.action.booking_ajax',
				dataType: 'json',
				method: 'POST',
				data: {
					building: $(this).find('.name').text(),
				}

			}).done( function(data) {
					
				if(!data.s) {
					console.log('error');
					return false;
				}

			});

		});


		$.ajax({

			url: '/ajax/?mod=catalog.action.booking_get_ajax',
			dataType: 'json',

		}).done( function(data) {
				
			if(!data.s) {
				console.log('error');
				return false;
			}

			console.log( data );

		});*/

	});

}(jQuery))