SELECT
    orders.`id` AS externalId,
    FROM_UNIXTIME( orders.`date_created` ) AS createdAt,
    orders.`user_id` AS customerId,
    orders.`user_full_name` AS firstName,
    orders.`user_email` AS email,
    orders.`user_phone` AS phone,
    orders.`user_comment` AS customerComment,
    orders.`user_deliver_to` AS deliveryAddress,
    (
        CASE orders.`delivery_method` 

            WHEN '5' 
            THEN 'delivery-russia'

            WHEN '63' 
            THEN 'delivery-moscow'

        END
    ) AS deliveryType,
    (
        CASE orders.`payment_method` 

            WHEN '0' 
            THEN 'courier-cash'

            WHEN '1' 
            THEN 'courier-cash'

            WHEN '11' 
            THEN 'cash-on-delivery'

            WHEN '12' 
            THEN 'yakassa'

            WHEN '13' 
            THEN 'paylate'

        END
    ) AS paymentType,
    (
        SELECT
            GROUP_CONCAT(
                CONCAT_WS(
                    ';',
                    shop_orders_products.`product_id`,
                    shop_orders_products.`prd_size`,
                    shop_orders_products.`quantity`,
                    shop_products.`category_id`
                )
                SEPARATOR '|'
            )
        FROM
            shop_orders_products
        LEFT JOIN 
            shop_products
        ON 
            shop_products.`id` = shop_orders_products.`product_id`
        WHERE
            shop_orders_products.`order_id` = orders.`id`
        GROUP BY
            shop_orders_products.`order_id`
    ) AS items,
    (
        SELECT 
            `field_data`
        FROM 
            custom_fields_data
        WHERE 
            custom_fields_data.`entity_id` = orders.`id` AND custom_fields_data.`field_id` = 97
    ) AS city,
    (
        SELECT 
            `field_data`
        FROM 
            custom_fields_data
        WHERE 
            custom_fields_data.`entity_id` = orders.`id` AND custom_fields_data.`field_id` = 100
    ) AS additionalPhone
FROM
    `shop_orders` AS orders
WHERE
    FROM_UNIXTIME(orders.`date_updated`) BETWEEN :lastSync AND NOW()
ORDER BY
    orders.`date_created` desc