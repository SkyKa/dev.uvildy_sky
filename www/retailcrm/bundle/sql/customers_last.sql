SELECT
    FROM_UNIXTIME( orders.`date_created` ) AS createdAt,
    orders.`user_id` AS customerId,
    orders.`user_full_name` AS firstName,
    orders.`user_email` AS email,
    orders.`user_phone` AS phone,
    orders.`user_deliver_to` AS deliveryAddress,
    (
        SELECT 
            `field_data`
        FROM 
            custom_fields_data
        WHERE 
            custom_fields_data.`entity_id` = orders.`id` AND custom_fields_data.`field_id` = 97
    ) AS city,
    (
        SELECT 
            `field_data`
        FROM 
            custom_fields_data
        WHERE 
            custom_fields_data.`entity_id` = orders.`id` AND custom_fields_data.`field_id` = 100
    ) AS additionalPhone
FROM
    `shop_orders` AS orders
WHERE
    FROM_UNIXTIME(orders.`date_created`) BETWEEN :lastSync AND NOW()
ORDER BY
    orders.`date_created` desc