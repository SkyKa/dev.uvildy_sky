SELECT
    orders.`date_create` AS createdAt,
    orders.`client_name` AS firstName,
    orders.`client_email` AS email,
    orders.`client_phone` AS phone
FROM
    `position_orders` AS orders
ORDER BY
    orders.`date_create` DESC
LIMIT 3