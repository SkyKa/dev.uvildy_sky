SELECT
    orders.`id` AS externalId,
    orders.`date_create` AS createdAt,
    orders.`client_name` AS firstName,
    orders.`client_email` AS email,
    orders.`client_phone` AS phone,
    orders.`client_comm` AS customerComment,
    orders.`num_1cid` AS num_1cid,
    orders.`duration` AS duration,
    orders.`clients` AS clients
FROM
    `position_orders` AS orders
ORDER BY
    orders.`date_create` desc
LIMIT 3