SELECT
    rooms.`id_1c` as id,
    rooms.`id_1c` as productId,
    rooms.`id_1c` as xmlId,
    rooms.`n_rooms` as num_rooms,
    rooms.`n_main_place` as num_place,
    rooms.`n_additional_place` as num_dopplace,
    'Y' as productActivity,
    products.`parent_id` as categoryId,
    products.`title` as name,
    (
        SELECT `catalog_section`.`title`
        FROM `catalog_section`
        WHERE `catalog_section`.`id`=categoryId
        LIMIT 1
    ) as category_title
FROM
    `catalog_section` as products
INNER JOIN
    `section_rooms` as rooms
ON 
    products.`id`=rooms.`id`
WHERE 
  products.`section_table`="section_rooms"  
ORDER BY
    products.`id`