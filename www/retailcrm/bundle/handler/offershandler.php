<?php

class OffersHandler implements HandlerInterface
{
    public function prepare($offers)
    {

        require '/../../../cms/classes/acquiring.php';
        require '/../../../cms/classes/acurl.php';
        require '/../../../cms/classes/auth.php';
        require '/../../../cms/classes/config.php';
        require '/../../../cms/classes/config_base.php';
        require '/../../../cms/classes/controller_base.php';
        require '/../../../cms/classes/controller_object.php';
        require '/../../../cms/classes/craftmobile.php';
        require '/../../../cms/classes/dumphper.php';
        require '/../../../cms/classes/form.php';
        require '/../../../cms/classes/import1c.php';
        require '/../../../cms/classes/install.php';
        require '/../../../cms/classes/installer.php';
        require '/../../../cms/classes/install_base.php';
        require '/../../../cms/classes/langvars.php';
        require '/../../../cms/classes/lesson_theme.php';
        require '/../../../cms/classes/alfabank_rest.php';
        require '/../../../cms/classes/mailer.php';
        require '/../../../cms/classes/min_js.php';
        require '/../../../cms/classes/mobile_detect.php';
        require '/../../../cms/classes/model_base.php';
        require '/../../../cms/classes/pager.php';
        require '/../../../cms/classes/pay_robokassa.php';
        require '/../../../cms/classes/registry.php';
        require '/../../../cms/classes/router.php';
        require '/../../../cms/classes/sendsms.php';
        require '/../../../cms/classes/smsru.php';
        require '/../../../cms/classes/table.php';
        require '/../../../cms/classes/template.php';
        require '/../../../cms/classes/utils.php';
        require '/../../../config.php';
        require '/../../../cms.php';

        $categories = $this->getCategories();
        $container = Container::getInstance();

        $products = array();

        $table = new Table('catalog_section');

        $programms = $table -> select('SELECT `catalog_section`.`id`, `catalog_section`.`title` 
            FROM `catalog_section`
            INNER JOIN `section_programms`
            ON  `section_programms`.`id`=`catalog_section`.`id`
            WHERE `section_programms`.`public` AND `section_programms`.`is_programm`');

        $ages = $table -> select('SELECT `id`, `title` FROM `section_ages`');

        if( empty( $programms ) || empty( $ages ) ) continue;

        foreach ($offers as $k => $v) {

            $photo = '';

        // Получаем секцию класса номера, в которой хранятся номера с фото
            $class_offer_section = $table -> select(
                'SELECT `catalog_section`.`id` FROM `catalog_section` 
                INNER JOIN `section_number_class` 
                ON (`catalog_section`.`id`=`section_number_class`.`id`) 
                WHERE `catalog_section`.`parent_id`=1147 AND `catalog_section`.`title`="'. $v['category_title'] .'" LIMIT 1');

            if( !empty($class_offer_section) ) {

                $photo_section = $table -> select(
                    'SELECT 
                        `section_appearance`.`id`
                    FROM 
                        `section_appearance`
                    INNER JOIN
                        `catalog_section`
                    ON
                        `section_appearance`.`id`=`catalog_section`.`id`
                    WHERE 
                        `catalog_section`.`parent_id`=:parent_id AND `section_appearance`.`n_rooms`=:n_rooms AND `section_appearance`.`n_main_place`=:n_main_place AND `section_appearance`.`n_additional_place`=:n_additional_place LIMIT 1',
                    array( 'parent_id' => $class_offer_section[0]['id'], 'n_rooms' => $v['num_rooms'], 'n_main_place' => $v['num_place'], 'n_additional_place' => $v['num_dopplace'] ) );

                if( !empty( $photo_section ) ) {

                    $images = $table -> select('SELECT * FROM `position_photo_gallery` WHERE `section_id`=:id ORDER BY `position` DESC', 
                        array( 'id' => $photo_section[0]['id'] ) );

                    if( !empty( $images ) ) {

                        foreach( $images as $image ) {

                            $photo = $container->shopUrl . '/' . $image['img'];

                        }

                    }
                }

            }


            foreach( $programms as $programm ) {

                foreach( $ages as $age ) {

                    $product = $offers[$k];

                    $categoryId = $v['categoryId'];
                    $product['categoryId'] = array($categoryId);

                    $product['xmlId'] = $offers[$k]['xmlId'] . '-' . $programm['id'] . '-' . $age['id'];
                    $product['id'] = $product['xmlId'];

                    $product['picture'] = $photo;

                    $product['params']['programm']['code'] = 'programm';
                    $product['params']['programm']['name'] = 'Программа';
                    $product['params']['programm']['value'] = $programm['title'];

                    $product['params']['age']['code'] = 'age';
                    $product['params']['age']['name'] = 'Возрастная категория';
                    $product['params']['age']['value'] = $age['title'];

                    $products[] = array_filter($product);

                }     
                           
            }

        }
        
        return $products;
    }

    private function getCategories()
    {
        $builder = new ExtendedOffersBuilder();
        $data = $builder->buildCategories();

        $categories = array();
        $process = true;

        foreach ($data as $category) {

            if( $category['parentId'] == 0 ) {
                $category['parentId'] = null;
            }

            $categories[$category['id']] = array(
                'id' => $category['id'],
                'parentId' => $category['parentId'],
                'name' => $category['name']
            );
        }

        while($process) {
            $count = 0;

            foreach($categories as $k => $v) {
                if ($v['parentId'] != 0) {
                    $categories[$k]['parentId'] = $categories[$v['parentId']]['parentId'];
                    $count++;
                }
            }
            if ($count <= 0) {
                $process = false;
            }
        }

        return $categories;
    }

}