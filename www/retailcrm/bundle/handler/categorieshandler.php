<?php

class CategoriesHandler implements HandlerInterface
{
    public function prepare($data)
    {
        $categories = array();

        foreach ($data as $category) {

            if( $category['parent_id'] == 0 ) {
                $category['parent_id'] = null;
            }

            $categories[] = array(
                'id' => $category['category_id'],
                'parentId' => $category['parent_id'],
                'name' => $category['category_name']
            );
        }

        return $categories;
    }
}