<?php

class OrdersHandler implements HandlerInterface
{
    public function prepare($data)
    {

        require '/../../../cms/classes/acquiring.php';
        require '/../../../cms/classes/acurl.php';
        require '/../../../cms/classes/auth.php';
        require '/../../../cms/classes/config.php';
        require '/../../../cms/classes/config_base.php';
        require '/../../../cms/classes/controller_base.php';
        require '/../../../cms/classes/controller_object.php';
        require '/../../../cms/classes/craftmobile.php';
        require '/../../../cms/classes/dumphper.php';
        require '/../../../cms/classes/form.php';
        require '/../../../cms/classes/import1c.php';
        require '/../../../cms/classes/install.php';
        require '/../../../cms/classes/installer.php';
        require '/../../../cms/classes/install_base.php';
        require '/../../../cms/classes/langvars.php';
        require '/../../../cms/classes/lesson_theme.php';
        require '/../../../cms/classes/alfabank_rest.php';
        require '/../../../cms/classes/mailer.php';
        require '/../../../cms/classes/min_js.php';
        require '/../../../cms/classes/mobile_detect.php';
        require '/../../../cms/classes/model_base.php';
        require '/../../../cms/classes/pager.php';
        require '/../../../cms/classes/pay_robokassa.php';
        require '/../../../cms/classes/registry.php';
        require '/../../../cms/classes/router.php';
        require '/../../../cms/classes/sendsms.php';
        require '/../../../cms/classes/smsru.php';
        require '/../../../cms/classes/table.php';
        require '/../../../cms/classes/template.php';
        require '/../../../cms/classes/utils.php';
        require '/../../../config.php';
        require '/../../../cms.php';

        $table = new Table('catalog_section');

        $orders = array();

        foreach ($data as $record) {
            $order = array();

            $order['externalId'] = $record['externalId'];
            $order['createdAt'] = $record['createdAt'];

            $order['customerId'] = $record['phone'] . '-f'; 

            $order['firstName'] = $record['firstName'];
            $order['email'] = $record['email'];
            $order['phone'] = $record['phone'];

            $order['customFields']['duration'] = $record['duration'];

            $order['customerComment'] = $record['customerComment'];

            $order['paymentType'] = 'bank-card';

            $order['items'] = array();

            $items = json_decode($record['clients'], true);

            foreach ($items as $item) {

                $good = array();

                $programm = $table -> select('SELECT `id` FROM `catalog_section` WHERE `section_table`="section_programms" AND `title`="'. $item['programm'] .'" LIMIT 1');

                if( empty($programm) ) continue;
                $programm = end($programm);

                $age = $table -> select('SELECT `id` FROM `section_ages` WHERE `title`="'. $item['age'] .'" LIMIT 1');

                if( empty($age) ) continue;
                $age = end($age);

                $good['productId'] = $record['num_1cid'] . '-' . $programm['id'] . '-' . $age['id'];
                $good['quantity'] = 1;
                $good['initialPrice'] = $item['price'];

                array_push($order['items'], $good);
            }

            $order = DataHelper::filterRecursive($order);
            array_push($orders, $order);
        }

        return $orders;
    }
}