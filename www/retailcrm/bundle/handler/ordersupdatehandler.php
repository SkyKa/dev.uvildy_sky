<?php

class OrdersUpdateHandler implements HandlerInterface
{
    public function prepare($data)
    {

        $orders = array();

        foreach ($data as $record) {
            $order = array();

            $order['externalId'] = $record['externalId'];
            $order['createdAt'] = $record['createdAt'];

            $order['customerId'] = (!empty($record['customerId'])) ? $record['customerId'] : $record['phone'] . '-f'; 

            $order['firstName'] = $record['firstName'];
            $order['email'] = $record['email'];
            $order['phone'] = $record['phone'];

            $order['additionalPhone'] = $record['additionalPhone'];

            $order['delivery']['address']['city'] = $record['city'];

            $order['customerComment'] = $record['customerComment'];
            
            $order['delivery']['address']['text'] = $record['deliveryAddress'];
            $order['delivery']['code'] = $record['deliveryType'];

            $order['paymentType'] = $record['paymentType'];

            $order['items'] = array();

            $items = explode('|', $record['items']);

            foreach ($items as $item) {
                $data = explode(';', $item);
                $item = array();

            // Если не одежда (без размеров)
                if( $data[3] == 3032 || $data[3] == 3033 ) {
                    $item['productId'] = $data[0];
                }
            // Если одежда 
                else {
            // Одежда больших размеров
                    if( $data[3] == 3055 ) {
                        if( empty( $data[1] ) ) {
                            $item['productId'] = $data[0] . '-' . '58';
                        }
                        else {
                            $item['productId'] = $data[0] . '-' . $data[1];
                        }
                    }
            // Остальная одежда
                    else {
                        if( empty( $data[1] ) ) {
                            $item['productId'] = $data[0] . '-' . '42';
                        }
                        else {
                            $item['productId'] = $data[0] . '-' . $data[1];
                        }
                    }

                }

                $item['quantity'] = (isset($data[2])) ? (int) $data[2] : 0;

                array_push($order['items'], $item);
            }

            $order = DataHelper::filterRecursive($order);
            array_push($orders, $order);
        }

        return $orders;
    }
}