<?php

class CustomersHandler implements HandlerInterface
{

    public function prepare($customers)
    {
        $container = Container::getInstance();

        foreach( $customers as $k => $customer ) {
        	$customers[$k]['externalId'] = $customer['phone'] . '-f';
            $customers[$k]['phones'][]['number'] = $customer['phone'];
        }

        return $customers;
    }

}